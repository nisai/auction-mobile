import 'package:get/get.dart';
import 'package:mobile/core/gql_client.dart';
import 'package:mobile/documents/province_document.dart';
import '../shared/models/province_model.dart';
class ProvinceRepository {

  final _api = Get.find<GraphQLClient>();

  Future<List<ProvinceModel>> getProvinces()  async {
    final result = await _api.call()
        .query(ProvinceDocument.getAllProvince
    );
    final data = result['data'];
    if(data != null)  {
      final data = result['data'];
      final list = (data['items']['select'] as List)
          .cast<Map<String, dynamic>>()
          .map(ProvinceModel.fromJson)
          .toList();
      return list;
    }
    return [];
  }

}
