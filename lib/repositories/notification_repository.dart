import 'package:mobile/core/gql_client.dart';
import 'package:mobile/documents/notification_document.dart';
import 'package:mobile/shared/models/notification_model.dart';

class NotificationRepository {

  final GraphQLClient graphQLClient;

  NotificationRepository({required this.graphQLClient});

  Future<List<NotificationModel>?> paginate({required final int page, final int? limit = 10})  async {
    final _result = await graphQLClient.call(authorization: true)
        .query(NotificationDocument.queryNotification,
        variables: <String, dynamic>{
          'page': page,
          'limit': limit,
        });
    if(_result['data'] != null) {
      final list =  (_result['data']["AppNotifications"]["select"] as List)
          .cast<Map<String, dynamic>>()
          .map(NotificationModel.fromJson)
          .toList();
      return list;
    }
    return null;
  }

  Future<bool> read(final int? id) async {
    final _result = await graphQLClient.call(authorization: true)
        .mutation(NotificationDocument.readNotification,
        variables: <String, dynamic>{
          'id': id,
        });
    return _result['data'] != null;
  }

}