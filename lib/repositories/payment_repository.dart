import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mobile/core/http_client.dart';

class PaymentRepository {

  final _api = Get.find<HttpClient>();

  Future<bool> createPaymentProduct({final int? productId}) async {
    try {
      final rest = await _api.getClient(authenticated: true);
      final response = await rest.post('/payment/product/$productId/all');
      final result = response.statusCode == 200;
      print("createPaymentProduct: result $result");
      return result;
    } catch(e) {
      Get.snackbar(
          'You deposit_wallet',
          'create deposit_wallet',
          colorText: Colors.red,
          snackPosition: SnackPosition.BOTTOM
      );
    }
    return false;
  }

  Future<double?> getProductTotalFee({final int? productId}) async {
    try {
      final rest = await _api.getClient(authenticated: true);
      final response = await rest.get<Map<String, dynamic>>('/product/price/$productId');
      final success = response.statusCode == 200;
      if(success) {
        final data = response.data;
        final totalFee = (data?['promotion_fee']?? 0) +  (data?['duration_fee']?? 0) + (data?['subtitle_fee']?? 0);
        print("getProductTotalFee: result $totalFee");
        return totalFee;
      }
    } catch(e) {
      print('error $e');
    }
    return null;
  }

}