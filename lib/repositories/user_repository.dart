import 'dart:convert';
import 'dart:developer';
import 'package:get/get.dart';
import 'package:mobile/core/gql_client.dart';
import 'package:mobile/core/http_client.dart';
import 'package:mobile/documents/user_document.dart';
import 'package:mobile/shared/logger/logger_utils.dart';
import 'package:mobile/shared/models/access_token_model.dart';
import 'package:mobile/shared/models/user_model.dart';

class UserRepository {

  final  _api = Get.find<HttpClient>();
  final  _gql = Get.find<GraphQLClient>();

  Future<AccessTokenModel?> login({ required final String username, required final String password}) async {

    final Map<String, dynamic> data = {
      "username": username,
      "password": password,
    };
    final rest = await _api.getClient();
    final result = await  rest.post<Map<String, dynamic>>('/auth/token', data: data);
    return AccessTokenModel.fromJson(result.data);

  }

  Future<AccessTokenModel?> generateTokenWithFirebase(String token) async {
    try {
      final Map<String, dynamic> data = {
        "token": token,
      };
      final rest = await _api.getClient();
      final result = await rest.post<Map<String, dynamic>>('/auth/token/firebase', data: data);
      return AccessTokenModel.fromJson(result.data);
    } catch(e) {
      print('auto login error $e');
    }
    return null;
  }

  Future<UserModel?> getAppUserWithFirebase() async {
    try {
      final rest = await _api.getClient(authenticated: true);
      final result = await rest.get<Map<String, dynamic>>('/user/current');
      return UserModel.fromJson(result.data);
    } catch(e) {
      print('auto login error $e');
    }
    return null;
  }

  Future<UserModel?> update({required final Map<String, dynamic>? data}) async {
    try {
      final input = jsonEncode(data);
      final rest = await _api.getClient(authenticated: true);
      final response = await rest.put<Map<String, dynamic>>('/user/current/update', data: input);
      final user =  UserModel.fromJson(response.data);
      print('updated user: ${user.toMap()}');
      return user;
    } catch(e) {
      log("Service update $e");
    }
    return null;
  }

  Future<Map<String, dynamic>?> register({required final Map<String, dynamic> data}) async {
    try {
      final input = jsonEncode(data);
      final rest = await _api.getClient();
      final response = await rest.post<Map<String, dynamic>>('/user', data: input);
      return response.data;
    } catch(e) {
      log("Service update $e");
    }
    return null;
  }

  Future<UserModel?> getCurrentUser() async {
    try {
      final rest = await _api.getClient(authenticated: true);
      final response = await rest.get('/user/current');
      if(response.data != null) {
        final user =  UserModel.fromJson(response.data as Map<String, dynamic>);
        return user;
      }
    } catch(e) {
      logger.e("Error is $e");
    }
    return null;
  }

  Future<UserModel?> getUserByEmail(final String? email) async {
    try {
      final rest = await _api.getClient();
      final result = await rest.get<Map<String, dynamic>>('/user/find-email?email=$email');
      return UserModel.fromJson(result.data);
    } catch(e) {
      print('auto login error $e');
    }
    return null;
  }

  Future<bool?> resetPassword({ required final String username, required final String password}) async {

    final Map<String, dynamic> data = {
      "username": username,
      "password": password,
    };
    final rest = await _api.getClient();
    final result = await rest.post('/auth/reset-password', data: data);
    return result.statusCode == 200;
  }

  Future<UserModel?> changePassword({required final String oldPassword, required final String newPassword}) async {

    final rest = await _api.getClient(authenticated: true);
    final response = await rest.put<Map<String, dynamic>>('/user/changePassword?old=$oldPassword&new=$newPassword');
    final user =  UserModel.fromJson(response.data);
    return user;
  }

  Future<bool?> createMessageToken({required final String? messageToken}) async {
    try {
      var result = await _gql.call(authorization: true).mutation(UserDocument.createOrUpdateFirebaseToken,  variables: {"token": messageToken});
      final success =  result['data']['CreateFireBaseToken']['token'] != null;
      print('createMessageToken  $success');
      return success;
    } catch(e) {
      print('createMessageToken error: $e');
    }
    return null;
  }

  Future<Map<String, dynamic>?> getBuyingItemsByUserId(final int userId) async {
    try {
      final _result = await _gql.call(authorization: true)
          .query(UserDocument.getBuyingItems,
          variables: <String, dynamic>{'userId': userId});
      return _result;
    } catch(e) {
      print('error $e');
    }
    return null;
  }


  Future<Map<String, dynamic>?> getSellingByUserId(final int? userId) async {
    try {
      var _result = await _gql.call(authorization: true)
          .query(UserDocument.getSellingItems,
          variables: <String, dynamic>{'userId': userId});
      return _result;

    } catch(e) {
      print('error get selling product');
    }
    return null;
  }

  Future<Map<String, dynamic>?> getSellerDetails(final int? userId) async {
    try {
      var _result = await _gql.call(authorization: true)
          .query(UserDocument.getSellerDetail,
          variables: <String, dynamic>{'userId': userId});
      return _result;
    } catch(e) {
      print('error get selling product');
    }
    return null;
  }

  Future<Map<String, dynamic>?> getSellerFeedback({final int? userId, final int? makerId}) async {
    try {
      var _result = await _gql.call(authorization: true)
          .query(UserDocument.getSellerFeedback,
          variables: <String, dynamic>{'userId': userId, 'makerId': makerId});
      return _result;
    } catch(e) {
      print('error get selling product');
    }
    return null;
  }

  Future<Map<String, dynamic>?> updateFeedBack({final Map<String, dynamic>? data}) async {
    try {
      var _result = await _gql.call(authorization: true)
          .mutation(UserDocument.updateFeedback,
          variables: data);
      return _result;
    } catch(e) {
      print('error updateFeedBack $e');
    }
    return null;
  }

  Future<Map<String, dynamic>?> createFeedBack({final Map<String, dynamic>? data}) async {
    try {
      var _result = await _gql.call(authorization: true)
          .mutation(UserDocument.createFeedback,
          variables: data);
      return _result;
    } catch(e) {
      print('error createFeedBack $e');
    }
    return null;
  }

}