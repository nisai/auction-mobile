import 'package:get/get.dart';
import 'package:mobile/core/gql_client.dart';
import 'package:mobile/documents/category_document.dart';
import 'package:mobile/shared/models/category_model.dart';
import 'package:collection/collection.dart';
import 'package:mobile/shared/models/product_model.dart';

class CategoryRepository {

  final _api = Get.find<GraphQLClient>();

  Future<Map<String, dynamic>?> getParents()  async {
    var result = await _api.call()
        .query(CategoryDocument.getParents);
    return result;
  }

  Future<CategoryModel?> getParentByChildId(final int? categoryId)  async {
    try {
      var result = await _api.call()
          .query(CategoryDocument.getParentByChildId, variables: <String, dynamic>{
        'categoryId': categoryId}
      );
      final list =  (result['data']['Categories']['select'] as List)
          .cast<Map<String, dynamic>>()
          .map(CategoryModel.fromJson)
          .toList();
      return list.firstOrNull;
    } catch(e) {
      print('getParentByChildId: eee $e');
    }
    return null;
  }

  Future<Map<String, dynamic>?> getAllChildCategory()  async {

    var result = await _api.call()
        .query(CategoryDocument.getAllChildCategory);
    return result;
  }

  Future<Map<String, dynamic>?> getLatestProduct()  async {

    var result = await _api.call()
        .query(CategoryDocument.getLatestProduct);
    return result;
  }

  Future<Map<String, dynamic>?> getChild({final int? parentId})  async {

    var result = await _api.call()
        .query(CategoryDocument.getChildrenWitProduct, variables: <String, dynamic>{'parentId': parentId});

    return result;
  }

  Future<List<ProductModel>?> getProductByCategoryId(final List<int?>? categories)  async {
    try {
      var result = await _api.call()
          .query(CategoryDocument.getProductByCategory, variables: <String, dynamic>{
        'categories': categories}
      );

      final _products = (result['data']['Products']['select'] as List).cast<Map<String, dynamic>>()
          .map(ProductModel.fromJson)
          .toList();

      return _products;
    } catch(e) {
      print('getProductByCategoryId: error $e');
    }
    return null;
  }

  Future<List<CategoryModel>?> getChildrenByParent(final int? parentId) async {
    try {
      var result = await _api.call()
          .query(CategoryDocument.getChildrenByParent,  variables: <String, dynamic>{
        'parentId': parentId}
      );
      if(result != null) {
        return (result['data']['Categories']['select'] as List)
            .cast<Map<String, dynamic>>()
            .map(CategoryModel.fromJson)
            .toList();
      }
    } catch(e) {

    }
    return null;
  }

}