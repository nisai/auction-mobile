import 'package:get/get.dart';
import 'package:mobile/core/gql_client.dart';
import 'package:mobile/documents/main_home_document.dart';
import 'package:mobile/documents/product_document.dart';
import 'package:mobile/shared/models/product_model.dart';
import 'package:mobile/shared/models/product_promotion_model.dart';
import 'package:mobile/shared/models/user_item_model.dart';
import 'package:mobile/utils/constant.dart';
import 'package:collection/collection.dart';
class ProductRepository {

  final _api = Get.find<GraphQLClient>();

  Future<Map<String, dynamic>?> getDashboard()  async {
    final result = await _api.call()
        .query(MainHomeDocument.getFirstPage,
        variables: <String, dynamic>{
          'page': 1,
          'limit': Constant.limitItems
        }
    );
    return result;
  }

  Future<Map<String, dynamic>?> getMoreLatest({required final int page})  async {
    final result = await _api.call()
        .query(MainHomeDocument.getMoreLatest,
        variables: <String, dynamic>{
          'page': page,
          'limit': Constant.limitItems
        }
    );
    return result;
  }


  Future<ProductModel?> getProductById({
    final int? productId}) async {
    try {
      final result = await _api.call()
          .query(ProductDocument.getProductById,
          variables: <String, dynamic>{
            'productId': productId,
          });
      final data = result['data'];
      if(data != null)  {
        return ProductModel.fromJson(data['Product'] as Map<String, dynamic>);
      }
    } catch(e) {
      print('error $e');
    }
    return null;
  }

  Future<ProductPromotionModel?> getProductPromotion({final int? productId}) async{
    try {
      final result = await _api.call()
          .query(ProductDocument.getProductPromotions,
          variables: <String, dynamic>{
            'productId': productId
          });

        if(result != null) {
          final json = (result['data']['promotions']['select'] as List).firstOrNull;
          print('getProductPromotions : $result');
           return ProductPromotionModel.fromJson(json);
        }
    } catch(e) {
      print('getProductPromotions error $e');
    }
    return null;
  }

  Future<Map<String ,dynamic>?> getProductComments({
    final int? productId}) async {
    try {
      final _result = await _api.call()
          .query(ProductDocument.getProductComments,
          variables: <String, dynamic>{
            'productId': productId
          });

      return _result;

    } catch(e) {
      print('error $e');
    }
    return null;
  }

  Future<Map<String ,dynamic>?> getProductDetails({
    final int? productId,
    final int? categoryId,
    final int? userId}) async {
    try {
      final _result = await _api.call()
          .query(ProductDocument.getProductDetails,
          variables: <String, dynamic>{
            'productId': productId,
            'categoryId': categoryId,
            'userId': userId,
          });

      return _result;

    } catch(e) {
      print('error $e');
    }
    return null;
  }

  Future<bool> buyProduct(final int? productId) async {
    try {
      final _result = await _api.call(authorization: true)
          .mutation(ProductDocument.buyNowProduct,
          variables: <String, dynamic>{
            'id': productId,
          });
      print('result data ${_result['data']}');
      return _result['data'] != null;
    } catch (e) {
      final message = e.toString().split(":");
      Get.snackbar(
          'Buy product',
          message.last,
          snackPosition: SnackPosition.TOP,
          duration: Duration(seconds: 5)
      );
    }
    return false;
  }

  Future<bool> bidProduct(final Map<String, dynamic> input) async {
    try {
      final _result = await _api.call(authorization: true)
          .mutation(ProductDocument.bidProduct,
          variables: input);
      print('bidProduct data ${_result['data']}');
      return _result['data'] != null;
    } catch (e) {
      final message = e.toString().split(":");
      Get.snackbar(
          'Place bid',
          message.last,
          snackPosition: SnackPosition.TOP,
          duration: Duration(seconds: 5)
      );
    }
    return false;
  }

  Future<bool> autoBidProduct(final Map<String, dynamic> input) async {
    try {
      final _result = await _api.call(authorization: true)
          .mutation(ProductDocument.autoBidProduct,
          variables: input);
      print('autoBidProduct data ${_result['data']}');
      return _result['data'] != null;
    } catch (e) {
      final message = e.toString().split(":");
      Get.snackbar(
          'Auto bid',
          message.last,
          snackPosition: SnackPosition.BOTTOM
      );
    }
    return false;
  }

  Future<bool> commentProduct(final Map<String, dynamic> input) async {
    try {
      final _result = await _api.call(authorization: true)
          .mutation(ProductDocument.commentProduct,
          variables: input);
      print('commentProduct  ${_result['data']}');
      return _result['data'] != null;
    } catch (e) {
      final message = e.toString().split(":");
      Get.snackbar(
          'Comment product',
          message.last,
          snackPosition: SnackPosition.BOTTOM
      );
    }
    return false;
  }

  Future<Map<String, dynamic>?> getWatchListItemByUserId(final int? userId) async {
    try {
      final _result = await _api.call(authorization: true)
          .query(ProductDocument.getWatchListItems,
          variables: <String, dynamic>{'userId': userId});
      return _result;
    } catch(e) {
      print('error $e');
    }
    return null;
  }


  Future<WatchProductModel?> checkWishlist({final int? productId, final int? userId}) async {
    try {
      final Map<String, dynamic> input = {
        'userId': userId,
        'productId': productId,
      };
      final _result = await _api.call(authorization: true)
          .query(ProductDocument.existWishlist,
          variables: input);

      List<WatchProductModel>? watchItems = (_result['data']['WatchLists']['select'] as List)
          .cast<Map<String, dynamic>>()
          .map(WatchProductModel.fromJson)
          .toList();
      return watchItems.first;
    } catch(error) {
      print('error $error');
    }
    return null;
  }

  Future<bool?> addWishlist({final int? productId}) async {
    try {
      final Map<String, dynamic> input = {
        "id": productId,
      };
      final _result = await _api.call(authorization: true)
          .mutation(ProductDocument.createWishList,
          variables: input);
      return _result['data'] != null;
    } catch (e) {
      final message = e.toString().split(":");
      Get.snackbar(
          'add wishlist product',
          message.last,
          snackPosition: SnackPosition.BOTTOM
      );
    }
    return false;
  }

  Future<bool?> deleteWatchList({final int? wishlistId}) async {
    try {
      final Map<String, dynamic> input = {
        "id": wishlistId,
      };
      final response = await _api.call(authorization: true)
          .mutation(ProductDocument.deleteWishList,
          variables: input);

      final result =  response['data']['DeleteWatchList']['id'] != null;
      return result;
    } catch (e) {
      final message = e.toString().split(":");
      Get.snackbar(
          'add wishlist product',
          message.last,
          snackPosition: SnackPosition.BOTTOM
      );
    }
    return false;
  }

  Future<List<ProductModel>?> search({required final int page, required final String search})  async {
    try {
      final result = await _api.call()
          .query(ProductDocument.searchPaginate,
          variables: <String, dynamic>{
            'page': page,
            'limit': Constant.limitItems,
            'search': '$search',
          }
      );

      if(result['data'] != null) {
        final data = result['data'];
        final list = (data['items']['select'] as List)
            .cast<Map<String, dynamic>>()
            .map(ProductModel.fromJson)
            .toList();
        print('paginate: $list');

        return list;
      }
    } catch(e) {
      print('error: $e');

    }
    return null;
  }

  Future<List<ProductModel>?> getWithLabel({required final int page, required final String? label})  async {
    try {
      final result = await _api.call()
          .query(ProductDocument.getWithLabel,
          variables: <String, dynamic>{
            'page': page,
            'limit': Constant.limitItems,
            'label': label,
          }
      );

      if(result['data'] != null) {
        final data = result['data'];
        final list = (data['items']['select'] as List)
            .cast<Map<String, dynamic>>()
            .map(ProductModel.fromJson)
            .toList();
        print('paginate: $list');

        return list;
      }
    } catch(e) {
      print('error: $e');

    }
    return null;
  }


  Future<Map<String, dynamic>?> getCreateProductData() async {
    var result = await _api.call()
        .query(ProductDocument.getCreateData);
    return result;
  }

  Future<ProductModel?> createProduct({final Map<String, dynamic>? input}) async{
    var result = await _api.call(authorization: true)
        .mutation(ProductDocument.createProduct,  variables: input);
    final product =  ProductModel.fromJson(result['data']['CreateProduct'] as Map<String, dynamic>);
    print('create response ${product.toString()}');
    return product;
  }

  Future<ProductModel?> updateProduct({final Map<String, dynamic>? input}) async{
    try {

      var result = await _api.call(authorization: true)
          .mutation(ProductDocument.updateProduct,  variables: input);
      return ProductModel.fromJson(result['data']['UpdateProduct'] as Map<String, dynamic>);

    } catch(e) {
      print('update product error $e');
    }
    return null;
  }


  Future<bool?> createProductPromotion({final int? productId, final int? promotionId}) async {
    var result = await _api.call(authorization: true)
        .mutation(ProductDocument.createPromotionProduct,  variables: {"productId": productId, "promotionId": promotionId});
    final success =  result['data']['CreateProductPromotion'] != null;
    print('createProductPromotion  $success}');
    return success;
  }

  Future<bool?> deleteProductPromotion({final int? id}) async {
    try {
      var result = await _api.call(authorization: true).mutation(ProductDocument.deleteProductPromotion,  variables: {"id": id});
      final success =  result['data']['DeleteProductPromotion'] != null;
      print('DeleteProductPromotion  $success}');
      return success;
    } catch(e) {
      print('DeletePromotionProduct error: $e');
    }
    return null;
  }

}
