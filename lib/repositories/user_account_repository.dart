import 'package:get/get.dart';
import 'package:mobile/core/gql_client.dart';
import 'package:mobile/documents/user_document.dart';
import 'package:mobile/shared/models/user_account_model.dart';

class UserAccountRepository {

  final  _api = Get.find<GraphQLClient>();

  Future<Map<String, dynamic>?> getUserAccountWithTransaction() async {
    try {
      var _result = await _api.call(authorization: true)
          .query(UserDocument.getUserAccountWithTransaction);
      return _result;
    } catch (e) {
      print('error $e');
    }
    return null;
  }

  Future<UserAccountModel?> getUserAccount() async {
    try {
      final result = await _api.call(authorization: true).query(UserDocument.getUserAccounts);
      final data = result?['data'];
      if(data != null) {
        final userAccount = (data['UserAccounts']['select'] as List)
            .cast<Map<String, dynamic>>()
            .map(UserAccountModel.fromJson)
            .toList()
            .first;
        print('_userAccount ${userAccount.toString()}');
        return userAccount;
      }
    } catch (e) {
      print('error $e');
    }
    return null;
  }

}
