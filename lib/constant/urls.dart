class URLs {
  static const apiKey = 'FPJY4vItud3/3Gv++tRCsZLP7iiAzNmmnVl08butUdTiQKsrwRW+7raK1QE7YtULJvr1C5LBfqBcviEU%';
  static const baseUrl = 'https://music.angkormusic.com/api/v1';
  static const homeUri = '$baseUrl/';
  static const musicUri = '$baseUrl/music';
  static const incrementUri = '$baseUrl/increment';
  static const settingsUri = '$baseUrl/settings';
  static const artistUri = '$baseUrl/artist';
  static const albumUri = '$baseUrl/album';
  static const playlistUri = '$baseUrl/playlist';
  static const genreUri = '$baseUrl/genre';
}
