
import 'dart:io';

import 'package:get/get.dart';
import 'package:path_provider/path_provider.dart';

class AppPath {

  static const hiveBoxDownload = "boxDownload";
  static const hiveBoxFavorite = "boxFavorite";
  static const hiveBoxUserPlaylist = "boxUserPlaylist";

  static Future<String> hivePath() async {
    final hivePath = (await rootPath()?? '') + Platform.pathSeparator + 'Data';
    final savedDir = Directory(hivePath);
    bool exists = await savedDir.exists();
    if (!exists) {
      savedDir.create();
    }

    return hivePath;
  }

  static Future<String?> downloadPath() async {
    final downloadPath = (await rootPath()?? '') + Platform.pathSeparator + 'Download';
    final savedDir = Directory(downloadPath);
    bool exists = await savedDir.exists();
    if (!exists) {
      savedDir.create();
    }

    return downloadPath;
  }

  static Future<String?> rootPath() async {
    final directory = GetPlatform.isAndroid
        ? await getExternalStorageDirectory()
        : await getApplicationDocumentsDirectory();
    return directory?.path;
  }

}
