import 'package:mobile/shared/models/code_value.dart';
import 'package:collection/collection.dart';

class FeeResolver {

  static const String BID_14_DAY_FEE = "BID_14_DAY_FEE";
  static const String BID_30_DAY_FEE = "BID_30_DAY_FEE";
  static const String SUBTITLE_FEE = "SUBTITLE_FEE";

  static double? getBid14DayFee({required final List<CodeValue> list}) {
      try {
        final codeValue = _findByCode(list, code: BID_14_DAY_FEE);
        return double.tryParse('${codeValue?.value}');
      } catch(e) {
        print('e $e');
      }
      return null;
  }

  static double? getBid30DayFee({required final List<CodeValue> list}) {
    try {
      final codeValue = _findByCode(list, code: BID_30_DAY_FEE);
      return double.tryParse('${codeValue?.value}');
    } catch(e) {
      print('e $e');
    }
    return 0;
  }

  static double? getSubTitleFee({required final List<CodeValue> list}) {
    try {
      final codeValue = _findByCode(list, code: SUBTITLE_FEE);
      return double.tryParse('${codeValue?.value}');
    } catch(e) {
      print('e $e');
    }
    return null;
  }

  static CodeValue? _findByCode(final List<CodeValue> list, {final String? code}) {
    try {
      return list.firstWhereOrNull((element) => element.code == code);
    } catch(e) {
      print('e $e');
    }
    return null;
  }

}