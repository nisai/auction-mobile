import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:get/get.dart';
import 'package:mobile/repositories/user_repository.dart';
import 'package:mobile/shared/models/notification_topic.dart';
import 'package:mobile/utils/utility.dart';

class NotificationService extends GetxService {

  final _repository = Get.find<UserRepository>();
  final _notification = FlutterLocalNotificationsPlugin();

  @override
  void onInit() async {

// initialise the plugin. app_icon needs to be a added as a drawable resource to the Android head project

    await Utility.handleSendFirebaseMessageTokenIfLoggedIn();


    _initLocalNotification().then((_) {
      debugPrint('init local....');
    });


    FirebaseMessaging.onMessage.listen((final RemoteMessage message) {
      handleTopic(message);
    });

    FirebaseMessaging.onMessageOpenedApp.listen((RemoteMessage message) {
      print('A new onMessageOpenedApp event was published!');
      print('FirebaseMessaging: onMessageOpenedApp ${message.notification}');
    });

    super.onInit();
  }


  Future<void> _initLocalNotification() async {
    const AndroidInitializationSettings initializationSettingsAndroid = AndroidInitializationSettings('app_icon');
    final IOSInitializationSettings initializationSettingsIOS = IOSInitializationSettings(onDidReceiveLocalNotification: onDidReceiveLocalNotification);
    final InitializationSettings initializationSettings = InitializationSettings(android: initializationSettingsAndroid, iOS: initializationSettingsIOS);
    await _notification.initialize(initializationSettings, onSelectNotification: selectNotification);
  }


  Future<void> handleTopic(final RemoteMessage message)  async {
    final receiveTopic = message.data['type']?? '';

    RemoteNotification? notification = message.notification;
    Map<String, dynamic> data = message.data;

    final String title = notification?.title?? '';
    final String body = notification?.body?? '';

    switch(receiveTopic) {
      case NotificationTopic.bidWin:
        break;
      case NotificationTopic.bidFinishReserveMet:
        break;
      case NotificationTopic.questionAsked:
        break;
      case NotificationTopic.questionAnswered:
        break;
      case NotificationTopic.bidFinishReserveNotMet:
        break;
      case NotificationTopic.favoriteFinish24h:
        break;
      case NotificationTopic.bidFinish24h:
        break;
      case NotificationTopic.insufficientBalance:
        break;
      case NotificationTopic.productOrder:
        break;
      case NotificationTopic.productBid:
        break;
      case NotificationTopic.favBid:
        break;
      case NotificationTopic.favRelist:
        break;
      case NotificationTopic.feeCharge:
        break;
      case NotificationTopic.topUp:
        break;
      case NotificationTopic.higherBid:
        break;
    }
    const AndroidNotificationDetails androidPlatformChannelSpecifics = AndroidNotificationDetails('your channel id', 'your channel name',
        channelDescription: 'your channel description',
        importance: Importance.max,
        priority: Priority.high,
        ticker: 'ticker');

    const IOSNotificationDetails iOSPlatformChannelSpecifics =
    IOSNotificationDetails(subtitle: 'the subtitle');

    const NotificationDetails platformChannelSpecifics = NotificationDetails(android: androidPlatformChannelSpecifics, iOS: iOSPlatformChannelSpecifics);
    await _notification.show(
      0,
      title,
      body,
      platformChannelSpecifics,
      payload: 'item x',
    );
  }


  void selectNotification(String? payload) async {
    if (payload != null) {
      debugPrint('notification payload: $payload');
    }
  }

  void onDidReceiveLocalNotification(
      int id, String? title, String? body, String? payload) async {
    debugPrint('display a dialog with the notification details, tap ok to go to another page');

  }

}