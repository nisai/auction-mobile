import 'package:dio/dio.dart';
import 'package:get/get.dart' hide Response;
import 'package:mobile/core/local_storage.dart';
import 'package:mobile/shared/models/access_token_model.dart';
import 'package:mobile/utils/constant.dart';
import 'package:mobile/utils/urls.dart';


class HttpClient {

  final Dio _dio;
  final _storage = Get.find<LocalStorage>();

  HttpClient(this._dio);

  Future<Dio> getClient({final bool authenticated = false, final bool useBasePath = true}) async {
    _dio..options.baseUrl = useBasePath? URLs.baseUrl: '';
    _dio..options.connectTimeout = 10000;
    _dio..options.receiveTimeout = 30000;
    _dio..interceptors.clear();
    if (authenticated) {
      final AccessTokenModel? _token = _storage.read(Constant.accessToken, construct: (map) => AccessTokenModel.fromJson(map));
      final String? token = _token?.accessToken;
      final String bearerToken = "Bearer $token";
      _dio..interceptors.add(QueuedInterceptorsWrapper(
        onRequest: (final RequestOptions options, final RequestInterceptorHandler handler) {
          if (token != null) {
            options.headers["Authorization"] = bearerToken;
          }
          handler.next(options);
        },

        onError: (final DioError error, final ErrorInterceptorHandler handler) {
          final bool invalidOrExpiredToken = token != null &&  error.response?.statusCode == 401;
          if (invalidOrExpiredToken) {
            var tokenDio = Dio()
              ..options.baseUrl = URLs.baseUrl
              ..options.connectTimeout = 1000
              ..options.receiveTimeout = 30000;
            final RequestOptions options = error.response!.requestOptions;
            // invalid token
            if (options.headers['Authorization'] != bearerToken) {
              options.headers['Authorization'] = bearerToken;
              //repeat
              _dio.fetch(options).then((r) => handler.resolve(r),
                onError: (e) {
                  handler.reject(e);
                },
              );
              return;
            }

            final Map<String, dynamic> _refreshTokenData = {
              "token": _token?.refreshToken,
            };

            tokenDio.post<Map<String, dynamic>>('/auth/refresh-token', data: _refreshTokenData).then((result) {
              //update csrfToken
              final AccessTokenModel? _refreshToken = AccessTokenModel.fromJson(result.data);
              final bool validToken = _refreshToken != null && _refreshToken.accessToken !=  null;
              if (validToken) {
                _storage.write(Constant.accessToken, _refreshToken.toMap());
                options.headers['Authorization'] = "Bearer ${_refreshToken.accessToken}";
              }
            }).then((e) {
              //repeat
              _dio.fetch(options).then(
                    (r) => handler.resolve(r),
                onError: (e) {
                  handler.reject(e);
                },
              );
            });
            return;
          }
          return handler.next(error);
        },

        onResponse: (final Response response, final ResponseInterceptorHandler handler) {
          handler.next(response);
        },

      ));
    }
    return _dio;
  }
}

