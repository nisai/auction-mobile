import 'package:get/get.dart';
import 'package:hasura_connect/hasura_connect.dart';
import 'package:mobile/core/local_storage.dart';
import 'package:mobile/shared/logger/logger_utils.dart';
import 'package:mobile/shared/models/access_token_model.dart';
import 'package:mobile/utils/constant.dart';
import 'package:mobile/utils/urls.dart';

class GraphQLClient {

  HasuraConnect call({bool authorization = false}) {
    if(authorization) {
      return HasuraConnect(URLs.graphql, interceptors: [GraphQLInterceptor(authorization: authorization)]);
    }
    return HasuraConnect(URLs.graphql);
  }
}

class GraphQLInterceptor extends InterceptorBase {
  final bool authorization;

  final _storage = Get.find<LocalStorage>();

  GraphQLInterceptor({required this.authorization});

  @override
  Future<Request> onRequest(Request request, HasuraConnect connect) async {
    if(authorization) {
      final AccessTokenModel? _token = _storage.read(Constant.accessToken, construct: (map) => AccessTokenModel.fromJson(map));
      final String? token = _token?.accessToken;
      if (token != null) {
        try {
          request.headers["Authorization"] = "Bearer $token";
        } catch (e) {
          logger.e('$e');
        }
      }
    }
    return request;
  }
}
