import 'package:dio/dio.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:get/get.dart';
import 'package:mobile/core/gql_client.dart';
import 'package:mobile/core/http_client.dart';
import 'package:mobile/core/local_storage.dart';
import 'package:mobile/core/notification_service.dart';
import 'package:mobile/repositories/product_repository.dart';
import 'package:mobile/repositories/user_repository.dart';
import 'package:mobile/services/user_service.dart';

class LandingBinding extends Bindings {
  @override
  void dependencies() {
    Get.put<LocalStorage>(LocalStorage(), permanent: true);
    Get.put<HttpClient>(HttpClient(Dio()), permanent: true);
    Get.put<GraphQLClient>(GraphQLClient(), permanent: true);
    Get.put<FirebaseAuth>(FirebaseAuth.instance, permanent: true);
    Get.put<UserRepository>(UserRepository(), permanent: true);
    Get.put<AuthService>(AuthService(), permanent: true);
    Get.put<NotificationService>(NotificationService(), permanent: true);
    Get.put<ProductRepository>(ProductRepository(), permanent: true);
  }
}
