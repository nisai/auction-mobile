import 'dart:convert' as convert;

import 'package:get_storage/get_storage.dart';


class LocalStorage{
  final storage = GetStorage();

  write(String key, dynamic value) {
    storage.write(key, convert.jsonEncode(value));
  }

  read<S>(String key, {S Function(Map<String, dynamic>)? construct}) {
    String? value = storage.read(key);

    if (value == null) return;

    if (construct == null) return convert.jsonDecode(value);
    Map<String, dynamic> json = convert.jsonDecode(value);
    return construct(json);
  }

  remove(String key) {
    storage.remove(key);
  }

  removeAll() {

  }
}
