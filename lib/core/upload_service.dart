import 'dart:io';
import 'package:dio/dio.dart';
import 'package:get/get.dart' hide FormData, MultipartFile;
import 'package:mobile/core/http_client.dart';
import 'package:mobile/utils/constant.dart';

class UploadService {

  final  _api = Get.find<HttpClient>();

  Future<String?> uploadFile({final String? imagePath}) async {
    try {
      if(imagePath != null) {
        final file = File(imagePath);
        final fileName = file.path.split('/').last;
        final formData = FormData.fromMap({
          "file": await MultipartFile.fromFile(file.path, filename: fileName),
        });
        final rest = await _api.getClient(authenticated: true);
        final result = await rest.post(Constant.uploadUri, data: formData);
        final path = result.data['path'];
        print('upload path from camrea $path');
        return path;
      }
    } catch(e) {
      print("upload File error $e");
    }
    return null;
  }

}