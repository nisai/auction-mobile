import 'package:flutter/material.dart';
import 'package:mobile/core/theme/colors.dart';

class AppThemes {
  static final light = ThemeData.light().copyWith(
    backgroundColor: AppColor.dashAmber,
    primaryColor: AppColor.primaryColor,
  );


  static final dark = ThemeData.dark().copyWith(
    backgroundColor: Colors.black,
  );
}