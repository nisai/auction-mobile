//
// import 'package:get/get.dart';
// import 'package:mobile/core/local_storage.dart';
// import 'package:mobile/routes/app_pages.dart';
// import 'package:mobile/shared/models/access_token_model.dart';
//
//
// class AccessTokenController extends GetxController {
//
//   AccessTokenController(this._storage);
//
//   static AccessTokenController get to => Get.find();
//
//   static const ACCESS_TOKEN = 'access-token';
//
//   AccessTokenModel accessToken;
//
//   final LocalStorage _storage;
//   final isLogged = true.obs;
//
//   bool get loggedIn => isLogged.value;
//
//   @override
//   void onInit() {
//     ever(isLogged, fireRoute);
//     super.onInit();
//     accessToken = _storage.read(ACCESS_TOKEN, construct: (map) => AccessTokenModel.fromJson(map));
//     if (accessToken == null) {
//       isLogged.value = false;
//     }
//   }
//
//   fireRoute(logged) {
//     if (!logged) {
//       print('implementation init route');
//       //Get.toNamed(Routers.initialRoute);
//     }
//   }
//
//   void save(final AccessTokenModel accessToken) async {
//     await _storage.write(ACCESS_TOKEN, accessToken.toMap());
//     this.accessToken = accessToken;
//     update();
//   }
//
//   Future<void> logout() async {
//     await _storage.remove(ACCESS_TOKEN);
//     accessToken = null;
//     isLogged.value = false;
//     update();
//    Get.offNamedUntil(AppPages.INITIAL, (route) => false);
//   }
// }
