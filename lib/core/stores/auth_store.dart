//
// import 'package:get/get.dart';
// import 'package:mobile/core/local_storage.dart';
// import 'package:mobile/shared/models/app_user.dart';
// import 'package:mobile/shared/models/user_model.dart';
//
// class AuthStore extends GetxController {
//
//   AuthStore(this._storage);
//
//   static AuthStore get to => Get.find();
//
//   static const TOKEN = 'TOKEN';
//   static const USER = 'USER';
//
//   String token;
//   UserModel user;
//   bool isLoggin;
//
//   final LocalStorage _storage;
//   final isLogged = true.obs;
//
//   @override
//   void onInit() {
//     ever(isLogged, fireRoute);
//     super.onInit();
//     token = _storage.read(TOKEN);
//     user = _storage.read(USER, construct: (map) => UserModel.fromJson(map));
//     if (token == null && user == null) {
//       isLogged.value = false;
//     }
//   }
//
//   fireRoute(logged) {
//     if (!logged) {
//       print('implementation init route');
//       //Get.toNamed(Routers.initialRoute);
//     }
//   }
//
//   void save(AppUser app) {
//     saveUser(app.user);
//     saveToken(app.jwt);
//     update();
//   }
//
//   void saveUser(UserModel user) async {
//     await _storage.write(USER, user.toMap());
//     this.user = user;
//     update();
//   }
//
//   void saveToken(String token) async {
//     await _storage.write(TOKEN, token);
//     this.token = token;
//     update();
//   }
//
//   Future<void> logout() async {
//
//     await _storage.remove(USER);
//     await _storage.remove(TOKEN);
//     user = null;
//     token = null;
//     isLogged.value = false;
//     update();
//    // Get.offNamedUntil(Routers.initialRoute, (route) => false);
//   }
// }
