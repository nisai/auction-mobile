import 'package:flutter/painting.dart';

class AppColor {
  static const Color primaryPurple = const Color(0xFF6334DC);
  static const Color primaryWhite = const Color(0xFFFFFFFF);
  static const Color primaryWhiteSmoke = const Color(0xFFF5F5F5);
  static const Color primaryPlatinum = const Color(0xFFE5E4E2);
  static const Color textDark = const Color(0xFF444444);
  static const Color textGrey = const Color(0xFF808080);
  static const Color lightGrey = const Color(0xFFD0D0D0);
  static const Color appBarDark = const Color(0xFF0B0320);
  static const Color dashBlue = const Color(0xFFDFF0FC);
  static const Color dashPurple = const Color(0xFFE2DDF9);
  static const Color dashAmber = const Color(0xFFF2E4D7);
  static const Color primaryColor = const  Color(0xfff7892b);
}

const kBackgroundColor = Color(0xFFFEFFFF); //White Background
const kPrimaryColor = Color(0xFF035AA6);
const kSecondaryColor = Color(0xFF95A5A6);
const kTextColor = Color(0xFF273242); //Black Color
const kTextLightColor = Color(0xFF747474);
const kBlueColor = Color(0xFF3862FD);
const kLightBlueColor = Color(0xFFE7EEFE);
const kGreyColor = Color(0xFFE7EAF1);
const kDarkGreyColor = Color(0xFF757575);
const kSelectItemColor = Color(0xFF35485d);
const kDeepBlueColor = Color(0xFF594CF5);
const kRedColor = Color(0xFFF65054);
const kGreenColor = Color(0xFF2BD0A8);
const kDarkButtonBg = Color(0xFF273546);
const kTabBarBg = Color(0xFFEEEEEE);
const kTextBlueColor = Color(0xFF5594bf);
const kFormInputColor = Color(0xFFc7c8ca);
const kSectionTileColor = Color(0xFFdddcdd);
