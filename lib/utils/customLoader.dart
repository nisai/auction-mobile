import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CustomLoader {

  factory CustomLoader() => _instance;
  static CustomLoader get instance => _instance;
  static final CustomLoader _instance = CustomLoader._internal();

  OverlayState? _overlayState;
  OverlayEntry? _overlayEntry;

  CustomLoader._internal();

  showLoader(final BuildContext context) {
    print('show loader');
    _overlayState = Overlay.of(context);
    _overlayEntry = OverlayEntry(
      builder: (BuildContext context) {
        return Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            child: _buildLoader()
        );
      },
    );

    if(_overlayEntry != null) {
      _overlayState?.insert(_overlayEntry!);
    }

  }

  hideLoader() {
    try {
      _overlayEntry?.remove();
      _overlayEntry = null;
    } catch (e) {
      print("Exception:: $e");
    }
  }

  void dispose() {
    _overlayEntry?.remove();
    _overlayEntry = null;
  }

  _buildLoader({Color? backgroundColor}) {
    if (backgroundColor == null) {
      backgroundColor = const Color(0xffa8a8a8).withOpacity(.5);
    }
    var height = 150.0;
    return CustomScreenLoader(
      height: height,
      width: height,
      backgroundColor: backgroundColor,
    );
  }
}

class CustomScreenLoader extends StatelessWidget {
  final Color backgroundColor;
  final double height;
  final double width;
  const CustomScreenLoader(
      {Key? key,
        this.backgroundColor = const Color(0xfff8f8f8),
        this.height = 30,
        this.width = 30})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: backgroundColor,
      child: Container(
        height: height,
        width: height,
        alignment: Alignment.center,
        child: Container(
          padding: EdgeInsets.all(50),
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.all(Radius.circular(10))),
          child: Stack(
            alignment: Alignment.center,
            children: <Widget>[
              Platform.isIOS
                  ? CupertinoActivityIndicator(
                radius: 35,
              )
                  : CircularProgressIndicator(
                strokeWidth: 2,
              ),
              Image.asset(
                'assets/icon/icon.png',
                height: 30,
                width: 30,
              )
            ],
          ),
        ),
      ),
    );
  }
}