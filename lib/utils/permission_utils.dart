import 'dart:io';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';
const STORAGE_NAME =  'khmer_auction';

Future<bool> hasStoragePermission() async {
  if (Platform.isIOS) {
    return Future.value(true);
  } else {
    final permissionStatus = await Permission.storage.request();
    return Future.value(permissionStatus.isGranted);
  }
}

Future<bool> hasPermissionCamera() async {
  var status = await Permission.camera.status;
  if (status.isGranted) {
    return true;
  }

  // You can can also directly ask the permission about its status.
  if (await Permission.location.isRestricted) {
    return false;
  }
  final permissionStatus = await Permission.storage.request();
  return Future.value(permissionStatus.isGranted);
}

Future<String?> getStorageDirectory() async {
  if (Platform.isIOS) {
    var d = await getApplicationSupportDirectory();

    return join(d.path, STORAGE_NAME);
  } else {
    return join(await _getSDCard()?? '', STORAGE_NAME);
  }
}

Future<bool?> hasExternalStorage() async {
  try {
    var result = await _getSDCard();

    return result?.isNotEmpty;
  } catch (e) {
    return Future.value(false);
  }
}

Future<String?> _getSDCard() async {
  final appDocumentDir = await getExternalStorageDirectories(type: StorageDirectory.podcasts);
  String? path;
  if (appDocumentDir!.isNotEmpty) {
    // See if we can find the last card without emulated
    for (var d in appDocumentDir) {
      print('Found path ${d.absolute.path}');
      if (!d.path.contains('emulated')) {
        path = d.absolute.path;
      }
    }
  }
  if (path == null) {
    throw ('No SD card found');
  }
  return path;
}

String? safePath(String? s) {
  return s?.replaceAll(RegExp(r'[^\w\s\.]+'), '');
}
