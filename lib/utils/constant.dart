import 'package:flutter/material.dart';

class Constant {


  static const GRAPH_QL_API ='https://api.khmerauction.com/graphql';
  static const BASE_API ='https://api.khmerauction.com';

  static const maxPostImage = 8;


  // set it to false to disable log data and error in console.
  // set it to true to enable log data and error in console
  static const bool enableQueryLog = true;

  static const uploadUri = '/upload';

  static const singerUri = '/singer';
  static const categoryUri = '/category';
  static const albumUri = '/album';
  static const firstOpenAppKey = "first-open";
  static const themeKey = "app-theme";

  static const login = "/oauth/token";
  static const registerUri = "/user";
  static const userUpdateUri = "/user/current/update";
  static const changePasswordUri = "/user/changePassword";
  static const currentUser = "/user/current";
  static const accessToken = "/access-token";

  static const productFeeUri = '/product/price';
  static const productSearchUri = '/search';

  // payments pipay
  static const piPayPaymentInitUri = '/pipay/init-deposit_wallet';
  static const piPayStartTransactionUri = 'https://onlinepayment-uat.pipay.com/starttransaction';

  static const int limitItems = 20;

  static const khmerLanguageCode = 'kh';
  static const englishLanguageCode = 'en';

  static const languageKey = 'language';
  static const firebaseMessageTokenAlreadyCached = 'firebase-message-token-already-cache';
  static const firstOpenKey = 'first_open';

  static const accessTokenKey = 'access-token';

  static const appPackage = 'com.khmerauction.auction';

  static const facebookPageId = '111140801239571';
  static const facebookPageUri = 'https://web.facebook.com/Khmer-Remix-111140801239571';

}

const Color lightThemePrimaryColor = Color(0xffFFFFFF);
const Color lightThemePrimaryColorDark = Color(0xffE5EBF0);
const Color lightThemeAccentColor = Color(0xff5B37B7);

const Color darkThemePrimaryColor = Color(0xff212121);
const Color darkThemePrimaryColorDark = Color(0xff2B2B2B);
const Color darkThemeAccentColor = Color(0xff68F0AE);

const Color amoledThemePrimaryColor = Color(0xff000000);
const Color amoledThemePrimaryColorDark = Color(0xff2B2B2B);
const Color amoledThemeAccentColor = Color(0xff68F0AE);

final List<ThemeData> themes = [lightTheme, darkTheme, amoledTheme];

const List<Color> cardColors = [
  Color(0xffadb6c6),
  Color(0xff963e63),
  Color(0xffe6a542),
  Color(0xff519b89),
  Color(0xffab8c67),
  Color(0xffde34a0),
  Color(0xff5a9def),
  Color(0xfff4663c),
  Color(0xffa9c95a),
];

final darkTheme = ThemeData(
  primaryColor: darkThemePrimaryColor,
  primaryColorDark: darkThemePrimaryColorDark,
  canvasColor: Colors.transparent,
  primaryIconTheme: IconThemeData(color: Colors.white),
  textTheme: TextTheme(
    headline1: TextStyle(
        fontFamily: 'Battambang',
        fontWeight: FontWeight.bold,
        color: Colors.white,
        fontSize: 22),
    headline2: TextStyle(
        fontFamily: 'Battambang',
        fontWeight: FontWeight.bold,
        color: Colors.white,
        fontSize: 20),
    headline3: TextStyle(
        fontFamily: 'Battambang',
        fontWeight: FontWeight.bold,
        color: Colors.white,
        fontSize: 18),
    headline4: TextStyle(
        fontFamily: 'Battambang',
        fontWeight: FontWeight.bold,
        color: Colors.white,
        fontSize: 16),
    headline5: TextStyle(
        fontFamily: 'Battambang',
        fontWeight: FontWeight.bold,
        color: Colors.white,
        fontSize: 14),

    headline6: TextStyle(
        fontFamily: 'Battambang',
        fontWeight: FontWeight.bold,
        color: Colors.white,
        fontSize: 12),
    bodyText1: TextStyle(
        fontFamily: 'Battambang',
        fontWeight: FontWeight.bold,
        color: Colors.white,
        fontSize: 18),
    bodyText2: TextStyle(
        fontFamily: 'Battambang',
        fontWeight: FontWeight.bold,
        color: Colors.white,
        fontSize: 16),
    caption: TextStyle(
        fontFamily: 'Battambang',
        fontWeight: FontWeight.bold,
        color: Colors.white,
        fontSize: 12),
  ), colorScheme: ColorScheme.fromSwatch().copyWith(secondary: darkThemeAccentColor),
);

final amoledTheme = ThemeData(
  primaryColor: amoledThemePrimaryColor,
  primaryColorDark: amoledThemePrimaryColorDark,
  canvasColor: Colors.transparent,
  primaryIconTheme: IconThemeData(color: Colors.white),
  textTheme: TextTheme(
    headline6: TextStyle(
        fontFamily: 'Battambang',
        fontWeight: FontWeight.bold,
        color: Colors.white,
        fontSize: 18),
    bodyText1: TextStyle(
        fontFamily: 'Battambang',
        fontWeight: FontWeight.bold,
        color: Colors.white,
        fontSize: 16),
    bodyText2: TextStyle(
        fontFamily: 'Battambang',
        fontWeight: FontWeight.bold,
        color: Colors.white,
        fontSize: 14),
    caption: TextStyle(
        fontFamily: 'Battambang',
        fontWeight: FontWeight.bold,
        color: Colors.white,
        fontSize: 10),
  ), colorScheme: ColorScheme.fromSwatch().copyWith(secondary: amoledThemeAccentColor),
);

final lightTheme = ThemeData(
  primaryColorDark: lightThemePrimaryColorDark,
  primaryColor: lightThemePrimaryColor,
  canvasColor: Colors.transparent,
  primaryIconTheme: IconThemeData(color: Colors.black),
  textTheme: TextTheme(
    headline6: TextStyle(
        fontFamily: 'Battambang',
        fontWeight: FontWeight.bold,
        color: Colors.black,
        fontSize: 18),
    bodyText1: TextStyle(
        fontFamily: 'Battambang',
        fontWeight: FontWeight.bold,
        color: Colors.black,
        fontSize: 16),
    bodyText2: TextStyle(
        fontFamily: 'Battambang',
        fontWeight: FontWeight.bold,
        color: Colors.black,
        fontSize: 14),
    caption: TextStyle(
        fontFamily: 'Battambang',
        fontWeight: FontWeight.bold,
        color: Colors.black,
        fontSize: 10),
  ), colorScheme: ColorScheme.fromSwatch().copyWith(secondary: lightThemeAccentColor),
);

const List<String> phoneResolutions = [
  "1080x1920",
  "750x1334",
  "2960x1440",
  "640x1136",
  "1440x2560",
  "640x960",
  "720x1280",
  "1920x1080",
  "800x1280",
  "480x800",
  "768x1280",
  "800x480",
  "828x1792",
  "854x480",
  "960x540",
  "1125x2436",
  "1242x2688",
  "1280x720",
  "2560x1440",
];

const String songPlaceholder = "assets/song_placeholder.png";
const String albumPlaceholder = "assets/album_placeholder.png";
const String singerPlaceholder = "assets/singer_placeholder.png";
const String appIcon = "assets/icon/icon.png";
