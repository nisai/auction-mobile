import 'package:connectivity/connectivity.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:mobile/core/local_storage.dart';
import 'package:mobile/repositories/user_repository.dart';
import 'package:mobile/shared/models/access_token_model.dart';
import 'package:mobile/shared/models/user_model.dart';
import 'package:mobile/utils/constant.dart';

abstract class Utility  {

  static Future<bool> checkInternet() async {
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.mobile) {
      print('I am connected to a mobile network');
    } else if (connectivityResult == ConnectivityResult.wifi) {
      print('I am connected to a wifi network.');
    }
    return connectivityResult != ConnectivityResult.none;
  }

  static Future<bool?> isLoggedIn() async {
    try {
      final _storage = Get.find<LocalStorage>();
      final AccessTokenModel? _token = _storage.read(Constant.accessToken, construct: (map) => AccessTokenModel.fromJson(map));
      return _token?.accessToken != null;
    } catch(e) {
     print('Utility.isLoggedIn: error $e');
    }
    return null;
  }

  static Future<UserModel?> getCurrentUser() async {
    try {
      final _storage = Get.find<LocalStorage>();
      final UserModel? user = _storage.read(Constant.currentUser, construct: (map) => UserModel.fromJson(map));
      return user;
    } catch(e) {
      print('Utility.getCurrentUser: error $e');
    }
    return null;
  }

  static Future<void> handleSendFirebaseMessageTokenIfLoggedIn() async {
    final login = await isLoggedIn()?? false;
    if (login) {
      final _box = GetStorage();
      final bool cached = _box.read(Constant.firebaseMessageTokenAlreadyCached) ?? false;
      if(!cached) {
        final String? token = await FirebaseMessaging.instance.getToken();
        if(token != null) {
          final _repository = Get.find<UserRepository>();
          await _repository.createMessageToken(messageToken: token);
          await _box.write(Constant.firebaseMessageTokenAlreadyCached, true);
        }
      }
    }
  }
}