import 'package:timeago/timeago.dart' as timeago;

class Helpers {

  static String getRemainFromExpectDate({final String? expectFinishedAt}) {
    try {
      int now = DateTime.now().millisecondsSinceEpoch;
      DateTime target = DateTime.parse(expectFinishedAt?? '');
      Duration remaining = Duration(milliseconds: target.millisecondsSinceEpoch - now);

      if(remaining.inDays > 1) {
        return '${remaining.inDays}days';
      }

      if(remaining.inHours > 1) {
        String twoDigitMinutes = twoDigits(remaining.inMinutes.remainder(60));
        return "${remaining.inHours}h :${twoDigitMinutes}m";
      }

      String twoDigitMinutes = twoDigits(remaining.inMinutes.remainder(60));
      String twoDigitSeconds = twoDigits(remaining.inSeconds.remainder(60));

      return "${twoDigitMinutes}m :${twoDigitSeconds}s ";
    } catch(e) {
      print("getRemainFromExpectDate have issue $e");
    }
    return '';
  }

  static String twoDigits(int n) => n.toString().padLeft(2, "0");

  static String? getTimeHuman({final String? createdAt}) {
    try {
      var estimateTs = DateTime.parse(createdAt?? Duration.zero.toString());
      return timeago.format(estimateTs);
    } catch(e) {
      print("getTimeHuman have issue $e");
    }
    return null;
  }
}