import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:mobile/routes/app_pages.dart';
import 'package:mobile/utils/utility.dart';

class EnsureAuthMiddleware extends GetMiddleware {

    @override
    int? get priority => 1;

    @override
    Future<GetNavConfig?> redirectDelegate(GetNavConfig route) async {
      // you can do whatever you want here
      // but it's preferable to make this method fast
      // await Future.delayed(Duration(milliseconds: 500));
      final bool isLoggIn = await Utility.isLoggedIn()?? false;
      if (!isLoggIn) {
        final newRoute = Routes.authenticationThenTo(route.location!);
        return GetNavConfig.fromRoute(newRoute);
      }
      return await super.redirectDelegate(route);
    }

    @override
    GetPageBuilder? onPageBuildStart(GetPageBuilder? page) {
      print('Bindings of ${page.toString()} are ready');
      return page;
    }

    @override
    Widget onPageBuilt(Widget page) {
      print('Widget ${page.toStringShort()} will be showed');
      return page;
    }

    @override
    void onPageDispose() {
      print('PageDisposed');
    }

}


class EnsureAuthedVerifyMiddleware extends GetMiddleware {
  @override
  int? get priority => 2;

}

class EnsureNotAuthedMiddleware extends GetMiddleware {
  @override
  int? get priority => 2;

}
