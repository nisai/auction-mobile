import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:mobile/routes/app_pages.dart';
import 'package:mobile/utils/constant.dart';

class LanguageMiddleware extends GetMiddleware {

  @override
  int? get priority => 2;

  static final box = GetStorage();

  @override
  Future<GetNavConfig?> redirectDelegate(GetNavConfig route) async {
    try {
      final usedLanguage = box.read(Constant.languageKey);
      print('used $usedLanguage');
      if (usedLanguage == null) {
        return GetNavConfig.fromRoute(Routes.LANGUAGE);
      }
    } catch(e) {
      print('error $e');
    }
    return await super.redirectDelegate(route);
  }

}