import 'package:get/get.dart';
import 'package:mobile/modules/deposit_wallet/deposit_wallet_controller.dart';

class DepositWalletBinding extends Bindings {

  @override
  void dependencies() {
    Get.lazyPut(() => DepositWalletController());
  }

}
