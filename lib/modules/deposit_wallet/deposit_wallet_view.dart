import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mobile/core/local_storage.dart';
import 'package:mobile/core/theme/colors.dart';
import 'package:mobile/modules/deposit_wallet/deposit_wallet_controller.dart';
import 'package:mobile/routes/app_pages.dart';
import 'package:mobile/shared/models/access_token_model.dart';
import 'package:mobile/shared/models/payment_model.dart';
import 'package:mobile/utils/constant.dart';
import 'package:webview_flutter/webview_flutter.dart';

class DepositWalletView extends GetView<DepositWalletController> {

  @override
  Widget build(BuildContext context) {

    return GetBuilder<DepositWalletController>(builder: (controller){
      if(controller.shouldNext.value) {
        switch(controller.selectedPayment) {
          case PaymentModel.WING: return _buildWing();
          case PaymentModel.PIPAY: return _buildPiPay();
        }
      }
      return _buildForm();
    });
  }

  Widget _buildForm() {
    return Scaffold(
      appBar: AppBar(
        title: Text('select_payment'.tr, textAlign: TextAlign.start),
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () {
            Get.rootDelegate.offNamed(Routes.USER_ACCOUNT);
          },
        ),
      ),
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 20.0),
        child: Form(
            key: controller.formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(
                  child: ListView.builder(
                      primary: false,
                      shrinkWrap: true,
                      itemCount: controller.paymentOptions.length,
                      itemBuilder: (context, index) {
                        final item = controller.paymentOptions[index];
                        return Container(
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.all(Radius.circular(4)),
                              border:
                              Border.all(color: Colors.tealAccent.withOpacity(0.4), width: 1),
                              color: item.selected? Colors.tealAccent.withOpacity(0.2): null
                          ),
                          margin: EdgeInsets.all(2),
                          child: ListTile(
                            onTap: () {
                              controller.changeOptions(item);
                            },
                            leading: Radio(
                              value: item.selected? 1: 0,
                              groupValue: 1,
                              onChanged: (isChecked) {
                                controller.changeOptions(item);
                              },
                              activeColor: Colors.tealAccent.shade400,
                            ),
                            title: Row(
                              children: [
                                Container(
                                  width: 24,
                                  height: 24,
                                  child: Image.asset(item.image?? '',
                                  ),
                                ),
                                SizedBox(
                                  width: 5,
                                ),
                                Text(
                                  '${item.name}',
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 14,
                                      fontWeight: FontWeight.w600),
                                ),
                              ],
                            ),
                          ),
                        );
                      }
                  ),
                ),
                Container(
                  margin: EdgeInsets.symmetric(vertical: 10),
                  decoration: BoxDecoration(
                      border: Border.all(color: AppColor.primaryColor),
                      borderRadius: BorderRadius.all(Radius.circular(10))),
                  child: TextFormField(
                    keyboardType: TextInputType.number,
                    controller: controller.amountTextController.value,
                    decoration: InputDecoration(
                        contentPadding: EdgeInsets.symmetric(horizontal: 20),
                        hintText: 'amount'.tr,
                        border: InputBorder.none,
                        filled: true),
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return 'Please enter amount';
                      }
                      return null;
                    },
                  ),
                ),
                SizedBox(height: 5.0),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      child: MaterialButton(
                        color: Color(0xfff7892b),
                        child: Text(
                          'continue'.tr,
                          style: TextStyle(color: Colors.white),
                        ),
                        onPressed: () {

                          final bool valid = controller.formKey.currentState?.validate()?? false;
                          if(valid) {
                            controller.handleNext();
                          }
                        },
                      ),
                    ),
                  ],
                ),
              ],
            )),
      ),
    );
  }

  Widget _buildWing() {
    return SafeArea(
        child: Container(
          child: WebView(
            initialUrl: controller.wingUrl,
            javascriptMode: JavascriptMode.unrestricted,
            onWebViewCreated: (WebViewController webViewController) async {
              final _storage = Get.find<LocalStorage>();
              final AccessTokenModel? _token = _storage.read(Constant.accessToken, construct: (map) => AccessTokenModel.fromJson(map));
              String? token = _token?.accessToken;
              Map<String, String> headers = {"Authorization": "Bearer $token"};
              webViewController.loadUrl(controller.wingUrl, headers: headers);
            },
            navigationDelegate: (NavigationRequest request) {
              print('allowing navigation to $request');
              return NavigationDecision.navigate;
            },
            onPageStarted: (String url) async {
              print('Page started loading: $url');
            },
            onPageFinished: (String url)  async {
              print('--- Page finished loading: $url');
              if(url.contains(controller.wingSuccessUrl)) {
               // controller.handleSuccessPayment();
                await controller.handleSuccessDeposit();
              } else if(url.contains(controller.wingFailedUrl)) {

                await controller.handleCancelDeposit();
              }
            },
            gestureNavigationEnabled: true,
          ),
        )
    );
  }

  Widget _buildPiPay() {
    return SafeArea(
        child: Container(
          child: WebView(
            initialUrl: controller.pipayUrl,
            javascriptMode: JavascriptMode.unrestricted,
            onWebViewCreated: (WebViewController webViewController) async {
              final _storage = Get.find<LocalStorage>();
              final AccessTokenModel? _token = _storage.read(Constant.accessToken, construct: (map) => AccessTokenModel.fromJson(map));
              String? token = _token?.accessToken;
              Map<String, String> headers = {"Authorization": "Bearer $token"};
              webViewController.loadUrl(controller.pipayUrl, headers: headers);
            },
            navigationDelegate: (NavigationRequest request) {
              print('allowing navigation to $request');
              return NavigationDecision.navigate;
            },
            onPageStarted: (String url) {
              print('Page started loading: $url');
            },
            onPageFinished: (String url) async {
              print('--- Page finished loading: $url');
              if(url.contains(controller.pipaySuccessUrl)) {
                //controller.handleSuccessPayment();
                await controller.handleSuccessDeposit();
              } else if(url.contains(controller.pipayFailedUrl)) {
                await controller.handleCancelDeposit();
              }
            },
            gestureNavigationEnabled: true,
          ),
        )
    );
  }
}
