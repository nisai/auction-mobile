import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:mobile/routes/app_pages.dart';
import 'package:mobile/shared/models/payment_model.dart';


class DepositWalletController extends GetxController {

  final formKey = GlobalKey<FormState>();
  final amountTextController = TextEditingController().obs;
  double get amount => double.tryParse(amountTextController.value.text)?? 0.0;
  var paymentOptions = <PaymentModel>[].obs;

  var _thenTo = ''.obs;

  var shouldNext = false.obs;

  String get selectedPayment => paymentOptions.firstWhere((element) => element.selected == true).id.toString();

  //bool get isWing => paymentOptions.where((item) => item.id == PaymentModel.WING).isNotEmpty;

  @override
  void onInit() {

    _handleInit();

    super.onInit();
  }

  @override
  void onReady() {

    super.onReady();
  }

  void _handleInit() {
    final _paymentOption = <PaymentModel>[
      PaymentModel(id: PaymentModel.WING, name: 'wing'.tr, image: 'assets/payments/wing.png', selected: true),
      PaymentModel(id: PaymentModel.PIPAY, name: 'pipay'.tr, image: 'assets/payments/pipay.png'),
    ];

    paymentOptions(_paymentOption);
    final String? thenTo = Get.rootDelegate.currentConfiguration?.currentPage?.parameters?['then'];
    _thenTo(thenTo);

    update();
  }

  void handleNext() {
     shouldNext(true);
     update();
  }

  void handleDepositInit(final BuildContext context) {

    final double amount = double.tryParse(amountTextController.value.text)?? 0;

    print('deposit_wallet: $selectedPayment');
    if(selectedPayment == PaymentModel.WING) {
      WidgetsBinding.instance.addPostFrameCallback((_) {
        Get.rootDelegate.toNamed(Routes.WING, arguments: amount);
      });
    } else if(selectedPayment == PaymentModel.PIPAY) {
      WidgetsBinding.instance.addPostFrameCallback((_) {
        Get.rootDelegate.toNamed(Routes.PIPAY, arguments: amount);
      });
    }
  }

  void changeOptions(PaymentModel item) {
    paymentOptions.forEach((element) {
      element.selected = false;
    });
    paymentOptions[paymentOptions.indexOf(item)].selected = true;
    update();
  }

  Future<void> handleCancelDeposit() async {
    shouldNext(false);
    update();
  }

  Future<void> handleSuccessDeposit() async  {
    Get.rootDelegate.offNamed(_thenTo.value.isNotEmpty? _thenTo.value:  Routes.USER_ACCOUNT);
  }

   final pipaySuccessUrl = 'https://api.khmerauction.com/pipay/success';
   final pipayFailedUrl = 'https://api.khmerauction.com/pipay/fail';
  String get pipayUrl => "https://api.khmerauction.com/payment/pipay/init?amount=$amount";

  final wingSuccessUrl = 'https://api.khmerauction.com/wing/success';
  final wingFailedUrl = 'https://api.khmerauction.com/wing/fail';
  String get  wingUrl => "https://api.khmerauction.com/payment/wing/init?amount=$amount";
}
