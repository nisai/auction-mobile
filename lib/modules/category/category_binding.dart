import 'package:get/get.dart';
import 'package:mobile/modules/category/category_controller.dart';
import 'package:mobile/repositories/category_repository.dart';

class CategoryBinding extends Bindings {

  @override
  void dependencies() {
    Get.lazyPut<CategoryRepository>(() => CategoryRepository());
    Get.lazyPut<CategoryController>(() => CategoryController(repository: Get.find()));
  }

}