import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mobile/modules/category/category_controller.dart';
import 'package:mobile/routes/app_pages.dart';
import 'package:mobile/shared/models/category_model.dart';
import 'package:mobile/shared/widgets/network_image.dart';
import 'package:mobile/shared/widgets/product_item_widget.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class CategoryView extends GetView<CategoryController> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SmartRefresher(
        enablePullDown: true,
        enablePullUp: true,
        header: WaterDropHeader(),
        footer: CustomFooter(
          builder: (BuildContext context, LoadStatus? mode){
            Widget body ;
            if(mode==LoadStatus.idle){
              body =  Text("pull up load");
            }
            else if(mode==LoadStatus.loading){
              body =  CupertinoActivityIndicator();
            }
            else if(mode == LoadStatus.failed){
              body = Text("Load Failed!Click retry!");
            }
            else if(mode == LoadStatus.canLoading){
              body = Text("release to load more");
            }
            else{
              body = Text("No more Data");
            }
            return Container(
              height: 55.0,
              child: Center(child:body),
            );
          },
        ),
        controller: controller.refreshController,
        onRefresh: controller.onRefresh,
        onLoading: controller.onLoading,
        child: GetBuilder<CategoryController>(builder: (controller) {
          return Container(
            child: Row(children: [
              Container(
                width: 50.0,
                child: Container(
                  decoration: BoxDecoration(
                    border: Border(
                      right: BorderSide(
                        color: Colors.black,
                        width: 0.1,
                      ),
                    ),
                  ),
                  child: _buildParent(context),
                ),
              ),
              Expanded(
                child: _buildChildren(context),
              ),
            ],),
          );
        }),
      ),
    );
  }

  Widget _buildParent(final BuildContext context) {
    return Container(
        padding: const EdgeInsets.symmetric(horizontal: 3.0),
        child: ListView.separated(
          separatorBuilder: (context, index) {
            return Divider();
          },
          scrollDirection: Axis.vertical,
          itemCount: controller.parents.length,
          itemBuilder: (context, index) {
            final item = controller.parents[index];
            return GestureDetector(
              onTap: () async {
                await controller.onChangeParent(context, item.id);
              },
              child: _buildCategory(item),
            );
          },
        ));
  }
  
  Widget _buildCategory(final CategoryModel item) {
    return Container(
      width: 60,
      padding: EdgeInsets.symmetric(horizontal: 4, vertical: 8),
      child: Wrap(
        alignment: WrapAlignment.center,
        children: <Widget>[
          Container(
            height: 35,
            child: CacheNetworkImageWidget(url: item.imageUrl),
          ),
          SizedBox(height: 5.0),
          Text(
            '${item.name}',
            maxLines: 2,
            overflow: TextOverflow.ellipsis,
            style: TextStyle(fontSize: 11.0),
            textAlign: TextAlign.center,
          ),
        ],
      ),
    );
  }

  Widget _buildChildren(final BuildContext context) {
    return ListView(
      children: [
        if(controller.children.isNotEmpty)
          Container(
            margin: EdgeInsets.symmetric(vertical: 0.0),
            padding: EdgeInsets.symmetric(horizontal: 5.0),
            height: 60.0,
            child:  ListView.builder(
              scrollDirection: Axis.horizontal,
              itemBuilder: (ctx, index) {
                final item = controller.children[index];
                return Container(
                  child: GestureDetector(
                    onTap: () async {
                      await controller.onChangeChild(context, item.id);
                    },
                    child: _buildCategory(item),
                  ),
                );
              },
              itemCount: controller.children.length,
            ),
          ),

        GridView.builder(
            physics: NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            itemCount: controller.products.length,
            padding: EdgeInsets.symmetric(horizontal: 5.0, vertical: 5.0),
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 2,
              crossAxisSpacing: 5,
              mainAxisSpacing: 10,
              childAspectRatio: 0.6,
            ),
            itemBuilder: (BuildContext context, int index) {
              final item = controller.products[index];
              return Container(
                child: GestureDetector(
                    onTap: () {
                      Get.rootDelegate.toNamed(Routes.productDetail(productId: '${item.id}'));
                    },
                    child: ProductItem(product: item, height: 400, imageHeight: 160,)
                ),
              );
            }
        )
      ],
    );
  }
}
