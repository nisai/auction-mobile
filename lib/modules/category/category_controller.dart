import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:mobile/repositories/category_repository.dart';
import 'package:mobile/shared/models/category_model.dart';
import 'package:mobile/shared/models/product_model.dart';
import 'package:mobile/utils/customLoader.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class CategoryController extends GetxController {

  final refreshController = RefreshController();

  var parents = <CategoryModel>[].obs;
  var products = <ProductModel>[].obs;
  var children = <CategoryModel>[].obs;

  void onRefresh() async{
    // monitor network fetch
    await Future.delayed(Duration(milliseconds: 1000));
    // if failed,use refreshFailed()
    print('completed refresh');
    refreshController.refreshCompleted();
  }

  void onLoading() async {
    print('doing load');
    // monitor network fetch
    await Future.delayed(Duration(milliseconds: 1000));
    // if failed,use loadFailed(),if no data return,use LoadNodata()
    update();
    refreshController.loadComplete();
  }

  final CategoryRepository repository;

  CategoryController({required this.repository});
  @override
  void onInit() {
    super.onInit();

    // load parent category
    _firstLoad().then((_) {
      print('parent: loaded ');
      update();
    });

    _loadAllChild().then((_) {
      print('_loadAllChild: category');
      update();
    });

    _loadAllProduct().then((value) {
      print('_loadAllProduct: category');
      update();
    });

  }

  Future<void> _firstLoad() async{
    final result = await repository.getParents();
    if (result != null) {
      final _parents = (result['data']['Categories']['select'] as List).cast<Map<String, dynamic>>()
          .map(CategoryModel.fromJson)
          .toList();

      if(_parents.isNotEmpty) {
        _parents.insert(0, CategoryModel(id: null, name: 'All', image: '')); // at index 0 we are adding A
        parents(_parents);
      }
    }
  }

  Future<void> _loadAllChild() async {
    try {
      final result = await repository.getAllChildCategory();
      if(result != null) {
        final _child = (result['data']['Categories']['select'] as List).cast<Map<String, dynamic>>()
            .map(CategoryModel.fromJson)
            .toList();
        if(_child.isNotEmpty) {
          children(_child);
        }
      }
    } catch(e) {

    }
  }

  Future<void> _loadAllProduct() async {
    final result = await repository.getLatestProduct();
    print('result: ${result.toString()}');
    if (result != null) {
      final _products = (result['data']['Products']['select'] as List).cast<Map<String, dynamic>>()
          .map(ProductModel.fromJson)
          .toList();
      products(_products);
      update();
    }
  }

  Future<void> onChangeChild(final BuildContext context, final int? childId) async {
    try {
      CustomLoader.instance.showLoader(context);
      final _listProduct = await repository.getProductByCategoryId([childId]);
      print('result: ${_listProduct.toString()}');
      products(_listProduct);
      update();
    } catch(e) {
      print('onChangeChild: error $e');
    } finally {
      CustomLoader.instance.hideLoader();
    }
  }

  Future<void> onChangeParent(final BuildContext context, final int? parentId) async {
    try {
      CustomLoader.instance.showLoader(context);
      final int categoryId = parentId?? 0;
      print('parent id : $categoryId');

      if(categoryId > 0) {
        final result = await repository.getChild(parentId: parentId);
        print('result: ${result.toString()}');
        if (result != null) {
          final _children = (result['data']['Categories']['select'] as List).cast<Map<String, dynamic>>()
              .map(CategoryModel.fromJson)
              .toList();
          print('has children: $_children');
          children(_children);
        }
        final List<CategoryModel>? _childIds = await repository.getChildrenByParent(parentId);

        final List<int?>? queryIds = _childIds?.map((e) => e.id).toList();
        final _listProduct = await repository.getProductByCategoryId([categoryId]..addAll(queryIds?? []));
        print('result: ${_listProduct.toString()}');
        products(_listProduct);
      } else {
        await _loadAllChild();
      }

      update();

    } catch(e) {
      print('onChangeParent error $e');
    } finally {
      CustomLoader.instance.hideLoader();
    }
  }

  @override
  void onClose() {
    CustomLoader.instance.dispose();
    super.onClose();
  }
}