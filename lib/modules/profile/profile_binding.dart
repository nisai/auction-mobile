import 'package:get/get.dart';
import 'package:mobile/core/upload_service.dart';
import 'package:mobile/modules/profile/profile_controller.dart';

class ProfileBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => UploadService());
    Get.lazyPut(() => ProfileController());
  }
}
