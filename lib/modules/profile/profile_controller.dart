import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:mobile/core/upload_service.dart';
import 'package:mobile/repositories/user_repository.dart';
import 'package:mobile/shared/logger/logger_utils.dart';
import 'package:mobile/shared/models/user_model.dart';
import 'package:mobile/utils/customLoader.dart';

class ProfileController extends GetxController {

  final _picker = ImagePicker();
  final _loader = CustomLoader();

  var loading = false.obs;

  final _uploadService = Get.find<UploadService>();
  final _userRepository = Get.find<UserRepository>();

  @override
  void onInit() {

    super.onInit();
  }

  Future<void> handleChangeProfile(final BuildContext context) async {
    try {
      final XFile? result = await _picker.pickImage(source: ImageSource.gallery);
      if(result?.path != null)
      {
        loading(true);
        _loader.showLoader(context);
        final path = await _uploadService.uploadFile(imagePath: result?.path);
        final UserModel? user = await _userRepository.getCurrentUser();
        print('user: $user');
        final _data = user?.toMap();
        _data?['profileImage'] = path;
        await _userRepository.update(data: _data);
        loading(false);
        _loader.hideLoader();
        update();
      }
    } catch(e) {
      print('handleChangeProfile error $e');
    }
  }

  Future<bool?> handleUpdate(final BuildContext context, {Map<String, dynamic>? data}) async {
    try {
      _loader.showLoader(context);
      print('change input: $data');
      await _userRepository.update(data: data);
      _loader.hideLoader();
      update();
    } catch(e) {
      print('handleUpdate err $e');
    }
    return null;
  }

  Future<UserModel?> getUserInformation() async {
    try {
      return _userRepository.getCurrentUser();
    } catch(e) {
      logger.i('error $e');
    }
    return null;
  }

  @override
  void onClose() {
    _loader.dispose();
    super.onClose();
  }
}
