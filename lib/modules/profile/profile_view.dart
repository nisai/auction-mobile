import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:get/get.dart';
import 'package:mobile/modules/profile/profile_controller.dart';
import 'package:mobile/routes/app_pages.dart';
import 'package:mobile/shared/models/user_model.dart';
import 'package:mobile/shared/widgets/simple_loading.dart';
import 'package:form_builder_validators/form_builder_validators.dart';

class ProfileView extends GetView<ProfileController> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(
              'profile'.tr
          ),
          leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () {
              Get.rootDelegate.offNamed(Routes.OTHERS);
            },
          ),
        ),
        body: GetBuilder<ProfileController>(builder: (controller) {
          return FutureBuilder<UserModel?>(
              future: controller.getUserInformation(),
              builder: (BuildContext context, AsyncSnapshot<UserModel?> snapshot) {
                if(snapshot.connectionState == ConnectionState.done) {
                  final UserModel? user = snapshot.data;
                  if(user != null) {
                    final formKey = GlobalKey<FormBuilderState>();
                    return SingleChildScrollView(
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: FormBuilder(
                          key: formKey,
                          autovalidateMode: AutovalidateMode.onUserInteraction,
                          child: Column(
                            children: <Widget>[
                              SizedBox(
                                  height: 200,
                                  child: Center(child:
                                  Stack(children: [
                                    Container(
                                      width: 200.0,
                                      height: 200.0,
                                      child: CircleAvatar(
                                        backgroundImage: NetworkImage(user.profileUrl),
                                      ),
                                    ),
                                    Positioned(
                                      bottom: 20,
                                      right: 0,
                                      left: 0,
                                      child: GestureDetector(
                                        onTap: () async {
                                          await controller.handleChangeProfile(context);
                                        },
                                        child: Icon(Icons.camera_enhance, color: Colors.black),
                                      ),
                                    ),
                                  ]),
                                  )
                              ),
                              SizedBox(height: 10.0),

                              FormBuilderTextField(
                                name: 'firstName',
                                initialValue: user.firstName?? null,
                                keyboardType: TextInputType.text,
                                decoration: new InputDecoration(
                                  fillColor: Colors.white,
                                  border: OutlineInputBorder(
                                    borderSide: BorderSide(
                                        color: Colors.black,
                                        width: 5.0),
                                  ),
                                  focusedBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.all(Radius.circular(5.0)),
                                      borderSide: BorderSide(color: Colors.green)),
                                  filled: true,
                                  contentPadding:
                                  EdgeInsets.only(bottom: 10.0, left: 10.0, right: 10.0),
                                  labelText: 'firstname'.tr,
                                ),
                                validator: FormBuilderValidators.compose(
                                    [FormBuilderValidators.required(context)]),
                              ),
                              SizedBox(
                                height: 10.0,
                              ),

                              FormBuilderTextField(
                                name: 'lastName',
                                initialValue: user.lastName?? null,
                                keyboardType: TextInputType.text,
                                decoration: new InputDecoration(
                                  fillColor: Colors.white,
                                  border: OutlineInputBorder(
                                    borderSide: BorderSide(
                                        color: Colors.black,
                                        width: 5.0),
                                  ),
                                  focusedBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.all(Radius.circular(5.0)),
                                      borderSide: BorderSide(color: Colors.green)),
                                  filled: true,
                                  contentPadding:
                                  EdgeInsets.only(bottom: 10.0, left: 10.0, right: 10.0),
                                  labelText: 'lastname'.tr,
                                ),
                                validator: FormBuilderValidators.compose(
                                    [FormBuilderValidators.required(context)]),
                              ),
                              SizedBox(
                                height: 10.0,
                              ),

                              FormBuilderTextField(
                                name: 'userName',
                                initialValue: user.userName?? null,
                                keyboardType: TextInputType.text,
                                decoration: new InputDecoration(
                                  fillColor: Colors.white,
                                  border: OutlineInputBorder(
                                    borderSide: BorderSide(
                                        color: Colors.black,
                                        width: 5.0),
                                  ),
                                  focusedBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.all(Radius.circular(5.0)),
                                      borderSide: BorderSide(color: Colors.green)),
                                  filled: true,
                                  contentPadding:
                                  EdgeInsets.only(bottom: 10.0, left: 10.0, right: 10.0),
                                  labelText: 'username'.tr,
                                ),
                                validator: (String? value) {
                                  if (value == null || value.isEmpty) {
                                    return "Please enter a valid username";
                                  }
                                  return GetUtils.isUsername(value)
                                      ? null
                                      : "Please enter a valid username";
                                },
                              ),

                              SizedBox(height: 10.0),

                              FormBuilderTextField(
                                name: 'email',
                                initialValue: user.email?? null,
                                keyboardType: TextInputType.emailAddress,
                                decoration: new InputDecoration(
                                  fillColor: Colors.white,
                                  border: OutlineInputBorder(
                                    borderSide: BorderSide(
                                        color: Colors.black,
                                        width: 5.0),
                                  ),
                                  focusedBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.all(Radius.circular(5.0)),
                                      borderSide: BorderSide(color: Colors.green)),
                                  filled: true,
                                  contentPadding:
                                  EdgeInsets.only(bottom: 10.0, left: 10.0, right: 10.0),
                                  labelText: 'email'.tr,
                                ),
                                validator: FormBuilderValidators.compose(
                                    [FormBuilderValidators.required(context), FormBuilderValidators.email(context)]),
                              ),
                              SizedBox(
                                height: 15.0,
                              ),
                              FormBuilderTextField(
                                name: 'mobile',
                                initialValue: user.mobile?? null,
                                keyboardType: TextInputType.phone,
                                cursorColor: Colors.black,
                                decoration: new InputDecoration(
                                  fillColor: Colors.white,
                                  border: OutlineInputBorder(
                                    borderSide: BorderSide(
                                        color: Colors.black,
                                        width: 5.0),
                                  ),
                                  focusedBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.all(Radius.circular(5.0)),
                                      borderSide: BorderSide(color: Colors.green)),
                                  filled: true,
                                  contentPadding:
                                  EdgeInsets.only(bottom: 10.0, left: 10.0, right: 10.0),
                                  labelText: 'phone'.tr,
                                ),
                                // onChanged: _onChanged,
                                validator: FormBuilderValidators.compose(
                                    [FormBuilderValidators.required(context)]),
                              ),
                              SizedBox(
                                height: 15.0,
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Container(
                                    child: MaterialButton(
                                      color: Color(0xfff7892b),
                                      child: Text(
                                        'update'.tr,
                                        style: TextStyle(color: Colors.white),
                                      ),
                                      onPressed: () async {
                                        final bool valid =  formKey.currentState?.saveAndValidate()?? false;
                                        if(valid) {
                                          await controller.handleUpdate(context, data: formKey.currentState?.value);
                                        }
                                      },
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ),
                    );
                  }
                }
                return SimpleLoading();
              }
          );
        })
    );
  }
}
