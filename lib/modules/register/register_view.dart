import 'package:flare_flutter/flare_actor.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:mobile/modules/register/register_controller.dart';
import 'package:mobile/routes/app_pages.dart';
import 'package:mobile/shared/widgets/language_wiget.dart';
import 'package:pin_code_fields/pin_code_fields.dart';

class RegisterView extends GetView<RegisterController> {

  @override
  Widget build(BuildContext context) {
    return GetBuilder<RegisterController>(builder: (controller) {
      return Scaffold(
        appBar: AppBar(
          title: Text(
              'register_now'.tr
          ),
          leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () {
              Get.rootDelegate.popRoute(popMode: PopMode.History);
            },
          ),
          actions: [
            LanguageWidget(),
          ],
        ),
        body: SingleChildScrollView(
          child: controller.registerCompleted? _buildOTP(context): _buildRegisterForm(context),
        ),
      );
    });
  }

  Widget _buildOTP(final BuildContext context) {
    return Container(
      color: Colors.blue.shade50,
      child: GestureDetector(
        onTap: () {
        },
        child: Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          child: ListView(
            children: <Widget>[
              SizedBox(height: 30),
              Container(
                height: MediaQuery.of(context).size.height / 3,
                child: FlareActor(
                  "assets/otp/otp.flr",
                  animation: "otp",
                  fit: BoxFit.fitHeight,
                  alignment: Alignment.center,
                ),
              ),
              SizedBox(height: 8),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 8.0),
                child: Text(
                  'Phone Number Verification',
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 22),
                  textAlign: TextAlign.center,
                ),
              ),
              Padding(
                padding:
                const EdgeInsets.symmetric(horizontal: 30.0, vertical: 8),
                child: RichText(
                  text: TextSpan(
                      text: "Enter the code sent to ",
                      children: [
                        TextSpan(
                            text: controller.fullPhoneNumber,
                            style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.bold,
                                fontSize: 15)),
                      ],
                      style: TextStyle(color: Colors.black54, fontSize: 15)),
                  textAlign: TextAlign.center,
                ),
              ),
              SizedBox(
                height: 20,
              ),
              Form(
                key: controller.registerVerificationFormKey,
                child: Padding(
                    padding: const EdgeInsets.symmetric(vertical: 8.0, horizontal: 30),
                    child: PinCodeTextField(
                      autoDisposeControllers: false,
                      appContext: context,
                      pastedTextStyle: TextStyle(
                        color: Colors.green.shade600,
                        fontWeight: FontWeight.bold,
                      ),
                      length: 6,
                      obscureText: true,
                      obscuringCharacter: '*',
                      blinkWhenObscuring: true,
                      animationType: AnimationType.fade,
                      validator: (String? v) {
                        if (v?.length != 6) {
                          return "Use codes we sent you. It's should be 6 numbers";
                        }
                        return null;
                      },
                      pinTheme: PinTheme(
                        shape: PinCodeFieldShape.box,
                        borderRadius: BorderRadius.circular(5),
                        fieldHeight: 50,
                        fieldWidth: 40,
                        activeFillColor:  Colors.orange,
                        // _.smsOtpt..hasError ? Colors.orange : Colors.white,
                      ),
                      cursorColor: Colors.black,
                      animationDuration: Duration(milliseconds: 300),
                      backgroundColor: Colors.blue.shade50,
                      enableActiveFill: true,
                      //  errorAnimationController: errorController,
                      controller: controller.smsCodeController.value,
                      keyboardType: TextInputType.number,
                      boxShadows: [
                        BoxShadow(
                          offset: Offset(0, 1),
                          color: Colors.black12,
                          blurRadius: 10,
                        )
                      ],
                      onCompleted: (v) {
                        controller.completedOtp(true);
                        print("Completed");
                      },
                      // onTap: () {
                      //   print("Pressed");
                      // },
                      onChanged: (value) {
                        print(value);

                      },
                      beforeTextPaste: (text) {
                        print("Allowing to paste $text");
                        //if you return true then it will show the paste confirmation dialog. Otherwise if false, then nothing will happen.
                        //but you can show anything you want here, like your pop up saying wrong paste format or etc
                        return true;
                      },
                    )),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 30.0),
                child: Text("*Please fill up all the cells properly",
                  style: TextStyle(
                      color: Colors.red,
                      fontSize: 12,
                      fontWeight: FontWeight.w400),
                ),
              ),
              SizedBox(
                height: 20,
              ),
              Row(
                children: [
                  RichText(
                    textAlign: TextAlign.center,
                    text: TextSpan(
                        text: "Didn't receive the code?",
                        style: TextStyle(color: Colors.black54, fontSize: 15)),
                  ),
                  GetX<RegisterController>(builder: (controller) {
                    print('rebuild ${controller.allowSmsDuration}');
                    if(controller.allowSmsDuration == 0) {
                      return Container(
                        child: TextButton(
                          onPressed: () async {
                            await controller.handleResent(context);
                          },
                          child: RichText(
                            text: TextSpan(
                                text: " RESEND",
                                style: TextStyle(
                                    color: Color(0xFF91D3B3),
                                    fontWeight: FontWeight.bold,
                                    fontSize: 16)),
                          ),
                        ),
                      );
                    }
                    return Container(
                      child: Text("wait ${controller.allowSmsDuration} seconds"),
                    );
                  }),
                ],
              ),
              SizedBox(
                height: 14,
              ),
              GestureDetector(
                onTap: () async {
                  if(controller.validOTP) {
                    await controller.handleVerifySMS(context);
                  }
                },
                child: Container(
                  padding: EdgeInsets.symmetric(vertical: 15),
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(5)),
                      boxShadow: <BoxShadow>[
                        BoxShadow(
                            color: Colors.grey.shade200,
                            offset: Offset(2, 4),
                            blurRadius: 5,
                            spreadRadius: 2)
                      ],
                      gradient: LinearGradient(
                          begin: Alignment.centerLeft,
                          end: Alignment.centerRight,
                          colors: [Color(0xfffbb448), Color(0xfff7892b)])),
                  child: Text(
                    'Verify',
                    style: TextStyle(fontSize: 20, color: controller.validOTP? Colors.white: Colors.grey),
                  ),
                ),
              ),
            ],
          ),
        ),
      )
    );
  }

  Widget _buildRegisterForm(final BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20),
      height: MediaQuery.of(context).size.height,
      decoration: BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(5)),
          boxShadow: <BoxShadow>[
            BoxShadow(
                color: Colors.grey.shade200,
                offset: Offset(2, 4),
                blurRadius: 5,
                spreadRadius: 2)
          ],
          gradient: LinearGradient(
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
              colors: [Color(0xfffbb448), Color(0xffe46b10)])),
      child: Form(
        key: controller.registerFormKey,
        child: ListView(
          children: <Widget>[
            Container(
              padding: EdgeInsets.only(bottom: 10.0),
              child: RichText(
                textAlign: TextAlign.center,
                text: TextSpan(
                    text: 'register_intro'.tr,
                    style: GoogleFonts.portLligatSans(
                      //textStyle: Theme.of(context).textTheme.display1,
                      fontSize: 16,
                      fontWeight: FontWeight.w700,
                      color: Colors.white,
                    )),
              ),
            ),
            Container(
              child: Image.asset('assets/images/logo2.png'),
            ),
            Container(
              margin: EdgeInsets.symmetric(vertical: 5),
              decoration: BoxDecoration(
                  border: Border.all(color: Colors.white),
                  borderRadius:
                  BorderRadius.all(Radius.circular(10))),
              child: TextFormField(
                controller: controller.firstNameController.value,
                style: TextStyle(fontSize: 12, color: Colors.white),
                decoration: InputDecoration(
                    contentPadding:
                    EdgeInsets.symmetric(horizontal: 20),
                    hintText: 'first_name'.tr,
                    border: InputBorder.none,
                    filled: true,
                    hintStyle: TextStyle(color: Colors.white)
                ),
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'Please enter your first name';
                  }
                  return null;
                },
              ),
            ),

            Container(
              margin: EdgeInsets.symmetric(vertical: 5),
              decoration: BoxDecoration(
                  border: Border.all(color: Colors.white),
                  borderRadius:
                  BorderRadius.all(Radius.circular(10))),
              child: TextFormField(
                controller: controller.lastNameController.value,
                style: TextStyle(fontSize: 12, color: Colors.white),
                decoration: InputDecoration(
                    contentPadding:
                    EdgeInsets.symmetric(horizontal: 20),
                    hintText: 'last_name'.tr,
                    border: InputBorder.none,
                    filled: true,
                    hintStyle: TextStyle(color: Colors.white)
                ),
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'Please enter your last name';
                  }
                  return null;
                },
              ),
            ),

            Container(
              margin: EdgeInsets.symmetric(vertical: 5),
              decoration: BoxDecoration(
                  border: Border.all(color: Colors.white),
                  borderRadius:
                  BorderRadius.all(Radius.circular(10))),
              child: TextFormField(
                controller: controller.phoneController.value,
                keyboardType: TextInputType.phone,
                style: TextStyle(fontSize: 12, color: Colors.white),
                decoration: InputDecoration(
                    contentPadding:
                    EdgeInsets.symmetric(horizontal: 20),
                    hintText: 'phone'.tr,
                    border: InputBorder.none,
                    filled: true,
                    hintStyle: TextStyle(color: Colors.white)
                ),
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return "Please enter a valid phone number";
                  }
                  return null;
                },
              ),
            ),

            Container(
              margin: EdgeInsets.symmetric(vertical: 5),
              decoration: BoxDecoration(
                  border: Border.all(color: Colors.white),
                  borderRadius:
                  BorderRadius.all(Radius.circular(10))),
              child: TextFormField(
                controller: controller.emailController.value,
                style: TextStyle(fontSize: 12, color: Colors.white),
                keyboardType: TextInputType.emailAddress,
                decoration: InputDecoration(
                    contentPadding:
                    EdgeInsets.symmetric(horizontal: 20),
                    hintText: 'email'.tr,
                    border: InputBorder.none,
                    filled: true,
                    hintStyle: TextStyle(color: Colors.white)
                ),
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return "Please enter a valid email";
                  }
                  return GetUtils.isEmail(value)
                      ? null
                      : "Please enter a valid email";
                },
              ),
            ),

            Container(
              margin: EdgeInsets.symmetric(vertical: 5),
              decoration: BoxDecoration(
                  border: Border.all(color: Colors.white),
                  borderRadius:
                  BorderRadius.all(Radius.circular(10))),
              child: TextFormField(
                controller: controller.userNameController.value,
                style: TextStyle(fontSize: 12, color: Colors.white),
                keyboardType: TextInputType.emailAddress,
                decoration: InputDecoration(
                    contentPadding:
                    EdgeInsets.symmetric(horizontal: 20),
                    hintText: 'username'.tr,
                    border: InputBorder.none,
                    filled: true,
                    hintStyle: TextStyle(color: Colors.white)
                ),
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return "Please enter a valid username";
                  }
                  return GetUtils.isUsername(value)
                      ? null
                      : "Please enter a valid username";
                },
              ),
            ),

            Container(
              margin: EdgeInsets.symmetric(vertical: 5),
              decoration: BoxDecoration(
                  border: Border.all(color: Colors.white),
                  borderRadius:
                  BorderRadius.all(Radius.circular(10))),
              child: TextFormField(
                controller: controller.passwordController.value,
                obscureText: true,
                style: TextStyle(fontSize: 12, color: Colors.white),
                decoration: InputDecoration(
                    contentPadding:
                    EdgeInsets.symmetric(horizontal: 20),
                    hintText: 'your_password'.tr,
                    border: InputBorder.none,
                    filled: true,
                    hintStyle: TextStyle(color: Colors.white)
                ),
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'Please enter your password';
                  }
                  return null;
                },
              ),
            ),

            Container(
              margin: EdgeInsets.symmetric(vertical: 5),
              decoration: BoxDecoration(
                  border: Border.all(color: Colors.white),
                  borderRadius:
                  BorderRadius.all(Radius.circular(10))),
              child: TextFormField(
                controller: controller.confirmationPasswordController.value,
                obscureText: true,
                style: TextStyle(fontSize: 12, color: Colors.white),
                decoration: InputDecoration(
                    contentPadding:
                    EdgeInsets.symmetric(horizontal: 20),
                    hintText: 'password_confirmation'.tr,
                    border: InputBorder.none,
                    filled: true,
                    hintStyle: TextStyle(color: Colors.white)
                ),
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'Please enter your password confirm';
                  }
                  if(value != controller.password) {
                    return 'Please enter password confirm not match';
                  }
                  return null;
                },
              ),
            ),

            Container(
              margin: EdgeInsets.symmetric(vertical: 5),

              decoration: BoxDecoration(
                  border: Border.all(color: Colors.white),
                  borderRadius:
                  BorderRadius.all(Radius.circular(10))),
              child: FormBuilderDropdown<int?>(
                initialValue: controller.postProvince.value > 0? controller.postProvince.value: null,
                onChanged: (valueId) {
                  controller.onChangeProvince(valueId);
                },
                name: 'provinceId',
                decoration: InputDecoration(
                    contentPadding: EdgeInsets.symmetric(horizontal: 20),
                    hintText: 'province'.tr,
                    border: InputBorder.none,
                    filled: true,
                    hintStyle: TextStyle(color: Colors.white)
                ),
                validator: FormBuilderValidators.compose(
                    [FormBuilderValidators.required(context)]),
                items: controller.provinces
                    .map((item) => item)
                    .map((item) => DropdownMenuItem(
                    value: item.id,
                    child: Text("${item.name}",)))
                    .toList(),
              ),
            ),

            SizedBox(
              height: 10,
            ),

            GestureDetector(
              onTap: () async {
                final bool valid = controller.registerFormKey.currentState!.validate();
                if (valid) {
                  await controller.handleRegister(context);
                }
              },
              child: Container(
                padding: EdgeInsets.symmetric(vertical: 10),
                alignment: Alignment.center,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(5)),
                    boxShadow: <BoxShadow>[
                      BoxShadow(
                          color: Color(0xffdf8e33).withAlpha(100),
                          offset: Offset(2, 4),
                          blurRadius: 8,
                          spreadRadius: 2)
                    ],
                    color: Colors.white),
                child: Text(
                  'register_now'.tr,
                  style: TextStyle(fontSize: 12, color: Color(0xfff7892b)),
                ),
              ),
            ),

            SizedBox(
              height: 5,
            ),
            Container(
              padding: EdgeInsets.all(15),
              alignment: Alignment.bottomCenter,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                crossAxisAlignment: CrossAxisAlignment.end,
                children: <Widget>[
                  Text(
                    'you_have_account'.tr,
                    style: TextStyle(fontSize: 12, color: Colors.white),
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  GestureDetector(
                    onTap: () {
                      Get.rootDelegate.offNamed(Routes.LOGIN);
                    },
                    child: Text(
                      'login'.tr,
                      style: TextStyle(fontSize: 12, color: Colors.white, fontWeight: FontWeight.bold),
                    ),
                  )
                ],
              ),
            ),
            SizedBox(
              height: 50,
            ),
          ],
        ),
      ),
    );
  }

}
