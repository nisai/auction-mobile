import 'package:get/get.dart';
import 'package:mobile/modules/register/register_controller.dart';
import 'package:mobile/repositories/province_repository.dart';

class RegisterBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => ProvinceRepository());
    Get.lazyPut(() => RegisterController());
  }

}