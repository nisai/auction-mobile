import 'dart:async';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:mobile/core/local_storage.dart';
import 'package:mobile/repositories/province_repository.dart';
import 'package:mobile/repositories/user_repository.dart';
import 'package:mobile/routes/app_pages.dart';
import 'package:mobile/shared/models/access_token_model.dart';
import 'package:mobile/shared/models/province_model.dart';
import 'package:mobile/shared/models/user_model.dart';
import 'package:mobile/utils/constant.dart';
import 'package:mobile/utils/customLoader.dart';
import 'package:mobile/utils/utility.dart';

class RegisterController extends GetxController {

  final  _repository = Get.find<UserRepository>();
  final _provinceRepository = Get.find<ProvinceRepository>();

  var provinces = <ProvinceModel>[].obs;

  final registerVerificationFormKey = GlobalKey<FormState>();
  final registerFormKey = GlobalKey<FormState>();

  final _loader = CustomLoader();

  var firstNameController = TextEditingController().obs;
  var lastNameController = TextEditingController().obs;
  var emailController = TextEditingController().obs;
  var phoneController = TextEditingController().obs;
  var userNameController = TextEditingController().obs;
  var passwordController = TextEditingController().obs;
  var confirmationPasswordController = TextEditingController().obs;
  var smsCodeController = TextEditingController().obs;

  String get firstName => firstNameController.value.text;
  String get lastName => lastNameController.value.text;
  String get phoneNumber => phoneController.value.text;
  String get password => passwordController.value.text;
  String get userName => userNameController.value.text;
  String get email => emailController.value.text;

  var postProvince = 0.obs;

  void onChangeProvince(final int? val) {
    postProvince(val);
    update();
  }

  String get smsCode => smsCodeController.value.text;

  var _registerCompleted = false.obs;
  bool get registerCompleted => _registerCompleted.value;


  String get fullPhoneNumber {
    final phone =  '$countryDialCode${phoneNumber.replaceFirst(new RegExp(r'^0+'), '')}'.replaceAll(new RegExp(r'^0+(?=.)'), '');
    print('fullPhoneNumber is : $phone');
    return phone;
  }

  var loginOrLogout = false.obs;

  static RegisterController get to => Get.find<RegisterController>();


  var _accessToken = AccessTokenModel().obs;
  AccessTokenModel? get accessToken => _accessToken.value;


  final LocalStorage _storage = Get.find<LocalStorage>();

  bool get isLogin => accessToken != null;


  var _countryDialCode = '+855'.obs;
  String get countryDialCode => _countryDialCode.value;

  var _verificationId = ''.obs;
  String get verificationId => _verificationId.value;

  var _codeSent = false.obs;
  bool get codeSent => _codeSent.value;

  var _firstSent = true.obs;

  bool get firstSent => _firstSent.value;

  var _allowSmsDuration = 0.obs;

  var _validOTP = false.obs;
  bool get validOTP => _validOTP.value;


  int get allowSmsDuration => _allowSmsDuration.value;

  // LoginStatus get status => _status;
  var _resentToken = 0.obs;
  int get resentToken => _resentToken.value;

  final FirebaseAuth _auth = Get.find();

  User? get getUser => _auth.currentUser;

  @override
  void onInit() {
    _resolveExistUser().then((value) {
      update();
    });

    _fetchProvince();

    super.onInit();
  }

  Future<void> _resolveExistUser() async {
    if(Get.arguments != null) {
      User? user = Get.arguments as User;
      print('from existing user ${user.toString()}');
      firstNameController.value.text = user.displayName?.split(' ').first?? '';
      lastNameController.value.text = user.displayName?.split(' ').last?? '';
      userNameController.value.text = user.uid;
      emailController.value.text = user.email?? '';
      phoneController.value.text = user.phoneNumber?? '';
    }
  }

  Future<void> _fetchProvince() async {
     _provinceRepository.getProvinces().then((value) {
       provinces(value);
       update();
     });
  }

  Future<void> handleResent(final BuildContext context) async {
    try {
      _firstSent(false);
      update();
      await _verifyPhoneNumber(
          fullPhoneNumber,
          forceResendingToken: resentToken
      );
    } catch(e) {
      print('handleResent: $e');
    } finally {
      update();
    }
  }

  Future<void> handleRegister(final BuildContext context) async {
    _loader.showLoader(context);

    try {
      final data = {
        "firstName": firstName,
        "lastName": lastName,
        "password": password,
        "userName": userName,
        "mobile": phoneNumber,
        "email": email,
        'provinceId':  postProvince.value,
      };
      print('data: ${data}');

      final Map<String, dynamic>? result = await _repository.register(data: data);
      if(result != null) {
        if(result['error'] != true && result['data'] != null) {
          _registerCompleted(true);
          await _sendContinue();
          update();
        } else {
          final String message = result['message'];
          Get.snackbar('Your register account unsuccessfully', message, snackPosition: SnackPosition.BOTTOM);
        }

      } else {
        Get.snackbar('Your register account unsuccessfully', 'Please use your real username and password to reset_password', snackPosition: SnackPosition.BOTTOM);
      }
    } catch(e) {
      Get.snackbar('Your register account unsuccessfully', 'Please use your real username and password to reset_password', snackPosition: SnackPosition.BOTTOM);
      print('controller: $e');

      print('error register : $e');
    } finally {
      _loader.hideLoader();
    }
  }

  Future<void> _onCodeAutoRetrievalTimeout(String verificationId) async {
    _verificationId(verificationId);
    print('onCodeAutoRetrievalTimeout $verificationId');
    update();
  }

  Future<void> _onCodeSent(String? verificationId, int? resentToken) async {
    print('onCodeSent $verificationId , resentToken: $resentToken');
    try {
      _verificationId(verificationId);
      _resentToken(resentToken);
      // duration must above sent code variable
      _allowSmsDuration(30);
      _codeSent(true);
      _resolveWaitingTimeOut();
      update();
    } catch(e) {

    } finally {
      _loader.hideLoader();
    }
  }

  void changeCountryCode(final String? dialCode) {
    _countryDialCode(dialCode);
    update();
  }

  void completedOtp(final bool completed)  {
    _validOTP(true);
    update();
  }

  Future<void> _sendContinue() async {
    try{

      _validOTP(false);
      _firstSent(true);
      update();

      await _verifyPhoneNumber(
          fullPhoneNumber,
          forceResendingToken: resentToken
      );
    } catch(e) {
      print('sendContinue error $e');
    } finally {
      update();
    }
  }

  Future<void> handleVerifySMS(final BuildContext context) async {
    _loader.showLoader(context);
    await _verifySmsCode(verificationId: verificationId, smsCode: smsCode);
  }


  Future<void> _verifyPhoneNumber(final String p, {required int forceResendingToken}) async {
    return  _auth.verifyPhoneNumber(
      phoneNumber: p,
      verificationCompleted: _onVerificationCompleted,
      verificationFailed: (FirebaseAuthException e) {
        print('_verifyPhoneNumber => verificationFailed $e');
      },
      codeSent: _onCodeSent,
      forceResendingToken: forceResendingToken,
      codeAutoRetrievalTimeout: _onCodeAutoRetrievalTimeout,
    );
  }


  Future<void> _verifySmsCode({required String verificationId, required String smsCode}) async {
    final PhoneAuthCredential credential = PhoneAuthProvider.credential(
        verificationId: verificationId,
        smsCode: smsCode
    );
    _onVerificationCompleted(credential);
  }

  Future<void> _onVerificationCompleted(PhoneAuthCredential credential) async {

    try {
      UserCredential userCredential = await _auth.signInWithCredential(credential);
      print('complete verify phone ${userCredential.user}');
      if(userCredential.user != null) {

        print('user $phoneNumber and password $password');

        final _token = await _repository.login(username: phoneNumber, password: password);
        if(_token != null && _token.accessToken !=  null) {
          print('user logged ${_token.accessToken}');
          await _storage.write(Constant.accessToken, _token.toMap());
          final UserModel? user = await _repository.getCurrentUser();
          if(user != null) {
            await _storage.write(Constant.currentUser, user.toMap());
          }

          await Utility.handleSendFirebaseMessageTokenIfLoggedIn();

          WidgetsBinding.instance.addPostFrameCallback((_) {
            Get.rootDelegate.offNamed(Routes.HOME);
          });
        }

      }

    } on FirebaseAuthException catch (error){
      print("error : $error");

      update();
    } finally {
    _loader.hideLoader();
    }

  }

  @override
  void dispose() {
    super.dispose();
  }

  void _resolveWaitingTimeOut() {
    print('allowSmsDuration : $allowSmsDuration');
    Timer.periodic(Duration(seconds: 1), (Timer t) {
      if (allowSmsDuration == 0) {
          t.cancel();
          _validOTP(false);
          update();

      } else {
        _allowSmsDuration.value --;
      }
    },
    );
  }

}