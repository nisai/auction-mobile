import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:mobile/modules/other/other_controller.dart';
import 'package:mobile/utils/constant.dart';
import 'package:mobile/routes/app_pages.dart';
import 'package:mobile/shared/models/menu_model.dart';
import 'package:url_launcher/url_launcher.dart';

class OtherView extends GetView<OtherController> {

  final menus = <MenuModel>[

    MenuModel(id: 'profile', name: 'profile'.tr, icon: FontAwesomeIcons.user, onTap: () {
     Get.rootDelegate.offNamed(Routes.PROFILE);
    }),

    MenuModel(id: 'your_account', name: 'your_account'.tr, icon: FontAwesomeIcons.userAlt, onTap: () {
      Get.rootDelegate.offNamed(Routes.USER_ACCOUNT);
    }),

    MenuModel(id: 'change_password', name: 'change_password'.tr, icon: Icons.security, onTap: () {
      Get.rootDelegate.offNamed(Routes.CHANGE_PASSWORD);
    }),

    MenuModel(id: 'language', name: 'language'.tr, icon: Icons.language, onTap: () {
      // is back
      Get.rootDelegate.offNamed(Routes.LANGUAGE, arguments: false);
    }),

    MenuModel(id: 'buying_item', name: 'buying_item'.tr, icon: Icons.shopping_bag, onTap: () {
      Get.rootDelegate.offNamed(Routes.BUYING_ITEM);
    }),

    MenuModel(id: 'selling_item', name: 'selling_item'.tr, icon: Icons.shopping_bag, onTap: () {
      Get.rootDelegate.offNamed(Routes.SELLING_ITEM);
    }),

    MenuModel(id: 'watchlist', name: 'watchlist'.tr, icon: Icons.favorite, onTap: () {
      Get.rootDelegate.toNamed(Routes.WISHLIST);
    }),

    MenuModel(id: 'rating', name: 'rating'.tr, icon: Icons.rate_review, onTap: () async {
      final _url = 'https://play.google.com/store/apps/details?id=${Constant.appPackage}';
      await canLaunch(_url) ? await launch(_url) : throw 'Could not launch $_url';
    }),

    MenuModel(id: 'logout', name: 'logout'.tr, icon: FontAwesomeIcons.signOutAlt, onTap: () async {
      await OtherController.to.logout();
      Get.rootDelegate.offNamed(Routes.HOME);
    }),

  ];


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GridView.builder(
          shrinkWrap: true,
          scrollDirection: Axis.vertical,
          padding: EdgeInsets.all(20.0),
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 2,
              crossAxisSpacing: 10,
              mainAxisSpacing: 10,
              childAspectRatio: 2),
          itemCount: menus.length,
          itemBuilder: (BuildContext context, int index) {
            final item = menus[index];
            return GestureDetector(
              onTap: item.onTap,
              child: _buildItem(item),
            );
          }),
    );
  }

  Widget _buildItem(final MenuModel item) {
    return Card(
      child: Container(
        height: 100,
        child: Column(
          children: <Widget>[
            Container(
              child: Icon(item.icon),
            ),
            SizedBox(height: 7.0),
            Container(
              alignment: Alignment.center,
              child: Text(
                '${item.name}',
                style: TextStyle(fontSize: 14),
                maxLines: 1,
                textAlign: TextAlign.center,
                overflow: TextOverflow.ellipsis,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
