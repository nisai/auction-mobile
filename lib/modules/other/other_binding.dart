import 'package:get/get.dart';
import 'package:mobile/modules/other/other_controller.dart';

class OtherBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(
        () => OtherController(),
    );
  }

}