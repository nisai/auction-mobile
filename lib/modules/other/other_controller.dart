import 'package:get/get.dart';
import 'package:mobile/core/local_storage.dart';
import 'package:mobile/utils/constant.dart';

class OtherController extends GetxController {

  static final OtherController to = Get.find<OtherController>();

  final _storage = Get.find<LocalStorage>();

  Future<void> logout() async {
    await _storage.remove(Constant.currentUser);
    await _storage.remove(Constant.accessToken);
  }

}