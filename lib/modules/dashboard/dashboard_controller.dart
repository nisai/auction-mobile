import 'package:get/get.dart';
import 'package:mobile/repositories/product_repository.dart';
import 'package:mobile/shared/models/banner_model.dart';
import 'package:mobile/shared/models/category_model.dart';
import 'package:mobile/shared/models/product_model.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class DashboardController extends GetxController {

  var _page = 1.obs;
  int get page => _page.value;

  var banners = <BannerModel>[].obs;
  var categories = <CategoryModel>[].obs;
  var highLighted = <ProductModel>[].obs;
  var urgent = <ProductModel>[].obs;
  var closingSoon = <ProductModel>[].obs;
  var r1000 = <ProductModel>[].obs;
  var latest = <ProductModel>[].obs;

  final ProductRepository repository;

  DashboardController({required this.repository});

  final RefreshController refreshController = RefreshController(initialRefresh: false);

  @override
  void onInit() {
    _loadFirstPage().then((_) {
      update();
    });

    super.onInit();
  }

  Future<void> onRefresh() async {
    _page(1);
    _loadFirstPage().then((_) {
      update();
      refreshController.refreshCompleted();
    });

  }

  Future<void> onLoading() async {
    try {
      _page.value ++;
      final result = await repository.getMoreLatest(page: page);

      if(result != null) {
        var data = result['data'] as dynamic;
        final _latest =  (data['latest']['select'] as List)
            .cast<Map<String, dynamic>>()
            .map(ProductModel.fromJson)
            .toList();
        latest.addAll(_latest);
      }
      update();
      refreshController.loadComplete();
    } catch(e) {

    }
  }

  Future<void> _loadFirstPage() async {
    final result = await repository.getDashboard();
    if(result != null) {
      var data = result['data'] as dynamic;
      final _banners =
      (data['banners']['select'][0]['values']['data'] as List<dynamic>)
          .cast<Map<String, dynamic>>()
          .map(BannerModel.fromJson)
          .toList();

      final _categories = (data['categories']['select'] as List)
          .cast<Map<String, dynamic>>()
          .map(CategoryModel.fromJson)
          .toList();

      final _highLighted = (data['highLighted']['select'] as List)
          .cast<Map<String, dynamic>>()
          .map(ProductModel.fromJson)
          .toList();

      final _urgent = (data['urgent']['select'] as List)
          .cast<Map<String, dynamic>>()
          .map(ProductModel.fromJson)
          .toList();

      final _closingSoon = (data['closingSoon']['select'] as List)
          .cast<Map<String, dynamic>>()
          .map(ProductModel.fromJson)
          .toList();

      final _r1000 = (data['r1000']['select'] as List)
          .cast<Map<String, dynamic>>()
          .map(ProductModel.fromJson)
          .toList();

      final _latest = (data['latest']['select'] as List)
          .cast<Map<String, dynamic>>()
          .map(ProductModel.fromJson)
          .toList();

      banners(_banners);
      categories(_categories);
      highLighted(_highLighted);
      urgent(_urgent);
      closingSoon(_closingSoon);
      r1000(_r1000);
      latest(_latest);
    }
  }

  @override
  void onClose() {
    refreshController.dispose();

    super.onClose();
  }
}