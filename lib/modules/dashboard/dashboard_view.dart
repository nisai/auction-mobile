import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mobile/modules/dashboard/dashboard_controller.dart';
import 'package:mobile/routes/app_pages.dart';
import 'package:mobile/shared/models/product_label.dart';
import 'package:mobile/shared/models/product_search_filter.dart';
import 'package:mobile/shared/widgets/product_item_widget.dart';
import 'package:mobile/shared/widgets/product_list.dart';
import 'package:mobile/shared/widgets/simple_banner_widget.dart';
import 'package:mobile/shared/widgets/simple_header.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class DashboardView extends GetView<DashboardController> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: SmartRefresher(
          enablePullDown: true,
          enablePullUp: true,
          header: WaterDropHeader(),
          footer: CustomFooter(builder: (final BuildContext context, final LoadStatus? mode) {
              Widget body;
              if (mode == LoadStatus.idle) {
                body = Text("pull up load");
              } else if (mode == LoadStatus.loading) {
                body = CupertinoActivityIndicator();
              } else if (mode == LoadStatus.failed) {
                body = Text("Load Failed!Click retry!");
              } else if (mode == LoadStatus.canLoading) {
                body = Text("release to load more");
              } else {
                body = Text("No more Data");
              }
              return Container(
                height: 55.0,
                child: Center(child: body),
              );
            },
          ),
          controller: controller.refreshController,
          onRefresh: controller.onRefresh,
          onLoading: controller.onLoading,
          child: GetBuilder<DashboardController>(builder: (controller) {
            return SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  if(controller.banners.isNotEmpty) SizedBox(height: 200.0, child: SimpleBannerWidget(banners: controller.banners)),
                  if(controller.highLighted.isNotEmpty) _buildHighlight(),
                  if(controller.urgent.isNotEmpty) _buildUrgent(),
                  if(controller.closingSoon.isNotEmpty) _buildClosingSoon(),
                  if(controller.r1000.isNotEmpty) _buildr1000(),

                  SizedBox(height: 10.0),
                  SimpleHeader(
                    text: 'latest'.tr,
                  ),
                  GridView.builder(
                      physics: NeverScrollableScrollPhysics(),
                      shrinkWrap: true,
                      itemCount: controller.latest.length,
                      padding: EdgeInsets.symmetric(horizontal: 5.0, vertical: 5.0),
                      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 2,
                        crossAxisSpacing: 5,
                        mainAxisSpacing: 10,
                        childAspectRatio: 0.6,
                      ),
                      itemBuilder: (BuildContext context, int index) {
                        // give -1 because don't want effect hogiration margin
                        final item = controller.latest[index];
                        return  GestureDetector(
                            onTap: () {
                              Get.rootDelegate.toNamed(Routes.productDetail(productId: '${item.id}'));
                            },
                            child: ProductItem(product: item));
                      }
                  ),
                ],
              ),
            );
          }),
        ),
      ),
    );
  }


  Widget _buildHighlight() {
    return Column(
      children: [
        SizedBox(
          height: 10,
        ),
        SimpleHeader(
            text: 'highlight'.tr,
            actionText: 'see_all'.tr,
            onTap: () {
              Get.rootDelegate.toNamed(Routes.PRODUCT, arguments: ProductSearchFilter(label: ProductLabel.HIGH_LIGHTED));
            }
        ),
        SizedBox(
          height: 320,
          child: ProductList(
            list: controller.highLighted,
          ),
        ),
      ],
    );
  }

  Widget _buildUrgent() {
    return Column(
      children: [
        SizedBox(
          height: 10,
        ),
        SimpleHeader(
            text: 'urgent'.tr,
            actionText: 'see_all'.tr,
            onTap: () {
              Get.rootDelegate.toNamed(Routes.PRODUCT, arguments: ProductSearchFilter(label: ProductLabel.URGENT));
            }
        ),
        SizedBox(
          height: 320,
          child: ProductList(
            list: controller.urgent,
          ),
        ),
      ],
    );
  }

  Widget _buildClosingSoon() {
    return Column(
      children: [

        SizedBox(
          height: 10,
        ),

        SimpleHeader(
            text: 'close_soon'.tr,
            actionText: 'see_all'.tr,
            onTap: () {
              Get.rootDelegate.toNamed(Routes.PRODUCT, arguments: ProductSearchFilter(label: ProductLabel.FINISH_SOON));
            }
        ),
        SizedBox(
            height: 320,
            child: ProductList(
              list: controller.closingSoon,
            )
        ),

      ],
    );
  }

  Widget _buildr1000() {
    return Column(
      children: [

        SizedBox(
          height: 10,
        ),

        SimpleHeader(
            text: "1000R",
            actionText: 'see_all'.tr,
            onTap: () {
              Get.rootDelegate.toNamed(Routes.PRODUCT, arguments: ProductSearchFilter(label: ProductLabel.R1000));
            }
        ),
        SizedBox(
            height: 320,
            child: ProductList(
              list: controller.r1000,
            )
        ),
      ],
    );
  }

}
