import 'package:get/get.dart';
import 'package:mobile/modules/dashboard/dashboard_controller.dart';
import 'package:mobile/repositories/product_repository.dart';

class DashboardBinding extends Bindings {

  @override
  void dependencies() {
    Get.lazyPut<ProductRepository>(() => ProductRepository());
    Get.lazyPut<DashboardController>(() => DashboardController(repository: Get.find()));
  }

}