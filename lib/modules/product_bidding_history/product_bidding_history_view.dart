import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mobile/modules/product_bidding_history/product_bidding_history_controller.dart';
import 'package:mobile/routes/app_pages.dart';
import 'package:mobile/shared/widgets/avatar_fade_Image.dart';
import 'package:mobile/utils/helpers.dart';

class ProductBiddingHistoryView extends GetView<ProductBiddingHistoryController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Bid history'),
        elevation: 0,
        centerTitle: true,
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () {
            Get.rootDelegate.offNamed(Routes.productDetail(productId: controller.productId));
          },
        ),
      ),
      body: GetBuilder<ProductBiddingHistoryController>(builder: (controller) {
        return Padding(
          padding: EdgeInsets.symmetric(horizontal: 10.0),
          child: SingleChildScrollView(
            child: ListView.builder(
                scrollDirection: Axis.vertical,
                physics: NeverScrollableScrollPhysics(),
                shrinkWrap: true,
                itemCount: controller.product.bidPrice?.length?? 0,
                itemBuilder: (BuildContext context, int index) {
                  final bidPrice = controller.product.bidPrice?[index];
                  final createdAt = Helpers.getTimeHuman(
                      createdAt: bidPrice?.createdAt);
                  final amount = bidPrice?.amount;
                  return Padding(
                    padding: EdgeInsets.only(left: 10.0),
                    child: ListTile(
                      leading: Container(
                        width: 40.0,
                        height: 40.0,
                        child: AvatarFadeImage(
                            url: bidPrice?.createdBy?.profileUrl?? ''),
                      ),
                      subtitle: Text("$createdAt"),
                      title: Text("${bidPrice?.createdBy?.userName}", style: TextStyle(fontSize: 12.0),),
                      trailing: Text("USD $amount"),
                    ),
                  );
                }),
          ),
        );
      },),
    );
  }
}
