import 'package:get/get.dart';
import 'package:mobile/modules/product_bidding_history/product_bidding_history_controller.dart';
import 'package:mobile/repositories/product_repository.dart';
import 'package:mobile/repositories/user_repository.dart';

class ProductBiddingHistoryBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<UserRepository>(() => UserRepository());
    Get.lazyPut<ProductRepository>(() => ProductRepository());
    Get.create<ProductBiddingHistoryController>(
          () => ProductBiddingHistoryController(
              productId: Get.parameters['productId'] ?? '',
              userRepository: Get.find<UserRepository>(),
              productRepository: Get.find<ProductRepository>()
      ),
    );
  }
}
