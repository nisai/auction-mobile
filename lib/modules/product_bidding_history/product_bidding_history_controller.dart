import 'dart:developer';

import 'package:get/get.dart';
import 'package:mobile/repositories/product_repository.dart';
import 'package:mobile/repositories/user_repository.dart';
import 'package:mobile/shared/models/product_model.dart';
class ProductBiddingHistoryController extends GetxController {

  var _product = ProductModel().obs;
  ProductModel get product => _product.value;

  ProductBiddingHistoryController({required final this.productId, required this.userRepository, required this.productRepository});

  final String productId;
  final ProductRepository productRepository;
  final UserRepository userRepository;

  var _loading = false.obs;
  bool get loading => _loading.value;

  @override
  void onInit() {
    print('onReady');

    log('product id is $productId');

    _loadProduct().then((_){

      update();
     // _loadProductDetail();
    });

    super.onInit();
  }

  Future<void> _loadProduct() async {
    try {
      _loading(true);
      update();

      final result = await productRepository.getProductById(productId: int.tryParse(productId));
      if(result != null) {
        _product(result);
        print('product is : ${product.toString()}');
      }

      _loading(false);
      update();
    } catch(e) {
      print('error load product $e');
    }
  }
}
