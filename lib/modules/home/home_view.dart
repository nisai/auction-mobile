import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mobile/core/theme/colors.dart';
import 'package:mobile/modules/home/home_controller.dart';
import 'package:mobile/routes/app_pages.dart';

class HomeView extends GetView<HomeController> {

  @override
  Widget build(BuildContext context) {
    return GetRouterOutlet.builder(
      builder: (context, delegate, currentRoute) {
        //This router outlet handles the appbar and the bottom navigation bar
        final currentLocation = currentRoute?.location;
        var currentIndex = 0;
        if (currentLocation?.startsWith(Routes.CATEGORY) == true) {
          currentIndex = 1;
        }
        if (currentLocation?.startsWith(Routes.NOTIFICATION) == true) {
          currentIndex = 2;
        }
        if (currentLocation?.startsWith(Routes.OTHERS) == true) {
          currentIndex = 3;
        }

        return Scaffold(
          body: GetRouterOutlet(
            initialRoute: Routes.DASHBOARD,
            // anchorRoute: Routes.HOME,
            key: Get.nestedKey(Routes.HOME),
          ),
          bottomNavigationBar: Theme(
            data: Theme.of(context).copyWith(
              // sets the background color of the `BottomNavigationBar`
                canvasColor: AppColor.primaryColor,
                // sets the active color of the `BottomNavigationBar` if `Brightness` is light
                primaryColor: Colors.red,
                textTheme: Theme
                    .of(context)
                    .textTheme
                    .copyWith(caption: new TextStyle(color: Colors.yellow))), // sets the inactive color of the `BottomNavigationBar`
            child: BottomNavigationBar(
              currentIndex: currentIndex,
              onTap: (value) {
                  switch (value) {
                    case 0:
                      delegate.toNamed(Routes.HOME);
                      break;
                    case 1:
                      delegate.toNamed(Routes.CATEGORY);
                      break;
                    case 2:
                      delegate.toNamed(Routes.NOTIFICATION);
                      break;
                    case 3:
                      delegate.toNamed(Routes.OTHERS);
                      break;
                    default:
                  }
                },
              items: [
                // _Paths.HOME + [Empty]
                BottomNavigationBarItem(
                  icon: Icon(Icons.home),
                  label: 'home'.tr,
                ),
                // _Paths.HOME + Routes.PROFILE
                BottomNavigationBarItem(
                  icon: Icon(Icons.category),
                  label: 'category'.tr,
                ),
                // _Paths.HOME + _Paths.PRODUCTS
                BottomNavigationBarItem(
                  icon: Icon(Icons.notifications),
                  label: 'notification'.tr,
                ),
                BottomNavigationBarItem(
                  icon: Icon(Icons.read_more),
                  label: 'more'.tr,
                ),
              ],
              selectedFontSize: 12,
              unselectedFontSize: 12,
              showSelectedLabels: true,
              showUnselectedLabels: true,
            ),
          ),
          floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
          floatingActionButton: Visibility(
            child: Container(
              height: 50.0,
              width: 100.0,
              child: FittedBox(
                child: FloatingActionButton.extended(
                  heroTag: 'post_hero',
                  elevation: 1.0,
                  backgroundColor: AppColor.primaryColor,
                  icon: const Icon(Icons.camera_alt, size: 14, color: Colors.white,),
                  label: Text('post_submit'.tr, style: TextStyle(fontSize: 14, color: Colors.white),),
                  onPressed: () {
                    Get.rootDelegate.toNamed(Routes.POST_LISTING);
                  },
                ),
              ),
            ),
          ),
        );

      },
    );
  }
}