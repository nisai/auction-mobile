import 'package:get/get.dart';
import 'package:mobile/modules/product_payment/product_payment_controller.dart';
import 'package:mobile/repositories/payment_repository.dart';
import 'package:mobile/repositories/product_repository.dart';
import 'package:mobile/repositories/user_account_repository.dart';

class ProductPaymentBiding extends Bindings {
  @override
  void dependencies() {

    Get.lazyPut(() => PaymentRepository());
    Get.lazyPut(() => UserAccountRepository());
    Get.lazyPut(() => ProductRepository());
    Get.lazyPut(() => ProductPaymentController(
        productId: Get.parameters['productId'] ?? '',
        productRepository: Get.find(),
        userAccountRepository: Get.find(),
        paymentRepository: Get.find()));
  }
}
