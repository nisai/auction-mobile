
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:mobile/repositories/payment_repository.dart';
import 'package:mobile/repositories/product_repository.dart';
import 'package:mobile/repositories/user_account_repository.dart';
import 'package:mobile/routes/app_pages.dart';
import 'package:mobile/shared/models/product_model.dart';
import 'package:mobile/shared/models/user_account_model.dart';
import 'package:mobile/utils/customLoader.dart';

class ProductPaymentController extends GetxController {

  final _loader = CustomLoader();

  final PaymentRepository paymentRepository;
  final ProductRepository productRepository;
  final UserAccountRepository userAccountRepository;

  var _product = ProductModel().obs;
  ProductModel get product => _product.value;

  var _userAccount = UserAccountModel().obs;
  UserAccountModel get userAccount => _userAccount.value;

  var _loading = true.obs;
  bool get loading => _loading.value;

  final String productId;

  ProductPaymentController({
    required this.productId,
    required this.productRepository,
    required this.userAccountRepository,
    required this.paymentRepository});

  var _totalFee = 0.0.obs;
  double get totalFee => _totalFee.value;
  double get accountBalance => userAccount.amount?? 0;
  bool get hasEnoughBalance => accountBalance > totalFee;

  @override
  onInit() {
    super.onInit();
    _loadData().then((value) {
      update();
    });
  }

  Future<void> _loadData() async {
    _loading(true);
    await _getProductInformation();
    await _getProductFee();
    await _getUserBalance();
    _loading(false);
  }

  Future<void> _getProductInformation() async {
      try {
        final result = await productRepository.getProductById(productId: int.tryParse(productId));
        if(result != null) {
          _product(result);
        }
      } catch(e) {
        print('_getProductInformation: $e');
      }
  }

  Future<void> _getUserBalance() async {
     try {
       final result = await userAccountRepository.getUserAccount();

       if(result != null) {
         _userAccount(result);
       }
     } catch(e) {
       print('_getUserBalance: $e');
     }
  }

  Future<void> _getProductFee() async {
    try {
      final result =  await paymentRepository.getProductTotalFee(productId: int.tryParse(productId));
      if(result != null) {
        _totalFee(result);
      }
    } catch(e) {

    }
  }

  Future<void> handlePayment(final BuildContext context) async {
    try {
      _loader.showLoader(context);

      final result = await paymentRepository.createPaymentProduct(productId: int.tryParse(productId));

      if(result) {
        Get.rootDelegate.offNamed(Routes.productDetail(productId: productId));
      }
      print('result deposit_wallet done: $result');

      _loader.hideLoader();
    } catch(e) {
      print('handlePayment: $e');
    }

  }

}