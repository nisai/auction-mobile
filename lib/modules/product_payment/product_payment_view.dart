import 'package:adaptive_dialog/adaptive_dialog.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mobile/modules/product_payment/product_payment_controller.dart';
import 'package:mobile/routes/app_pages.dart';
import 'package:mobile/shared/models/product_model.dart';
import 'package:mobile/shared/widgets/network_image.dart';
import 'package:mobile/shared/widgets/simple_loading.dart';

class ProductPaymentView extends GetView<ProductPaymentController> {

  @override
  Widget build(BuildContext context) {
    return GetBuilder<ProductPaymentController>(
        builder: (controller) {
          if(controller.loading) {
            return SimpleLoading();
          }
          return Scaffold(
            appBar: AppBar(
              elevation: 0,
              titleSpacing: 0,
              title: Container(
                color: Color(0xfff7892b),
                child: Row(
                  mainAxisSize: MainAxisSize.max,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Container(
                      width: 100.0,
                      child: TextButton(
                        onPressed: () {
                          Get.rootDelegate.offNamed(Routes.HOME);
                        },
                        child: Text('back_process'.tr, style: TextStyle(color: Colors.white),),
                      ),
                    ),
                    Expanded(
                        child: Container(
                          child: Text('Confirm your payment', style: TextStyle(color: Colors.white),),
                        )
                    ),
                    Container(
                      width: 100.0,
                      child: TextButton(
                        onPressed: () {
                          Get.rootDelegate.offNamed(Routes.SELLING_ITEM);
                        },
                        child: Text('draft'.tr, style: TextStyle(color: Colors.white),),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            body: SafeArea(
              child: SingleChildScrollView(
                child: Padding(
                  padding: EdgeInsets.all(20),
                  child: Column(
                    children: <Widget>[
                      SizedBox(height: 20,),
                      _buildItem(controller.product),
                      SizedBox(
                          height: 50.0,
                          child: ListTile(
                            title: Text('total_fee'.tr,
                                style: TextStyle(
                                    fontSize: 13, fontWeight: FontWeight.bold)),
                            subtitle: Row(
                              children: [
                                Text('balance'.tr, style: TextStyle(fontSize: 10)),
                                Text('${controller.accountBalance}',
                                    style:
                                    TextStyle(fontSize: 10, color: Colors.red)),
                              ],
                            ),
                            trailing: Text(
                                controller.totalFee <= 0
                                    ? 'No fee'
                                    : '${controller.totalFee}',
                                style: TextStyle(
                                    fontSize: 13, fontWeight: FontWeight.bold)),
                          )),
                    ],
                  ),
                ),
              ),
            ),
            bottomNavigationBar: Container(
              color: Color(0xfff7892b),
              child: Row(
                mainAxisSize: MainAxisSize.max,
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  _buildBack(),
                  Expanded(
                      child: Padding(
                        padding: EdgeInsets.symmetric(horizontal: 10.0),
                        child: LinearProgressIndicator(
                          value: 1.0,
                          valueColor: AlwaysStoppedAnimation<Color>(Colors.lightBlue),
                          backgroundColor: Colors.white12,
                        ),
                      )),

                  _buildNext(context),
                ],
              ),
            ),
          );
        });
  }

  Widget _buildBack() {
    return Container(
      width: 100.0,
      child: TextButton(
        onPressed: () {
          Get.rootDelegate.offNamed(Routes.HOME);
        },
        child: Text('back_process'.tr, style: TextStyle(color: Colors.white),),
      ),
    );
  }

  Widget _buildNext(final BuildContext context) {
    if(controller.hasEnoughBalance) {
      return Container(
        width: 100.0,
        child: TextButton(
          onPressed: () async {
            final OkCancelResult result = await showOkCancelAlertDialog(
              context: context,
              title: 'confirm your payment',
              message: 'Do you really want to continue?',
              isDestructiveAction: true,
            );
            if(result == OkCancelResult.ok) {
              await controller.handlePayment(context);
            }
          },
          child: Text('pay'.tr, style: TextStyle(color: Colors.white),),
        ),
      );
    }

    return Container(
      width: 100.0,
      child: TextButton(
        onPressed: () {
          final thenTo = Routes.productPayment(productId: '${controller.productId}');
          Get.rootDelegate.toNamed(Routes.depositThenTo(thenTo));
        },
        child: Text('deposit'.tr, style: TextStyle(color: Colors.white),),
      ),
    );
  }

  Widget _buildItem(final ProductModel? item){
    return Card(
      child: Column(
        children: <Widget>[
          ListTile(
            leading: Container(
              width: 100.0,
              height: 300.0,
              child: CacheNetworkImageWidget(radius: 0, url: '${item?.imageUrl}'),
            ),
            title: GestureDetector(
              onTap: () {
                Get.rootDelegate.toNamed(Routes.productDetail(productId: '${controller.productId}'.toString()));
              },
              child: Text(
                  "${item?.name}"
              ),
            ),
            subtitle: Padding(
              padding: EdgeInsets.symmetric(vertical: 10.0),
              child: Row(
                children: [
                  Container(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text('${item?.description}'),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            trailing: Text("${item?.price}"),
          ),
        ],
      ),
    );
  }
}
