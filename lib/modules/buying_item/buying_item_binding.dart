import 'package:get/get.dart';
import 'package:mobile/modules/buying_item/buying_item_controller.dart';
class BuyingItemBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => BuyingItemController());
  }
}
