import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mobile/repositories/user_repository.dart';
import 'package:mobile/shared/logger/logger_utils.dart';
import 'package:mobile/shared/models/user_item_model.dart';
import 'package:mobile/shared/models/user_model.dart';
import 'package:mobile/utils/utility.dart';

class BuyingItemController extends GetxController with GetSingleTickerProviderStateMixin {

  final List<String> tabs = [
    "Items I Purchased",
    "Items I Win",
    "Items I Lost",
    "Item I bid"
  ];

  var orderItems  = <ProductOrderModel>[].obs;
  var winItems  = <WinLostProductModel>[].obs;
  var lostItems = <WinLostProductModel>[].obs;
  var bidItems = <BidProductModel>[].obs;



  late TabController tabController;

  final _repository = Get.find<UserRepository>();

  @override
  void onInit() {

    tabController = TabController(vsync: this, length: tabs.length);

    _fetchData().then((value) {
      update();
    });

    super.onInit();
  }


  Future<void> _fetchData() async {
    try {
      final UserModel? user = await Utility.getCurrentUser();
      final Map<String, dynamic>? _result = await _repository.getBuyingItemsByUserId(user?.id?? 0);
      if(_result?['data'] != null) {
        var data = _result?['data'];
        var _orderItems = (data['ProductOrders']['select'] as List)
            .cast<Map<String, dynamic>>()
            .map(ProductOrderModel.fromJson)
            .toList();

        var _bidItems = (data['BidPrices']['select'] as List)
            .cast<Map<String, dynamic>>()
            .map(BidProductModel.fromJson)
            .toList();

        var _loseProducts = (data['LoseProducts']['select'] as List)
            .cast<Map<String, dynamic>>()
            .map(WinLostProductModel.fromJson)
            .toList();

        var _winProducts = (data['WinProducts']['select'] as List)
            .cast<Map<String, dynamic>>()
            .map(WinLostProductModel.fromJson)
            .toList();
        orderItems(_orderItems);
        winItems(_winProducts);
        lostItems(_loseProducts);
        bidItems(_bidItems);
      }
    } catch(e) {
      logger.e('error $e');
    }
  }

}
