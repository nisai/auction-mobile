import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mobile/modules/buying_item/buying_item_controller.dart';
import 'package:mobile/routes/app_pages.dart';
import 'package:mobile/shared/models/user_item_model.dart';
import 'package:mobile/shared/widgets/network_image.dart';

class BuyingItemView extends GetView<BuyingItemController> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GetBuilder<BuyingItemController>(builder: (controller) {
        return NestedScrollView(
          headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
            return <Widget>[
              SliverAppBar(
                leading: IconButton(
                  icon: Icon(Icons.arrow_back_ios, color: Colors.white),
                  onPressed: () {
                    Get.rootDelegate.offNamed(Routes.OTHERS);
                  },
                ),
                floating: false,
                snap: false,
                pinned: true,
                title: Text('buying_item'.tr, textAlign: TextAlign.start),
                bottom: TabBar(
                  labelColor: Colors.white,
                  controller: controller.tabController,
                  indicatorSize: TabBarIndicatorSize.tab,
                  indicatorWeight: 5.0,
                  isScrollable: true,
                  tabs:
                  controller.tabs.map((tab) => Tab(text: tab)).toList(),
                ),
              )
            ];
          },
          body: TabBarView(
              controller: controller.tabController,
              children: <Widget>[
                ListView.builder(
                  itemCount: controller.orderItems.length,
                  itemBuilder: (context, index) {
                    final item = controller.orderItems[index];
                    return _buildItemOrder(item: item);
                  },
                ),
                ListView.builder(
                  itemCount: controller.winItems.length,
                  itemBuilder: (context, index) {
                    final item = controller.winItems[index];
                    return _buildItemWinLost(item: item);
                  },
                ),
                ListView.builder(
                  itemCount: controller.lostItems.length,
                  itemBuilder: (context, index) {
                    final item = controller.lostItems[index];
                    return _buildItemWinLost(item: item);
                  },
                ),
                ListView.builder(
                  itemCount: controller.bidItems.length,
                  itemBuilder: (context, index) {
                    final item = controller.bidItems[index];
                    return _buildItemBiding(item: item);
                  },
                )
              ]),
        );
      }),
    );
  }

  Widget _buildItemOrder({final ProductOrderModel? item}){
    return new Padding(padding: new EdgeInsets.all(10.0),
        child: new Card(
          child: new Column(
            children: <Widget>[
              ListTile(
                onTap: () {
                  Get.rootDelegate.toNamed(Routes.productDetail(productId: '${item?.product?.id}'));
                },
                leading: Container(
                  width: 50.0,
                  height: 80.0,
                  child: CacheNetworkImageWidget(url: '${item?.product?.imageUrl}'),
                ),
                title: Text(
                    "${item?.product?.name}",
                    overflow: TextOverflow.ellipsis,
                ),
                subtitle: Padding(
                  padding: EdgeInsets.symmetric(vertical: 10.0),
                  child: Text('${item?.product?.description}', maxLines: 2, overflow: TextOverflow.ellipsis,),
                ),
                trailing: Text("\$${item?.product?.buyNowPrice}"),
              ),
            ],
          ),
        )
    );
  }

  Widget _buildItemWinLost({final WinLostProductModel? item}){
    return new Padding(padding: new EdgeInsets.all(10.0),
        child: new Card(
          child: new Column(
            children: <Widget>[
              ListTile(
                onTap: () {
                  Get.rootDelegate.toNamed(Routes.productDetail(productId: '${item?.product?.id}'));
                },
                leading: Container(
                  width: 50.0,
                  height: 80.0,
                  child: CacheNetworkImageWidget(url: '${item?.product?.imageUrl}'),
                ),
                title: Text(
                    "${item?.product?.name}"
                ),
                subtitle: Padding(
                  padding: EdgeInsets.symmetric(vertical: 10.0),
                  child: Row(
                    children: [
                      Container(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text('${item?.product?.description}'),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                trailing: Text("\$${item?.bidPrice?.amount}"),
              ),
            ],
          ),
        )
    );
  }

  Widget _buildItemBiding({final BidProductModel? item}){
    return new Padding(padding: new EdgeInsets.all(10.0),
        child: new Card(
          child: new Column(
            children: <Widget>[
              ListTile(
                onTap: () {
                  Get.rootDelegate.toNamed(Routes.productDetail(productId: '${item?.product?.id}'));
                },
                leading: Container(
                  width: 50.0,
                  height: 80.0,
                  child: CacheNetworkImageWidget(url: '${item?.product?.imageUrl}'),
                ),
                title: Text(
                    "${item?.product?.name}"
                ),
                subtitle: Padding(
                  padding: EdgeInsets.symmetric(vertical: 10.0),
                  child: Text('${item?.product?.description}', maxLines: 2, overflow: TextOverflow.ellipsis,),
                ),
                trailing: Text("\$${item?.amount}"),
              ),
            ],
          ),
        )
    );
  }

}

class BuildBidItem extends StatelessWidget {

  final BidProductModel model;

  const BuildBidItem({Key? key, required this.model}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: CircleAvatar(
        child: CacheNetworkImageWidget(url: model.product?.imageUrl),
      ),
      title: Text('${model.product?.name}'),
      subtitle: Text('${model.product?.originalPrice}'),
      trailing: Icon(Icons.arrow_forward_ios),
      onTap: () {
        //Navigator.push(context, MaterialPageRoute(builder: (context) => ProductPreviewScreen(model: model.product)));
      },
      selected: true,
    );

  }
}

class BuildWinLostItem extends StatelessWidget {

  final WinLostProductModel model;

  const BuildWinLostItem({Key? key, required this.model}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: CircleAvatar(
        child: CacheNetworkImageWidget(url: model.product?.imageUrl),
      ),
      title: Text('${model.product?.name}'),
      subtitle: Text('${model.product?.originalPrice}'),
      trailing: Icon(Icons.arrow_forward_ios),
      onTap: () {
       // Navigator.push(context, MaterialPageRoute(builder: (context) => ProductPreviewScreen(model: model.product)));
      },
      selected: true,
    );

  }
}
