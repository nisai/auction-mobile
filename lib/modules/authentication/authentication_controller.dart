import 'dart:async';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_facebook_auth/flutter_facebook_auth.dart';
import 'package:get/get.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:mobile/core/local_storage.dart';
import 'package:mobile/repositories/user_repository.dart';
import 'package:mobile/routes/app_pages.dart';
import 'package:mobile/shared/models/access_token_model.dart';
import 'package:mobile/shared/models/user_model.dart';
import 'package:mobile/utils/constant.dart';

class AuthenticationController extends GetxController {

  final UserRepository repository;
  var _loading = false.obs;
  var _appUser = UserModel().obs;

  AuthenticationController({required this.repository});

  UserModel get appUser => _appUser.value;

  bool get authenticated => _accessToken.value.accessToken != null;

  var loginOrLogout = false.obs;

  var _accessToken = AccessTokenModel().obs;
  AccessTokenModel? get accessToken => _accessToken.value;


  final _storage = Get.find<LocalStorage>();
  final  _auth = Get.find<FirebaseAuth>();

  bool get isLogin => accessToken != null;

  bool get loading => _loading.value;


  @override
  void onInit() {

    super.onInit();
  }

  Future<void> handleStoreToken(final AccessTokenModel accessToken) async {
    await _storage.write(Constant.accessToken, accessToken.toMap());
    _accessToken(accessToken);
    update();
  }

  Future<void> logout() async {
    await _storage.remove(Constant.accessToken);
    _accessToken(null);
    _appUser(null);

    update();

  }

  Future<void> signInWithGoogle() async {
    try {
      print('signInWithGoogle');
      final GoogleSignInAccount? googleUser = await GoogleSignIn().signIn();
      // Obtain the auth details from the request
      final GoogleSignInAuthentication? googleAuth = await googleUser?.authentication;
      final credential = GoogleAuthProvider.credential(
        accessToken: googleAuth?.accessToken,
        idToken: googleAuth?.idToken,
      );
      final UserCredential userCredential = await _auth.signInWithCredential(credential);
      await socialNextPage(userCredential);
    } on FirebaseAuthException catch(e) {
      print('signInWithGoogle error $e');
    }
  }

  Future<void> signInWithFacebook() async {
    try {
      print('signInWithFacebook');
      final LoginResult result = await FacebookAuth.instance.login(loginBehavior: LoginBehavior.nativeWithFallback);
      if(result.status == LoginStatus.success) {
        // Create a credential from the access token
        final credential = FacebookAuthProvider.credential(result.accessToken!.token);
        // Once signed in, return the UserCredential
        final UserCredential userCredential = await _auth.signInWithCredential(credential);
        await socialNextPage(userCredential);
      }
    } on FirebaseAuthException catch (e) {
      print('signInWithFacebook error $e');
    }
  }

  Future<void> socialNextPage(final UserCredential userCredential) async {
    try {
      final existUser = await repository.getUserByEmail(userCredential.user?.email);

      if(existUser != null) {
        Get.rootDelegate.offNamed(Routes.LOGIN, arguments: existUser);
      } else {
        Get.rootDelegate.offNamed(Routes.REGISTER, arguments: userCredential.user);
      }

    } catch(e) {

    }
  }

}