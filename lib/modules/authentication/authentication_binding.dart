import 'package:get/get.dart';
import 'package:mobile/modules/authentication/authentication_controller.dart';

class AuthenticationBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => AuthenticationController(repository: Get.find()));
  }

}