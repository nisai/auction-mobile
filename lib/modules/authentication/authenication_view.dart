import 'package:flutter/material.dart';
import 'package:flutter_signin_button/button_list.dart';
import 'package:flutter_signin_button/button_view.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:mobile/core/theme/colors.dart';
import 'package:mobile/modules/authentication/authentication_controller.dart';
import 'package:mobile/routes/app_pages.dart';
import 'package:mobile/shared/widgets/language_wiget.dart';

class AuthenticationView extends GetView {

  Widget _login() {
    return GestureDetector(
      onTap: () {
        final String thenTo = Get.rootDelegate.currentConfiguration!.currentPage!.parameters?['then']??  '';
        if(thenTo.isNotEmpty) {
          Get.rootDelegate.offNamed(Routes.loginThenTo(thenTo));
        } else {
          Get.rootDelegate.toNamed(Routes.LOGIN);
        }
      },
      child: Container(
        padding: EdgeInsets.symmetric(vertical: 13),
        alignment: Alignment.center,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(5)),
            boxShadow: <BoxShadow>[
              BoxShadow(
                  color: Color(0xffdf8e33).withAlpha(100),
                  offset: Offset(2, 4),
                  blurRadius: 8,
                  spreadRadius: 2)
            ],
          color: AppColor.primaryColor,),
        child: Text(
          'login'.tr,
          style: TextStyle(fontSize: 20, color: Colors.white),
        ),
      ),
    );
  }

  Widget _register() {
    return GestureDetector(
      onTap: () {
        Get.rootDelegate.toNamed(Routes.REGISTER);
      },
      child: Container(
        padding: EdgeInsets.symmetric(vertical: 13),
        alignment: Alignment.center,
        decoration: BoxDecoration(
          color: AppColor.primaryColor,
          borderRadius: BorderRadius.all(Radius.circular(5)),
          border: Border.all(color: Colors.white, width: 2),
        ),
        child: Text(
          'register_now'.tr,
          style: TextStyle(fontSize: 20, color: Colors.white),
        ),
      ),
    );
  }


  Widget _title() {
    return RichText(
      textAlign: TextAlign.center,
      text: TextSpan(
          text: 'account_to_continue'.tr,
          style: GoogleFonts.portLligatSans(
            //textStyle: Theme.of(context).textTheme.display1,
            fontSize: 18,
            fontWeight: FontWeight.w700,
            color: Colors.black,
          )),
    );
  }

  @override
  Widget build(BuildContext context) {
    return GetBuilder<AuthenticationController>(
        builder: (_) {
          return Scaffold(
            appBar: AppBar(
              title: Container(
                child: Text('account_to_continue'.tr, style: TextStyle(color: Colors.white)),
              ),
              backgroundColor: AppColor.primaryColor,
              centerTitle: false,
              leading: IconButton(
                icon: Icon(Icons.arrow_back),
                onPressed: ()  {
                  Get.rootDelegate.popRoute(popMode: PopMode.History);
                },
              ),

              actions: [
                LanguageWidget(),
              ],
            ),
            body: Container(
              padding: EdgeInsets.symmetric(horizontal: 20),
              height: MediaQuery.of(context).size.height,
              child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      height: 250,
                      child: Image.asset('assets/images/language_select.jpg'),
                    ),

                    _title(),
                    SizedBox(
                      height: 30,
                    ),
                    _login(),
                    SizedBox(
                      height: 5,
                    ),
                    _register(),
                    SizedBox(
                      height: 5,
                    ),
                    Container(
                        margin: EdgeInsets.only(top: 10, bottom: 0),
                        child: Column(
                          children: <Widget>[
                            Text(
                              'quick_social_login'.tr,
                              style: TextStyle(color: Colors.white, fontSize: 17),
                            ),
                            SizedBox(
                              height: 5,
                            ),
                            SignInButton(
                              Buttons.FacebookNew,
                              onPressed: () async {
                                await _.signInWithFacebook();
                              },
                            ),
                            SizedBox(
                              height: 5,
                            ),
                            SignInButton(
                              Buttons.GoogleDark,
                              onPressed: () async {
                                await _.signInWithGoogle();
                              },
                            ),
                          ],
                        ))
                  ],
                ),
              ),
            ),
          );
        }
    );
  }
}
