import 'package:get/get.dart';
import 'package:mobile/modules/product_detail/product_detail_controller.dart';
import 'package:mobile/repositories/product_repository.dart';
import 'package:mobile/repositories/user_repository.dart';

class ProductDetailBinding extends Bindings {

  @override
  void dependencies() {
    Get.create<ProductDetailController>(
          () => ProductDetailController(
            productId: Get.parameters['productId'] ?? '',
              userRepository: Get.find<UserRepository>(),
              productRepository: Get.find<ProductRepository>()
      ),
    );
  }
}
