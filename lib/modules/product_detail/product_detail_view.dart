import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mobile/routes/app_pages.dart';
import 'package:mobile/shared/widgets/bid_count_down_widget.dart';
import 'package:mobile/shared/widgets/product/detail_loading.dart';
import 'package:mobile/shared/widgets/product_list.dart';
import 'package:mobile/shared/widgets/simple_slide_image.dart';
import 'package:mobile/shared/widgets/wishlist_toggle_widget.dart';
import 'product_detail_controller.dart';

class ProductDetailView extends GetView<ProductDetailController> {

  final _global = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {

    return GetBuilder<ProductDetailController>(builder: (controller) {
      return SafeArea(
        top: false,
        bottom: false,
        child: Scaffold(
          key: _global,
          appBar: AppBar(
            title: Text(controller.product.name?? '',
              style: TextStyle(color: Colors.white),
            ),
            actions: [
              //WishlistButtonWidget(product: controller.product),
            ],
            leading: new IconButton(
              icon: new Icon(Icons.arrow_back),
              onPressed: () {
                Get.rootDelegate.popRoute(popMode: PopMode.History);
              },
            ),
          ),
          body: controller.loading? DetailLoadingShimmerWidget(): SingleChildScrollView(
            child: Column(
              children: [

                SizedBox(
                    height: 300.0,
                    child: Stack(
                      children: [
                        SimpleSlidImage(
                            slides: controller.product.slideImages,
                            onTap: (index) {
                              //Navigator.push(context, MaterialPageRoute(builder: (context) => PreviewImageScreen(images: provider.model?.slideImage)));
                            }),
                        Positioned(
                          right: 5,
                          bottom: 0,
                          child: WishListToggleWidget(productId: int.tryParse(controller.productId), iconSize: 50),
                        ),
                      ],
                    )
                ),

                Container(
                  child:  Text(
                    '${controller.product.name}',
                    style: TextStyle(fontSize: 30),
                    textAlign: TextAlign.left,
                    maxLines: 3,
                  ),
                ),

                SizedBox(
                  height: 5.0,
                ),

                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 10.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [

                      Column(
                        mainAxisAlignment: MainAxisAlignment.end,
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: [
                          Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Container(
                                    child: Text("\$${controller.product.getHighestBid?? 0}", style: TextStyle(fontSize: 30, color: Colors.red))
                                ),
                                SizedBox(width: 20.0,),
                                Container(
                                  child: Row(
                                    children: [
                                      Icon(Icons.handyman, size: 20,),
                                      Text(
                                        '(${controller.product.bidPrice?.length?? 0})',
                                        style: TextStyle(
                                          fontSize: 20,
                                          fontWeight: FontWeight.bold,
                                          height: 1,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ]),

                          _buildReservePriceLabel()

                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Container(
                            height: 30,
                            width: 100,
                            decoration: BoxDecoration(
                              border: Border.all(
                                color: Colors.black,
                              ),
                              //borderRadius: BorderRadius.circular(10.0),
                            ),
                            child: Center(
                              child: Text('\$${controller.product.getHighestBid}'),
                            ),
                          ),
                          SizedBox(width: 15.0),
                          Container(
                              child: ElevatedButton(
                                onPressed: () {
                                  Get.rootDelegate.toNamed(Routes.productBidding(productId: controller.productId));
                                },
                                child: Text(
                                  "Place Bid",
                                  style: TextStyle(
                                    fontSize: 12.0,
                                    fontWeight: FontWeight.bold,
                                  ),
                                  textAlign: TextAlign.center,
                                ),
                              )),
                        ],
                      ),

                      SizedBox(
                        height: 5.0,
                      ),

                      if(!controller.isOwner)
                      Column(children: [
                        if(controller.product.canBuyNow)
                          Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Container(
                                  child: Text("\$${controller.product.buyNowPrice?? 0}", style: TextStyle(fontSize: 30, color: Colors.red))
                              ),
                              SizedBox(width: 20.0,),
                              Container(
                                  child: ElevatedButton(
                                    onPressed: () {
                                      Get.rootDelegate.toNamed(Routes.productBuying(productId: controller.productId));
                                    },
                                    child: Text(
                                      "Buy Now",
                                      style: TextStyle(
                                        fontSize: 12.0,
                                        fontWeight: FontWeight.bold,
                                      ),
                                      textAlign: TextAlign.center,
                                    ),
                                  )),
                            ],
                          ),

                      ]),

                      SizedBox(
                        height: 10.0,
                      ),

                      Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Container(
                                child: Text("Closes : ", style: TextStyle(fontSize: 12, color: Colors.black))
                            ),
                            Container(child: BidCountdownWidget(createdAt: controller.product.expectFinishAt?? ''))
                          ]),

                    ],
                  ),
                ),


                SizedBox(
                  height: 30.0,
                ),

                SizedBox(
                  height: 1.5,
                  child: Container(
                    color: Colors.black12,
                  ),
                ),

                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 10.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [

                      Row(
                        children: <Widget>[
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                "Description",
                                style: TextStyle(
                                    color: Colors.black87.withOpacity(0.7),
                                    fontSize: 20
                                ),
                              ),
                              SizedBox(
                                height: 3,
                              ),
                              Container(
                                  child: Text("${controller.product.descriptionNoHtml}", style: TextStyle(color: Colors.grey, fontSize: 12),
                                  ))
                            ],
                          )
                        ],
                      ),
                    ],
                  ),
                ),

                SizedBox(
                  height: 30.0,
                ),

                SizedBox(
                  height: 1.5,
                  child: Container(
                    color: Colors.black12,
                  ),
                ),

                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 10.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        children: <Widget>[
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                "Delivery Method",
                                style: TextStyle(
                                    color: Colors.black87.withOpacity(0.7),
                                    fontSize: 20),
                              ),
                              SizedBox(
                                height: 3,
                              ),
                              Container(
                                child: Row(children: (controller.product.delivery?? []).map((e) {
                                  return Padding(padding: EdgeInsets.only(right: 10), child: Text("${e?? 'unknow'}", style: TextStyle(color: Colors.grey, fontSize: 12)),);
                                }).toList(),),
                              ),
                            ],
                          )
                        ],
                      ),

                    ],
                  ),
                ),

                SizedBox(
                  height: 30.0,
                ),

                SizedBox(
                  height: 1.5,
                  child: Container(
                    color: Colors.black12,
                  ),
                ),

                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 10.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        children: <Widget>[
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                "Payment Methods",
                                style: TextStyle(
                                    color: Colors.black87.withOpacity(0.7),
                                    fontSize: 20),
                              ),
                              SizedBox(
                                height: 3,
                              ),
                              Container(
                                child:  Text("Cash, ABA, Wing, Bank deposit", style: TextStyle(color: Colors.grey, fontSize: 12)),
                              ),
                            ],
                          )
                        ],
                      ),

                    ],
                  ),
                ),



                SizedBox(
                  height: 30.0,
                ),

                SizedBox(
                  height: 1.5,
                  child: Container(
                    color: Colors.black12,
                  ),
                ),

                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 10.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        children: <Widget>[
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                "Questions and Answers",
                                style: TextStyle(
                                    color: Colors.black87.withOpacity(0.7),
                                    fontSize: 20),
                              ),
                              SizedBox(
                                height: 3,
                              ),
                              Container(
                                child:  Text("Theres ${controller.comments.length} Questions", style: TextStyle(color: Colors.grey, fontSize: 12)),
                              ),

                            ],
                          )
                        ],
                      ),

                      SizedBox(
                        height: 5,
                      ),

                      GestureDetector(
                        onTap: () async {
                          Get.rootDelegate.offNamed(Routes.productComment(productId: controller.productId));
                        },
                        child: Container(
                          padding: EdgeInsets.symmetric(vertical: 10),
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.all(Radius.circular(5)),
                              boxShadow: <BoxShadow>[
                                BoxShadow(
                                    color: Color(0xffdf8e33).withAlpha(100),
                                    offset: Offset(2, 4),
                                    blurRadius: 8,
                                    spreadRadius: 2)
                              ],
                              color: Color(0xfff7892b)),
                          child: Text(
                            'ASK QUESTION TO SELLER'.tr,
                            style: TextStyle(fontSize: 12, color: Colors.white),
                          ),
                        ),
                      ),

                    ],
                  ),
                ),


                SizedBox(
                  height: 30.0,
                ),

                SizedBox(
                  height: 1.5,
                  child: Container(
                    color: Colors.black12,
                  ),
                ),

                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 10.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        children: <Widget>[
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                "About the seller",
                                style: TextStyle(
                                    color: Colors.black87.withOpacity(0.7),
                                    fontSize: 20),
                              ),
                              SizedBox(
                                height: 3,
                              ),
                              Row(
                                children: [
                                  Container(
                                    width: 50.0,
                                    height: 50.0,
                                    child: CircleAvatar(
                                      backgroundImage: NetworkImage(controller.product.createdBy?.profileUrl?? ''),
                                    ),
                                  ),
                                  SizedBox(width: 5.0),
                                  GestureDetector(
                                    onTap: () {
                                      Get.rootDelegate.toNamed(Routes.sellerDetail(userId: '${controller.product.createdBy?.id}'));
                                    },
                                    child: Column(
                                      children: [
                                        Text('${controller.product.createdBy?.userName}',
                                          style: TextStyle(
                                              color: Colors.black87.withOpacity(0.7),
                                              fontSize: 12),
                                        ),

                                        Text('${controller.product.createdBy?.status}',
                                          style: TextStyle(
                                              color: Colors.black87.withOpacity(0.7),
                                              fontSize: 12),
                                        ),
                                      ],
                                    )
                                  ),
                                ],
                              ),

                            ],
                          )
                        ],
                      ),

                    ],
                  ),
                ),


                SizedBox(
                  height: 1.5,
                  child: Container(
                    color: Colors.black12,
                  ),
                ),


                SizedBox(
                  height: 30.0,
                ),
                if(controller.recommends.isNotEmpty)
                Column(
                  children: [
                    SizedBox(
                      height: 50.0,
                      child: Container(
                          child:
                          Text('Recommends', style: TextStyle(fontSize: 17.0))),
                    ),
                    SizedBox(
                      height: 350,
                      child: ProductList(list: controller.recommends),
                    )
                  ],
                ),
              ],
            ),
          ),
        ),
      );
    });
  }

  Widget _buildReservePriceLabel() {

    if(!controller.product.hasReversePrice)
      return  Container(
        child: Text("No Reserve", style: TextStyle(fontSize: 11, fontWeight: FontWeight.bold)),
      );

    if(!controller.product.isReservePriceMet)
      return Container(
        child: Text("Reserve Not Met", style: TextStyle(fontSize: 11, fontWeight: FontWeight.bold)),
      );

    //if(product.isReservePriceMet)
    return Container(
      child: Text("Reserve Met", style: TextStyle(fontSize: 11, fontWeight: FontWeight.bold)),
    );
  }
}
