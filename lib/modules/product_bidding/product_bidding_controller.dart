import 'dart:developer';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:mobile/repositories/product_repository.dart';
import 'package:mobile/repositories/user_repository.dart';
import 'package:mobile/routes/app_pages.dart';
import 'package:mobile/shared/models/user_item_model.dart';
import 'package:mobile/shared/models/product_model.dart';
import 'package:mobile/shared/models/user_model.dart';
import 'package:mobile/utils/customLoader.dart';

class ProductBiddingController extends GetxController {

  final _loader = CustomLoader();
  var _product = ProductModel().obs;

  ProductModel get product => _product.value;

  ProductBiddingController({required final this.productId, required this.userRepository, required this.productRepository});

  final String productId;
  final ProductRepository productRepository;
  final UserRepository userRepository;

  final bidController = TextEditingController().obs;


  var _loading = false.obs;
  bool get loading => _loading.value;

  var _autoBidItem = AutoBidPriceModel().obs;
  AutoBidPriceModel get autoBidItem => _autoBidItem.value;

  var _autoBid = false.obs;
  bool get autoBid => _autoBid.value;


  @override
  void onInit() {
    print('onReady');

    log('product id is $productId');

    _loadProduct().then((value){
      _loadProductDetail();
    });

    super.onInit();
  }

  Future<void> handleBuyNow(final productId) async {
   final value = await productRepository.buyProduct(productId);
     if(value) {
       Get.snackbar(
           'Buy now',
           'Successfully buy product',
           snackPosition: SnackPosition.TOP
       );
     }
  }

  Future<void> _loadProduct() async {
    try {
      _loading(true);
      update();

      final result = await productRepository.getProductById(productId: int.tryParse(productId));
      if(result != null) {
        _product(result);
        print('product is : ${result.toString()}');
      }
      _loading(false);
      update();
    } catch(e) {
      print('error load product $e');
    }
  }

  Future<void> _loadProductDetail() async {
    _loading(true);
    update();

    final UserModel? user = await userRepository.getCurrentUser();
    final resultDetail = await productRepository.getProductDetails(productId: int.tryParse(productId), categoryId: product.category?.id, userId: user?.id?? 0);

    if(resultDetail != null) {
      final data = resultDetail['data'];

      List<AutoBidPriceModel>? autoBidItems = (data['AutoBids']['select'] as List)
          .cast<Map<String, dynamic>>()
          .map(AutoBidPriceModel.fromJson)
          .toList();

      if(autoBidItems.isNotEmpty) {
        _autoBid(true);
        _autoBidItem(autoBidItems.first);
        bidController.value.text = '${_autoBidItem.value.amount}';
      } else {
        bidController.value.text = '${product.nextBidAmount}';
      }

      _loading(false);
      update();
    }

  }

  Future<void> handleBid(final BuildContext context) async {
    try {
      _loader.showLoader(context);

      final Map<String, dynamic> input = {
        'id': product.id,
        'amount': double.parse(bidController.value.text),
      };

      if(autoBid) {
        await _autoBidProduct(input);
      } else {
        await _bidProduct(input);
      }

      Get.rootDelegate.toNamed(Routes.productBiddingHistory(productId: productId));

    } catch(e) {

    } finally {
      _loader.hideLoader();
    }
  }


  Future<void> _bidProduct(final Map<String, dynamic> input) async {
    var result = await productRepository.bidProduct(input);
    if(result) {
      Get.snackbar(
          'place bid',
          'Successfully place bid product',
          snackPosition: SnackPosition.TOP
      );
    }
  }

  Future<void> _autoBidProduct(final Map<String, dynamic> input) async {
    var result = await productRepository.autoBidProduct(input);
    if(result) {
      Get.snackbar(
          'Auto bid',
          'Successfully set auto bid product',
          snackPosition: SnackPosition.TOP,
        duration: Duration(seconds: 5)
      );
    }
  }

  void userAutoBiding(final bool value) {
    _autoBid(value);
  }

}
