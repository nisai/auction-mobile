import 'package:get/get.dart';
import 'package:mobile/modules/product_bidding/product_bidding_controller.dart';
import 'package:mobile/repositories/product_repository.dart';
import 'package:mobile/repositories/user_repository.dart';

class ProductBiddingBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<ProductRepository>(() => ProductRepository());
    Get.lazyPut<ProductBiddingController>(
          () => ProductBiddingController(
              productId: Get.parameters['productId'] ?? '',
              userRepository: Get.find<UserRepository>(),
              productRepository: Get.find<ProductRepository>()
      ),
    );
  }
}
