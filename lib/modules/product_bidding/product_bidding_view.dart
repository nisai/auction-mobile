import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mobile/modules/product_bidding/product_bidding_controller.dart';
import 'package:mobile/routes/app_pages.dart';

class ProductBiddingView extends GetView<ProductBiddingController>{

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Place bid",
          style: TextStyle(color: Colors.white),
        ),
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () {
            Get.rootDelegate.offNamed(Routes.productDetail(productId: controller.productId));
          },
        ),
      ),
      body: GetBuilder<ProductBiddingController>(builder: (controller) {
        return SingleChildScrollView(
          child: SafeArea(
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 15),
              child: Column(
                children: [
                  SizedBox(
                    height: 5.0,
                  ),
                  Text(
                    '${controller.product.name}',
                    style: TextStyle(fontSize: 24),
                    textAlign: TextAlign.left,
                  ),
                  SizedBox(
                    height: 5.0,
                  ),
                  Row(
                    mainAxisSize: MainAxisSize.max,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                          width: 100.0,
                          child: Text('Starting Bid')
                      ),
                      Container(
                          width: 100.0,
                          child: Text('${controller.product.originalPrice}')
                      )
                    ],
                  ),
                  Row(
                    children: [
                      Expanded(child: Text('Your Bid')),
                      Spacer(),
                      Container(
                          width: 100.0,
                          child: Obx(() => TextField(
                            controller: controller.bidController.value,
                          )))
                    ],
                  ),

                  SizedBox(
                    height: 10.0,
                  ),

                  Column(
                    children: [
                      Card(
                        child: Column(
                          children: [
                            Row(
                              mainAxisSize: MainAxisSize.max,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Container(
                                    width: 100,
                                    child: Text("Auto bid")
                                ),
                                Container(
                                  width: 60,
                                  child: ObxValue((data) => Switch(
                                    value: controller.autoBid,
                                    onChanged: controller.userAutoBiding,
                                    activeTrackColor: Colors.lightGreenAccent,
                                    activeColor: Colors.green,
                                  ),
                                    false.obs,
                                  ),
                                )
                              ],
                            ),
                            Container(child: Text("Auto bid keep you in the lead by placing a bid whenever you're outbid, up to the maximum your set"))
                          ],
                        ),
                      )
                    ],
                  ),

                ],
              ),
            ),
          ),
        );
      },),
      bottomNavigationBar: Container(
        color: Color(0xfff7892b),
        child: Row(
          mainAxisSize: MainAxisSize.max,
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Container(
              width: 100.0,
              child: TextButton(
                onPressed: () {
                  Get.rootDelegate.popRoute(popMode: PopMode.History);
                },
                child: Text('Cancel', style: TextStyle(color: Colors.white),),
              ),
            ),

            Container(
              width: 100.0,
              child: TextButton(
                onPressed: () {
                  controller.handleBid(context);
                },
                child: Obx(() => Text(controller.autoBid? 'Set auto bid': 'Place bid', style: TextStyle(color: Colors.white),)),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
