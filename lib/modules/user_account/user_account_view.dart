
import 'package:flutter/material.dart';
import 'package:flutter_sticky_header/flutter_sticky_header.dart';
import 'package:get/get.dart';
import 'package:mobile/modules/user_account/user_account_controller.dart';
import 'package:mobile/routes/app_pages.dart';
import 'package:mobile/shared/models/account_transaction_model.dart';
import 'package:mobile/utils/colors.dart';

class UserAccountView extends GetView<UserAccountController> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('your_account'.tr, textAlign: TextAlign.start),
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () {
            Get.rootDelegate.offNamed(Routes.OTHERS);
          },
        ),
      ),
      body: GetBuilder<UserAccountController>(builder: (controller) {
        return CustomScrollView(slivers: [
          SliverToBoxAdapter(
            child: SizedBox(
              height: 120,
              child: Container(
                color: Color(0xfff7892b),
                child: Padding(
                  padding: EdgeInsets.symmetric(vertical: 10, horizontal: 20.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text('available_balance'.tr,
                          style: TextStyle(
                              color: Colors.white, fontSize: 14.0),
                          textAlign: TextAlign.start),
                      SizedBox(height: 5.0),
                      Text("${controller.userAccount.amount ?? '0.0'}",
                          style: TextStyle(
                              color: Colors.white, fontSize: 16.0),
                          textAlign: TextAlign.start),
                      SizedBox(height: 5.0),
                      Container(
                        width: 100,
                        child: ElevatedButton(
                          onPressed: () {
                            Get.rootDelegate.toNamed(Routes.DEPOSIT_WALLET);
                          },
                          child: Text('deposit'.tr, style: TextStyle(fontSize: 11)),
                          style: ElevatedButton.styleFrom(
                            primary: AppColor.primaryColor, // set the background color
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
          for (final String date in controller.headerKeys)
            _StickyHeaderList(date: date, list: controller.userAccountTransactions[date]?? const []),
        ]);
      }),
    );
  }
}

class _StickyHeaderList extends StatelessWidget {
  const _StickyHeaderList({
    Key? key,
    required this.list,
    this.date,
  }) : super(key: key);

  final String? date;
  final List<AccountTransactionModel> list;

  @override
  Widget build(BuildContext context) {
    return SliverStickyHeader(
      header: Header(date: date?? ''),
      sliver: SliverList(
        delegate: SliverChildBuilderDelegate((context, i) {
            final item = list[i];

            final bool debit = item.transactionType == "DEBIT";

            return ListTile(
              leading: CircleAvatar(
                child: IconButton(
                  icon: Icon(
                    Icons.monetization_on,
                    color: Colors.white,
                  ),
                  onPressed: () {},
                ),
              ),
              title: Row(
                children: [
                  Text("${list[i].transactionType}"),
                ],
              ),
              subtitle: Text(
                '${item.transactionDetail}',
                style: TextStyle(
                  overflow: TextOverflow.ellipsis
                ),
              ),
              trailing: Text("${!debit ? '' : '-'} ${list[i].amount}",
                  style: TextStyle(color: debit ? Colors.red : Colors.green)),
            );
          },
          childCount: list.length,
        ),
      ),
    );
  }
}

class Header extends StatelessWidget {
  const Header({
    Key? key,
    this.date,
  }) : super(key: key);

  final String? date;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 40,
      color: Color(0xfff7892b),
      padding: EdgeInsets.symmetric(horizontal: 20.0),
      alignment: Alignment.centerLeft,
      child: Text(
        '$date',
        style: const TextStyle(color: Colors.white),
      ),
    );
  }
}