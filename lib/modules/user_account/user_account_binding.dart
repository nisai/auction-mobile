import 'package:get/get.dart';
import 'package:mobile/repositories/user_account_repository.dart';
import 'package:mobile/modules/user_account/user_account_controller.dart';

class UserAccountBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => UserAccountRepository());
    Get.lazyPut(() => UserAccountController(repository: Get.find<UserAccountRepository>()));
  }

}
