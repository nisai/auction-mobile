import 'package:get/get.dart';
import 'package:mobile/repositories/user_account_repository.dart';
import 'package:mobile/shared/models/account_transaction_model.dart';
import 'package:mobile/shared/models/user_account_model.dart';

import "package:collection/collection.dart";

class UserAccountController extends GetxController {

  //static final UserAccountController to = Get.find<UserAccountController>();

  var headerKeys = <String>[].obs;
  var _loaded = false.obs;

  bool get loaded => _loaded.value;

  var _userAccount = UserAccountModel().obs;

  UserAccountModel get userAccount => _userAccount.value;


  var userAccountTransactions = Map<String, List<AccountTransactionModel>>().obs;


  final UserAccountRepository repository;

  UserAccountController({required this.repository});

  @override
  void onInit() {
    _fetchData().then((_) {
      update();
    });

    super.onInit();
  }


  Future<void> ensureLatestBalance() async  {
    await _fetchData();
    update();
  }

  Future<void> _fetchData() async {
    try {

      final result = await repository.getUserAccountWithTransaction();
      print('user account state ${result.toString()}');
      if(result?['data'] != null) {
        final data = result?['data'];
        final _userAccounts = (data['UserAccounts']['select'] as List)
            .cast<Map<String, dynamic>>()
            .map(UserAccountModel.fromJson)
            .toList();

        var _userAccountTransactions = Map<String, List<AccountTransactionModel>>();
        final _headerKeys = <String>[];
        final transactions = (data['UserAccountTransactions']['select'] as List)
            .cast<Map<String, dynamic>>()
            .map(AccountTransactionModel.fromJson)
            .toList();

        if(transactions.isNotEmpty) {
          _userAccountTransactions = groupBy(transactions, (AccountTransactionModel model) => model.createdAt!.split(" ")[0]);
          _userAccountTransactions.forEach((k,v) {
            _headerKeys.add('$k');
          });
        }

        _userAccount(_userAccounts.firstOrNull);
        userAccountTransactions(_userAccountTransactions);
        headerKeys(_headerKeys);
        _loaded(true);
      }
    } catch(e) {
      print('error $e');
    }

  }

}
