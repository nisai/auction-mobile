import 'package:flutter/cupertino.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:get/get.dart';
import 'package:mobile/repositories/user_repository.dart';
import 'package:mobile/utils/customLoader.dart';


class ChangePasswordController extends GetxController{

  final formKey = GlobalKey<FormBuilderState>();

  final _loader = CustomLoader();

  var oldPasswordController = TextEditingController().obs;
  var newPasswordController = TextEditingController().obs;
  var confirmPasswordController = TextEditingController().obs;

  var currentPasswordHide = true.obs;
  var newPasswordHide = true.obs;
  var showConfirm = true.obs;

  String get oldPassword => oldPasswordController.value.text.trim();
  String get newPassword => newPasswordController.value.text.trim();

  final UserRepository repository;

  ChangePasswordController({required this.repository});


  @override
  void onInit() {
    super.onInit();

  }


  Future<void> handleChangePassword(final BuildContext context) async {
    try {
      _loader.showLoader(context);
      final result = await repository.changePassword(oldPassword: oldPassword, newPassword: newPassword);

      if(result != null) {
        newPasswordController.value.text = '';
        oldPasswordController.value.text = '';
        confirmPasswordController.value.text = '';
        Get.snackbar('Password change success', 'You has been update password completed', snackPosition: SnackPosition.BOTTOM);
      }
      update();

    } catch(e) {
      Get.snackbar('Password change unsuccessfully', 'Your current password invalid', snackPosition: SnackPosition.BOTTOM);
    } finally {
      _loader.hideLoader();
    }
  }


  @override
  void onReady() {
    print('The build method is done. '
        'Your controller is ready to call dialogs and snackbars');
    super.onReady();
  }

  void handleToggleCurrentPasswordHide() {

    currentPasswordHide(!currentPasswordHide.value);
    update();

  }

  void handleToggleNewPasswordHide() {
    newPasswordHide(!newPasswordHide.value);
    update();
  }

  void handleToggleConfirmPassword() {
    showConfirm(!showConfirm.value);
    update();
  }

}
