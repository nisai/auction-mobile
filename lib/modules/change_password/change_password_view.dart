import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:get/get.dart';
import 'package:mobile/modules/change_password/change_password_controller.dart';
import 'package:mobile/routes/app_pages.dart';
import 'package:form_builder_validators/form_builder_validators.dart';

class ChangePasswordView extends GetView<ChangePasswordController> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('change_password'.tr, textAlign: TextAlign.start),
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () {
            Get.rootDelegate.offNamed(Routes.OTHERS);
          },
        ),
      ),
      body: SingleChildScrollView(
        physics: AlwaysScrollableScrollPhysics(),
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 10.0),
          child:  FormBuilder(
            key: controller.formKey,
            autovalidateMode: AutovalidateMode.disabled,
            child: GetBuilder<ChangePasswordController>(builder: (controller) {
              return Column(
                children: <Widget>[
                  SizedBox(height: 20.0),
                  FormBuilderTextField(
                    controller: controller.oldPasswordController.value,
                    name: 'currentPassword',
                    keyboardType: TextInputType.text,
                    obscureText: controller.currentPasswordHide.value,
                    decoration: InputDecoration(
                      fillColor: Colors.white,
                      border: OutlineInputBorder(
                        borderSide: BorderSide(
                            color: Colors.black,
                            width: 5.0),
                      ),
                      focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(5.0)),
                          borderSide: BorderSide(color: Colors.green)),
                      filled: true,
                      contentPadding:
                      EdgeInsets.only(bottom: 10.0, left: 10.0, right: 10.0),
                      labelText: 'current_password'.tr,
                      hintText: 'Current Password',
                      // Here is key idea
                      suffixIcon: IconButton(
                        icon: Icon(
                          // Based on passwordVisible state choose the icon
                          controller.currentPasswordHide.value
                              ? Icons.visibility
                              : Icons.visibility_off,
                          color: Theme.of(context).primaryColorDark,
                        ),
                        onPressed: () {
                          controller.handleToggleCurrentPasswordHide();
                        },
                      ),
                    ),
                    validator: FormBuilderValidators.compose(
                        [FormBuilderValidators.required(context)]),
                  ),
                  SizedBox(
                    height: 15.0,
                  ),
                  FormBuilderTextField(
                    controller: controller.newPasswordController.value,
                    name: 'newPassword',
                    keyboardType: TextInputType.text,
                    obscureText: controller.newPasswordHide.value,
                    decoration: InputDecoration(
                      fillColor: Colors.white,
                      border: OutlineInputBorder(
                        borderSide: BorderSide(
                            color: Colors.black,
                            width: 5.0),
                      ),
                      focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(5.0)),
                          borderSide: BorderSide(color: Colors.green)),
                      filled: true,
                      contentPadding:
                      EdgeInsets.only(bottom: 10.0, left: 10.0, right: 10.0),
                      labelText: 'new_password'.tr,
                      hintText: 'New password',
                      // Here is key idea
                      suffixIcon: IconButton(
                        icon: Icon(
                          // Based on passwordVisible state choose the icon
                          controller.newPasswordHide.value
                              ? Icons.visibility
                              : Icons.visibility_off,
                          color: Theme.of(context).primaryColorDark,
                        ),
                        onPressed: () {
                          controller.handleToggleNewPasswordHide();
                        },
                      ),
                    ),
                    validator: FormBuilderValidators.compose(
                        [FormBuilderValidators.required(context)]),
                  ),
                  SizedBox(
                    height: 15.0,
                  ),
                  FormBuilderTextField(
                    controller: controller.confirmPasswordController.value,
                    name: 'confirmPassword',
                    keyboardType: TextInputType.text,
                    obscureText: controller.showConfirm.value,
                    decoration: InputDecoration(
                      fillColor: Colors.white,
                      border: OutlineInputBorder(
                        borderSide: BorderSide(
                            color: Colors.black,
                            width: 5.0),
                      ),
                      focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(5.0)),
                          borderSide: BorderSide(color: Colors.green)),
                      filled: true,
                      contentPadding:
                      EdgeInsets.only(bottom: 10.0, left: 10.0, right: 10.0),
                      labelText: 'new_password'.tr,
                      hintText: 'New password',
                      // Here is key idea
                      suffixIcon: IconButton(
                        icon: Icon(
                          // Based on passwordVisible state choose the icon
                          controller.showConfirm.value
                              ? Icons.visibility
                              : Icons.visibility_off,
                          color: Theme.of(context).primaryColorDark,
                        ),
                        onPressed: () {
                          controller.handleToggleConfirmPassword();
                        },
                      ),
                    ),
                    validator: FormBuilderValidators.compose([FormBuilderValidators.required(context), (val){
                      if(val != controller.newPassword) {
                        return "Password must be match";
                      } else {
                        return null;
                      }
                    },
                    ]
                    ),
                  ),
                  SizedBox(
                    height: 15.0,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        child: MaterialButton(
                          color: Color(0xfff7892b),
                          child: Text(
                            'change'.tr,
                            style: TextStyle(color: Colors.white),
                          ),
                          onPressed: () {
                            final valid =  controller.formKey.currentState?.saveAndValidate()?? false;
                            if(valid) {
                              controller.handleChangePassword(context);
                            }
                          },
                        ),
                      ),
                    ],
                  ),
                ],
              );
            }),
          ),
        ),
      ),
    );
  }
}
