import 'package:get/get.dart';
import 'package:mobile/modules/change_password/change_password_controller.dart';
import 'package:mobile/repositories/user_repository.dart';

class ChangePasswordBindings extends Bindings {

  @override
  void dependencies() {
    Get.lazyPut(() => ChangePasswordController(repository: Get.find<UserRepository>()));
  }

}
