import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mobile/repositories/user_repository.dart';
import 'package:mobile/shared/models/product_model.dart';
import 'package:mobile/shared/models/user_model.dart';

class SellingItemController extends GetxController with GetSingleTickerProviderStateMixin {

  var draftItems = <ProductModel>[].obs;
  var sellingItems = <ProductModel>[].obs;
  var soldItems = <ProductModel>[].obs;
  var expiredItems = <ProductModel>[].obs;


  late TabController tabController;

  final _repository = Get.find<UserRepository>();

  final List<String>  tabs = ['Draft Items', 'Selling Items', 'Sold Items', 'Unsold Items'];

  @override
  void onInit() {
    tabController = TabController(vsync: this, length: tabs.length);

    _fetchData().then((value) {

    });


    super.onInit();
  }

  @override
  void onReady() {
    print('The build method is done. '
        'Your controller is ready to call dialogs and snackbars');
    super.onReady();
  }


  Future<void> _fetchData() async {
    final UserModel? user = await _repository.getCurrentUser();
    final result = await _repository.getSellingByUserId(user?.id);
    if(result != null) {
      var data = result['data'] as Map<String, dynamic>;
      var _draftItems = (data['drafts']['select'] as List)
          .cast<Map<String, dynamic>>()
          .map(ProductModel.fromJson)
          .toList();

      var _sellingItems = (data['selling']['select'] as List)
          .cast<Map<String, dynamic>>()
          .map(ProductModel.fromJson)
          .toList();

      var _soldItems = (data['sold']['select'] as List)
          .cast<Map<String, dynamic>>()
          .map(ProductModel.fromJson)
          .toList();

      var _expiredItems = (data['expired']['select'] as List)
          .cast<Map<String, dynamic>>()
          .map(ProductModel.fromJson)
          .toList();

      draftItems(_draftItems);
      sellingItems(_sellingItems);
      soldItems(_soldItems);
      expiredItems(_expiredItems);

      update();
    }
  }
}
