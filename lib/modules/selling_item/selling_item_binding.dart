import 'package:get/get.dart';
import 'package:mobile/modules/selling_item/selling_item_controller.dart';

class SellingItemBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => SellingItemController());
  }
}
