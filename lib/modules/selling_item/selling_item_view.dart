
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mobile/modules/selling_item/selling_item_controller.dart';
import 'package:mobile/routes/app_pages.dart';
import 'package:mobile/shared/models/product_model.dart';
import 'package:mobile/shared/widgets/network_image.dart';

class SellingItemView extends GetView<SellingItemController> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: NestedScrollView(
        headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
          return <Widget>[
            SliverAppBar(
              leading: IconButton(
                icon: Icon(Icons.arrow_back_ios, color: Colors.white),
                onPressed: () {
                  Get.rootDelegate.offNamed(Routes.OTHERS);
                },
              ),
              floating: false,
              snap: false,
              pinned: true,
              title: Text('selling_item'.tr, textAlign: TextAlign.start),
              bottom: TabBar(
                labelColor: Colors.white,
                controller: controller.tabController,
                indicatorSize: TabBarIndicatorSize.tab,
                indicatorWeight: 5.0,
                isScrollable: true,
                tabs:
                controller.tabs.map((tab) => Tab(text: tab)).toList(),
              ),
            )
          ];
        },
        body: GetBuilder<SellingItemController>(builder: (controller) {
          return TabBarView(
              controller: controller.tabController,
              children: <Widget>[
                ListView.builder(
                  itemCount: controller.draftItems.length,
                  itemBuilder: (context, i) {
                    return _buildDraftItem(item: controller.draftItems[i]);
                  },
                ),
                ListView.builder(
                  itemCount: controller.sellingItems.length,
                  itemBuilder: (context, i) {
                    return _buildSellingItem(
                        item: controller.sellingItems[i]);
                  },
                ),
                ListView.builder(
                  itemCount: controller.soldItems.length,
                  itemBuilder: (context, i) {
                    return _buildSoldItem(
                        item: controller.soldItems[i]);
                  },
                ),
                ListView.builder(
                  itemCount: controller.expiredItems.length,
                  itemBuilder: (context, i) {
                    return _buildExpiredItem(
                        item: controller.expiredItems[i]);
                  },
                ),
              ]);
        }),
      ),
    );
  }

  Widget _buildDraftItem({final ProductModel? item}){
    return new Padding(padding: new EdgeInsets.all(10.0),
        child: new Card(
          child: new Column(
            children: <Widget>[
              ListTile(
                onTap: () {
                  Get.rootDelegate.toNamed(Routes.productDetail(productId: '${item?.id}'));
                },
                leading: Container(
                  width: 50.0,
                  height: 80.0,
                  child: CacheNetworkImageWidget(url: '${item?.imageUrl}'),
                ),
                title: Text("${item?.name}"),
                subtitle: Padding(
                  padding: EdgeInsets.symmetric(vertical: 10.0),
                  child: Row(
                    children: [
                      Container(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text('${item?.description}'),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                trailing: Text("${item?.price}"),
              ),
              ButtonTheme(
                child: new ButtonBar(
                  children: <Widget>[
                    TextButton(
                      child: const Text('Edit'),
                      onPressed: () async {
                        Get.rootDelegate.toNamed(Routes.postEdit(productId: '${item?.id}'));
                      },
                    ),
                  ],
                ),
              ),
            ],
          ),
        )
    );
  }

  Widget _buildSellingItem({final ProductModel? item}){
    return new Padding(padding: new EdgeInsets.all(10.0),
        child: new Card(
          child: new Column(
            children: <Widget>[
              ListTile(
                onTap: () {
                  Get.rootDelegate.toNamed(Routes.productDetail(productId: '${item?.id}'));
                },
                leading: Container(
                  width: 50.0,
                  height: 80.0,
                  child: CacheNetworkImageWidget(url: '${item?.imageUrl}'),
                ),
                title: Text("${item?.name}"),
                subtitle: Padding(
                  padding: EdgeInsets.symmetric(vertical: 10.0),
                  child: Row(
                    children: [
                      Container(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text('${item?.description}'),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                trailing: Text("${item?.getHighestBid?? item?.price}"),
              ),
            ],
          ),
        )
    );
  }

  Widget _buildSoldItem({final ProductModel? item}){
    return new Padding(padding: new EdgeInsets.all(10.0),
        child: new Card(
          child: new Column(
            children: <Widget>[
              ListTile(
                onTap: () {
                  Get.rootDelegate.toNamed(Routes.productDetail(productId: '${item?.id}'));
                },
                leading: Container(
                  width: 50.0,
                  height: 80.0,
                  child: CacheNetworkImageWidget(url: '${item?.imageUrl}'),
                ),
                title:  Text("${item?.name}"),
                subtitle: Padding(
                  padding: EdgeInsets.symmetric(vertical: 10.0),
                  child: Row(
                    children: [
                      Container(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text('${item?.description}'),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                trailing: Text("${item?.getHighestBid?? item?.price}"),
              ),
            ],
          ),
        )
    );
  }

  Widget _buildExpiredItem({final ProductModel? item}){
    return new Padding(padding: new EdgeInsets.all(10.0),
        child: new Card(
          child: new Column(
            children: <Widget>[
              ListTile(
                onTap: () {
                  Get.rootDelegate.toNamed(Routes.productDetail(productId: '${item?.id}'));
                },
                leading: Container(
                  width: 50.0,
                  height: 80.0,
                  child: CacheNetworkImageWidget(url: '${item?.imageUrl}'),
                ),
                title:Text("${item?.name}"),
                subtitle: Padding(
                  padding: EdgeInsets.symmetric(vertical: 10.0),
                  child: Row(
                    children: [
                      Container(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text('${item?.description}'),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                trailing: Text("${item?.getHighestBid?? item?.price}"),
              ),
            ],
          ),
        )
    );
  }

}