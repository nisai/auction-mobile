import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:mobile/modules/login/login_controller.dart';
import 'package:mobile/routes/app_pages.dart';

class LoginView extends GetView<LoginController> {

  @override
  Widget build(BuildContext context) {

    return GetBuilder<LoginController>(builder: (controller) {
      return Scaffold(
        appBar: AppBar(
          title: Text(
            'login'.tr
          ),
          leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () {
              Get.rootDelegate.popRoute(popMode: PopMode.History);
            },
          ),
        ),
        body: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
            height: MediaQuery.of(context).size.height,
            decoration: BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(5)),
                boxShadow: <BoxShadow>[
                  BoxShadow(
                      color: Colors.grey.shade200,
                      offset: Offset(2, 4),
                      blurRadius: 5,
                      spreadRadius: 2)
                ],
                gradient: LinearGradient(
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                    colors: [Color(0xfffbb448), Color(0xffe46b10)])),
            child: Form(
              key: controller.loginFormKey,
              child: ListView(
                children: <Widget>[
                  SizedBox(
                    height: 80.0,
                  ),
                  _intro(),

                  Container(
                    margin: EdgeInsets.symmetric(vertical: 5),
                    decoration: BoxDecoration(
                        border: Border.all(color: Colors.white),
                        borderRadius:
                        BorderRadius.all(Radius.circular(10))),
                    child: TextFormField(
                      controller: controller.phoneController.value,
                      keyboardType: TextInputType.text,
                      style: TextStyle(fontSize: 12, color: Colors.white),
                      decoration: InputDecoration(
                          contentPadding:
                          EdgeInsets.symmetric(horizontal: 20),
                          hintText: 'phone_or_email'.tr,
                          border: InputBorder.none,
                          filled: true,
                          hintStyle: TextStyle(color: Colors.white)
                      ),
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return "Please enter a valid phone number or email";
                        }
                        return null;
                      },
                    ),
                  ),

                  Container(
                    margin: EdgeInsets.symmetric(vertical: 5),
                    decoration: BoxDecoration(
                        border: Border.all(color: Colors.white),
                        borderRadius:
                        BorderRadius.all(Radius.circular(10))),
                    child: TextFormField(
                      controller: controller.passwordController.value,
                      obscureText: true,
                      style: TextStyle(fontSize: 12, color: Colors.white),
                      decoration: InputDecoration(
                          contentPadding:
                          EdgeInsets.symmetric(horizontal: 20),
                          hintText: 'your_password'.tr,
                          border: InputBorder.none,
                          filled: true,
                          hintStyle: TextStyle(color: Colors.white)
                      ),
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return 'Please enter your password';
                        }
                        return null;
                      },
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  GestureDetector(
                    onTap: () async {
                      final valid = controller.loginFormKey.currentState!.validate();
                      if (valid) {
                        await controller.handleLogin(context);
                      }
                    },
                    child: Container(
                      padding: EdgeInsets.symmetric(vertical: 10),
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(5)),
                          boxShadow: <BoxShadow>[
                            BoxShadow(
                                color: Color(0xffdf8e33).withAlpha(100),
                                offset: Offset(2, 4),
                                blurRadius: 8,
                                spreadRadius: 2)
                          ],
                          color: Colors.white),
                      child: Text(
                        'login'.tr,
                        style: TextStyle(fontSize: 12, color: Color(0xfff7892b)),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  _forgetPasswordLabel(),
                  _divider(),
                  _createAccountLabel(),
                ],
              ),
            ),
          ),
        ),
      );
    });
  }

  Widget _divider() {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10),
      child: Row(
        children: <Widget>[
          SizedBox(
            width: 20,
          ),
          Expanded(
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 10),
              child: Divider(
                thickness: 1,
              ),
            ),
          ),
          Text('or', style: TextStyle(fontSize: 24, color: Colors.white)),
          Expanded(
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 10),
              child: Divider(
                thickness: 1,
              ),
            ),
          ),
          SizedBox(
            width: 20,
          ),
        ],
      ),
    );
  }


  Widget _forgetPasswordLabel() {
    return Container(
      padding: EdgeInsets.all(15),
      alignment: Alignment.bottomCenter,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        crossAxisAlignment: CrossAxisAlignment.end,
        children: <Widget>[
          Text(
            'you_forgot_password'.tr,
            style: TextStyle(fontSize: 12, color: Colors.white),
          ),
          SizedBox(
            width: 10,
          ),
          GestureDetector(
            onTap: () {
              Get.rootDelegate.offNamed('${Routes.RESET_PASSWORD}');
            },
            child: Text(
              'reset_password'.tr,
              style: TextStyle(fontSize: 12, color: Colors.white, fontWeight: FontWeight.bold),
            ),
          )
        ],
      ),
    );
  }

  Widget _createAccountLabel() {
    return Container(
      padding: EdgeInsets.all(15),
      alignment: Alignment.bottomCenter,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            'you_have_account'.tr,
            style: TextStyle(fontSize: 12, color: Colors.white),
          ),
          SizedBox(
            width: 10,
          ),
          GestureDetector(
            onTap: () {
              Get.rootDelegate.offNamed(Routes.REGISTER);
            },
            child: Text(
              'register_now'.tr,
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 12,
                  fontWeight: FontWeight.w600),
            ),
          )
        ],
      ),
    );
  }

  Widget _intro() {
    return Container(
      padding: EdgeInsets.only(bottom: 10.0),
      child: RichText(
        textAlign: TextAlign.center,
        text: TextSpan(
            text: 'login_to_continue'.tr,
            style: GoogleFonts.portLligatSans(
              //textStyle: Theme.of(context).textTheme.display1,
              fontSize: 20,
              fontWeight: FontWeight.w700,
              color: Colors.white,
            )),
      ),
    );
  }
}
