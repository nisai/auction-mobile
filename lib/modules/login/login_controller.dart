import 'dart:async';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:mobile/core/local_storage.dart';
import 'package:mobile/repositories/user_repository.dart';
import 'package:mobile/routes/app_pages.dart';
import 'package:mobile/shared/models/access_token_model.dart';
import 'package:mobile/shared/models/user_model.dart';
import 'package:mobile/utils/constant.dart';
import 'package:mobile/utils/customLoader.dart';
import 'package:mobile/utils/utility.dart';

class LoginController extends GetxController {

  final loginFormKey = GlobalKey<FormState>();
  var phoneController = TextEditingController().obs;
  var passwordController = TextEditingController().obs;

  final _loader = CustomLoader();
  final _repository = Get.find<UserRepository>();

  String get phoneNumber => phoneController.value.text;

  String get username => phoneController.value.text;
  String get password => passwordController.value.text;

  var loginOrLogout = false.obs;

  static LoginController get to => Get.find<LoginController>();

  var _accessToken = AccessTokenModel().obs;
  AccessTokenModel? get accessToken => _accessToken.value;


  final _storage = Get.find<LocalStorage>();

  bool get isLogin => accessToken != null;


  var _countryDialCode = '+855'.obs;
  String get countryDialCode => _countryDialCode.value;

  @override
  void onInit() {

    _resolveExistUser().then((value) {
      update();
    });

    super.onInit();
  }

  Future<void> _resolveExistUser() async {
    if(Get.arguments != null) {
      UserModel? existUser = Get.arguments as UserModel;
      phoneController.value.text = existUser.email?? '';
    }
  }

  Future<void> handleLogin(final BuildContext context) async {
    try {
      _loader.showLoader(context);

      final _token = await _repository.login(username: username, password: password);
      if(_token != null && _token.accessToken !=  null) {
        print('user logged ${_token.accessToken}');
        await _storage.write(Constant.accessToken, _token.toMap());
        final UserModel? user = await _repository.getCurrentUser();
        if(user != null) {
          await _storage.write(Constant.currentUser, user.toMap());
        }

        await Utility.handleSendFirebaseMessageTokenIfLoggedIn();

        WidgetsBinding.instance.addPostFrameCallback((_) {
          final thenTo = Get.rootDelegate.currentConfiguration!.currentPage!.parameters?['then'];
          Get.rootDelegate.offNamed(thenTo ?? Routes.HOME);
        });

      } else {
        Get.snackbar('username or password incorrect', 'Please use your real username and password to login',
            snackPosition: SnackPosition.TOP,
            duration: Duration(seconds: 5)
        );
      }

    } catch(e) {
      Get.snackbar('username or password incorrect', 'Please use your real username and password to login',
          snackPosition: SnackPosition.TOP,
          duration: Duration(seconds: 5)
      );
      print('controller: $e');
    } finally {
      _loader.hideLoader();
    }
  }

  @override
  void dispose() {
    super.dispose();
  }


}