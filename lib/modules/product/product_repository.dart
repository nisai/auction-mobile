//
// import 'package:get/get.dart';
// import 'package:mobile/core/gql_client.dart';
// import 'package:mobile/documents/product_document.dart';
// import 'package:mobile/shared/models/product_label.dart';
// import 'package:mobile/shared/models/product_model.dart';
// import 'package:mobile/utils/constant.dart';
//
// class ProductRepository {
//
//   final _graphQL = Get.find<>();
//
//   Future<List<ProductModel>?> search({required final int page, required final String search})  async {
//     try {
//       final result = await _graphQL.call()
//           .query(ProductDocument.searchPaginate,
//           variables: <String, dynamic>{
//             'page': page,
//             'limit': Constant.limitItems,
//             'search': '$search',
//           }
//       );
//
//       if(result['data'] != null) {
//         final data = result['data'];
//         final list = (data['items']['select'] as List)
//             .cast<Map<String, dynamic>>()
//             .map(ProductModel.fromJson)
//             .toList();
//         print('paginate: $list');
//
//         return list;
//       }
//     } catch(e) {
//       print('error: $e');
//
//     }
//   }
//
//   Future<List<ProductModel>?> getWithLabel({required final int page, required final String? label})  async {
//     try {
//       final result = await _graphQL.call()
//           .query(ProductDocument.getWithLabel,
//           variables: <String, dynamic>{
//             'page': page,
//             'limit': Constant.limitItems,
//             'label': label,
//           }
//       );
//
//       if(result['data'] != null) {
//         final data = result['data'];
//         final list = (data['items']['select'] as List)
//             .cast<Map<String, dynamic>>()
//             .map(ProductModel.fromJson)
//             .toList();
//         print('paginate: $list');
//
//         return list;
//       }
//     } catch(e) {
//       print('error: $e');
//
//     }
//   }
//
// }
