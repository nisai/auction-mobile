import 'package:get/get.dart';
import 'package:mobile/repositories/product_repository.dart';
import 'package:mobile/shared/models/product_model.dart';
import 'package:mobile/shared/models/product_search_filter.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class ProductController extends GetxController {

  var _filter = ProductSearchFilter().obs;
  ProductSearchFilter get filter => _filter.value;

  var _forceOpenSearch = false.obs;
  bool get alwaysOpenSearch => _forceOpenSearch.value;

  var _loading = false.obs;

  bool get loading => _loading.value;

  var title = 'Product'.obs;
  var _page = 1.obs;
  int get page => _page.value;

  var _search = ''.obs;
  String get search => _search.value;

  var _items = <ProductModel>[].obs;

  List<ProductModel> get items => _items;

  Rx<RefreshController> _refreshController = RefreshController(initialRefresh: false).obs;
  RefreshController get refreshController => _refreshController.value;

  final ProductRepository repository;

  ProductController({required this.repository});

  @override
  void onInit() {

    final ProductSearchFilter? f = Get.rootDelegate.currentConfiguration?.currentPage?.arguments as ProductSearchFilter;
    _filter(f);
    _fetchHandler();
    super.onInit();
  }

   Future<void> onRefresh() async {
    _page(1);
    await _fetchHandler();
    _refreshController.value.refreshCompleted();
  }

  void onLoading() async {
    _page.value++;
    await _fetchHandler();
    _refreshController.value.loadComplete();
  }

  Future<void> _fetchHandler() async {
    if(filter.isSearch) {
      _forceOpenSearch(true);
      title("Search...");
    } else {
      if(filter.isLabel) {
        title(filter.getName);
        onLabel();
      }
    }
  }

  void onSearch(final String search) async {
    try {
      _loading(true);
      final result = await repository.search(page: page, search: search);
      _loading(false);
      _items(result);
    } catch(e) {

    } finally {
      update();
    }
  }

  void onLabel()  async {
    try {
      _loading(true);
      final result = await repository.getWithLabel(page: page, label: filter.getValue);
      _loading(false);
      _items(result);
    } catch(e) {

    } finally {
      update();
    }
  }

}
