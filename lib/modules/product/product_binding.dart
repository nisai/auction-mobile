import 'package:get/get.dart';
import 'package:mobile/modules/product/product_controller.dart';
import 'package:mobile/repositories/product_repository.dart';

class ProductBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => ProductRepository());
    Get.lazyPut(() => ProductController(repository: Get.find<ProductRepository>()));
  }
}
