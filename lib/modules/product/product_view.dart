import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:material_floating_search_bar/material_floating_search_bar.dart';
import 'package:mobile/core/theme/colors.dart';
import 'package:mobile/modules/product/product_controller.dart';
import 'package:mobile/routes/app_pages.dart';
import 'package:mobile/shared/widgets/product_item_widget.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class ProductView extends GetView<ProductController> {

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      body: FloatingSearchAppBar(
          alwaysOpened: controller.alwaysOpenSearch,
          automaticallyImplyBackButton: false,
          title: Text(controller.title(), style: TextStyle(fontSize: 14, color: Colors.white),),
          onFocusChanged: (bool isChanged) {
            print('onFocuse Change $isChanged');
            //  Get.rootDelegate.offNamed(Routes.USER_ACCOUNT);
          },
          leadingActions: [
            IconButton(
              icon: Icon(Icons.arrow_back),
              onPressed: ()  {
                Get.rootDelegate.offNamed(Routes.HOME);
              },
            )
          ],
          onQueryChanged: controller.onSearch,
          onSubmitted: controller.onSearch,
          transitionCurve: Curves.easeInOut,
          transitionDuration: const Duration(milliseconds: 800),
          color: AppColor.primaryColor,
          colorOnScroll:  AppColor.primaryPurple,
          iconColor: Colors.white,
          titleStyle: TextStyle(color: Colors.white),
          hintStyle: TextStyle(color: Colors.white),
          body: SmartRefresher(
            enablePullDown: true,
            enablePullUp: true,
            controller: controller.refreshController,
            onRefresh: controller.onRefresh,
            onLoading: controller.onLoading,
            child: GetBuilder<ProductController>(builder: (controller) {
              return SingleChildScrollView(
                child:  GridView.builder(
                    physics: NeverScrollableScrollPhysics(),
                    shrinkWrap: true,
                    itemCount: controller.items.length,
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 2,
                      crossAxisSpacing: 5,
                      mainAxisSpacing: 10,
                      childAspectRatio: 0.7,
                    ),
                    itemBuilder: (BuildContext context, int index) {
                      // give -1 because don't want effect hogiration margin
                      final item = controller.items[index];
                      return  GestureDetector(
                          onTap: () {
                            Get.rootDelegate.toNamed(Routes.productDetail(productId: '${item.id}'));
                          },
                          child: ProductItem(product: item));
                    }
                ),
              );
            }),
          )
      ),
    );
  }
}