import 'package:get/get.dart';
import 'package:mobile/modules/language/language_controller.dart';

class LanguageBinding extends Bindings {

  @override
  void dependencies() {
    Get.lazyPut<LanguageController>(() => LanguageController());
  }

}