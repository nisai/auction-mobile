import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:mobile/utils/constant.dart';
import 'package:mobile/routes/app_pages.dart';

class LanguageController extends GetxController {

  final box = GetStorage();

   String get getCurrentLanguage => box.read(Constant.languageKey) ?? Constant.englishLanguageCode;

   bool get isEnglish => getCurrentLanguage == Constant.englishLanguageCode;


  @override
  void onInit() {

    super.onInit();
  }

  Future<void> toggleLanguage() async {
     Locale locale = Locale('km', 'KH');
    if(getCurrentLanguage == Constant.khmerLanguageCode) {
      locale = Locale('en', 'US');
    }
    await Get.updateLocale(locale);
  }

  Future<void> useLanguage(final String language) async {
      await box.write(Constant.languageKey, language);
      var locale;
      if(language == Constant.khmerLanguageCode) {
         locale = Locale('km', 'KH');
      } else {
         locale = Locale('en', 'US');
      }

      await Get.updateLocale(locale);

      WidgetsBinding.instance.addPostFrameCallback((_) {
        Get.rootDelegate.offNamed(Routes.HOME);
      });
  }

}