import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mobile/utils/constant.dart';
import 'package:mobile/core/theme/colors.dart';
import 'package:mobile/modules/language/language_controller.dart';

class LanguageView extends GetView<LanguageController> {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: AppColor.primaryColor,
      ),
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          title: Text('Select your language'.tr),
          elevation: 0,
          centerTitle: true,
        ),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [

              Container(
                height: 300,
                child: Image.asset('assets/images/language_select.jpg'),
              ),
              SizedBox(
                height: 50,
              ),
              Text(
                'សូមជ្រើសរើសភាសា',
                style: TextStyle(
                  fontSize: 20,
                  color: Colors.black
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Text(
                'Select your language',
                style: TextStyle(
                  fontSize: 20,
                  color: Colors.black
                ),
              ),

              SizedBox(
                height: 10,
              ),

              Padding(
                padding: EdgeInsets.symmetric(horizontal: 80.0),
                child:  OutlinedButton(
                  style: OutlinedButton.styleFrom(
                    backgroundColor: AppColor.primaryColor,
                    textStyle: TextStyle(color: Colors.white),
                    side: BorderSide(
                      color: Colors.grey,
                      width: 3,
                    ),
                    shape: StadiumBorder(),
                  ),
                  onPressed: () async {
                   await controller.useLanguage(Constant.khmerLanguageCode);
                  },
                  child: Row(
                    children: [
                      Container(
                        height: 50,
                        padding: EdgeInsets.all(10.0),
                        child: Image.asset('assets/flags/kh.jpg'),
                      ),
                      SizedBox(width: 10.0,),
                      Text(
                        'KHMER',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Colors.white,
                          fontSize: 20.0,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 80.0),
                child:  OutlinedButton(
                  style: OutlinedButton.styleFrom(
                    textStyle: TextStyle(color: Colors.white),
                    backgroundColor: AppColor.primaryColor,
                    side: BorderSide(
                      color: Colors.grey,
                      width: 3,
                    ),
                    shape: StadiumBorder(),
                  ),
                  onPressed: () async {
                    await controller.useLanguage(Constant.englishLanguageCode);
                  },
                  child: Row(
                    children: [
                      Container(
                        height: 50,
                        padding: EdgeInsets.all(10.0),
                        child: Image.asset('assets/flags/en.jpg'),
                      ),
                      SizedBox(width: 10.0,),
                      Text(
                        'ENGLISH',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Colors.white,
                          fontSize: 20.0,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
