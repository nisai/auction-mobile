import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mobile/core/upload_service.dart';
import 'package:mobile/documents/category_document.dart';
import 'package:mobile/documents/product_document.dart';
import 'package:mobile/shared/models/category_model.dart';
import 'package:mobile/shared/models/product_model.dart';

class PostRepository {

  final _graphQLClient = Get.find();
  final _httpClient = Get.find();
  final _upload = Get.find<UploadService>();

  Future<Map<String, dynamic>?> getCreateProductData() async {
    var result = await _graphQLClient.call()
        .query(ProductDocument.getCreateData);
    return result;
  }

  Future<List<CategoryModel>?> getChildrenByParent(final int? parentId) async {
    try {
      var result = await _graphQLClient.call()
          .query(CategoryDocument.getChildrenByParent,  variables: <String, dynamic>{
        'parentId': parentId}
      );
      if(result != null) {
       return (result['data']['Categories']['select'] as List)
           .cast<Map<String, dynamic>>()
            .map(CategoryModel.fromJson)
            .toList();
      }
    } catch(e) {

    }
    return null;
  }

  Future<ProductModel?> createProduct({final Map<String, dynamic>? input}) async{
    var result = await _graphQLClient.call(authorization: true)
        .mutation(ProductDocument.createProduct,  variables: input);
    final product =  ProductModel.fromJson(result['data']['CreateProduct'] as Map<String, dynamic>);
    print('create response ${product.toString()}');
    return product;
  }

  Future<bool> createPaymentProduct({final int? productId}) async {
    try {
      final response = await _httpClient.call(authenticated: true).post('/deposit_wallet/product/$productId/all');
      final result = response.statusCode == 200;
      print("createPaymentProduct: result $result");
      return result;
    } catch(e) {
      Get.snackbar(
          'You deposit_wallet',
         'create deposit_wallet',
          colorText: Colors.red,
          snackPosition: SnackPosition.BOTTOM
      );
    }
    return false;
  }

}
