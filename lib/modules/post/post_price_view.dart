import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:get/get.dart';
import 'package:mobile/modules/post/post_controller.dart';
import 'package:form_builder_validators/form_builder_validators.dart';

class PostPriceView extends GetView<PostController> {

  @override
  Widget build(BuildContext context) {
    return GetX<PostController>(
        builder: (controller) {
          return Scaffold(
            appBar: AppBar(
              titleSpacing: 0,
              elevation: 0,
              title: Column(
                children: [
                  Container(
                    height: 35.0,
                    padding: EdgeInsets.symmetric(horizontal: 15.0),
                    child: Row(
                      mainAxisSize: MainAxisSize.max,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Expanded(
                          flex: 1,
                          child: GestureDetector(
                            onTap: () {
                              controller.toTitleView();
                            },
                            child: Text('back_process'.tr, style: TextStyle(color: Colors.white, fontSize: 12),),
                          ),
                        ),
                        Expanded(
                            flex: 8,
                            child: Padding(
                              padding: EdgeInsets.symmetric(horizontal: 10.0),
                              child: LinearProgressIndicator(
                                value: 0.60,
                                valueColor: AlwaysStoppedAnimation<Color>(Colors.lightBlue),
                                backgroundColor: Colors.white12,
                              ),
                            )),
                        Expanded(
                          flex: 1,
                          child: GestureDetector(
                            onTap: () {
                              final bool valid = controller.priceFormKey.currentState?.saveAndValidate()?? false;
                              if(valid) {
                                controller.toPromotionView();
                              }
                            },
                            child: Text('next_process'.tr, style: TextStyle(color: Colors.white, fontSize: 12),),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    child:  Text('PRICE & DELIVERY SETTING'.tr, style: TextStyle(color: Colors.white),
                    ),
                  ),
                ],
              ),
             /* title: Container(
                color: Color(0xfff7892b),
                child: Row(
                  mainAxisSize: MainAxisSize.max,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Container(
                      width: 100.0,
                      child: TextButton(
                        onPressed: () {
                          controller.toTitleView();
                        },
                        child: Text('back_process'.tr, style: TextStyle(color: Colors.white),),
                      ),
                    ),
                    Expanded(
                        child: Padding(
                          padding: EdgeInsets.symmetric(horizontal: 10.0),
                          child: LinearProgressIndicator(
                            value: 0.20,
                            valueColor: AlwaysStoppedAnimation<Color>(Colors.lightBlue),
                            backgroundColor: Colors.white12,
                          ),
                        )),
                    Container(
                      width: 100.0,
                      child: TextButton(
                        onPressed: () {
                          final bool valid = controller.priceFormKey.currentState?.saveAndValidate()?? false;
                          if(valid) {
                            controller.toPromotionView();
                          }
                        },
                        child: Text('next_process'.tr, style: TextStyle(color: Colors.white),),
                      ),
                    ),
                  ],
                ),
              ),*/
            ),
            body: SafeArea(
              child: SingleChildScrollView(
                child: Padding(
                  padding: EdgeInsets.all(20),
                  child: FormBuilder(
                    key: controller.priceFormKey,
                    autovalidateMode: AutovalidateMode.onUserInteraction,
                    child: Column(
                      children: <Widget>[
                        FormBuilderTextField(
                          onChanged: (val) {
                            controller.onChangeOriginalPrice(val);
                          },
                          initialValue: controller.originalPrice.value,
                          name: 'originalPrice',
                          keyboardType: TextInputType.number,
                          decoration: InputDecoration(
                              fillColor: Colors.white,
                              border: OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.white, width: 5.0),
                              ),
                              focusedBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.all(Radius.circular(5.0)),
                                  borderSide: BorderSide(color: Colors.green)),
                              filled: true,
                              labelText: 'original_price'.tr,
                              labelStyle: TextStyle(fontSize: 12.0)),
                          validator: FormBuilderValidators.compose([
                            FormBuilderValidators.required(context),
                          ]),
                        ),
                        SizedBox(height: 10.0),
                        FormBuilderTextField(
                          initialValue: controller.buyNowPrice.value,
                          onChanged: (val) {
                            controller.onChangeBuyNowPrice(val);
                          },
                          name: 'buyNowPrice',
                          keyboardType: TextInputType.number,
                          decoration: new InputDecoration(
                              fillColor: Colors.white,
                              border: OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.black, width: 5.0),
                              ),
                              focusedBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.all(Radius.circular(5.0)),
                                  borderSide: BorderSide(color: Colors.green)),
                              filled: true,
                              labelText: 'buy_now_price'.tr,
                              labelStyle: TextStyle(fontSize: 12.0)),
                          //validator: FormBuilderValidators.compose([FormBuilderValidators.required(context),]),
                        ),
                        SizedBox(height: 10.0),
                        FormBuilderTextField(
                          initialValue: controller.reservePrice.value,
                          onChanged: (val) {
                            controller.onChangeReservePrice(val);
                          },
                          name: 'reservePrice',
                          keyboardType: TextInputType.number,
                          decoration: new InputDecoration(
                              fillColor: Colors.white,
                              border: OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.black, width: 5.0),
                              ),
                              focusedBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.all(Radius.circular(5.0)),
                                  borderSide: BorderSide(color: Colors.green)),
                              filled: true,
                              labelText: 'reserve_price'.tr,
                              labelStyle: TextStyle(fontSize: 12.0)),
                          validator: FormBuilderValidators.compose([
                            FormBuilderValidators.required(context),
                          ]),
                        ),
                        SizedBox(height: 15.0),
                        FormBuilderFilterChip(
                          initialValue: controller.delivery,
                          name: "delivery",
                          onChanged: (val) {
                            controller.onChangeDelivery(val);
                          },
                          selectedColor: Colors.green,
                          labelStyle: Theme.of(context).textTheme.caption,
                          decoration: new InputDecoration(
                              fillColor: Colors.white,
                              border: OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.white, width: 5.0),
                              ),
                              focusedBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.all(Radius.circular(5.0)),
                                  borderSide: BorderSide(color: Colors.green)),
                              filled: true,
                              contentPadding: EdgeInsets.only(
                                  bottom: 10.0, left: 10.0, right: 10.0),
                              labelText: 'delivery_method'.tr,
                              labelStyle: TextStyle(fontSize: 12.0)),
                          options: controller.deliveryMethods
                              .map((e) => FormBuilderFieldOption(
                              child: Text('${e.value}'), value: e.code))
                              .toList(),
                        ),
                        SizedBox(height: 15.0),
                        FormBuilderFilterChip(
                          initialValue: controller.payments,
                          name: "payments",
                          onChanged: (val) {
                            controller.onChangeDelivery(val);
                          },
                          selectedColor: Colors.green,
                          labelStyle: Theme.of(context).textTheme.caption,
                          decoration: new InputDecoration(
                              fillColor: Colors.white,
                              border: OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.white, width: 5.0),
                              ),
                              focusedBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.all(Radius.circular(5.0)),
                                  borderSide: BorderSide(color: Colors.green)),
                              filled: true,
                              contentPadding: EdgeInsets.only(
                                  bottom: 10.0, left: 10.0, right: 10.0),
                              labelText: 'payment method'.tr,
                              labelStyle: TextStyle(fontSize: 12.0)),
                          options: controller.paymentMethods
                              .map((e) => FormBuilderFieldOption(
                              child: Text('${e.value}'), value: e.code))
                              .toList(),
                        ),
                        SizedBox(height: 15.0),
                        FormBuilderChoiceChip<String?>(
                          initialValue: controller.bidDuration.value,
                          onChanged: (val) {
                            controller.onChangeBidDuration(val);
                          },
                          validator: FormBuilderValidators.compose(
                              [FormBuilderValidators.required(context)]),
                          selectedColor: Colors.green,
                          labelStyle: Theme.of(context).textTheme.caption,
                          decoration: new InputDecoration(
                              fillColor: Colors.white,
                              border: OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.white, width: 5.0),
                              ),
                              focusedBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.all(Radius.circular(5.0)),
                                  borderSide: BorderSide(color: Colors.green)),
                              filled: true,
                              contentPadding: EdgeInsets.only(
                                  bottom: 10.0, left: 10.0, right: 10.0),
                              labelText: 'bid_duration'.tr,
                              labelStyle: TextStyle(fontSize: 12.0)),
                          name: "bidDuration",
                          //validator: FormBuilderValidators.compose([FormBuilderValidators.required(context),]),
                          options: controller.bidDurations
                              .map((e) => FormBuilderFieldOption(
                              child: Text('${e.value}'),
                              value: e.code))
                              .toList(),
                          // options: widget.provider.bidDurations.map((e) => FormBuilderFieldOption(child: Text(e.name), value: e.value,)).toList(),
                        ),
                        SizedBox(height: 15.0),

                      ],
                    ),
                  ),
                ),
              ),
            ),
            bottomNavigationBar: Container(
              color: Color(0xfff7892b),
              child: Row(
                mainAxisSize: MainAxisSize.max,
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Container(
                    width: 100.0,
                    child: TextButton(
                      onPressed: () {
                        controller.toTitleView();
                      },
                      child: Text('back_process'.tr, style: TextStyle(color: Colors.white),),
                    ),
                  ),
                  Expanded(
                      child: Padding(
                        padding: EdgeInsets.symmetric(horizontal: 10.0),
                        child: LinearProgressIndicator(
                          value: 0.60,
                          valueColor: AlwaysStoppedAnimation<Color>(Colors.lightBlue),
                          backgroundColor: Colors.white12,
                        ),
                      )),
                  Container(
                    width: 100.0,
                    child: TextButton(
                      onPressed: () {
                        final bool valid = controller.priceFormKey.currentState?.saveAndValidate()?? false;
                        if(valid) {
                          controller.toPromotionView();
                        }
                      },
                      child: Text('next_process'.tr, style: TextStyle(color: Colors.white),),
                    ),
                  ),
                ],
              ),
            ),
          );
        });
  }
}
