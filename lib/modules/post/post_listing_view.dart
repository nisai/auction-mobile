import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mobile/modules/post/post_camera_view.dart';
import 'package:mobile/modules/post/post_controller.dart';
import 'package:mobile/modules/post/post_photo_view.dart';
import 'package:mobile/modules/post/post_price_view.dart';
import 'package:mobile/modules/post/post_promotion_view.dart';
import 'package:mobile/modules/post/post_title_view.dart';
class PostListingView extends GetView<PostController> {

  @override
  Widget build(BuildContext context) {
    return GetBuilder<PostController>(
        builder: (controller) {
          switch(controller.selectedPage.value) {
            case 0: return PostCameraView();
            case 1:  return PostPhotoView();
            case 2:  return PostTitleView();
            case 3:  return PostPriceView();
            case 4:  return PostPromotionView();
          }
          return Container();
        }
    );

  }
}