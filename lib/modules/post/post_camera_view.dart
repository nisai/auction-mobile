import 'dart:io';

import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mobile/modules/post/post_controller.dart';
import 'package:mobile/shared/models/image_model.dart';

class PostCameraView extends GetView<PostController> {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<PostController>(
        builder: (controller) {
          return Scaffold(
            body: CameraPreview(controller.controllerCamera,
                child: Stack(
                  children: <Widget>[
                    Positioned(
                        top: 10.0,
                        right: 10.0,
                        child: Padding(
                          padding: EdgeInsets.only(top: 40.0),
                          child: Material(
                            color: Colors.transparent,
                            child: GestureDetector(
                              onTap:controller.handleCameraSwitch,
                              child: Container(
                                padding: EdgeInsets.all(4.0),
                                child: Image.asset(
                                  'assets/images/ic_switch_camera_3.png',
                                  color: Colors.grey[200],
                                  width: 42.0,
                                  height: 42.0,
                                ),
                              ),
                            ),
                          ),
                        )
                    ),

                    Positioned(
                        top: 10.0,
                        left: 10.0,
                        child: Padding(
                          padding: EdgeInsets.only(top: 40.0),
                          child: Material(
                            color: Colors.transparent,
                            child: GestureDetector(
                              onTap: () {
                                Get.rootDelegate.popRoute(popMode: PopMode.History);
                              },
                              child: Container(
                                padding: EdgeInsets.all(4.0),
                                child: Icon(Icons.close, color: Colors.red,  size: 30,),
                              ),
                            ),
                          ),
                        )
                    ),

                    Positioned(
                      bottom: 0.0,
                      left: 0.0,
                      right: 0.0,
                      child: Column(
                        children: <Widget>[
                          Obx(() => _previewImages(controller.uploadImages)),
                          Row(
                            children: [
                              Expanded(flex: 4, child: Padding(
                                padding: EdgeInsets.symmetric(horizontal: 10.0),
                                child: Material(
                                  borderRadius: BorderRadius.all(Radius.circular(50.0)),
                                  color: Colors.transparent,
                                  child: ButtonTheme(
                                    buttonColor: Color(0xfff7892b),
                                    minWidth: 100.0,
                                    height: 50.0,
                                    child: ElevatedButton(
                                      onPressed: () async {
                                        controller.handleBrowserGallery();
                                      },
                                      child: Text('gallery'.tr, style: TextStyle(color: Colors.white)),
                                    ),
                                  ),
                                ),
                              )),
                              Expanded(
                                flex: 2,
                                child: _buildCameraTakeButton(),
                              ),
                              Expanded(flex: 4, child: Padding(
                                padding: EdgeInsets.symmetric(horizontal: 10.0),
                                child: Material(
                                  color: Colors.transparent,
                                  borderRadius: BorderRadius.all(Radius.circular(50.0)),
                                  child: ButtonTheme(
                                    buttonColor: Color(0xfff7892b),
                                    minWidth: 100.0,
                                    height: 50.0,
                                    child: ElevatedButton(
                                      onPressed: () {
                                        controller.toPhotoView();
                                      },
                                      child: Text('next_process'.tr, style: TextStyle(color: Colors.white)),
                                    ),
                                  ),
                                ),
                              )),
                            ],
                          )
                        ],
                      ),
                    ),
                  ],
                )),
          );
        }
    );

  }

  Widget _buildCameraTakeButton() {
    return Padding(
        padding: EdgeInsets.symmetric(horizontal: 10.0),
        child:  Material(
          borderRadius: BorderRadius.all(Radius.circular(50.0)),
          color: Colors.transparent,
          child: InkWell(
            onTap: () {
              controller.handleTakePicture();
            },
            child: Container(
              padding: EdgeInsets.all(4.0),
              child: Image.asset(
                'assets/images/ic_shutter_1.png',
                width: 60.0,
                height: 60.0,
              ),
            ),
          ),
        )
    );
  }

  Widget _previewImages(final List<ImageModel> images) {
    if(images.isEmpty) {
      return Container();
    }
    return SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      child: Row(children: images.map(_previewImage).toList(),
      ),
    );
  }

  Widget _previewImage(final ImageModel item) {
    return Stack(
      children: <Widget>[
        Padding(
          padding: EdgeInsets.all(10.0),
          child: Container(width: 60, height: 60, child: Image.file(File(item.image), fit: BoxFit.cover)),
        ),
        Positioned(
          right: 0,
          top: 10,
          child: GestureDetector(
            child: Icon(
              Icons.remove_circle,
              size: 20,
              color: Colors.red,
            ),
            onTap: () {
              controller.handleRemovePicture(item);
            },
          ),
        ),
      ],
    );
  }
}