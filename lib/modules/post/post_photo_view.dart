import 'dart:io';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mobile/core/theme/colors.dart';
import 'package:mobile/modules/post/post_controller.dart';

class PostPhotoView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<PostController>(
        builder: (controller) {
          return Scaffold(
            appBar: AppBar(
              titleSpacing: 0,
              elevation: 0,
              title: Column(
                children: [
                  Container(
                    height: 35.0,
                    padding: EdgeInsets.symmetric(horizontal: 15.0),
                    child: Row(
                      mainAxisSize: MainAxisSize.max,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Expanded(
                          flex: 1,
                          child: GestureDetector(
                            onTap: () {
                              controller.toCameraView();
                            },
                            child: Text('back_process'.tr, style: TextStyle(color: Colors.white, fontSize: 12),),
                          ),
                        ),
                        Expanded(
                          flex: 8,
                            child: Padding(
                              padding: EdgeInsets.symmetric(horizontal: 10.0),
                              child: LinearProgressIndicator(
                                value: 0.20,
                                valueColor: AlwaysStoppedAnimation<Color>(Colors.lightBlue),
                                backgroundColor: Colors.white12,
                              ),
                            )),
                        Expanded(
                          flex: 1,
                          child: GestureDetector(
                            onTap: () {
                              controller.toTitleView();
                            },
                            child: Text('next_process'.tr, style: TextStyle(color: Colors.white, fontSize: 12),),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    child:  Text('SELECT PHOTO'.tr, style: TextStyle(color: Colors.white),
                    ),
                  ),
                ],
              ),
            ),
            body: SafeArea(
              child: SingleChildScrollView(
                child: GridView.builder(
                    physics: NeverScrollableScrollPhysics(),
                    shrinkWrap: true,
                    itemCount: controller.uploadImages.length,
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 3,
                      crossAxisSpacing: 0.2,
                      mainAxisSpacing: 10,
                      childAspectRatio: 0.6,
                    ),
                    itemBuilder: (BuildContext context, int index) {
                      final item = controller.uploadImages[index];
                      return Container(
                        padding: EdgeInsets.only(top: 5.0),
                        child: Center(
                          child: Stack(
                            children: <Widget>[
                              Image.file(File(item.image), fit: BoxFit.cover),
                              Positioned(
                                top: 0,
                                right: 0,
                                child: GestureDetector(
                                  onTap: (){
                                    controller.handleRemovePicture(item);
                                  },
                                  child: Icon(
                                    Icons.delete,
                                    color: Colors.red,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      );
                    }
                ),
              ),
            ),
            floatingActionButton: FloatingActionButton(
              onPressed: () {
                controller.handleBrowserGallery();
              },
              child: Icon(Icons.photo_album),
              backgroundColor: AppColor.primaryColor,
            ),
            bottomNavigationBar: Container(
              color: Color(0xfff7892b),
              child: Row(
                mainAxisSize: MainAxisSize.max,
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Container(
                    width: 100.0,
                    child: TextButton(
                      onPressed: () {
                        controller.toCameraView();
                      },
                      child: Text('back_process'.tr, style: TextStyle(color: Colors.white),),
                    ),
                  ),
                  Expanded(
                      child: Padding(
                        padding: EdgeInsets.symmetric(horizontal: 10.0),
                        child: LinearProgressIndicator(
                          value: 0.20,
                          valueColor: AlwaysStoppedAnimation<Color>(Colors.lightBlue),
                          backgroundColor: Colors.white12,
                        ),
                      )),
                  Container(
                    width: 100.0,
                    child: TextButton(
                      onPressed: () {
                        controller.toTitleView();
                      },
                      child: Text('next_process'.tr, style: TextStyle(color: Colors.white),),
                    ),
                  ),
                ],
              ),
            ),
          );
    });
  }
}
