import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:mobile/utils/constant.dart';
import 'package:mobile/routes/app_pages.dart';

class SplashController extends GetxController {

  static const splashDelay = 3; // should be 5, 1 is for develop

  var ready = false.obs;

  final box = GetStorage();

  bool get isFirstOpen => box.read(Constant.firstOpenKey) ?? true;

  @override
  void onInit() {

    ever(ready, goNext);

    loadData().then((_){
      print('splash loaded');
      update();
    });

    super.onInit();
  }

  Future<void> loadData() async {
    await Future.delayed(Duration(seconds: splashDelay));
    ready(true);
  }

  void goNext(bool callback) async {
    if(isFirstOpen) {
      // open first open to true
      await box.write(Constant.firstOpenKey, false);
      WidgetsBinding.instance.addPostFrameCallback((_) {
        Get.offAllNamed(Routes.LANGUAGE);
      });

    } else {
      WidgetsBinding.instance.addPostFrameCallback((_) {
        Get.rootDelegate..offNamed(Routes.DASHBOARD);
      });
    }
  }

}