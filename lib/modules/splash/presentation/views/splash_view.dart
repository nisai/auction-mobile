import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mobile/modules/splash/presentation/controllers/splash_controller.dart';
class SplashView extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return GetBuilder<SplashController>(builder: (controller) {
          return SafeArea(
            child: Scaffold(
              body: Container(
                height: MediaQuery.of(context).size.height,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(5)),
                    boxShadow: <BoxShadow>[
                      BoxShadow(
                          color: Colors.grey.shade200,
                          offset: Offset(2, 4),
                          blurRadius: 5,
                          spreadRadius: 2)
                    ],
                    gradient: LinearGradient(
                        begin: Alignment.topCenter,
                        end: Alignment.bottomCenter,
                        colors: [Color(0xfffbb448), Color(0xffe46b10)])),
                child: Center(
                  child: Image.asset(
                    "assets/icon/icon.png",
                  ),
                ),
              ),
            ),
          );
        }
    );
  }
}
