import 'package:adaptive_dialog/adaptive_dialog.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mobile/modules/product_buying/product_buying_controller.dart';
import 'package:mobile/routes/app_pages.dart';

class ProductBuyingView extends GetView<ProductBuyingController>{

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Buy now ",
          style: TextStyle(color: Colors.white),
        ),
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () {
            Get.rootDelegate.offNamed(Routes.productDetail(productId: controller.productId));
          },
        ),
      ),
      body: GetBuilder<ProductBuyingController>(builder: (controller) {
        return SingleChildScrollView(
          child: SafeArea(
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 15),
              child: Column(
                children: [
                  SizedBox(
                    height: 5.0,
                  ),
                  Text(
                    '${controller.product.name}',
                    style: TextStyle(fontSize: 24),
                    textAlign: TextAlign.left,
                  ),
                  SizedBox(
                    height: 5.0,
                  ),
                  Row(
                    mainAxisSize: MainAxisSize.max,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                          width: 100.0,
                          child: Text('price')
                      ),
                      Container(
                          width: 100.0,
                          child: Text('${controller.product.buyNowPrice}')
                      )
                    ],
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                ],
              ),
            ),
          ),
        );
      },),
      bottomNavigationBar: Container(
        color: Color(0xfff7892b),
        child: Row(
          mainAxisSize: MainAxisSize.max,
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Container(
              width: 100.0,
              child: TextButton(
                onPressed: () {
                  Get.rootDelegate.popRoute(popMode: PopMode.History);
                },
                child: Text('Cancel', style: TextStyle(color: Colors.white),),
              ),
            ),

            Container(
              width: 150.0,
              child: TextButton(
                onPressed: () async {
                  final OkCancelResult result = await showOkCancelAlertDialog(
                    context: context,
                    title: 'confirm your purchase now',
                    message: 'Do you really want to continue?',
                    isDestructiveAction: true,
                  );
                  if(result == OkCancelResult.ok) {
                    await controller.handleBuyNow(context);
                  }
                },
                child: Text('Purchase Now', style: TextStyle(color: Colors.white)),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
