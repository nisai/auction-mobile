import 'dart:developer';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:mobile/repositories/product_repository.dart';
import 'package:mobile/repositories/user_repository.dart';
import 'package:mobile/routes/app_pages.dart';
import 'package:mobile/shared/models/product_model.dart';
import 'package:mobile/shared/models/user_model.dart';
import 'package:mobile/utils/customLoader.dart';

class ProductBuyingController extends GetxController {

  final _loader = CustomLoader();

  var _product = ProductModel().obs;

  ProductModel get product => _product.value;

  ProductBuyingController({required final this.productId, required this.userRepository, required this.productRepository});

  final String productId;
  final ProductRepository productRepository;
  final UserRepository userRepository;

  final bidController = TextEditingController().obs;
  final commentController = TextEditingController().obs;

  var _loading = false.obs;

  bool get loading => _loading.value;


  @override
  void onInit() {
    print('onReady');

    log('product id is $productId');

    _loadProduct().then((value) {
      _loadProductDetail();
    });

    super.onInit();
  }

  Future<void> _loadProduct() async {
    try {
      _loading(true);
      update();

      final result = await productRepository.getProductById(
          productId: int.tryParse(productId));
      if (result != null) {
        _product(result);
        print('product is : ${result.toString()}');
      }

      _loading(false);
      update();
    } catch (e) {
      print('error load product $e');
    }
  }

  Future<void> _loadProductDetail() async {
    _loading(true);
    update();

    final UserModel? user = await userRepository.getCurrentUser();
    final resultDetail = await productRepository.getProductDetails(
        productId: int.tryParse(productId),
        categoryId: product.category?.id,
        userId: user?.id ?? 0);

    if (resultDetail != null) {
      final data = resultDetail['data'];

      _loading(false);
      update();
    }
  }

  Future<void> handleBuyNow(final BuildContext context) async {
    try {
      _loader.showLoader(context);
      final value = await productRepository.buyProduct(int.tryParse(productId));
      _loader.hideLoader();
      if (value) {
        Get.rootDelegate.offNamed(Routes.BUYING_ITEM);
      }
    } catch (e) {

    } finally {
    }
  }

  @override
  void onClose() {
    _loader.dispose();

    super.onClose();
  }
}
