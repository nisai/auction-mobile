import 'package:get/get.dart';
import 'package:mobile/modules/product_buying/product_buying_controller.dart';
import 'package:mobile/repositories/product_repository.dart';
import 'package:mobile/repositories/user_repository.dart';

class ProductBuyingBinding extends Bindings {
  @override
  void dependencies() {

    Get.lazyPut<UserRepository>(() => UserRepository());
    Get.lazyPut<ProductRepository>(() => ProductRepository());
    Get.lazyPut<ProductBuyingController>(
          () => ProductBuyingController(
              productId: Get.parameters['productId'] ?? '',
              userRepository: Get.find<UserRepository>(),
              productRepository: Get.find<ProductRepository>()
      ),
    );
  }
}
