import 'package:get/get.dart';
import 'package:mobile/core/upload_service.dart';
import 'package:mobile/modules/post_edit/post_edit_controller.dart';
import 'package:mobile/repositories/user_account_repository.dart';
import 'package:mobile/repositories/category_repository.dart';
import 'package:mobile/repositories/payment_repository.dart';
import 'package:mobile/repositories/product_repository.dart';

class PostEditBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<UserAccountRepository>(() => UserAccountRepository());
    Get.lazyPut<ProductRepository>(() => ProductRepository());
    Get.lazyPut<CategoryRepository>(() => CategoryRepository());
    Get.lazyPut<PaymentRepository>(() => PaymentRepository());
    Get.lazyPut<UploadService>(() => UploadService());
    Get.lazyPut<PostEditController>(
            () => PostEditController(
                productId: Get.parameters['productId'] ?? '',
                productRepository: Get.find(),
                categoryRepository: Get.find(),
                paymentRepository: Get.find(),
                userAccountRepository: Get.find(),
                uploadService: Get.find())
    );
  }
}
