import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mobile/modules/post_edit/post_edit_camera_view.dart';
import 'package:mobile/modules/post_edit/post_edit_controller.dart';
import 'package:mobile/modules/post_edit/post_edit_photo_view.dart';
import 'package:mobile/modules/post_edit/post_edit_price_view.dart';
import 'package:mobile/modules/post_edit/post_edit_promotion_view.dart';
import 'package:mobile/modules/post_edit/post_edit_title_view.dart';

class PostEditView extends GetView<PostEditController> {

  @override
  Widget build(BuildContext context) {
    return GetBuilder<PostEditController>(
        builder: (controller) {
          switch(controller.selectedPage.value) {
            case 0: return PostEditCameraView();
            case 1:  return PostEditPhotoView();
            case 2:  return PostEditTitleView();
            case 3:  return PostEditPriceView();
            case 4:  return PostEditPromotionView();
          }
          return Container();
        }
    );
  }
}