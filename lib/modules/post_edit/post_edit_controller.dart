import 'package:camera/camera.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:get/get.dart';
import 'package:mobile/constant/fee_resolver.dart';
import 'package:mobile/core/upload_service.dart';
import 'package:mobile/repositories/category_repository.dart';
import 'package:mobile/repositories/payment_repository.dart';
import 'package:mobile/repositories/product_repository.dart';
import 'package:mobile/repositories/user_account_repository.dart';
import 'package:mobile/routes/app_pages.dart';
import 'package:mobile/shared/models/category_model.dart';
import 'package:mobile/shared/models/code_value.dart';
import 'package:mobile/shared/models/image_model.dart';
import 'package:mobile/shared/models/product_model.dart';
import 'package:mobile/shared/models/product_promotion_model.dart';
import 'package:mobile/shared/models/promotion_model.dart';
import 'package:mobile/shared/models/province_model.dart';
import 'package:mobile/utils/constant.dart';
import 'package:mobile/utils/customLoader.dart';
import 'package:mobile/utils/permission_utils.dart';
import 'package:image_picker/image_picker.dart';

class PostEditController extends GetxController {

  final _picker = ImagePicker();
  final _loader = CustomLoader();

  final titleFormKey = GlobalKey<FormBuilderState>();
  final priceFormKey = GlobalKey<FormBuilderState>();
  final shippingFormKey = GlobalKey<FormBuilderState>();
  final promotionFormKey = GlobalKey<FormBuilderState>();

  var selectedPage = 1.obs;

  var _loading = false.obs;
  bool get loading => _loading.value;

  var _product = ProductModel().obs;
  ProductModel get product => _product.value;

  var _productPromotion = ProductPromotionModel().obs;

  ProductPromotionModel get productPromotion => _productPromotion.value;

  var _allowPromote = false.obs;
  bool get allowPromote => _allowPromote.value;

  var children = <CategoryModel>[].obs;
  var parents = <CategoryModel>[].obs;
  var provinces = <ProvinceModel>[].obs;
  var promotions = <PromotionModel>[].obs;
  var configs = <CodeValue>[].obs;

  var uploadImages = <ImageModel>[].obs;

  var cameras = <CameraDescription>[].obs;

  var _controllerCamera = CameraController(
    CameraDescription(
      name: "name",
      lensDirection: CameraLensDirection.back,
      sensorOrientation: 0,
    ),
    ResolutionPreset.low,
    enableAudio: false,
    imageFormatGroup: ImageFormatGroup.jpeg,
  ).obs;


  CameraController get controllerCamera => _controllerCamera.value;

  final paymentCodeFormKey = GlobalKey<FormState>();

  var isPosting = false.obs;

  bool get receivePostData => provinces.isNotEmpty;

  // var activeTitle = true.obs;
  // var activePrice = false.obs;
  // var activeShipping = false.obs;
  var activePromotion = false.obs;

  var promotionId = 0.obs;

  final provinceTextController = TextEditingController();

  var postParent = 0.obs;
  var postChildren = 0.obs;
  var postProvince = 0.obs;
  var postTitle = ''.obs;
  var postDescription = ''.obs;

  var originalPrice = ''.obs;
  var reservePrice = ''.obs;
  var buyNowPrice = ''.obs;

  var delivery = [].obs;
  var bidDuration = ''.obs;

  var promotionType = 0.obs;

  var subTitle = ''.obs;

  var useSubTitle = false.obs;

  var _showChildren = false.obs;
  bool get showChildren => _showChildren.value;

  var promotionFee = 0.0.obs;

  var durationFee =  0.0.obs;
  var subTitleFee =  0.0.obs;

  double get totalFee =>  durationFee.value + subTitleFee.value + promotionFee.value;
  bool get postHasFee =>  totalFee > 0.1;

  var _accountBalance = 0.0.obs;
  double get accountBalance => _accountBalance.value;

  bool get reachedMaxImage => uploadImages.length == Constant.maxPostImage;

  PostEditController({
    required this.productId,
    required this.productRepository,
    required this.paymentRepository,
    required this.categoryRepository,
    required this.userAccountRepository,
    required this.uploadService});
  final String productId;
  final ProductRepository productRepository;
  final PaymentRepository paymentRepository;
  final CategoryRepository categoryRepository;
  final UploadService uploadService;
  final UserAccountRepository userAccountRepository;

  @override
  void onInit()  {
    // on update
    print('PostEditController init:');

    _handleLoadData().then((value){
      update();
    });

    _handleInitCamera().then((value) {
      update();
    });

    super.onInit();
  }


  Future<void> _handleLoadData() async {
   final result = await productRepository.getCreateProductData();
   if(result != null) {
    final data =  result['data'] as Map<String, dynamic>;
    parents.value = (data['categories']['select'] as List)
        .cast<Map<String, dynamic>>()
        .map(CategoryModel.fromJson)
        .toList();

    provinces.value = (data['provinces']['select'] as List)
        .cast<Map<String, dynamic>>()
        .map(ProvinceModel.fromJson)
        .toList();

    promotions.value = (data['promotions']['select'] as List)
        .cast<Map<String, dynamic>>()
        .map(PromotionModel.fromJson)
        .toList();
    promotions.add(PromotionModel(id: 0, name: 'None', detail: "I don't want to promote it now"));

    configs.value = (data['SystemConfigs']['select'] as List)
        .cast<Map<String, dynamic>>()
        .map(CodeValue.fromJson)
        .toList();

    final userAccount = await userAccountRepository.getUserAccount();
    _accountBalance(userAccount?.amount?? 0);

    final ProductModel? getProduct = await productRepository.getProductById(productId: int.tryParse(productId));
    print('getProduct: ${getProduct?.toMap()}');
    if(getProduct != null) {
      _product(getProduct);
      getProduct.slideImage?.forEach((e) {
        uploadImages.add(ImageModel(image: e, uploaded: true));
      });

      onChangeTitle(getProduct.name).then((value) {
        print('onChangeTitle callback');
      });

      postDescription(getProduct.description);

      originalPrice('${getProduct.originalPrice}');
      reservePrice('${getProduct.reservePrice}');
      buyNowPrice('${getProduct.buyNowPrice}');

      if(getProduct.hasSubTitle) {
        useSubTitle(true);
        subTitle(getProduct.subTitle);
      }

      onChangeDelivery(getProduct.delivery).then((_) {
        print('onChangeDelivery callback');
      });
      onChangeBidDuration(getProduct.bidDuration).then((_) {
        print('onChangeBidDuration callback');
      });

      postProvince(getProduct.province?.id);
      final isParent = getProduct.category?.firstLevel?? false;
      if(isParent) {
        postParent(getProduct.category?.id);
      } else {
        final findParent = await categoryRepository.getParentByChildId(getProduct.category?.id);
        await onChangeParent(findParent?.id);
        postChildren(getProduct.category?.id);
      }

      final ProductPromotionModel? _resultProductPromotion = await productRepository.getProductPromotion(productId: int.tryParse(productId));
      _productPromotion(_resultProductPromotion);

      if(productPromotion.promotion != null) {
        onChangePromotionType(productPromotion.promotion?.id).then((_) {
          print('onChangePromotionType: callback');
        });
      }

    }

   }
  }

  // ensure has data for create

  void toCameraView() async {
    selectedPage.value = 0;
    update();
  }

  void toPhotoView() async {
    if(!receivePostData) {

    }
    selectedPage.value = 1;
    update();
  }


  void toTitleView() async {
    selectedPage.value = 2;
    update();
  }

  void toPriceView() async {

    selectedPage.value = 3;
    update();
  }

  void toPromotionView() async {
    selectedPage.value = 4;
    update();
  }

  Future<void> startPosting(final BuildContext context) async {
    try {
      _loader.showLoader(context);
      print('start posting');
      isPosting(true);
      final ProductModel? product = await _postProduct();

      isPosting(false);
      _loader.hideLoader();
      if(product != null) {
        if(postHasFee) {
            Get.rootDelegate.offNamed(Routes.productPayment(productId: '${product.id}'));
        } else {
            Get.rootDelegate.offNamed(Routes.productDetail(productId: '${product.id}'));
        }
      }
    } catch(e) {
      print('e $e');
    }
  }

  void handleRemovePicture(final ImageModel image) async {
    uploadImages.remove(image);
  }

  void handleCameraSwitch() {
    _handleCameraChange().then((value) {
      update();
    });
  }

  Future<bool?> _handleCameraChange() async{
    try {

      final lensDirection = _controllerCamera.value.description.lensDirection;
      var usingCamera;

      if(lensDirection == CameraLensDirection.front) {
        usingCamera = cameras.firstWhere((description) => description.lensDirection == CameraLensDirection.back);
      } else {
        usingCamera = cameras.firstWhere((description) => description.lensDirection == CameraLensDirection.front);
      }
      if(usingCamera != null){
        await _controllerCamera.value.dispose();
        final controller = await _initCamera(usingCamera);
        _controllerCamera(controller);
      } else {
        print('Asked camera not available');
      }
    } catch (e) {
      print('switchCamera $e');
    } finally {

    }
    return null;
  }

  Future<void> handleTakePicture() async {
    if(reachedMaxImage) {
      Get.snackbar('Your image reached maximum', 'We allow only ${Constant.maxPostImage} pictures for one listing', snackPosition: SnackPosition.TOP);
      return;
    }
    final valid = _controllerCamera.value.value.isInitialized && !_controllerCamera.value.value.isTakingPicture;
    try {
      if(valid) {
        final file = await _controllerCamera.value.takePicture();
        uploadImages.add(new ImageModel(image: file.path, uploaded: false));
      } else {
        print('not valid action ');
      }
    } on CameraException catch (e) {
      print('error ${e.description}');
    }
  }

  Future<bool> _handleInitCamera() async {
    try {
      final allowCamera = await hasPermissionCamera();
      if(allowCamera) {
        final _cameras = await availableCameras();
        cameras(_cameras);
        final usingCamera = cameras[0];
        final controller = await _initCamera(usingCamera);
        _controllerCamera(controller);
        return true;
      }
    } catch (e) {
      print('error fetch camera $e');
    }
    return false;
  }

  Future<CameraController?> _initCamera(final CameraDescription cameraDescription) async {
    final controller = CameraController(
      cameraDescription,
      ResolutionPreset.low,
      enableAudio: false,
      imageFormatGroup: ImageFormatGroup.jpeg,
    );
    try {
      await controller.initialize();
      return controller;
    } catch(e) {
      print('error when init camera $e');
    }
    return null;
  }

  Future<void> handleBrowserGallery() async {
    try {
      final List<XFile>? images = await _picker.pickMultiImage();
      images?.forEach((item) {
        if(!reachedMaxImage) {
          uploadImages.add(ImageModel(image: item.path, uploaded: false));
        }
      });

      if(reachedMaxImage) {
        Get.snackbar('Your image reached maximum', 'We allow only ${Constant.maxPostImage} pictures for one listing', snackPosition: SnackPosition.TOP);
      }
    } catch (e) {
      print("Error $e");
    } finally {

    }
  }

  Future<void> onChangeParent(final int? parentId) async {
    postParent(parentId);
    postChildren(0);
    final _children = await categoryRepository.getChildrenByParent(parentId);
    children(_children);
    _showChildren(_children?.isNotEmpty);
  }

  Future<void> onChangeChildren(final int? childrenId) async {
    postChildren(childrenId);
    update();
  }

  void onChangeProvince(final int? val) {
    postProvince(val);
    update();
  }

  Future<void> onChangeTitle(final String? val) async {
    postTitle(val);
    update();
  }

  void onChangeDescription(final String? val) {
    postDescription(val);
    update();
  }

  void onChangeOriginalPrice(final String? value) {
    originalPrice(value);
    update();
  }

  void onChangeSubTitle(final String? value) {
    subTitle(value);
    update();
  }

  void onChangeReservePrice(final String? value) {
    reservePrice(value);
    update();
  }

  void onChangeBuyNowPrice(final String? value) {
    buyNowPrice(value);
    update();
  }

  Future<void> onChangeDelivery(final List? value) async {
    delivery(value);
    update();
  }

  Future<void> onChangeBidDuration(final String? value) async {
    bidDuration(value);
    if(value != null && value == 'THIRTY') {
      var _durationFee = FeeResolver.getBid30DayFee(list: configs);
      durationFee(_durationFee);
    } else if(value != null && value == 'FOURTEEN') {
      var _durationFee = FeeResolver.getBid14DayFee(list: configs);
      durationFee(_durationFee);
    } else {
      durationFee(0.0);
    }
    update();
  }

  Future<void> onChangePromotionType(final int? id) async {
    promotionType(id);
    final promotion = promotions.firstWhereOrNull((element) => element.id == id);
    if(promotion != null && promotion.fee != null) {
      promotionFee(promotion.fee);
      activePromotion(true);
      promotionId(promotion.id);
    } else {
      promotionFee(0.0);
      activePromotion(false);
      promotionId(0);
    }
    update();
  }

  void onChangeUseSubTitle(bool? val) {
    useSubTitle(val);
    if(useSubTitle.value) {
      var _subTitleFee = FeeResolver.getSubTitleFee(list: configs);
      subTitleFee(_subTitleFee);
    } else {
      subTitleFee(0.0);
    }
    update();
  }

  Future<ProductModel?> _postProduct() async {

    try {
      var slideImage = <String>[];
      for(var imageModel in uploadImages) {
        if(!imageModel.uploaded) {
          String? path = await uploadService.uploadFile(imagePath: imageModel.image);
          if(path != null) {
            slideImage.add(path);
          }
        } else {
          slideImage.add(imageModel.image);
        }
      }

      final Map<String, dynamic> input = {
        'id': productId,
        'name': postTitle.value,
        'slideImage': slideImage,
        'originalPrice': originalPrice.value,
        'reservePrice':reservePrice.value,
        'buyNowPrice': buyNowPrice.value,
        'subTitle': subTitle.value,
        'delivery': delivery.toList(),
        'bidDuration': bidDuration.value,
        'description': postDescription.value,
        'provinceId':  postProvince.value,
        'categoryId':  postChildren.value == 0?  postParent.value: postChildren.value,
      };

      print('update input: $input');
      ProductModel? product = await  productRepository.updateProduct(input: input);

      if(activePromotion.value && promotionId.value != 0)
      {
        if(productPromotion.id != null) {
          final bool? result = await productRepository.deleteProductPromotion(id: productPromotion.id);
          print('result delete promotion $result');
        }

        await productRepository.createProductPromotion(productId: int.tryParse(productId), promotionId: promotionId.value);
      } else {
        // if update not select any promotion

        if(productPromotion.id != null) {
          final bool? result = await productRepository.deleteProductPromotion(id: productPromotion.id);
          print('result delete promotion $result');
        }
      }

      return  product;
    } catch(e) {

    }
    return null;
  }

  @override
  void onClose() async {
    await _controllerCamera.value.dispose();
    super.onClose();
  }

  List<CodeValue> get deliveryMethods => [
    CodeValue(value: 'FREE', code: 'FREE'),
    CodeValue(value: 'PICK UP', code: 'PICK_UP'),
    CodeValue(value: 'BUYER ARRANGE', code: 'BUYER_ARRANGE'),
    CodeValue(value: 'SELLER ARRANGE', code: 'SELLER_ARRANGE'),
  ];

  List<CodeValue> get bidDurations => [
    CodeValue(value: '1 day', code: 'ONE'),
    CodeValue(value: '3 days', code: 'THREE'),
    CodeValue(value: '7 days', code: 'SEVEN'),
    CodeValue(value: '14 days', code: 'FOURTEEN'),
    CodeValue(value: '30 days', code: 'THIRTY'),
  ];

}
