import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:get/get.dart';
import 'package:mobile/modules/post_edit/post_edit_controller.dart';
import 'package:mobile/shared/widgets/simple_loading.dart';
import 'package:form_builder_validators/form_builder_validators.dart';

class PostEditPromotionView extends GetView<PostEditController> {

  @override
  Widget build(BuildContext context) {
    return GetBuilder<PostEditController>(
        builder: (controller) {
          return Scaffold(
            appBar: AppBar(
              elevation: 0,
              title: Container(
                color: Color(0xfff7892b),
                child: Row(
                  mainAxisSize: MainAxisSize.max,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Container(
                      width: 100.0,
                      child: TextButton(
                        onPressed: () {
                          controller.toPriceView();
                        },
                        child: Text('back_process'.tr, style: TextStyle(color: Colors.white),),
                      ),
                    ),
                    Expanded(
                        child: Padding(
                          padding: EdgeInsets.symmetric(horizontal: 10.0),
                          child: LinearProgressIndicator(
                            value: 1.0,
                            valueColor: AlwaysStoppedAnimation<Color>(Colors.lightBlue),
                            backgroundColor: Colors.white12,
                          ),
                        )),
                    Container(
                      width: 100.0,
                      child: TextButton(
                        onPressed: () {
                          final bool valid = controller.promotionFormKey.currentState?.validate()?? false;
                          if(valid) {
                            controller.startPosting(context);
                          }
                        },
                        child: Text('start'.tr, style: TextStyle(color: Colors.white),),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            body: controller.loading?
            Center(child: SimpleLoading())
            : SafeArea(
              child: SingleChildScrollView(
                child: Padding(
                  padding: EdgeInsets.all(20),
                  child: FormBuilder(
                    key: controller.promotionFormKey,
                    autovalidateMode: AutovalidateMode.onUserInteraction,
                    child: Column(
                      children: <Widget>[
                        FormBuilderRadioGroup<int?>(
                          initialValue: controller.promotionType.value,
                          name: 'promotion_type',
                          onChanged: (val) {
                            controller.onChangePromotionType(val);
                          },
                          orientation: OptionsOrientation.vertical,
                          decoration: new InputDecoration(
                              fillColor: Colors.white,
                              border: OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.white, width: 5.0),
                              ),
                              focusedBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.all(Radius.circular(5.0)),
                                  borderSide: BorderSide(color: Colors.green)),
                              filled: true,
                              //contentPadding: EdgeInsets.only(bottom: 10.0, left: 10.0, right: 10.0),
                              labelText: 'promotion_option'.tr,
                              labelStyle: TextStyle(fontSize: 12.0)),
                          validator: FormBuilderValidators.compose([
                            FormBuilderValidators.required(context),
                          ]),
                          options: controller.promotions
                              .map((item) => FormBuilderFieldOption(
                              value: item.id,
                              child: Container(
                                height: 50.0,
                                child: ListTile(
                                  title: Text('${item.name}',
                                      style: TextStyle(fontSize: 12)),
                                  subtitle: Text(item.detail ?? '',
                                      style: TextStyle(fontSize: 10)),
                                  trailing: Text(
                                      item.fee == null ? 'No cost' : '${item.fee}',
                                      style: TextStyle(fontSize: 10)),
                                ),
                              )))
                              .toList(growable: false),
                        ),
                        FormBuilderField<bool?>(
                          name: 'useSubTitle',
                          initialValue: controller.useSubTitle.value,
                          onChanged: (val) {
                            controller.onChangeUseSubTitle(val);
                          },
                          builder: (FormFieldState<bool?> field) {
                            return SwitchListTile(
                              title: Text('Add subtitle optional'),
                              onChanged: (bool value) {
                                field.didChange(value);
                              },
                              value: field.value ?? false,
                            );
                          },
                        ),
                        if (controller.useSubTitle.value)
                          FormBuilderTextField(
                            name: 'subTitle',
                            onChanged: (val) {
                              controller.onChangeSubTitle(val);
                            },
                            initialValue: controller.subTitle.value,
                            decoration: new InputDecoration(
                                fillColor: Colors.white,
                                border: OutlineInputBorder(
                                  borderSide:
                                  BorderSide(color: Colors.black, width: 5.0),
                                ),
                                focusedBorder: OutlineInputBorder(
                                    borderRadius:
                                    BorderRadius.all(Radius.circular(5.0)),
                                    borderSide: BorderSide(color: Colors.green)),
                                filled: true,
                                labelText: 'EG, Good condition',
                                labelStyle: TextStyle(fontSize: 12.0)),
                            validator: FormBuilderValidators.compose(
                                [FormBuilderValidators.required(context)]),
                          ),
                        SizedBox(
                            height: 50.0,
                            child: ListTile(
                              title: Text('total_fee'.tr,
                                  style: TextStyle(
                                      fontSize: 13, fontWeight: FontWeight.bold)),
                              subtitle: Row(
                                children: [
                                  Text('balance'.tr, style: TextStyle(fontSize: 10)),
                                  Text(' ${controller.accountBalance}',
                                      style:
                                      TextStyle(fontSize: 10, color: Colors.red)),
                                ],
                              ),
                              trailing: Text(
                                  controller.totalFee <= 0
                                      ? 'No fee'
                                      : '${controller.totalFee}',
                                  style: TextStyle(
                                      fontSize: 13, fontWeight: FontWeight.bold)),
                            )),
                      ],
                    ),
                  ),
                ),
              ),
            ),
            bottomNavigationBar: Container(
              color: Color(0xfff7892b),
              child: Row(
                mainAxisSize: MainAxisSize.max,
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Container(
                    width: 100.0,
                    child: TextButton(
                      onPressed: () {
                        controller.toPriceView();
                      },
                      child: Text('back_process'.tr, style: TextStyle(color: Colors.white),),
                    ),
                  ),
                  Expanded(
                      child: Padding(
                        padding: EdgeInsets.symmetric(horizontal: 10.0),
                        child: LinearProgressIndicator(
                          value: 1.0,
                          valueColor: AlwaysStoppedAnimation<Color>(Colors.lightBlue),
                          backgroundColor: Colors.white12,
                        ),
                      )),
                  Container(
                    width: 100.0,
                    child: TextButton(
                      onPressed: () {
                        final bool valid = controller.promotionFormKey.currentState?.validate()?? false;
                        if(valid) {
                          controller.startPosting(context);
                        }
                      },
                      child: Text('start'.tr, style: TextStyle(color: Colors.white),),
                    ),
                  ),
                ],
              ),
            ),
          );
        });
  }
}
