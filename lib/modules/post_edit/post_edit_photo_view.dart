import 'dart:io';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mobile/core/theme/colors.dart';
import 'package:mobile/modules/post_edit/post_edit_controller.dart';
import 'package:mobile/shared/models/image_model.dart';
import 'package:mobile/shared/widgets/network_image.dart';

class PostEditPhotoView extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return GetX<PostEditController>(
        builder: (controller) {
          return Scaffold(
            appBar: AppBar(
              elevation: 0,
              title: Container(
                color: Color(0xfff7892b),
                child: Row(
                  mainAxisSize: MainAxisSize.max,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Container(
                      width: 100.0,
                      child: TextButton(
                        onPressed: () {
                          controller.toCameraView();
                        },
                        child: Text('back_process'.tr, style: TextStyle(color: Colors.white),),
                      ),
                    ),
                    Expanded(
                        child: Padding(
                          padding: EdgeInsets.symmetric(horizontal: 10.0),
                          child: LinearProgressIndicator(
                            value: 0.20,
                            valueColor: AlwaysStoppedAnimation<Color>(Colors.lightBlue),
                            backgroundColor: Colors.white12,
                          ),
                        )),
                    Container(
                      width: 100.0,
                      child: TextButton(
                        onPressed: () {
                          controller.toTitleView();
                        },
                        child: Text('next_process'.tr, style: TextStyle(color: Colors.white),),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            body: SafeArea(
              child: SingleChildScrollView(
                child: GridView.builder(
                    physics: NeverScrollableScrollPhysics(),
                    shrinkWrap: true,
                    itemCount: controller.uploadImages.length,
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 3,
                      crossAxisSpacing: 0.2,
                      mainAxisSpacing: 10,
                      childAspectRatio: 0.6,
                    ),
                    itemBuilder: (BuildContext context, int index) {
                      final ImageModel image = controller.uploadImages[index];
                      if(image.uploaded) {
                        print('image url: ${image.imageUrl}');
                        return Card(
                          clipBehavior: Clip.antiAlias,
                          child: Stack(
                            children: <Widget>[
                              Container(width: 120, height: 150, child: CacheNetworkImageWidget(url: image.imageUrl, radius: 0,)),
                              Positioned(
                                right: 5,
                                top: 5,
                                child: InkWell(
                                  child: Icon(
                                    Icons.remove_circle,
                                    size: 20,
                                    color: Colors.red,
                                  ),
                                  onTap: () {
                                    controller.handleRemovePicture(image);
                                  },
                                ),
                              ),
                            ],
                          ),
                        );
                      }
                      return Card(
                        clipBehavior: Clip.antiAlias,
                        child: Stack(
                          children: <Widget>[
                            Container(width: 120, height: 150, child: Image.file(File(image.imageUrl), fit: BoxFit.cover)),
                            Positioned(
                              right: 5,
                              top: 5,
                              child: InkWell(
                                child: Icon(
                                  Icons.remove_circle,
                                  size: 20,
                                  color: Colors.red,
                                ),
                                onTap: () {
                                  controller.handleRemovePicture(image);
                                },
                              ),
                            ),
                          ],
                        ),
                      );
                    }
                ),
              ),
            ),
            floatingActionButton: FloatingActionButton(
              onPressed: () {
                controller.handleBrowserGallery();
              },
              child: Icon(Icons.photo_album),
              backgroundColor: AppColor.primaryColor,
            ),
            bottomNavigationBar: Container(
              color: Color(0xfff7892b),
              child: Row(
                mainAxisSize: MainAxisSize.max,
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Container(
                    width: 100.0,
                    child: TextButton(
                      onPressed: () {
                        controller.toCameraView();
                      },
                      child: Text('back_process'.tr, style: TextStyle(color: Colors.white),),
                    ),
                  ),
                  Expanded(
                      child: Padding(
                        padding: EdgeInsets.symmetric(horizontal: 10.0),
                        child: LinearProgressIndicator(
                          value: 0.20,
                          valueColor: AlwaysStoppedAnimation<Color>(Colors.lightBlue),
                          backgroundColor: Colors.white12,
                        ),
                      )),
                  Container(
                    width: 100.0,
                    child: TextButton(
                      onPressed: () {
                        controller.toTitleView();
                      },
                      child: Text('next_process'.tr, style: TextStyle(color: Colors.white),),
                    ),
                  ),
                ],
              ),
            ),
          );
    });
  }
}
