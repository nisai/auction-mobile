import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:get/get.dart';
import 'package:mobile/modules/post_edit/post_edit_controller.dart';
import 'package:form_builder_validators/form_builder_validators.dart';

class PostEditTitleView extends GetView<PostEditController> {

  @override
  Widget build(BuildContext context) {
    return GetX<PostEditController>(
        builder: (controller) {
          return Scaffold(
            appBar: AppBar(
              elevation: 0,
              title: Container(
                color: Color(0xfff7892b),
                child: Row(
                  mainAxisSize: MainAxisSize.max,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Container(
                      width: 100.0,
                      child: TextButton(
                        onPressed: () {
                          controller.toPhotoView();
                        },
                        child: Text('back_process'.tr, style: TextStyle(color: Colors.white),),
                      ),
                    ),
                    Expanded(
                        child: Padding(
                          padding: EdgeInsets.symmetric(horizontal: 10.0),
                          child: LinearProgressIndicator(
                            value: 0.40,
                            valueColor: AlwaysStoppedAnimation<Color>(Colors.lightBlue),
                            backgroundColor: Colors.white12,
                          ),
                        )),
                    Container(
                      width: 100.0,
                      child: TextButton(
                        onPressed: () {
                          final bool valid = controller.titleFormKey.currentState?.validate()?? false;
                          if(valid) {
                            controller.toPriceView();
                          }
                        },
                        child: Text('next_process'.tr, style: TextStyle(color: Colors.white),),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            body: SafeArea(
              child: SingleChildScrollView(
                child: Padding(
                  padding: EdgeInsets.all(20),
                  child: FormBuilder(
                    key: controller.titleFormKey,
                    autovalidateMode: AutovalidateMode.onUserInteraction,
                    child: Column(
                      children: [
                        FormBuilderDropdown<int?>(
                          initialValue: controller.postProvince.value > 0? controller.postProvince.value: null,
                          onChanged: (valueId) {
                            controller.onChangeProvince(valueId);
                          },
                          name: 'provinceId',
                          decoration: new InputDecoration(
                              fillColor: Colors.white,
                              border: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: Colors.black,
                                    width: 5.0),
                              ),
                              focusedBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.all(Radius.circular(5.0)),
                                  borderSide: BorderSide(color: Colors.green)),
                              filled: true,
                              labelText: 'province'.tr,
                              labelStyle: TextStyle(fontSize: 12.0)
                          ),
                          hint: Text('select_province'.tr),
                          validator: FormBuilderValidators.compose([
                            FormBuilderValidators.required(context),
                          ]),
                          items: controller.provinces
                              .map((item) => item)
                              .map((item) => DropdownMenuItem(
                              value: item.id,
                              child: Text("${item.name}")))
                              .toList(),
                        ),
                        SizedBox(height: 15.0),
                        FormBuilderDropdown<int>(
                          initialValue: controller.postParent.value > 0? controller.postParent.value: null,
                          name: 'parentId',
                          onChanged: (value) {
                            controller.onChangeParent(value);
                          },
                          decoration: new InputDecoration(
                              fillColor: Colors.white,
                              border: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: Colors.black,
                                    width: 5.0),
                              ),
                              focusedBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.all(Radius.circular(5.0)),
                                  borderSide: BorderSide(color: Colors.green)),
                              filled: true,
                              labelText: 'category'.tr,
                              labelStyle: TextStyle(fontSize: 12.0)
                          ),
                          // initialValue: 'Male',
                          hint: Text('select_category'.tr),
                          items: controller.parents
                              .map((item) => item)
                              .map((item) => DropdownMenuItem(
                              value: item.id,
                              child: Text("${item.name}")))
                              .toList(),
                        ),
                        SizedBox(height: 15.0),
                        if(controller.showChildren)
                          Padding(padding: EdgeInsets.only(bottom: 15),
                              child: FormBuilderDropdown<int?>(
                                initialValue: controller.postChildren.value > 0? controller.postChildren.value: null,
                                name: 'childrenId',
                                onChanged: (valueId) {
                                  controller.onChangeChildren(valueId);
                                },
                                decoration: new InputDecoration(
                                    fillColor: Colors.white,
                                    border: OutlineInputBorder(
                                      borderSide: BorderSide(
                                          color: Colors.black,
                                          width: 5.0),
                                    ),
                                    focusedBorder: OutlineInputBorder(
                                        borderRadius: BorderRadius.all(Radius.circular(5.0)),
                                        borderSide: BorderSide(color: Colors.green)),
                                    filled: true,
                                    labelText: 'sub_category'.tr,
                                    labelStyle: TextStyle(fontSize: 12.0)
                                ),
                                // initialValue: 'Male',
                                hint: Text('select_sub_category'.tr),
                                items: controller.children
                                    .map((item) => item)
                                    .map((item) => DropdownMenuItem(
                                    value: item.id,
                                    child: Text("${item.name}")))
                                    .toList(),
                              )),
                        SizedBox(height: 15.0),
                        FormBuilderTextField(
                          onChanged: (val) {
                            controller.onChangeTitle(val);
                          },
                          name: 'name',
                          initialValue: controller.postTitle.value,
                          decoration: new InputDecoration(
                              fillColor: Colors.white,
                              border: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: Colors.black,
                                    width: 5.0),
                              ),
                              focusedBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.all(Radius.circular(5.0)),
                                  borderSide: BorderSide(color: Colors.green)),
                              filled: true,
                              labelText: 'title'.tr,
                              labelStyle: TextStyle(fontSize: 12.0)
                          ),
                          validator: FormBuilderValidators.compose(
                              [FormBuilderValidators.required(context)]),
                        ),
                        SizedBox(height: 15.0),
                        FormBuilderTextField(
                          onChanged: (val) {
                            controller.onChangeDescription(val);
                          },
                          maxLines: 5,
                          name: "description",
                          initialValue: controller.postDescription.value,
                          decoration: new InputDecoration(
                              fillColor: Colors.white,
                              border: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: Colors.white,
                                    width: 5.0),
                              ),
                              focusedBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.all(Radius.circular(5.0)),
                                  borderSide: BorderSide(color: Colors.green)),
                              filled: true,
                              contentPadding: EdgeInsets.only(bottom: 10.0, left: 10.0, right: 10.0),
                              labelText: 'description'.tr,
                              labelStyle: TextStyle(fontSize: 12.0)
                          ),
                          validator: FormBuilderValidators.compose([
                            FormBuilderValidators.required(context),
                          ]),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
            bottomNavigationBar: Container(
              color: Color(0xfff7892b),
              child: Row(
                mainAxisSize: MainAxisSize.max,
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Container(
                    width: 100.0,
                    child: TextButton(
                      onPressed: () {
                        controller.toPhotoView();
                      },
                      child: Text('back_process'.tr, style: TextStyle(color: Colors.white),),
                    ),
                  ),
                  Expanded(
                      child: Padding(
                        padding: EdgeInsets.symmetric(horizontal: 10.0),
                        child: LinearProgressIndicator(
                          value: 0.40,
                          valueColor: AlwaysStoppedAnimation<Color>(Colors.lightBlue),
                          backgroundColor: Colors.white12,
                        ),
                      )),
                  Container(
                    width: 100.0,
                    child: TextButton(
                      onPressed: () {
                        final bool valid = controller.titleFormKey.currentState?.validate()?? false;
                        if(valid) {
                          controller.toPriceView();
                        }
                      },
                      child: Text('next_process'.tr, style: TextStyle(color: Colors.white),),
                    ),
                  ),
                ],
              ),
            ),
          );
        });
  }
}