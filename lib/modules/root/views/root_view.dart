import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:mobile/modules/root/controllers/root_controller.dart';
import 'package:mobile/routes/app_pages.dart';
import 'package:mobile/shared/models/product_search_filter.dart';
import 'package:mobile/shared/widgets/wishlist_app_bar_toggle_widget.dart';


class RootView extends GetView<RootController> {

  @override
  Widget build(BuildContext context) {

    return GetRouterOutlet.builder(
      builder: (context, delegate, current) {
        return Scaffold(
          appBar: _buildAppBar(current),
          body: GetRouterOutlet(
            initialRoute: Routes.HOME,
            // anchorRoute: '/',
            // filterPages: (afterAnchor) {
            //   return afterAnchor.take(1);
            // },
          ),
        );
      },
    );
  }

  AppBar? _buildAppBar(GetNavConfig? current) {
    final String title = current?.location?? '';
    print('title $title');
    final bool showAppBar = title ==  Routes.ROOT
        || title.startsWith(Routes.HOME)
        || title.startsWith(Routes.NOTIFICATION)
        || title.startsWith(Routes.CATEGORY)
        || title.startsWith(Routes.OTHERS);
    if(showAppBar) {
      return AppBar(
        leading: WishListAppBarToggleWidget(),
        title: Container(
            height: kToolbarHeight,
            child: Center(child: _title())),
        titleSpacing: 0,
        centerTitle: true,
        actions: <Widget>[
          IconButton(
            icon: Icon(
              Icons.search,
              color: Colors.white,
            ),
            onPressed: () {
              Get.rootDelegate.offNamed(Routes.PRODUCT, arguments: ProductSearchFilter(forceSearch: true));
            },
          )
        ],
      );
    }

    return null;
  }

  Widget _title() {
    return RichText(
      textAlign: TextAlign.center,
      text: TextSpan(
          text: 'khmer_title'.tr,
          style: GoogleFonts.lato(
            fontSize: 30,
            fontWeight: FontWeight.bold,
            color: Colors.lightBlue[900],
          ),
          children: [
            TextSpan(
              text: '  ',
              style: TextStyle(color: Colors.black, fontSize: 30,),
            ),
            TextSpan(
              text: 'auction'.tr,
              style: GoogleFonts.lato(
                fontSize: 30,
                fontWeight: FontWeight.bold,
                color: Colors.white,
              ),
            ),
          ]),
    );
  }

}
