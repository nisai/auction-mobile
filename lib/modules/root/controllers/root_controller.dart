import 'dart:async';

import 'package:connectivity/connectivity.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';

class RootController extends GetxController {

  var isLoggedIn = false.obs;

  var connectionStatus = ConnectivityResult.none.obs;

  bool get isConnected => connectionStatus.value != ConnectivityResult.none;

  final Connectivity _connectivity = Connectivity();
  late StreamSubscription<ConnectivityResult> _connectivitySubscription;

  @override
  void onInit() {
    print('Connection Controller Init');
    super.onInit();



    initConnectivity().then((value)  {
      connectionStatus.value = value;
      update();
    });
    _connectivitySubscription = _connectivity.onConnectivityChanged.listen(_updateConnectionStatus);
  }

  Future<bool> checkConnection() async {
    ConnectivityResult result = ConnectivityResult.none;
    try {
      result = await _connectivity.checkConnectivity();
    } on PlatformException catch (error) {
      print('On Connection error $error');
    }
    return result != ConnectivityResult.none;
  }

  Future<ConnectivityResult> initConnectivity() async {
    ConnectivityResult result = ConnectivityResult.none;
    try {
      result = await _connectivity.checkConnectivity();
    } on PlatformException catch (error) {
      print('On Connection error $error');
    }

    return result;
  }

  void _updateConnectionStatus(ConnectivityResult result) {
    print('listen Result $result');
    switch (result) {
      case ConnectivityResult.wifi:
        connectionStatus.value = ConnectivityResult.wifi;
        break;
      case ConnectivityResult.mobile:
        connectionStatus.value = ConnectivityResult.mobile;
        break;
      case ConnectivityResult.none:
        connectionStatus.value = ConnectivityResult.none;
        break;
      default:
        break;
    }
    update();
  }

  @override
  dispose() async {
    super.dispose();

    await _connectivitySubscription.cancel();
  }
}
