import 'package:get/get.dart';
import 'package:mobile/modules/product_comment/product_comment_controller.dart';
import 'package:mobile/repositories/product_repository.dart';

class ProductCommentBinding extends Bindings {

  @override
  void dependencies() {
    Get.lazyPut<ProductRepository>(() => ProductRepository());
    Get.lazyPut<ProductCommentController>(
          () => ProductCommentController(
              productId: Get.parameters['productId'] ?? '',
              productRepository: Get.find<ProductRepository>()
      ),
    );
  }
}
