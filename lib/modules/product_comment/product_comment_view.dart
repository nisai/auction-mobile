import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mobile/core/theme/colors.dart';
import 'package:mobile/modules/product_comment/product_comment_controller.dart';
import 'package:mobile/routes/app_pages.dart';
import 'package:mobile/shared/models/comment_model.dart';
import 'package:mobile/shared/widgets/avatar_fade_Image.dart';
import 'package:mobile/utils/helpers.dart';

class ProductCommentView extends GetView<ProductCommentController> {

  final FocusNode focusNode = FocusNode();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Question & Answer'),
        elevation: 0,
        centerTitle: true,
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () {
            Get.rootDelegate.offNamed(Routes.productDetail(productId: controller.productId));
          },
        ),
      ),
      body: WillPopScope(
        child: Stack(
          children: <Widget>[
            Column(
              children: <Widget>[
                buildListMessage(),
              ],
            ),
            Align(
              alignment: Alignment.bottomCenter,
              child: _buildInput(context),
            )
          ],
        ),
        onWillPop: onBackPress,
      ),
    );
  }

  Future<bool> onBackPress() {
    return Future.value(true);
  }

  Widget buildListMessage() {
    return GetBuilder<ProductCommentController>(builder: (controller) {
      return Flexible(
        child: ListView.builder(
            scrollDirection: Axis.vertical,
            physics: NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            itemCount: controller.comments.length,
            itemBuilder: (BuildContext context, int index) {
              final item = controller.comments[index];
              return _buildItem(item);
            }),
      );
    });
  }

  Widget _buildItem(final CommentModel item) {
    final createdAt = Helpers.getTimeHuman(createdAt: item.createdAt);
    final username = item.createdBy?.userName?? 'unknown';
    final content = item.content;
    final bool isOwner = item.byOwner?? false;
    final profile = Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Container(
          width: 35.0,
          height: 35.0,
          child: AvatarFadeImage(url: item.createdBy?.profileUrl?? ''),
        ),
        Text("$username", overflow: TextOverflow.ellipsis, style: TextStyle(fontSize: 11.0))
      ],
    );
    if(isOwner) {
      return Padding(
        padding: EdgeInsets.only(right: 5.0),
        child: ListTile(
          subtitle: Text("$createdAt", textAlign: TextAlign.right,  style: TextStyle(fontSize: 10.0)),
          title: Text('$content', textAlign: TextAlign.right, style: TextStyle(fontSize: 12.0),),
          trailing: profile,
        ),
      );
    }
    return Padding(
      padding: EdgeInsets.only(left: 5.0),
      child: ListTile(
        leading: profile,
        subtitle: Text("$createdAt", textAlign: TextAlign.left,  style: TextStyle(fontSize: 10.0)),
        title: Text('$content', textAlign: TextAlign.left, style: TextStyle(fontSize: 12.0),),
      ),
    );
  }

  Widget _buildInput(final BuildContext context) {
    return Container(
      child: Row(
        children: <Widget>[
          // Button send image
          Material(
            child: Container(
              margin: EdgeInsets.symmetric(horizontal: 1.0),
              child: IconButton(
                icon: Icon(Icons.question_answer),
                onPressed: () {

                },
              ),
            ),
            color: Colors.white,
          ),

          // Edit text
          Flexible(
            child: Container(
              child: TextField(
                style: TextStyle(color: AppColor.primaryColor, fontSize: 15.0),
                controller: controller.commentController.value,
                decoration: InputDecoration.collapsed(
                  hintText: 'Type your message...',
                  hintStyle: TextStyle(color: Colors.grey),
                ),
                focusNode: focusNode,
              ),
            ),
          ),
          // Button send message
          Material(
            child: Container(
              margin: EdgeInsets.symmetric(horizontal: 8.0),
              child: IconButton(
                icon: Icon(Icons.send),
                onPressed: () {
                  controller.handleCreateQuestion(context);
                },
                color: AppColor.primaryColor,
              ),
            ),
            color: Colors.white,
          ),
        ],
      ),
      width: double.infinity,
      height: 50.0,
      decoration: BoxDecoration(border: Border(top: BorderSide(color: Colors.grey, width: 0.5)), color: Colors.white),
    );
  }
}
