import 'dart:async';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:mobile/repositories/product_repository.dart';
import 'package:mobile/shared/models/comment_model.dart';
import 'package:mobile/utils/customLoader.dart';

class ProductCommentController extends GetxController {

  final _loader = CustomLoader();

  var comments = <CommentModel>[].obs;

  ProductCommentController({required this.productId, required this.productRepository});

  final String productId;
  final ProductRepository productRepository;

  final commentController = TextEditingController().obs;

  var _loading = false.obs;
  bool get loading => _loading.value;


  @override
  void onInit() {
    Timer.periodic(new Duration(seconds: 5), (timer) {
      _loadComment();
    });

    _loadComment();

    super.onInit();
  }

  Future<void> _loadComment() async {
    _loading(true);
    update();

    final resultDetail = await productRepository.getProductComments(productId: int.tryParse(productId));

    if(resultDetail != null) {
      final data = resultDetail['data'];

      final _comments = (data['comments']['select'] as List)
          .cast<Map<String, dynamic>>()
          .map(CommentModel.fromJson)
          .toList();

      comments(_comments);
      _loading(false);
      update();
    }
  }

  Future<void> handleCreateQuestion(final BuildContext context) async {

    _loader.showLoader(context);

    try {
      final Map<String, dynamic> input = {
        'id': productId,
        'content': commentController.value.text,
      };
      var result = await productRepository.commentProduct(input);
      if(result) {
        commentController.value.text = '';
      }
      update();

      await _loadComment();

    } catch(e) {
      print('error $e');
    }

    _loader.hideLoader();
  }

}
