import 'package:get/get.dart';
import 'package:mobile/modules/seller_detail/seller_detail_controller.dart';

class SellerDetailBinding extends Bindings {

  @override
  void dependencies() {
    Get.lazyPut(() => SellerDetailController(userId: Get.parameters['userId'] ?? ''));
  }

}
