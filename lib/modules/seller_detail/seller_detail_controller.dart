import 'package:get/get.dart';
import 'package:mobile/repositories/user_repository.dart';
import 'package:mobile/shared/models/product_model.dart';
import 'package:mobile/shared/models/user_item_model.dart';
import 'package:mobile/shared/models/user_model.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class SellerDetailController extends GetxController with GetSingleTickerProviderStateMixin {

  final String userId;

  var rates = <UserRateModel>[].obs;
  var positiveRates = <UserRateModel>[].obs;
  var naturalRates = <UserRateModel>[].obs;
  var negativeRates = <UserRateModel>[].obs;

  bool get hasFeedBack => rates.length > 0;

  var _seller = UserModel().obs;

  UserModel get seller => _seller.value;

  SellerDetailController({required this.userId});

  var items  = <ProductModel>[].obs;

  var _page = 1.obs;

  int get page => _page.value;

  final _repository = Get.find<UserRepository>();

  final RefreshController refreshController = RefreshController(initialRefresh: false);


  @override
  void onInit() {

    _loadFirstPage().then((_) {
      update();
    });

    super.onInit();
  }


  Future<void> onRefresh() async {
    _page(1);
    _loadFirstPage().then((_) {
      update();
      refreshController.refreshCompleted();
    });

  }

  Future<void> onLoading() async {
    try {
      _page.value ++;

      refreshController.loadComplete();
    } catch(e) {

    }
  }

  Future<void> _loadFirstPage() async {
    try {

      final Map<String, dynamic>? _result = await _repository.getSellerDetails(int.tryParse(userId));

      if(_result?['data'] != null) {
        var data = _result?['data'];

        final _appUser = UserModel.fromJson(data?['AppUser']);
        _seller(_appUser);

        var _items = (data['Products']['select'] as List)
            .cast<Map<String, dynamic>>()
            .map(ProductModel.fromJson)
            .toList();

        items(_items);

        var _rates = (data['rates']['select'] as List)
            .cast<Map<String, dynamic>>()
            .map(UserRateModel.fromJson)
            .toList();

        var _positive = (data['positiveRates']['select'] as List)
            .cast<Map<String, dynamic>>()
            .map(UserRateModel.fromJson)
            .toList();

        var _natural = (data['naturalRates']['select'] as List)
            .cast<Map<String, dynamic>>()
            .map(UserRateModel.fromJson)
            .toList();

        var _negative = (data['negativeRates']['select'] as List)
            .cast<Map<String, dynamic>>()
            .map(UserRateModel.fromJson)
            .toList();

        rates(_rates);
        positiveRates(_positive);
        naturalRates(_natural);
        negativeRates(_negative);
      }
    } catch(e) {

      print('_loadFirstPage: error $e');
    }
  }

}
