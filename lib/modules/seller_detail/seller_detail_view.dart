import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mobile/modules/seller_detail/seller_detail_controller.dart';
import 'package:mobile/routes/app_pages.dart';
import 'package:mobile/shared/widgets/network_image.dart';
import 'package:mobile/shared/widgets/product_item_widget.dart';
import 'package:mobile/shared/widgets/simple_header.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class SellerDetailView extends GetView<SellerDetailController> {
  @override
  Widget build(BuildContext context) {
    return SmartRefresher(
      enablePullDown: true,
      enablePullUp: true,
      header: WaterDropHeader(),
      footer: CustomFooter(
        builder: (final BuildContext context, final LoadStatus? mode) {
          Widget body;
          if (mode == LoadStatus.idle) {
            body = Text("pull up load");
          } else if (mode == LoadStatus.loading) {
            body = CupertinoActivityIndicator();
          } else if (mode == LoadStatus.failed) {
            body = Text("Load Failed!Click retry!");
          } else if (mode == LoadStatus.canLoading) {
            body = Text("release to load more");
          } else {
            body = Text("No more Data");
          }
          return Container(
            height: 55.0,
            child: Center(child: body),
          );
        },
      ),
      controller: controller.refreshController,
      onRefresh: controller.onRefresh,
      onLoading: controller.onLoading,
      child: GetBuilder<SellerDetailController>(builder: (controller) {
        return Scaffold(
          appBar: AppBar(
            title: Text('About Seller', textAlign: TextAlign.start),
            leading: IconButton(
              icon: Icon(Icons.arrow_back),
              onPressed: () {
                Get.rootDelegate.popRoute(popMode: PopMode.Page);
              },
            ),
          ),
          body: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                SizedBox(
                    height: 250.0,
                    child: CacheNetworkImageWidget(
                        url: controller.seller.profileUrl)),
                SimpleHeader(
                  text: 'seller information'.tr,
                ),
                Padding(
                    padding: EdgeInsets.symmetric(horizontal: 5.0),
                    child: Column(children: [
                      Row(
                        children: [
                          Expanded(
                              flex: 1,
                              child: ListTile(
                                title: Text("Username"),
                                subtitle: Text('${controller.seller.userName}'),
                              )),
                          Expanded(
                            flex: 1,
                            child: ListTile(
                              title: Text("Member Number"),
                              subtitle: Text('${controller.seller.id}'),
                            ),
                          ),
                        ],
                      ),
                      Row(
                        children: [
                          Expanded(
                              flex: 1,
                              child: ListTile(
                                title: Text("Location"),
                                subtitle: Text('unknown'),
                              )),
                          Expanded(
                            flex: 1,
                            child: ListTile(
                              title: Text("Member Since"),
                              subtitle: Text('${controller.seller.createdAt}'),
                            ),
                          ),
                        ],
                      ),
                    ])),

                SizedBox(height: 10.0),

                SimpleHeader(
                  text: 'Feedback'.tr,
                  actionText: 'Sell all',
                  onTap: () {
                    Get.rootDelegate.toNamed(Routes.sellerFeedback(userId: controller.userId));
                  },
                ),

                _buildFeedBackLabel(context),

                SizedBox(height: 10.0),
                SimpleHeader(
                  text: 'listing'.tr,
                ),

                GridView.builder(
                    physics: NeverScrollableScrollPhysics(),
                    shrinkWrap: true,
                    itemCount: controller.items.length,
                    padding: EdgeInsets.symmetric(horizontal: 5.0, vertical: 5.0),
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 2,
                      crossAxisSpacing: 5,
                      mainAxisSpacing: 10,
                      childAspectRatio: 0.7,
                    ),
                    itemBuilder: (BuildContext context, int index) {
                      // give -1 because don't want effect hogiration margin
                      final item = controller.items[index];
                      return GestureDetector(
                          onTap: () {
                            Get.rootDelegate.toNamed(
                                Routes.productDetail(productId: '${item.id}'));
                          },
                          child: ProductItem(product: item));
                    }),
              ],
            ),
          ),
        );
      }),
    );
  }

  Widget _buildFeedBackLabel(final BuildContext context) {

    return Padding(
        padding: EdgeInsets.symmetric(horizontal: 5.0),
        child: Column(
      children: [
        Row(children: [
          Container(
            width: 50,
            child: Icon(
              Icons.sentiment_satisfied,
              color: Colors.lightGreen,
            ),
          ),
          Expanded(child: Text('${controller.positiveRates.length} Positive', overflow: TextOverflow.ellipsis,)),
        ],),
        Row(children: [
          Container(
            width: 50,
            child: Icon(
              Icons.sentiment_neutral,
              color: Colors.amber,
            ),
          ),
          Expanded(child: Text('${controller.naturalRates.length} Natural', overflow: TextOverflow.ellipsis,)),
        ],),

        Row(children: [
          Container(
            width: 50,
            child: Icon(
              Icons.sentiment_very_dissatisfied,
              color: Colors.red,
            ),
          ),
          Expanded(child: Text('${controller.negativeRates.length} Negative', overflow: TextOverflow.ellipsis,)),
        ],),
      ],
    )
    );
  }
}
