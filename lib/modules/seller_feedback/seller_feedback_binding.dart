import 'package:get/get.dart';
import 'package:mobile/modules/seller_feedback/seller_feedback_controller.dart';

class SellerFeedbackBinding extends Bindings {

  @override
  void dependencies() {
    Get.lazyPut<SellerFeedbackController>(
          () => SellerFeedbackController(
              userId: Get.parameters['userId'] ?? ''
      ),
    );
  }
}
