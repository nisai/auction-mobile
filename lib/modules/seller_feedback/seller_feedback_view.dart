import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:get/get.dart';
import 'package:mobile/core/theme/colors.dart';
import 'package:mobile/modules/seller_feedback/seller_feedback_controller.dart';
import 'package:mobile/routes/app_pages.dart';
import 'package:mobile/shared/models/user_item_model.dart';
import 'package:mobile/utils/helpers.dart';

class SellerFeedbackView extends GetView<SellerFeedbackController> {
  final FocusNode focusNode = FocusNode();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Feedback'),
        elevation: 0,
        centerTitle: true,
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () {
            Get.rootDelegate
                .offNamed(Routes.sellerDetail(userId: controller.userId));
          },
        ),
      ),
      body: GetBuilder<SellerFeedbackController>(builder: (controller) {
        return SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              SizedBox(height: 10.0),
              _buildSellerProfile(context),
              SizedBox(height: 10.0),
              _buildFeedBackLabel(context),
              SizedBox(height: 10.0),
              ListView.builder(
                  physics: NeverScrollableScrollPhysics(),
                  shrinkWrap: true,
                  itemCount: controller.rates.length,
                  itemBuilder: (BuildContext context, int index) {
                    final item = controller.rates[index];
                    return _buildItem(item);
                  }),
            ],
          ),
        );
      }),
    );
  }

  Widget _buildSellerProfile(final BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Container(
          width: 50.0,
          height: 50.0,
          child: CircleAvatar(
            backgroundImage: NetworkImage(controller.seller.profileUrl),
          ),
        ),
        SizedBox(width: 10.0),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Text(
              '${controller.seller.userName} (${controller.rates.length}) feedbacks',
              style: TextStyle(
                  color: Colors.black87.withOpacity(0.7), fontSize: 12),
            ),
            Text(
              'Member since ${controller.seller.createdAt}',
              style: TextStyle(fontSize: 12),
            ),
            RatingBar.builder(
                initialRating: 3,
                itemCount: 5,
                itemSize: 15.0,
                itemBuilder: (context, index) {
                  switch (index) {
                    case 0:
                      return Icon(
                        Icons.sentiment_very_dissatisfied,
                        color: Colors.red,
                      );
                    case 1:
                      return Icon(
                        Icons.sentiment_dissatisfied,
                        color: Colors.redAccent,
                      );
                    case 2:
                      return Icon(
                        Icons.sentiment_neutral,
                        color: Colors.amber,
                      );
                    case 3:
                      return Icon(
                        Icons.sentiment_satisfied,
                        color: Colors.lightGreen,
                      );
                    case 4:
                      return Icon(
                        Icons.sentiment_very_satisfied,
                        color: Colors.green,
                      );
                  }
                  return Icon(
                    Icons.sentiment_very_satisfied,
                    color: Colors.green,
                  );
                },
                onRatingUpdate: (rating) {
                  controller.onChangeRating(rating).then((value) {
                    showDialog(context);
                  });
                })
          ],
        ),
      ],
    );
  }

  showDialog(final BuildContext context) {
    Get.defaultDialog(
        title: '',
        content: GetBuilder<SellerFeedbackController>(builder: (controller) {
          return FormBuilder(
              key: controller.formKey,
              autovalidateMode: AutovalidateMode.disabled,
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    RatingBar.builder(
                        initialRating: controller.rating.value,
                        itemCount: 5,
                        itemSize: 15.0,
                        ignoreGestures: true,
                        itemBuilder: (context, index) {
                          switch (index) {
                            case 0:
                              return Icon(
                                Icons.sentiment_very_dissatisfied,
                                color: Colors.red,
                              );
                            case 1:
                              return Icon(
                                Icons.sentiment_dissatisfied,
                                color: Colors.redAccent,
                              );
                            case 2:
                              return Icon(
                                Icons.sentiment_neutral,
                                color: Colors.amber,
                              );
                            case 3:
                              return Icon(
                                Icons.sentiment_satisfied,
                                color: Colors.lightGreen,
                              );
                            case 4:
                              return Icon(
                                Icons.sentiment_very_satisfied,
                                color: Colors.green,
                              );
                          }
                          return Icon(
                            Icons.sentiment_very_satisfied,
                            color: Colors.green,
                          );
                        },
                        onRatingUpdate: (rating) {}),
                    SizedBox(
                      height: 10.0,
                    ),
                    FormBuilderTextField(
                      onChanged: (String? val) {
                        controller.onChangeComment(val);
                      },
                      name: 'comment',
                      initialValue: controller.comment.value,
                      decoration: new InputDecoration(
                          fillColor: Colors.white,
                          border: OutlineInputBorder(
                            borderSide:
                                BorderSide(color: Colors.black, width: 5.0),
                          ),
                          focusedBorder: OutlineInputBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(5.0)),
                              borderSide: BorderSide(color: Colors.green)),
                          filled: true,
                          labelText: 'comment'.tr,
                          labelStyle: TextStyle(fontSize: 12.0)),
                      validator: FormBuilderValidators.compose(
                          [FormBuilderValidators.required(context)]),
                    ),
                    SizedBox(
                      height: 10.0,
                    ),
                    RaisedButton(
                      onPressed: () {
                        final bool valid = controller.formKey.currentState
                                ?.saveAndValidate() ??
                            false;
                        if (valid) {
                          controller.handleRating(context).then((_) {
                            Get.back();
                          });
                        }
                      },
                      child: Text(
                        'Feedback',
                        style: TextStyle(color: Colors.white, fontSize: 16.0),
                      ),
                      color: AppColor.primaryColor,
                    )
                  ]));
        }),
        radius: 10.0);
  }

  Widget _buildFeedBackLabel(final BuildContext context) {
    return Row(
      children: [
        Expanded(
            child: Row(
          children: [
            Container(
              width: 50,
              child: Icon(
                Icons.sentiment_satisfied,
                color: Colors.lightGreen,
              ),
            ),
            Expanded(
                child: Text(
              '${controller.positiveRates.length} Positive',
              overflow: TextOverflow.ellipsis,
            )),
          ],
        )),
        Expanded(
          child: Row(
            children: [
              Container(
                width: 50,
                child: Icon(
                  Icons.sentiment_neutral,
                  color: Colors.amber,
                ),
              ),
              Expanded(
                  child: Text(
                '${controller.naturalRates.length} Natural',
                overflow: TextOverflow.ellipsis,
              )),
            ],
          ),
        ),
        Expanded(
          child: Row(
            children: [
              Container(
                width: 50,
                child: Icon(
                  Icons.sentiment_very_dissatisfied,
                  color: Colors.red,
                ),
              ),
              Expanded(
                  child: Text(
                '${controller.negativeRates.length} Negative',
                overflow: TextOverflow.ellipsis,
              )),
            ],
          ),
        ),
      ],
    );
  }

  Widget _buildItem(final UserRateModel item) {
    final createdAt = Helpers.getTimeHuman(createdAt: item.createdAt);
    final username = item.createdBy?.userName ?? 'unknown';
    final comment = item.comment;
    Widget icon = Container();
    final int rate = item.rate ?? 0;
    if (rate >= 4) {
      icon = Icon(
        Icons.sentiment_satisfied,
        color: Colors.lightGreen,
      );
    } else if (rate == 3) {
      icon = Icon(
        Icons.sentiment_neutral,
        color: Colors.amber,
      );
    } else {
      icon = Icon(
        Icons.sentiment_very_dissatisfied,
        color: Colors.red,
      );
    }

    Widget title = Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                child: Text(username),
              ),
              Expanded(
                  child: RatingBar.builder(
                      initialRating: rate.toDouble(),
                      itemCount: 5,
                      itemSize: 15.0,
                      itemBuilder: (context, index) {
                        switch (index) {
                          case 0:
                            return Icon(
                              Icons.sentiment_very_dissatisfied,
                              color: Colors.red,
                            );
                          case 1:
                            return Icon(
                              Icons.sentiment_dissatisfied,
                              color: Colors.redAccent,
                            );
                          case 2:
                            return Icon(
                              Icons.sentiment_neutral,
                              color: Colors.amber,
                            );
                          case 3:
                            return Icon(
                              Icons.sentiment_satisfied,
                              color: Colors.lightGreen,
                            );
                          case 4:
                            return Icon(
                              Icons.sentiment_very_satisfied,
                              color: Colors.green,
                            );
                        }
                        return Icon(
                          Icons.sentiment_very_satisfied,
                          color: Colors.green,
                        );
                      },
                      onRatingUpdate: (rating) {
                        print(rating);
                      })),
            ]),
        Text("$comment",
            textAlign: TextAlign.left, style: TextStyle(fontSize: 12.0)),
      ],
    );

    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          width: 50,
          child: icon,
        ),
        Expanded(
            child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            title,
            Text("$createdAt",
                textAlign: TextAlign.left, style: TextStyle(fontSize: 10.0))
          ],
        ))
      ],
    );
  }
}
