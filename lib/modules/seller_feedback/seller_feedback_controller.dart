import 'dart:async';
import 'package:flutter/cupertino.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:get/get.dart';
import 'package:mobile/repositories/user_repository.dart';
import 'package:mobile/shared/models/user_item_model.dart';
import 'package:mobile/shared/models/user_model.dart';
import 'package:mobile/utils/customLoader.dart';
import 'package:mobile/utils/utility.dart';
import 'package:collection/collection.dart';

class SellerFeedbackController extends GetxController {

  final _loader = CustomLoader();

  final formKey = GlobalKey<FormBuilderState>();

  var _seller = UserModel().obs;

  UserModel get seller => _seller.value;

  var rates = <UserRateModel>[].obs;
  var positiveRates = <UserRateModel>[].obs;
  var naturalRates = <UserRateModel>[].obs;
  var negativeRates = <UserRateModel>[].obs;

  final _repository = Get.find<UserRepository>();

  SellerFeedbackController({required this.userId});

  final String userId;

  var _loading = false.obs;
  bool get loading => _loading.value;

  var rating = 0.0.obs;
  var ratingId = 0.obs;
  var comment = ''.obs;


  @override
  void onInit() {
    Timer.periodic(new Duration(seconds: 5), (timer) {
      _loadFeedback();
    });

    _loadFeedback();

    super.onInit();
  }

  Future<void> _loadFeedback() async {
    _loading(true);
    update();

    int makerId = 0;
    final bool loggedIn = await Utility.isLoggedIn()?? false;
    if(loggedIn) {
      final UserModel? currentUser = await Utility.getCurrentUser();
      if(currentUser?.id != null) {
        makerId = currentUser!.id!;
      }
    }
    final Map<String, dynamic>? _result = await  _repository.getSellerFeedback(
      userId: int.tryParse(userId),
      makerId: makerId
    );

    if(_result?['data'] != null) {
      var data = _result?['data'];

      print('data: $data');


      final _appUser = UserModel.fromJson(data?['AppUser']);
      _seller(_appUser);

      var _currentRating = (data['ratings']['select'] as List)
          .cast<Map<String, dynamic>>()
          .map(UserRateModel.fromJson)
          .firstOrNull;
      if(_currentRating != null) {
        ratingId(_currentRating.id);
        rating(_currentRating.rate?.toDouble());
        comment(_currentRating.comment);
      }

      var _rates = (data['rates']['select'] as List)
          .cast<Map<String, dynamic>>()
          .map(UserRateModel.fromJson)
          .toList();

      var _positive = (data['positiveRates']['select'] as List)
          .cast<Map<String, dynamic>>()
          .map(UserRateModel.fromJson)
          .toList();

      var _natural = (data['naturalRates']['select'] as List)
          .cast<Map<String, dynamic>>()
          .map(UserRateModel.fromJson)
          .toList();

      var _negative = (data['negativeRates']['select'] as List)
          .cast<Map<String, dynamic>>()
          .map(UserRateModel.fromJson)
          .toList();

      rates(_rates);
      positiveRates(_positive);
      naturalRates(_natural);
      negativeRates(_negative);

    }

    update();
  }

  Future<void> onChangeRating(final double value) async {
    rating(value);
    update();
  }

  Future<void> onChangeComment(final String? value) async {
    comment(value);
    print('change change $value');
    update();
  }

  Future<void> handleRating(final BuildContext context) async {

    _loader.showLoader(context);

    try {
      if(ratingId.value != 0) {
         await _handleUpdate();
      } else {
        await _handleCreate();
      }

    } catch(e) {
      print('error $e');
    }

    await _loadFeedback();
    _loader.hideLoader();

    update();
  }


  Future<bool> _handleCreate() async {

    try {
      final Map<String, dynamic> input = {
        'userId': userId,
        'rate': rating.toInt(),
        'comment': formKey.currentState?.value['comment'],
      };
      print('input: $input');

      var result = await _repository.createFeedBack(data: input);
      if(result != null) {
        return true;
      }
    } catch(e) {
      print('error $e');
    }
    return false;
  }
  Future<bool> _handleUpdate() async {

    try {
      final Map<String, dynamic> input = {
        'id': ratingId.toInt(),
        'rate': rating.toInt(),
        'comment': formKey.currentState?.value['comment'],
      };
      print('update input: $input');

      var result = await _repository.updateFeedBack(data: input);
      if(result != null) {
        return true;
      }

    } catch(e) {
      print('error $e');
    }
    return false;
  }
}
