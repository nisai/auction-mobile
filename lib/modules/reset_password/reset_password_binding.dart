import 'package:dio/dio.dart';
import 'package:get/get.dart';
import 'package:mobile/core/http_client.dart';
import 'package:mobile/modules/reset_password/reset_password_controller.dart';

class ResetPasswordBinding extends Bindings {

  @override
  void dependencies() {
    Get.lazyPut<HttpClient>(() => HttpClient(Dio()));
    Get.lazyPut(() => ResetPasswordController(repository: Get.find()));
  }

}