import 'dart:async';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:mobile/core/local_storage.dart';
import 'package:mobile/repositories/user_repository.dart';
import 'package:mobile/routes/app_pages.dart';
import 'package:mobile/shared/models/access_token_model.dart';
import 'package:mobile/utils/constant.dart';
import 'package:mobile/utils/customLoader.dart';

class ResetPasswordController extends GetxController {

  final resetPasswordFormKey = GlobalKey<FormState>();
  final phoneFormKey = GlobalKey<FormState>();
  final _loader = CustomLoader();
  final _storage = Get.find<LocalStorage>();

  final UserRepository repository;

  var phoneController = TextEditingController().obs;
  var passwordController = TextEditingController().obs;
  var confirmationPasswordController = TextEditingController().obs;
  var smsCodeController = TextEditingController().obs;

  var _loading = false.obs;

  var _successVerification = false.obs;
  bool get successVerification => _successVerification.value;


  ResetPasswordController({required this.repository});


  String get password => passwordController.value.text;

  String get smsCode => smsCodeController.value.text;

  String get phoneNumber => phoneController.value.text;

  static ResetPasswordController get to => Get.find<ResetPasswordController>();


  var _accessToken = AccessTokenModel().obs;
  AccessTokenModel? get accessToken => _accessToken.value;


  final LocalStorage storage = Get.find<LocalStorage>();

  bool get isResetPassword => accessToken != null;


  var _countryDialCode = '+855'.obs;
  String get countryDialCode => _countryDialCode.value;

  String get fullPhoneNumber {
    final phone =  '$countryDialCode${phoneNumber.replaceFirst(new RegExp(r'^0+'), '')}'.replaceAll(new RegExp(r'^0+(?=.)'), '');
    print('fullPhoneNumber is : $phone');
    return phone;
  }


  var _verificationId = ''.obs;
  String get verificationId => _verificationId.value;

  var _codeSent = false.obs;
  bool get codeSent => _codeSent.value;

  var _firstSent = true.obs;

  bool get firstSent => _firstSent.value;

  var _allowVerify = false.obs;
  var _allowSmsDuration = 0.obs;

  bool get allowVerify => _allowVerify.value;

  int get allowSmsDuration => _allowSmsDuration.value;

  // LoginStatus get status => _status;
  var _resentToken = 0.obs;
  int get resentToken => _resentToken.value;

  final FirebaseAuth _auth = Get.find();

  User? get getUser => _auth.currentUser;

  bool get loading => _loading.value;


  @override
  void onInit() {

    super.onInit();
  }

  Future<void> handleResetPassword(BuildContext context) async {
    try {
      _loader.showLoader(context);
      final bool? success = await repository.resetPassword(username: phoneNumber, password: password);
      print('success: $success');

      if(success != null && success == true) {
        final _token = await repository.login(username: phoneNumber, password: password);
        if(_token != null && _token.accessToken !=  null) {
          print('user logged ${_token.accessToken}');
          await _storage.write(Constant.accessToken, _token.toMap());
          WidgetsBinding.instance.addPostFrameCallback((_) {
            Get.rootDelegate.offNamed(Routes.HOME);
          });
        }
      } else {
        Get.snackbar('username or password incorrect', 'Please use your real username and password to reset_password', snackPosition: SnackPosition.BOTTOM);
      }

    } catch(e) {
      Get.snackbar('username or password incorrect', 'Please use your real username and password to reset_password', snackPosition: SnackPosition.BOTTOM);
      print('controller: $e');
    } finally {
      _loader.hideLoader();
    }

  }

  Future<void> handleResent() async {
    try {
      smsCodeController.value.text = '';
      _firstSent(false);
      _loading(true);
      _successVerification(false);
      update();
      await _verifyPhoneNumber(
          fullPhoneNumber,
          forceResendingToken: resentToken
      );
      _loading(false);
    } catch(e) {
      print('handleResent: $e');
    } finally {
      update();
    }
  }


  Future<void> _onCodeAutoRetrievalTimeout(String verificationId) async {
    _verificationId(verificationId);
    print('onCodeAutoRetrievalTimeout $verificationId');
    update();
  }

  Future<void> _onCodeSent(String? verificationId, int? resentToken) async {
    print('onCodeSent $verificationId , resentToken: $resentToken');
    _verificationId(verificationId);
    _resentToken(resentToken);
    // duration must above sent code variable
    _allowSmsDuration(30);
    _codeSent(true);
    _resolveWaitingTimeOut();
    _loader.hideLoader();

    update();
  }

  void changeCountryCode(final String? dialCode) {
    _countryDialCode(dialCode);
    update();
  }

  void completedOtp(final bool completed)  {
    _allowVerify(true);
    update();
  }

  Future<void> sendContinue(final BuildContext context) async {
    try{
      _loader.showLoader(context);
      smsCodeController.value.text = '';
      _firstSent(true);
      _loading(true);
      update();

      await _verifyPhoneNumber(
          fullPhoneNumber,
          forceResendingToken: resentToken
      );
      _loading(false);
    } catch(e) {
      print('sendContinue error $e');
    } finally {
      update();
    }
  }

  Future<void> handleVerifySMS(final BuildContext context) async {
    _loader.showLoader(context);
    await _verifySmsCode(verificationId: verificationId, smsCode: smsCode);
  }

  Future<void> _verifyPhoneNumber(final String p, {required int forceResendingToken}) async {
    return  _auth.verifyPhoneNumber(
      phoneNumber: p,
      verificationCompleted: _onVerificationCompleted,
      verificationFailed: (FirebaseAuthException e) {
        print('_verifyPhoneNumber => verificationFailed $e');
        _loader.hideLoader();
      },
      codeSent: _onCodeSent,
      forceResendingToken: forceResendingToken,
      codeAutoRetrievalTimeout: _onCodeAutoRetrievalTimeout,
    );
  }


  Future<void> _verifySmsCode({required String verificationId, required String smsCode}) async {
    final PhoneAuthCredential credential = PhoneAuthProvider.credential(
        verificationId: verificationId,
        smsCode: smsCode
    );
    _onVerificationCompleted(credential);
  }

  Future<void> _onVerificationCompleted(PhoneAuthCredential credential) async {

    try {
      UserCredential userCredential = await _auth.signInWithCredential(credential);
      print('complete verify phone ${userCredential.user}');

      if(userCredential.user != null) {
        _successVerification(true);
        _loader.hideLoader();

        update();
      }
    } on FirebaseAuthException catch (error){
      print("error : $error");
    }

  }


  void _resolveWaitingTimeOut() {
    print('allowSmsDuration : $allowSmsDuration');
    Timer.periodic(Duration(seconds: 1), (Timer t) {
      if (allowSmsDuration == 0) {
        t.cancel();
        _allowVerify(false);
        update();

      } else {
        _allowSmsDuration.value --;
      }
    },
    );
  }

  @override
  void dispose() {

    super.dispose();
  }

}