import 'package:get/get.dart';
import 'package:mobile/core/gql_client.dart';
import 'package:mobile/modules/notification/notification_controller.dart';
import 'package:mobile/repositories/notification_repository.dart';

class NotificationBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<GraphQLClient>(()=>GraphQLClient());
    Get.lazyPut(() => NotificationRepository(graphQLClient: Get.find()));
    Get.lazyPut(() => NotificationController(repository: Get.find()));
  }

}