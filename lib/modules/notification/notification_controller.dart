import 'package:get/get.dart';
import 'package:mobile/repositories/notification_repository.dart';
import 'package:mobile/shared/models/notification_model.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class NotificationController extends GetxController{

  final refreshController = RefreshController(initialRefresh: false);

  final NotificationRepository repository;

  var _page = 1.obs;
  int get page => _page.value;

  var items = <NotificationModel>[].obs;

  NotificationController({required this.repository});
  @override
  void onInit() {
    super.onInit();
    onFirstPage();
  }


  Future<bool> onLoading() async {
    _page.value ++;
    final value = await repository.paginate(page: page);

    if(value != null) {
      items.addAll(value);
    }
    refreshController.loadComplete();
    return true;
  }

  Future<void> onFirstPage() async {
    final _items = await repository.paginate(page: page);
    print('load: ${_items.toString()}');
    items(_items);
    refreshController.refreshCompleted();
    update();
  }

  void handleRead(final int? id) async {
    await repository.read(id);
    await onFirstPage();
  }
}