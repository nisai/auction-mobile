import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mobile/modules/notification/notification_controller.dart';
import 'package:mobile/shared/widgets/notification/notification_item.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class NotificationView extends GetView<NotificationController> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SmartRefresher(
        enablePullDown: true,
        enablePullUp: true,
        header: WaterDropHeader(),
        footer: CustomFooter(
          builder: (BuildContext context, LoadStatus? mode) {
            Widget body;
            if (mode == LoadStatus.idle) {
              body = Text("pull up load");
            } else if (mode == LoadStatus.loading) {
              body = CupertinoActivityIndicator();
            } else if (mode == LoadStatus.failed) {
              body = Text("Load Failed!Click retry!");
            } else if (mode == LoadStatus.canLoading) {
              body = Text("release to load more");
            } else {
              body = Text("No more Data");
            }
            return Container(
              height: 55.0,
              child: Center(child: body),
            );
          },
        ),
        controller: controller.refreshController,
        onRefresh: controller.onFirstPage,
        onLoading: controller.onLoading,
        child: GetBuilder<NotificationController>(builder: (controller) {
          return ListView.builder(
              itemCount: controller.items.length,
              itemBuilder: (BuildContext context, int index) {
                return GestureDetector(
                  onTap: () {
                    controller.handleRead(controller.items[index].id);
                  },
                  child: NotificationItem(item: controller.items[index]),
                );
              });
        }),
      ),
    );
  }
}
