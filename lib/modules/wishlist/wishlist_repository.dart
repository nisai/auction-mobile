
import 'package:get/get.dart';
import 'package:mobile/core/gql_client.dart';
import 'package:mobile/documents/product_document.dart';

class WishlistRepository {

  final _graphQL = Get.find<GraphQLClient>();

  Future<Map<String, dynamic>?> getWatchListItemByUserId(final int? userId) async {
    try {
      final _result = await _graphQL.call(authorization: true)
          .query(ProductDocument.getWatchListItems,
          variables: <String, dynamic>{'userId': userId});
      return _result;
    } catch(e) {
      print('error $e');
    }
    return null;
  }


}
