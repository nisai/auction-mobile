import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mobile/modules/wishlist/wishlist_controller.dart';
import 'package:mobile/routes/app_pages.dart';
import 'package:mobile/shared/models/user_item_model.dart';
import 'package:mobile/shared/widgets/wishlist_product_item_widget.dart';

class WishlistView extends GetView<WishlistController> {

  @override
  Widget build(BuildContext context) {

    return GetBuilder<WishlistController>(builder: (controller) {
      return Scaffold(
        appBar: AppBar(
          title: Text('wishlist'.tr),
          elevation: 0,
          centerTitle: true,
          leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () {
              Get.rootDelegate.offNamed(Routes.OTHERS);
            },
          ),
        ),
        body: SingleChildScrollView(
          child:  GridView.builder(
              physics: NeverScrollableScrollPhysics(),
              shrinkWrap: true,
              itemCount: controller.items.length,
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 2,
                crossAxisSpacing: 0.2,
                mainAxisSpacing: 10,
                childAspectRatio: 0.7,
              ),
              itemBuilder: (BuildContext context, int index) {
                final WatchProductModel item = controller.items[index];
                return  GestureDetector(
                    onTap: () {
                      Get.rootDelegate.toNamed(Routes.productDetail(productId: '${item.product?.id}'));
                    },
                    child: WishlistProductItem(item: item));
              }
          ),
        ),
      );
    });
  }
}