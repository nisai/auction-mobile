import 'package:get/get.dart';
import 'package:mobile/modules/wishlist/wishlist_repository.dart';
import 'package:mobile/shared/models/user_item_model.dart';
import 'package:mobile/shared/models/user_model.dart';
import 'package:mobile/utils/utility.dart';

class WishlistController extends GetxController {

  var items = <WatchProductModel>[].obs;

  static final WishlistController to = Get.find<WishlistController>();

  final WishlistRepository repository;

  WishlistController({required this.repository});

  @override
  void onInit() {

    _fetchData().then((value) {
      update();
    });

    super.onInit();
  }

  Future<void> _fetchData() async {
    final isLoggedIn = await Utility.isLoggedIn()?? false;
    if(isLoggedIn) {
      final UserModel? user = await Utility.getCurrentUser();
      final Map<String, dynamic>? _result = await repository.getWatchListItemByUserId(user?.id);
      if(_result?['data'] != null) {
        var data = _result?['data'];
        var _items = (data['WatchLists']['select'] as List)
            .cast<Map<String, dynamic>>()
            .map(WatchProductModel.fromJson)
            .toList();
        items(_items);
      }
    }
  }


}
