import 'package:get/get.dart';
import 'package:mobile/modules/wishlist/wishlist_controller.dart';
import 'package:mobile/modules/wishlist/wishlist_repository.dart';

class WishlistBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => WishlistRepository());
    Get.lazyPut(() => WishlistController(repository: Get.find<WishlistRepository>()));
  }
}
