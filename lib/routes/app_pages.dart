import 'package:get/get.dart';
import 'package:mobile/middleware/auth_middleware.dart';
import 'package:mobile/middleware/language_middleware.dart';
import 'package:mobile/modules/authentication/authentication_binding.dart';
import 'package:mobile/modules/authentication/authenication_view.dart';
import 'package:mobile/modules/buying_item/buying_item_binding.dart';
import 'package:mobile/modules/buying_item/buying_item_view.dart';
import 'package:mobile/modules/category/category_binding.dart';
import 'package:mobile/modules/category/category_view.dart';
import 'package:mobile/modules/change_password/change_password_binding.dart';
import 'package:mobile/modules/change_password/change_password_view.dart';
import 'package:mobile/modules/dashboard/dasbhoard_binding.dart';
import 'package:mobile/modules/dashboard/dashboard_view.dart';
import 'package:mobile/modules/deposit_wallet/deposit_wallet_binding.dart';
import 'package:mobile/modules/deposit_wallet/deposit_wallet_view.dart';
import 'package:mobile/modules/home/home_binding.dart';
import 'package:mobile/modules/home/home_view.dart';
import 'package:mobile/modules/language/language_bindings.dart';
import 'package:mobile/modules/language/language_view.dart';
import 'package:mobile/modules/login/login_binding.dart';
import 'package:mobile/modules/login/login_view.dart';
import 'package:mobile/modules/notification/notification_binding.dart';
import 'package:mobile/modules/notification/notification_view.dart';
import 'package:mobile/modules/other/other_binding.dart';
import 'package:mobile/modules/other/other_view.dart';
import 'package:mobile/modules/post/post_listing_view.dart';
import 'package:mobile/modules/post_edit/post_edit_binding.dart';
import 'package:mobile/modules/post_edit/post_edit_view.dart';
import 'package:mobile/modules/product/product_binding.dart';
import 'package:mobile/modules/product/product_view.dart';
import 'package:mobile/modules/product_bidding/product_bidding_binding.dart';
import 'package:mobile/modules/product_bidding_history/product_bidding_history_binding.dart';
import 'package:mobile/modules/product_bidding_history/product_bidding_history_view.dart';
import 'package:mobile/modules/product_buying/product_buying_binding.dart';
import 'package:mobile/modules/product_buying/product_buying_view.dart';
import 'package:mobile/modules/product_comment/product_comment_binding.dart';
import 'package:mobile/modules/product_comment/product_comment_view.dart';
import 'package:mobile/modules/post/post_binding.dart';
import 'package:mobile/modules/product_detail/product_detail_binding.dart';
import 'package:mobile/modules/product_bidding/product_bidding_view.dart';
import 'package:mobile/modules/product_detail/product_detail_view.dart';
import 'package:mobile/modules/product_payment/product_payment_binding.dart';
import 'package:mobile/modules/product_payment/product_payment_view.dart';
import 'package:mobile/modules/profile/profile_binding.dart';
import 'package:mobile/modules/profile/profile_view.dart';
import 'package:mobile/modules/register/register_binding.dart';
import 'package:mobile/modules/register/register_view.dart';
import 'package:mobile/modules/reset_password/reset_password_binding.dart';
import 'package:mobile/modules/reset_password/reset_password_view.dart';
import 'package:mobile/modules/root/bindings/root_binding.dart';
import 'package:mobile/modules/root/views/root_view.dart';
import 'package:mobile/modules/seller_feedback/seller_feedback_binding.dart';
import 'package:mobile/modules/seller_feedback/seller_feedback_view.dart';
import 'package:mobile/modules/selling_item/selling_item_binding.dart';
import 'package:mobile/modules/selling_item/selling_item_view.dart';
import 'package:mobile/modules/splash/bindings/splash_binding.dart';
import 'package:mobile/modules/splash/presentation/views/splash_view.dart';
import 'package:mobile/modules/user_account/user_account_binding.dart';
import 'package:mobile/modules/user_account/user_account_view.dart';
import 'package:mobile/modules/seller_detail/seller_detaill_binding.dart';
import 'package:mobile/modules/seller_detail/seller_detail_view.dart';
import 'package:mobile/modules/wishlist/wishlist_binding.dart';
import 'package:mobile/modules/wishlist/wishlist_view.dart';

part 'app_routes.dart';

abstract class AppPages {

  static const INITIAL = Routes.HOME;


  static final routes = [

  GetPage(
    name: _Paths.ROOT,
    page: () => RootView(),
    binding: RootBinding(),
    participatesInRootNavigator: true,
    preventDuplicates: true,
    middlewares: [
      LanguageMiddleware(),
    ],
    children: [
      GetPage(
        preventDuplicates: true,
        name: _Paths.HOME,
        page: () => HomeView(),
        bindings: [
          HomeBinding(),
        ],

        title: null,
        children: [

          GetPage(
            name: _Paths.DASHBOARD,
            page: () => DashboardView(),
            binding: DashboardBinding(),
          ),

          GetPage(
            name: _Paths.CATEGORY,
            page: () => CategoryView(),
            title: 'Category',
            transition: Transition.size,
            binding: CategoryBinding(),
          ),


          GetPage(
            middlewares: [
              EnsureAuthMiddleware(),
            ],
            name: _Paths.NOTIFICATION,
            page: () => NotificationView(),
            title: 'Notification',
            transition: Transition.size,
            binding: NotificationBinding(),
          ),

          GetPage(
            middlewares: [
              EnsureAuthMiddleware(),
            ],
            name: _Paths.OTHERS,
            page: () => OtherView(),
            title: 'More',
            transition: Transition.zoom,
            binding: OtherBinding(),
          ),

        ],
      ),

      GetPage(
        name: Routes.SPLASH,
        page: () => SplashView(),
        binding: SplashBinding(),
      ),


      // future may allow resume
      GetPage(
          middlewares: [
            EnsureAuthMiddleware(),
          ],
          name: _Paths.POST_LISTING,
          page: () => PostListingView(),
          binding: PostBinding()
      ),

      GetPage(
        binding: ProductPaymentBiding(),
        name: _Paths.PRODUCT_PAYMENT,
        page: () => ProductPaymentView(),
      ),

      GetPage(
        binding: LanguageBinding(),
        name: _Paths.LANGUAGE,
        page: () => LanguageView(),
      ),

      GetPage(
        binding: PostEditBinding(),
        name: _Paths.POST_EDIT,
        page: () => PostEditView(),
      ),

      GetPage(
        name: _Paths.AUTHENTICATION,
        page: () => AuthenticationView(),
        binding: AuthenticationBinding(),
      ),

      GetPage(
        binding: LoginBinding(),
        name: _Paths.LOGIN,
        page: () => LoginView(),
      ),

      GetPage(
          binding: ResetPasswordBinding(),
          name: _Paths.RESET_PASSWORD,
          page: () => ResetPasswordView(),
      ),

      GetPage(
          binding: RegisterBinding(),
          name: _Paths.REGISTER,
          page: () => RegisterView(),
      ),

      GetPage(
        name: _Paths.PROFILE,
        page: () => ProfileView(),
        binding: ProfileBinding(),
      ),

      GetPage(
        name: _Paths.WISHLIST,
        page: () => WishlistView(),
        binding: WishlistBinding(),
      ),

      GetPage(
        name: _Paths.PRODUCT,
        page: () => ProductView(),
        binding: ProductBinding(),
      ),


      // product details
      GetPage(
          name: _Paths.PRODUCT_DETAIL,
          page: () => ProductDetailView(),
          binding: ProductDetailBinding(),
      ),

      GetPage(
        middlewares: [
          EnsureAuthMiddleware(),
        ],
        binding: ProductCommentBinding(),
        name: _Paths.PRODUCT_COMMENT,
        page: () => ProductCommentView(),
      ),

      GetPage(
        middlewares: [
          EnsureAuthMiddleware(),
        ],
        binding: ProductBiddingBinding(),
        name: _Paths.PRODUCT_BIDDING,
        page: () => ProductBiddingView(),
      ),

      GetPage(
        middlewares: [
          EnsureAuthMiddleware(),
        ],
        binding: ProductBiddingHistoryBinding(),
        name: _Paths.PRODUCT_BIDDING_HISTORY,
        page: () => ProductBiddingHistoryView(),
      ),

      GetPage(
        middlewares: [
          EnsureAuthMiddleware(),
        ],
        binding: ProductBuyingBinding(),
        name: _Paths.PRODUCT_BUYING,
        page: () => ProductBuyingView(),
      ),

      GetPage(
        binding: UserAccountBinding(),
        name: _Paths.USER_ACCOUNT,
        page: () => UserAccountView(),
      ),

      GetPage(
        binding: DepositWalletBinding(),
        name: _Paths.DEPOSIT_WALLET,
        page: () => DepositWalletView(),
      ),

      GetPage(
        name: _Paths.BUYING_ITEM,
        page: () => BuyingItemView(),
        binding: BuyingItemBinding(),
      ),

      GetPage(
        name: _Paths.SELLING_ITEM,
        page: () => SellingItemView(),
        binding: SellingItemBinding(),
      ),

      GetPage(
        binding: ChangePasswordBindings(),
        name: _Paths.CHANGE_PASSWORD,
        page: () => ChangePasswordView(),
      ),

      GetPage(
        binding: SellerDetailBinding(),
        name: _Paths.SELLER_DETAIL,
        page: () => SellerDetailView(),
      ),

      GetPage(
        binding: SellerFeedbackBinding(),
        name: _Paths.SELLER_FEEDBACK,
        page: () => SellerFeedbackView(),
      ),
    ]
  )
  ];
}
