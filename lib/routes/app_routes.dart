part of 'app_pages.dart';

abstract class Routes {

  static const ROOT = _Paths.ROOT;
  static const HOME = _Paths.HOME;
  static const CATEGORY = _Paths.HOME + _Paths.CATEGORY;
  static const NOTIFICATION = _Paths.HOME + _Paths.NOTIFICATION;
  static const OTHERS = _Paths.HOME + _Paths.OTHERS;
  static const DASHBOARD = _Paths.HOME + _Paths.DASHBOARD;

  static const POST_LISTING = _Paths.POST_LISTING;
  static const PRODUCT_PAYMENT = _Paths.PRODUCT_PAYMENT;
  static const DEPOSIT_WALLET = _Paths.DEPOSIT_WALLET;

  static const VERIFY_PAYMENT_CODE = '/verify-deposit_wallet';
  static const BID = '/bid';
  static const BID_HISTORY = '/bid-history';
  static const COUNTRY = '/country';
  static const DETAILS = '/details';
  static const LOGIN = _Paths.LOGIN;
  static const VERIFICATION_SMS = _Paths.VERIFICATION_SMS;
  static const CONNECTION = '/connection';

  static const RESET_PASSWORD = _Paths.RESET_PASSWORD;
  static const RESET_PASSWORD_VERIFICATION =  _Paths.RESET_PASSWORD + _Paths.RESET_PASSWORD_VERIFICATION;
  static const RESET_PASSWORD_FORM =  _Paths.RESET_PASSWORD + _Paths.RESET_PASSWORD_FORM;

  static const REGISTER = _Paths.REGISTER;
  static const AUTHENTICATION = _Paths.AUTHENTICATION;
  static const USER_DASHBOARD = '/dashboard-user';
  static const USER_ACCOUNT = _Paths.USER_ACCOUNT;
  static const CHANGE_PASSWORD = _Paths.CHANGE_PASSWORD;
  static const BUYING_ITEM = _Paths.BUYING_ITEM;
  static const SELLING_ITEM = _Paths.SELLING_ITEM;
  static const PROFILE = _Paths.PROFILE;

  static const WING = _Paths.WING;
  static const PIPAY = _Paths.PIPAY;

  static const LANGUAGE = _Paths.LANGUAGE;
  static const WISHLIST = _Paths.WISHLIST;
  static const PRODUCT = _Paths.PRODUCT;


  static const REGISTER_VERIFICATION = '/register-verification';
  static const LANDING = '/landing';
  static const SPLASH = '/splash';


 // static const DASHBOARD = '/dashboard';

  static const CHANGE_PIN = '/change-pin';

  static const PRODUCT_RECOMMEND = '/product-recommend';


  static String authenticationThenTo(final String thenTo) => '$AUTHENTICATION?then=${Uri.encodeQueryComponent(thenTo)}';
  static String loginThenTo(final String thenTo) => '$LOGIN?then=${Uri.encodeQueryComponent(thenTo)}';
  static String connectionThenTo(final String thenTo) => '$CONNECTION?then=${Uri.encodeQueryComponent(thenTo)}';
  static String depositThenTo(final String thenTo) => '$DEPOSIT_WALLET?then=${Uri.encodeQueryComponent(thenTo)}';

  static String sellerDetail({final String? userId}) => '/seller-detail/$userId';
  static String sellerFeedback({final String? userId}) => '/seller-feedback/$userId';
  static String productDetail({final String? productId}) => '/product-detail/$productId';
  static String productComment({final String? productId}) => '/product-comment/$productId';
  static String productBidding({final String? productId}) => '/product-bidding/$productId';
  static String productBuying({final String? productId}) => '/product-buying/$productId';
  static String productPayment({final String? productId}) => '/product-payment/$productId';
  static String productBiddingHistory({final String? productId}) => '/product-bidding-history/$productId';

  static String postEdit({final String? productId}) => '/post-edit/$productId';
}



abstract class _Paths {
  static const ROOT = '/';
  static const HOME = '/home';
  static const CATEGORY = '/category';
  static const NOTIFICATION = '/notification';
  static const RESET_PASSWORD = '/reset-password';
  static const RESET_PASSWORD_VERIFICATION = '/reset-password-verification';
  static const RESET_PASSWORD_FORM = '/reset-password-form';
  static const OTHERS = '/others';
  static const LOGIN = '/login';
  static const AUTHENTICATION = '/authentication';
  static const REGISTER = '/register';
  static const PRODUCT = '/products';
  static const WISHLIST = '/wish-list';
  static const SELLER_DETAIL = '/seller-detail/:userId';
  static const SELLER_FEEDBACK = '/seller-feedback/:userId';
  static const PRODUCT_DETAIL = '/product-detail/:productId';
  static const PRODUCT_COMMENT = '/product-comment/:productId';
  static const PRODUCT_BIDDING= '/product-bidding/:productId';
  static const PRODUCT_BUYING = '/product-buying/:productId';
  static const PRODUCT_PAYMENT = '/product-payment/:productId';
  static const PRODUCT_BIDDING_HISTORY = '/product-bidding-history/:productId';
  static const PROFILE = '/profile';
  static const DASHBOARD = '/dashboard';
  static const VERIFICATION_SMS = '/verification-sms';

  static const POST_LISTING = '/post-listing';

  static const USER_ACCOUNT = '/user-account';
  static const DEPOSIT_WALLET = '/deposit-wallet';
  static const WING = '/wing';
  static const PIPAY = '/pipay';
  static const CHANGE_PASSWORD = '/change-password';
  static const LANGUAGE = '/language';
  static const BUYING_ITEM = '/buying-item';
  static const SELLING_ITEM = '/selling-item';

  static const POST_EDIT = '/post-edit/:productId';
}
