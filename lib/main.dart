import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:mobile/core/theme/colors.dart';
import 'package:mobile/core/landing_binding.dart';
import 'lang/translation_service.dart';
import 'routes/app_pages.dart';
import 'shared/logger/logger_utils.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:form_builder_validators/form_builder_validators.dart';

void main() async {

  WidgetsFlutterBinding.ensureInitialized();

  await GetStorage.init();

  await initServices();

  runApp(App());
}

class App extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: Firebase.initializeApp(
        options: const FirebaseOptions(
          apiKey: 'AIzaSyB46ZeY1E6i7gd83k1QGyuzNJLHOLzsgKo',
          appId: '1:815601442430:android:198a2433d390911a2f822f',
          messagingSenderId: '815601442430',
          projectId: 'khmer-auction-d8bf5',
        ),
      ),
      builder: (context, snapshot) {
        if (snapshot.hasError) {
          return Container(child: Text("We are sorry your device not support google service."));
        }
        if (snapshot.connectionState == ConnectionState.done) {
          return MyApp();
        }
        return Container();
      },
    );
  }
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp.router(
      supportedLocales: const [
        Locale('en'),
        Locale('kh'),
      ],
      localizationsDelegates: const [
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        FormBuilderLocalizations.delegate,
      ],
      initialBinding: LandingBinding(),
      theme: ThemeData(
        brightness: Brightness.light,
        primaryColor: AppColor.primaryColor,
        appBarTheme: AppBarTheme(
          backgroundColor: AppColor.primaryColor, //use your hex code here
        ),
        fontFamily: 'Battambang',
        colorScheme: ColorScheme.fromSwatch().copyWith(secondary: AppColor.primaryColor),
      ),
      debugShowCheckedModeBanner: false,
      enableLog: true,
      logWriterCallback: (final String text, {final bool? isError}) {
        logger.d('isError: $isError, text: $text');
      },
      getPages: AppPages.routes,
      locale: TranslationService.locale,
      fallbackLocale: Locale('en', 'US'),
      translations: TranslationService(),
      customTransition: SizeTransitions(),
    );
  }
}

class SizeTransitions extends CustomTransition {
  @override
  Widget buildTransition(
      BuildContext context,
      Curve? curve,
      Alignment? alignment,
      Animation<double> animation,
      Animation<double> secondaryAnimation,
      Widget child) {
    return Align(
      alignment: Alignment.center,
      child: SizeTransition(
        sizeFactor: CurvedAnimation(
          parent: animation,
          curve: curve!,
        ),
        child: child,
      ),
    );
  }
}


Future<void> initServices() async {
  print('starting services ...');
  /// Here is where you put get_storage, hive, shared_pref initialization.
  /// or moor connection, or whatever that's async
  ///
  await Get.putAsync(() => DbService().init());
  print('All services started...');


}

class DbService extends GetxService {
  Future<DbService> init() async {
    print('$runtimeType ready!');
    return this;
  }
}