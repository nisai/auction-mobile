class NotificationDocument {
   static const queryNotification = r"""
    query paginateNotification($page: Int!, $limit: Int!){
       AppNotifications(
         page:{start: $page, limit: $limit}
      ){
        pages
        total
         select{
           id(orderBy: DESC)
           title,
           detail,
           type,
           status,
           createdAt,
           image
          
        }  
       }
   }     
    """;


   static const readNotification = r'''
    mutation readNotification($id: Long!) {
        UpdateAppNotification(
          id: $id, 
          input: {
            status: SEEN
          }) 
        {
          id
        }
      }
   ''';
}