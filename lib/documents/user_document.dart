class UserDocument {

  static const getBuyingItems = r'''
    query getBuyingItems($userId: Long!) {
         ProductOrders (where: { createdBy: { id: { EQ: $userId } } }){
            total
            pages
            select {
              id
              product {
                ...getProductFields
              }
            }
          }
     
         BidPrices(where:{createdBy: {id: {EQ: $userId}}}) {
            total
           pages
           select{
            id
            amount
            createdBy {
             ...getUserFields
            }
            product {
               ...getProductFields
            }
            createdAt
           }
         }
         
         LoseProducts {
           total
           pages
           select{
            id
            user {
             ...getUserFields
            }
            product {
               ...getProductFields
            }
            bidPrice {
              id,
              amount,
              createdAt
            }
           }
         }
         
         WinProducts {
           total
           pages
           select{
            id
            state
            user {
             ...getUserFields
            }
            product {
               ...getProductFields
            }
            bidPrice {
              id,
              amount,
              createdAt
            }
           }
         } 
    }
    
    fragment getProductFields on Product {
        id(orderBy: DESC)
        name
        subTitle
        description
        createdAt
        bidFinishAt
        expectFinishAt
        description,
        status
        bgImage
        slideImage
        bidDuration
        delivery
        buyNowPrice
        originalPrice
        reservePrice
        
        province {
          id,
          name
        }
        
        category {
          orderNumber
          name
          logoImage
          id
        }
        
        bidPrice {
          createdBy {
            ...getUserFields
          }
          createdAt
          id
          amount
        }
        
        highestBid {
          id
          createdAt
          amount
        }
    }
    
    fragment getUserFields on AppUser {
      id
      userName
      status
      role
      profileImage
      mobile
      email
    }
  ''';


  static const getSellingItems = r''' 
    query getSellingItem ($userId: Long!) { 
     drafts: Products(where:{createdBy: {id: {EQ: $userId}}, status:{EQ: SCHEDULED}}){
         select{
            ...getProductFields
         }
     }
     selling: Products(where: {createdBy: {id: {EQ: $userId}}, status: { EQ: BID_IN_PROGRESS } }){
         total
         pages
         select{
            ...getProductFields
         }
     }
       
     sold: Products(where: {createdBy: {id: {EQ: $userId}}, status: { EQ: SOLD } }){
         total
         pages
         select{
            ...getProductFields
         }
     }
         
     expired: Products(where: {createdBy: {id: {EQ: $userId}}, status: { EQ: EXPIRED } }){
         total
         pages
         select{
            ...getProductFields
         }
     }
  }
  
  fragment getProductFields on Product {
      id(orderBy: DESC)
      name
      subTitle
      description
      createdAt(orderBy: DESC)
      bidFinishAt
      expectFinishAt
      description,
      status
      bgImage
      slideImage
      bidDuration
      delivery
      buyNowPrice
      originalPrice
      reservePrice
      label
      province {
        id,
        name
      }
      category {
        orderNumber
        name
        logoImage
        id
      }
      bidPrice {
        createdBy {
          ...getUserFields
        }
        createdAt
        id
        amount
      }
  }
  
  fragment getUserFields on AppUser {
    id
    userName
    status
    role
    profileImage
    mobile
    email
  }
  ''';


  static const getUserAccountWithTransaction = r'''
   query userAccount{
   
     UserAccounts {
        select{
          id
          amount
        }
      }
  
      UserAccountTransactions{
        select{
          createdAt(orderBy:DESC)
          amount
          transactionType
          transactionDetail
          partyAccountDetail
        }
      }
      
    }
  ''';

  static const getUserAccounts = r'''
   query userAccount{
     UserAccounts {
        select{
          id
          amount
        }
      }
    }
  ''';


  static const createOrUpdateFirebaseToken = r'''
  mutation createOrUpdateFirebaseToken($token: String) {
    CreateFireBaseToken(input: { token: $token }) {
        token
    }
  }

  ''';


  static const getSellerDetail = r'''
  query getSellerDetail($userId: Long!) {
	
    Products (where: { createdBy: { id: { EQ: $userId } },  status: { EQ: BID_IN_PROGRESS }}){
      total
      pages
      select {
        ...getProductFields
      }
    }
    
    AppUser(id: $userId) {
      ...getUserFields
    }
    
    rates: UserRates (where: {user: {id: {EQ: $userId}}}) {
			select{ 
				id
				user {
					...getUserFields
				}
				createdBy{
					...getUserFields
				}
				rate
				comment
				createdAt
			}
		}
   
    positiveRates: UserRates (where: {rate: {GE: 4} AND: {user: {id: {EQ: $userId}}}}) {
			select{ 
				id
				user {
					...getUserFields
				}
				createdBy{
					...getUserFields
				}
				rate
				comment
				createdAt
			}
		}
	 
	  naturalRates: UserRates (where: {rate: {EQ: 3} AND: {user: {id: {EQ: $userId}}}}){
			select{ 
				id
				rate
				user {
					...getUserFields
				}
				createdBy {
					...getUserFields
				}
				comment
				createdAt
			}
		}
	
	  negativeRates: UserRates (where: {rate: {LT: 3} AND: {user: {id: {EQ: $userId}}}}){
			select{ 
				id
				user {
					...getUserFields
				}
				createdBy {
					...getUserFields
				}
				rate
				comment
				createdAt
			}
		}
    
  }
  
  fragment getProductFields on Product {
    id(orderBy: DESC)
    name
    subTitle
    description
    createdAt
    bidFinishAt
    expectFinishAt
    description
    status
    bgImage
    slideImage
    bidDuration
    delivery
    buyNowPrice
    originalPrice
    reservePrice
    label
    province {
      id
      name
    }
  
    category {
      orderNumber
      name
      logoImage
      id
    }
  
    bidPrice {
      createdBy {
        ...getUserFields
      }
      createdAt
      id
      amount
    }
  
    highestBid {
      id
      createdAt
      amount
    }
  }
  
  fragment getUserFields on AppUser {
    id
    userName
    status
    role
    profileImage
    mobile
    email
    createdAt
  }

  ''';

  static const getSellerFeedback = r'''
  query getSellerFeedback($userId: Long!, $makerId: Long!) {
  
    AppUser(id: $userId) {
      ...getUserFields
    }
    
    ratings: UserRates (where: {createdBy: {id: {EQ: $makerId}} AND: {user: {id: {EQ: $userId}}}}) {
			select{ 
				id
				user {
					...getUserFields
				}
				createdBy {
					...getUserFields
				}
				rate
				comment
				createdAt
			}
		}
    
    rates: UserRates (where: {user: {id: {EQ: $userId}}}) {
			select{ 
				id
				user {
					...getUserFields
				}
				createdBy {
					...getUserFields
				}
				rate
				comment
				createdAt
			}
		}
   
    positiveRates: UserRates (where: {rate: {GE: 4} AND: {user: {id: {EQ: $userId}}}}) {
			select{ 
				id
				user {
					...getUserFields
				}
				createdBy {
					...getUserFields
				}
				rate
				comment
				createdAt
			}
		}
	 
	  naturalRates: UserRates (where: {rate: {EQ: 3} AND: {user: {id: {EQ: $userId}}}}){
			select{ 
				id
				rate
				user {
					...getUserFields
				}
				createdBy{
					...getUserFields
				}
				comment
				createdAt
			}
		}
	
	  negativeRates: UserRates (where: {rate: {LT: 3} AND: {user: {id: {EQ: $userId}}}}){
			select{ 
				id
				user {
					...getUserFields
				}
				createdBy{
					...getUserFields
				}
				rate
				comment
				createdAt
			}
		}
		
  }
  
  fragment getUserFields on AppUser {
    id
    userName
    status
    role
    profileImage
    mobile
    email
    createdAt
  }

  ''';


  static const createFeedback = r'''
  
  mutation CreateUserRate($userId: Long, $rate: Int, $comment: String){
    CreateUserRate(input: {
      userId: $userId
      rate: $rate,
      comment: $comment,
    }) {
      id
    }
  }
  ''';

  static const updateFeedback = r'''
  mutation UpdateUserRate($id: Long!, $rate: Int, $comment: String){
    UpdateUserRate(id: $id, input: {
      rate: $rate,
      comment: $comment,
    }) {
      id
    }
  }
  ''';

}