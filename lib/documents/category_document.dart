class CategoryDocument {

  static const getParents = r'''
     query getParents {
        Categories(where:{
          firstLevel:{
            EQ:true
          }
        }){
          select{
            id
            name
            logoImage
          }
        }
    }
  ''';

  static const getParentByChildId = r'''
     query getParentByChildId($categoryId: Long!) {
        Categories{
          select{
             id
             name
             logoImage
             firstLevel
             child(where:{id: {EQ: $categoryId}}){
               id
               name
              logoImage
              firstLevel
             }
          }
        }
    }
  ''';

  static const getAllChildCategory = r'''
     query getAllChildCategory {
        Categories(where:{
          firstLevel:{
            EQ:false
          }
        }){
          select{
            id
            name
            logoImage
          }
        }
    }
  ''';

  static const getChildrenByParent = r'''
 query getChildrens($parentId: Long!){
     Categories(where:{
       parent:{
         id:{EQ: $parentId}
       }
     }){
       select{
         id
         name
         logoImage
       }
     }
 }
''';


  static const getChildrenWitProduct = r"""
  
    query getChildrens($parentId: Long!){
         Categories(where:{
           parent:{
             id:{EQ: $parentId}
           }
         }){
           select{
             id
             name
             logoImage
           }
         }                       
     }
    """;

  static const getLatestProduct = r"""
  
    query getLatestProduct{                     
         Products(page:{start: 1, limit: 100}, where:{status:{EQ: BID_IN_PROGRESS}})
         {
            total
            pages
            select{
              ...getProductFields
            }
         }
     }
     fragment getProductFields on Product {
        id(orderBy: DESC)
        name
        subTitle
        description
        createdAt
        bidFinishAt
        expectFinishAt
        description,
        status
        bgImage
        slideImage
        bidDuration
        delivery
        buyNowPrice
        originalPrice
        reservePrice
        label
        province {
          id,
          name
        }
        category {
          orderNumber
          name
          logoImage
          id
        }
        bidPrice {
          createdBy {
            ...getUserFields
          }
          createdAt
          id
          amount
        }
    }
      
    fragment getUserFields on AppUser {
      id
      userName
      status
      role
      profileImage
      mobile
      email
    }
    """;



  static const getProductByCategory = r"""
  
    query getProductByCategory($categories: [Long]){                     
         Products(
           where: { category: { id: { IN: $categories} }, status: { EQ: BID_IN_PROGRESS } }){
            total
            pages
            select{
              ...getProductFields
            }
         }
     }
     fragment getProductFields on Product {
        id(orderBy: DESC)
        name
        subTitle
        description
        createdAt
        bidFinishAt
        expectFinishAt
        description,
        status
        bgImage
        slideImage
        bidDuration
        delivery
        buyNowPrice
        originalPrice
        reservePrice
        label
        province {
          id,
          name
        }
        category {
          orderNumber
          name
          logoImage
          id
        }
        bidPrice {
          createdBy {
            ...getUserFields
          }
          createdAt
          id
          amount
        }
    }
      
    fragment getUserFields on AppUser {
      id
      userName
      status
      role
      profileImage
      mobile
      email
    }
    """;

}