class MainHomeDocument {

  static const getFirstPage =  r'''
    query getFirstPage($page: Int!, $limit: Int!) {
       banners: KeyValues(where: {key: {EQ: "slide_image"}}) {
         select {
          values :jsonValue
         }
       }
       
      categories: Categories(where:{
       firstLevel:{
         EQ:true
         }
       }){
         select{
           id
           name
           logoImage
         }
       }
       
       highLighted: Products(where:{label:{EQ: HIGH_LIGHTED}, status:{EQ: BID_IN_PROGRESS}}){
           total
           pages
           select{
              ...getProductFields
           }
       }
       
       urgent: Products(where:{label:{EQ: URGENT}, status:{EQ: BID_IN_PROGRESS}}){
           total
           pages
           select{
              ...getProductFields
           }
       }
       
      closingSoon: Products(where:{label:{EQ: FINISH_SOON}, status:{EQ: BID_IN_PROGRESS}}){
           total
           pages
           select{
              ...getProductFields
           }
       }
       
       r1000: Products(where:{label:{EQ: R1000}, status:{EQ: BID_IN_PROGRESS}}){
           total
           pages
           select{
              ...getProductFields
           }
       }
       
       latest: Products(page:{start: $page, limit: $limit},where:{status:{EQ: BID_IN_PROGRESS}}){
           total
           pages
           select{
              ...getProductFields
           }
       }
    }
    
    fragment getProductFields on Product {
        id(orderBy: DESC)
        name
        subTitle
        description
        createdAt
        bidFinishAt
        expectFinishAt
        description,
        status
        bgImage
        slideImage
        bidDuration
        delivery
        buyNowPrice
        originalPrice
        reservePrice
        label
        province {
          id,
          name
        }
        
        category {
          orderNumber
          name
          logoImage
          id
        }
         highestBid{
           createdBy {
              ...getUserFields
            }
            createdAt
            id
            amount
          }
          
          bidPrice {
            createdBy {
              ...getUserFields
            }
            createdAt
             id
             amount
          }  
      
    }

  fragment getUserFields on AppUser {
    id
    userName
    status
    role
    profileImage
    mobile
    email
  }
''';

  static const getMoreLatest = r'''
       query getHomeNext($page: Int!, $limit: Int!){
      latest: Products(page:{start: $page, limit: $limit}, where:{status:{EQ: BID_IN_PROGRESS}}){
         total
         pages
         select{
            ...getProductFields
         }
      }
   }

  fragment getProductFields on Product {
      id(orderBy: DESC)
      name
      subTitle
      description
      createdAt
      bidFinishAt
      expectFinishAt
      description,
      status
      bgImage
      slideImage
      bidDuration
      delivery
      buyNowPrice
      originalPrice
      reservePrice
      label
      province {
        id,
        name
      }
      category {
        orderNumber
        name
        logoImage
        id
      }
      
      highestBid{
       createdBy {
          ...getUserFields
        }
        createdAt
        id
        amount
      }
      
      bidPrice {
        createdBy {
          ...getUserFields
        }
        createdAt
         id
         amount
      }  
  }
  
  fragment getUserFields on AppUser {
    id
    userName
    status
    role
    profileImage
    mobile
    email
  }
  ''';

}