class ProductDocument {

  static const searchPaginate = r'''
       query searchPaginate($page: Int!, $limit: Int!, $search: String) {
          items: Products(page:{start: $page, limit: $limit}, where:{status:{EQ: BID_IN_PROGRESS} name: {LIKE: $search}}){
             total
             pages
             select{
                ...getProductFields
             }
          }
       }

  fragment getProductFields on Product {
      id(orderBy: DESC)
      name
      subTitle
      description
      createdAt
      bidFinishAt
      expectFinishAt
      description,
      status
      bgImage
      slideImage
      bidDuration
      delivery
      buyNowPrice
      originalPrice
      reservePrice
      reserveMet
      label
      province {
        id,
        name
      }
      category {
        orderNumber
        name
        logoImage
        id
      }
      
      highestBid{
       createdBy {
          ...getUserFields
        }
        createdAt
        id
        amount
      }
      
      bidPrice {
        createdBy {
          ...getUserFields
        }
        createdAt
         id
         amount
      }  
  }
  
  fragment getUserFields on AppUser {
    id
    userName
    status
    role
    profileImage
    mobile
    email
  }
  ''';

  static const getWithLabel = r'''
       query searchPaginate($page: Int!, $limit: Int!, $label: ProductLabel) {
          items: Products(page:{start: $page, limit: $limit}, where:{label:{EQ: $label}, status:{EQ: BID_IN_PROGRESS}}){
             total
             pages
             select{
                ...getProductFields
             }
          }
       }
  fragment getProductFields on Product {
      id(orderBy: DESC)
      name
      subTitle
      description
      createdAt
      bidFinishAt
      expectFinishAt
      description,
      status
      bgImage
      slideImage
      bidDuration
      delivery
      buyNowPrice
      originalPrice
      reservePrice
      reserveMet
      label
      province {
        id,
        name
      }
      category {
        orderNumber
        name
        logoImage
        id
      }
      
      highestBid{
       createdBy {
          ...getUserFields
        }
        createdAt
        id
        amount
      }
      
      bidPrice {
        createdBy {
          ...getUserFields
        }
        createdAt
         id
         amount
      }  
  }
  
  fragment getUserFields on AppUser {
    id
    userName
    status
    role
    profileImage
    mobile
    email
  }
  ''';


  static const getCreateData = r'''
  query queryCreateProjectData{
   categories: Categories(where:{
      firstLevel:{
        EQ:true
      }
    }){
      select{
        id
        name
        logoImage
      }
    }
   provinces: Provinces {
       select{
         id,
         name
       }
    }
   promotions: Promotions {
     select {
      duration
      id
      type
      name
      detail
      fee
     }
   }
   
     SystemConfigs(where:{
    code:{
      IN:["SUBTITLE_FEE","BID_14_DAY_FEE","BID_30_DAY_FEE"]
    }
  }){
    select{
      code
      value
    }
  }
}
  ''';



  static const getSingleProduct = r'''
    query getSingleProduct($productId: Long!){
     Product(id: $productId) {
        ...getProductFields
     }
    categories: Categories(where:{
        firstLevel:{
          EQ:true
        }
      }){
        select{
          id
          name
          logoImage
        }
      }
     provinces: Provinces {
         select{
           id,
           name
         }
      }
     promotions: Promotions {
       select {
        duration
        id
        type
        name
        detail
        fee
       }
     }
     
       SystemConfigs(where:{
      code:{
        IN:["SUBTITLE_FEE","BID_14_DAY_FEE","BID_30_DAY_FEE"]
      }
    }){
      select{
        code
        value
      }
    }
    
   }
     
  fragment getProductFields on Product {
      id(orderBy: DESC)
      name
      subTitle
      description
      createdAt
      bidFinishAt
      expectFinishAt
      description,
      status
      bgImage
      slideImage
      bidDuration
      delivery
      buyNowPrice
      originalPrice
      reservePrice
      reserveMet
      label
      province {
        id,
        name
      }
      category {
        orderNumber
        name
        logoImage
        id
        parent {
          orderNumber
          name
          logoImage
          id
        }
      }
      
       highestBid{
       createdBy {
          ...getUserFields
        }
        createdAt
        id
        amount
      }
      
      bidPrice {
        createdBy {
          ...getUserFields
        }
        createdAt
         id
         amount
      }  
    
  }
  
  fragment getUserFields on AppUser {
    id
    userName
    status
    role
    profileImage
    mobile
    email
  }

  ''';
  

  static const createProduct = r'''
      mutation (
      $categoryId: Long, 
      $name: String!, 
      $subTitle: String, 
      $delivery: [DeliveryEnum], 
      $payments: [ProductPaymentEnum], 
      $description: String, 
      $slideImage: List!,
      $provinceId: Long, 
      $originalPrice: BigDecimal,
      $reservePrice: BigDecimal,
      $buyNowPrice: BigDecimal,
      $bidDuration: BidDurationEnum!,
      ){
          CreateProduct(input:{
            categoryId: $categoryId
            name: $name
            subTitle: $subTitle
            delivery: $delivery
            payments: $payments
            description: $description
            slideImage: $slideImage
            provinceId: $provinceId
            originalPrice: $originalPrice
            reservePrice: $reservePrice
            buyNowPrice: $buyNowPrice
            bidDuration: $bidDuration
          }) {
            id
            createdAt
            originalPrice
            bidFinishAt
            status
            slideImage
            bidDuration
            payments
            category {
              orderNumber
              name
              logoImage
              id
            }
            description
            name
            highestBid{
              createdAt
              id
              amount
            }
            bidPrice {
              createdAt
               id
               amount
            }  
          }
        }
      ''';

  static const updateProduct = r'''
      mutation (
        $id: Long!,
        $categoryId: Long, 
        $name: String!, 
        $subTitle: String, 
        $delivery: [DeliveryEnum], 
        $description: String, 
        $slideImage: List!, 
        $provinceId: Long, 
        $originalPrice: BigDecimal,
        $reservePrice: BigDecimal,
        $buyNowPrice: BigDecimal,
        $bidDuration: BidDurationEnum,
      ){
           UpdateProduct(id: $id, input:{
            categoryId: $categoryId
            name: $name
            subTitle: $subTitle
            delivery: $delivery
            description: $description
            slideImage: $slideImage
            provinceId: $provinceId
            originalPrice: $originalPrice
            reservePrice: $reservePrice
            bgImage: $bgImage
            buyNowPrice: $buyNowPrice
            bidDuration: $bidDuration
          }){
            id
            createdAt
            originalPrice
            bidFinishAt
            status
            slideImage
            bidDuration
            category {
              orderNumber
              name
              logoImage
              id
            }
            description
            name
            highestBid {
              id
              createdAt
              amount
            }
          
            bidPrice {
              createdAt
               id
               amount
            }  
            
          
          }
        }
      ''';


  static const getProductPromotions = r'''
    query getProductPromotions($productId: Long!){
      promotions: ProductPromotions(where:{product:{id:{EQ: $productId}}}){
        select {
          id
          promotion {
            id
            fee
            detail
            type
            image
            duration
          }
        }
      }
    }
  ''';

  static const getProductById = r'''
    query getProductById($productId: Long!){
       Product(id: $productId) {
          ...getProductFields
       }
    }
    
    fragment getProductFields on Product {
        id(orderBy: DESC)
        name
        subTitle
        description
        createdAt
        bidFinishAt
        expectFinishAt
        description,
        status
        bgImage
        slideImage
        bidDuration
        delivery
        buyNowPrice
        originalPrice
        reservePrice
        reserveMet
        label
        province {
          id,
          name
        }
        
        category {
          firstLevel
          orderNumber
          name
          logoImage
          id
        }
        
        highestBid{
         createdBy {
            ...getUserFields
          }
          createdAt
          id
          amount
        }
        
        bidPrice {
          createdBy {
            ...getUserFields
          }
          createdAt
           id
           amount
        }
        
        createdBy {
          ...getUserFields
        }
    }
    
    fragment getUserFields on AppUser {
      id
      userName
      status
      role
      profileImage
      mobile
      email
    }

  ''';

  static const getProductComments = r'''
    query getProductComments($productId: Long!){
       comments: Comments(where: {product:{id:{EQ: $productId}}}) { 
         select{
          id
          content
          byOwner
          createdBy {
            ...getUserFields
          }
          createdAt
        }
       }
     }
     
  fragment getUserFields on AppUser {
    id
    userName
    status
    role
    profileImage
    mobile
    email
  }
  ''';

  static const getProductDetails = r'''
    query getSingleProductWithRecommend($productId: Long!, $categoryId: Long!, $userId: Long!){
    
       comments: Comments(where: {product:{id:{EQ: $productId}}}) { 
         select{
          id,
          content,
          createdBy {
            ...getUserFields
          }
          createdAt
        }
       }
       
       recommends: Products(where: {category:{id:{EQ: $categoryId}}  status: { EQ: BID_IN_PROGRESS } AND :{id: {NE: $productId}}}, page:{start: 1, limit: 100}){
          select {
           ...getProductFields
          }
       }
       
      AutoBids(where: {
        createdBy: {
          id:{EQ: $userId}
        }
        product: {
          id: {
            EQ: $productId
          }
        } 
      }){
           total
           pages
           select{
            id
            status
            createdBy {
             ...getUserFields
            }
            product {
               ...getProductFields
            }
            createdAt
           }
      }
      
     }
    
    fragment getProductFields on Product {
        id(orderBy: DESC)
        name
        subTitle
        description
        createdAt
        bidFinishAt
        expectFinishAt
        description,
        status
        bgImage
        slideImage
        bidDuration
        delivery
        buyNowPrice
        originalPrice
        reservePrice
        reserveMet
        label
        province {
          id,
          name
        }
        category {
          orderNumber
          name
          logoImage
          id
        }
        highestBid{
         createdBy {
            ...getUserFields
          }
          createdAt
          id
          amount
        }
        
        bidPrice {
          createdBy {
            ...getUserFields
          }
          createdAt
           id
           amount
        }
    }
    
    fragment getUserFields on AppUser {
      id
      userName
      status
      role
      profileImage
      mobile
      email
    }

  ''';

  static const buyNowProduct = r'''
    mutation createProductOrder($id: Long!){
      CreateProductOrder(input: {productId: $id}){
        id
      }
    }
  ''';



  static const autoBidProduct = r'''
     mutation createAutoBid($id: Long!, $amount: BigDecimal!){
        CreateAutoBid(input: {productId: $id, amount: $amount}){
          id
          amount
        }
     }
  ''';


  static const bidProduct = r'''
    mutation createBidPrice($id: Long!, $amount: BigDecimal!) {
      CreateBidPrice(input: {productId: $id, amount: $amount}){
        id
        amount
      }
    }
  ''';


  static const commentProduct = r''' 
  mutation createComment($id: Long, $content: String){
    CreateComment(
      input: {
        productId: $id,
        content: $content
      }
    ) {
      id,
      content
    }
  }
  ''';

  static const getWatchListItems = r'''
    query getWatchListItems($userId: Long!) {
      WatchLists(where: {
        createdBy: {
          id:{EQ: $userId}
        }
      }){
           total
           pages
           select{
            id
            status
            createdBy {
             ...getUserFields
            }
            product {
               ...getProductFields
            }
            createdAt
           }
         }
    }
    
    fragment getProductFields on Product {
        id(orderBy: DESC)
        name
        subTitle
        description
        createdAt
        bidFinishAt
        expectFinishAt
        description,
        status
        bgImage
        slideImage
        bidDuration
        delivery
        buyNowPrice
        originalPrice
        reservePrice
        reserveMet
        label
        province {
          id,
          name
        }
        category {
          orderNumber
          name
          logoImage
          id
        }
        bidPrice {
          createdBy {
            ...getUserFields
          }
          createdAt
          id
          amount
        }
    }
    
    fragment getUserFields on AppUser {
      id
      userName
      status
      role
      profileImage
      mobile
      email
    }
  ''';

  static const createWishList = r''' 
  mutation createWishListProduct($id: Long){
    CreateWatchList(
      input: {
        productId: $id
      }
    ) {
      id
    }
  }
  ''';

  static const deleteWishList = r''' 
  mutation deleteWishListProduct($id: Long!){
    DeleteWatchList(id: $id) {
      id
    }
  }
  ''';


  static const existWishlist = r''' 
  query existWishlist($userId: Long!, $productId: Long!) {
      WatchLists(where: {
        product:{
          id:{EQ: $productId}
        }
        createdBy: {
          id:{EQ: $userId}
        }
      }){
         select{
            id
            status
            createdBy {
             ...getUserFields
            }
            product {
               ...getProductFields
            }
            createdAt
           }
      } 
  }
  
  fragment getProductFields on Product {
        id(orderBy: DESC)
        name
        subTitle
        description
        createdAt
        bidFinishAt
        expectFinishAt
        description,
        status
        bgImage
        slideImage
        bidDuration
        delivery
        buyNowPrice
        originalPrice
        reservePrice
        label
        province {
          id,
          name
        }
        category {
          orderNumber
          name
          logoImage
          id
        }
        bidPrice {
          createdBy {
            ...getUserFields
          }
          createdAt
          id
          amount
        }
    }
    
    fragment getUserFields on AppUser {
      id
      userName
      status
      role
      profileImage
      mobile
      email
    }
    
  ''';

  static const createPromotionProduct = r'''
      mutation($productId: Long!, $promotionId: Long!){
        CreateProductPromotion(input:{
          productId: $productId
          promotionId: $promotionId
        }){
          id
        }
      }
  ''';

  static const deleteProductPromotion = r'''
      mutation($id: Long!){
        DeleteProductPromotion(id: $id){
          id
        }
      }
  ''';

  static const updatePromotionProduct = r'''
      mutation($id: Long!, $productId: Long!, $promotionId: Long!){
        UpdateProductPromotion(id: $id, input:{
          productId: $productId
          promotionId: $promotionId
        }){
          id
        }
      }
  ''';



}