import 'package:get/get.dart';
import 'package:mobile/core/gql_client.dart';
import 'package:mobile/documents/product_document.dart';
import 'package:mobile/repositories/user_repository.dart';
import 'package:mobile/shared/models/user_item_model.dart';
import 'package:mobile/shared/models/user_model.dart';
import 'package:mobile/utils/utility.dart';
import 'package:collection/collection.dart';

class AuthService extends GetxService {

  final _repository = Get.find<UserRepository>();

  static AuthService get to => Get.find<AuthService>();

  final _gql = Get.find<GraphQLClient>();

  Future<bool?> checkLoggedIn({final bool? pingServer = false}) async {
    try {
      if(pingServer?? false) {
        final UserModel? currentUser = await _repository.getCurrentUser();
        return currentUser?.id != null;
      }
      final isLoggedIn = await Utility.isLoggedIn()?? false;
      return isLoggedIn;
    } catch(e) {
      print('logged $e');
    }
    return null;
  }

  Future<WatchProductModel?> getWatchListByProductId({final int? productId}) async {
    try {
      final isLoggedIn = await Utility.isLoggedIn()?? false;
      if(isLoggedIn) {
        final UserModel? user = await Utility.getCurrentUser();
        final Map<String, dynamic> input = {
          'userId': user?.id,
          'productId': productId,
        };
        final _result = await _gql.call(authorization: true)
            .query(ProductDocument.existWishlist,
            variables: input);
        List<WatchProductModel>? watchItems = (_result['data']['WatchLists']['select'] as List)
            .cast<Map<String, dynamic>>()
            .map(WatchProductModel.fromJson)
            .toList();
        return watchItems.firstOrNull;
      }
    } catch(error) {
      print('error $error');
    }
    return null;
  }


  Future<bool?> createWatchList({final int? productId}) async {
    try {
      final Map<String, dynamic> input = {
        "id": productId,
      };
      final _result = await _gql.call(authorization: true)
          .mutation(ProductDocument.createWishList,
          variables: input);
      return _result['data'] != null;
    } catch (e) {
      print('error $e');
    }
    return false;
  }

  Future<bool?> deleteWatchList({final int? wishlistId}) async {
    try {
      final Map<String, dynamic> input = {
        "id": wishlistId,
      };
      final response = await _gql.call(authorization: true)
          .mutation(ProductDocument.deleteWishList,
          variables: input);

      final result =  response['data']['DeleteWatchList']['id'] != null;
      return result;
    } catch (e) {
      print('error $e');
    }
    return false;
  }

}