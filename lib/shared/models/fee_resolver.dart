import 'package:collection/collection.dart';
import 'package:mobile/shared/models/code_value.dart';

class FeeResolver {

  static const String BID_14_DAY_FEE = "BID_14_DAY_FEE";
  static const String BID_30_DAY_FEE = "BID_30_DAY_FEE";
  static const String SUBTITLE_FEE = "SUBTITLE_FEE";

  static double? getBid14DayFee({required final List<CodeValue> list}) {
      try {
        final codeValue = _findByCode(list, code: BID_14_DAY_FEE);
        return double.tryParse('${codeValue?.value}');
      } catch(e) {
        print('e $e');
      }
      return 0;
  }

  static double? getBid30DayFee({required final List<CodeValue> list}) {
    try {
      final codeValue = _findByCode(list, code: BID_30_DAY_FEE);
      if(codeValue != null) {
        return double.tryParse('${codeValue.value}');
      }
    } catch(e) {
      print('e $e');
    }
    return null;
  }

  static double? getSubTitleFee({required final List<CodeValue> list}) {
    try {
      final codeValue = _findByCode(list, code: SUBTITLE_FEE);
      return double.tryParse('${codeValue?.value}');
    } catch(e) {
      print('e $e');
    }
    return 0;
  }

  static CodeValue? _findByCode(final List<CodeValue> list, {required final String code}) {
    try {
      return list.firstWhereOrNull((element) => element.code == code);
    } catch(e) {
      print('e $e');
    }
    return null;
  }

  static CodeValue? _findByValue(final List<CodeValue> list, {required final String value}) {
    try {
      return list.firstWhereOrNull((element) => element.value == value);
    } catch(e) {
      print('e $e');
    }
    return null;
  }

}