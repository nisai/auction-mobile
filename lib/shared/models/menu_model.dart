import 'package:flutter/cupertino.dart';

class MenuModel {

  final String? id;
  final String? name;
  final IconData? icon;
  GestureTapCallback? onTap;
  MenuModel({
    this.id,
    this.name,
    this.icon,
    this.onTap});

  @override
  String toString() {
    return '{id: $id, name: $name, icon: $icon}';
  }
}
