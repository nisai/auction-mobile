class AccessTokenModel {

  final String? accessToken;
  final String? refreshToken;
  const AccessTokenModel({
     this.accessToken,
     this.refreshToken,
  });

  static AccessTokenModel fromJson(Map<String, dynamic>? json) {
    return AccessTokenModel(
      accessToken: json?['access_token'],
      refreshToken: json?['refresh_token'],
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'access_token': accessToken,
      'refresh_token': refreshToken,
    };
  }

  @override
  bool operator ==(Object other) => identical(this, other) || other is AccessTokenModel && accessToken == other.accessToken;

  @override
  int get hashCode => accessToken.hashCode;

  @override
  String toString() {
    return 'AccessTokenModel{accessToken: $accessToken, refreshToken: $refreshToken) }';
  }

}
