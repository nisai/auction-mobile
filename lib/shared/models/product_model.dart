import 'package:mobile/shared/models/category_model.dart';
import 'package:mobile/shared/models/province_model.dart';
import 'package:mobile/shared/models/user_item_model.dart';
import 'package:mobile/shared/models/user_model.dart';
import 'package:mobile/utils/constant.dart';

class ProductModel {
  ProductModel({
    this.id,
    this.name,
    this.status,
    this.originalPrice,
    this.slideImage,
    this.description,
    this.bidFinishAt,
    this.expectFinishAt,
    this.category,
    this.bidPrice,
    this.buyNowPrice,
    this.reservePrice,
    this.reserveMet,
    this.subTitle,
    this.delivery,
    this.bidDuration,
    this.province,
    this.highestBid,
    this.createdBy,
    this.label,
  });

  final int? id;
  final String? name;
  final String? subTitle;
  final double? originalPrice;
  final double? reservePrice;
  final bool? reserveMet;
  final double? buyNowPrice;
  final List<String>? slideImage;
  final String? description;
  final String? bidFinishAt;
  final String? expectFinishAt;
  final CategoryModel? category;
  final List<BidPriceModel>? bidPrice;
  final List? delivery;
  final String? bidDuration;
  final ProvinceModel? province;
  final String? status;

  final BidPriceModel? highestBid;
  final UserModel? createdBy;
  final String? label;



  List<String>? get slideImages =>  slideImage?.map((e) {
    final url =  '${Constant.BASE_API}$e';
    return url;
  }).toList();

  bool get hasBidPrice => bidPrice != null && bidPrice?.isNotEmpty == true && highestBid != null;

  double? get getHighestBid {
    double? _currentBid = 0;
    try{
      if(hasBidPrice) {
        _currentBid =  double.tryParse('${highestBid?.amount}');
      } else {
        _currentBid = double.tryParse('$originalPrice');
      }
      return _currentBid;
    } catch (e) {
      print('getHighestBid error $e');
    }
    return null;
  }

  bool get isUrgent => label != null && label == 'URGENT';

  bool get isHighlight => label != null && label == 'HIGH_LIGHTED';

  bool get hasReversePrice => reservePrice != null && reservePrice! > 0;
  bool get isReservePriceMet => reservePrice != null && reserveMet == true;

  double? get nextBidAmount {
    try {
      double _highestBid = getHighestBid?? 0.0;
      double _price = price?? 0;
      if(_highestBid> _price) {
        return _highestBid + 0.25;
      }
      return _price;
    } catch(e) {
    }
    return null;
  }

  double? get price {
    return double.tryParse('$originalPrice');
  }

  String? get imageUrl {
    if (hasImage) {
     return slideImages?[0];
    }
    return null;
  }

  String? get bgImage {
    if (hasImage) {
     return slideImages?[0];
    }
    return null;
  }

  bool get hasImage {
    return slideImages != null;
  }

  bool get hasSlide {
    return slideImage != null;
  }

  String get descriptionNoHtml {
    return description??''.replaceAll(RegExp(r"<\/?[^>]*>"), "");
  }

  bool get canBuyNow {
    try {
      double? _buyNowPrice = double.tryParse('$buyNowPrice');
      return _buyNowPrice != null;
    } catch(e) {
      print('buyNow error $e');
    }
    return false;
  }

  bool get hasSubTitle => subTitle != null && subTitle != '';

  @override
  bool operator ==(Object other) => identical(this, other) || other is ProductModel && id == other.id;

  @override
  int get hashCode => id.hashCode;

  static ProductModel fromJson(final Map<String, dynamic>? json) {
    List<String>? slideImage;
    if(json?['slideImage'] != null) {
      slideImage = (json?['slideImage'] as List).map((e) => "$e").toList();
    }

    List<BidPriceModel>? bidPrice;

    if(json?['bidPrice'] != null) {
      bidPrice = (json?['bidPrice'] as List)
          .cast<Map<String, dynamic>>()
          .map(BidPriceModel.fromJson)
          .toList();
    }

    BidPriceModel? highestBid = BidPriceModel.fromJson(json?['highestBid']);

    return ProductModel(
        id: json?["id"],
        name: json?["name"],
        subTitle: json?["subTitle"],
        status: json?["status"],
        description: json?['description'],
        originalPrice: double.tryParse('${json?["originalPrice"]}'),
        buyNowPrice: double.tryParse('${json?["buyNowPrice"]}'),
        reservePrice: double.tryParse('${json?["reservePrice"]}'),
        reserveMet: json?["reserveMet"],
        bidFinishAt: json?["bidFinishAt"],
        expectFinishAt: json?["expectFinishAt"],
        slideImage: slideImage,
        bidPrice: bidPrice,
        category: CategoryModel.fromJson(json?['category']),
        province: ProvinceModel.fromJson(json?['province']),
        highestBid: highestBid,
        bidDuration: json?['bidDuration'],
        delivery: json?['delivery'],
        label: json?['label'],
        createdBy: UserModel.fromJson(json?['createdBy']),
    );
  }

  Map<String, dynamic> toMap() {
    return {
        "id": id,
        "name": name,
        "status": status,
        "subTitle": subTitle,
        "originalPrice": originalPrice,
        "reservePrice": reservePrice,
        "reserveMet": reserveMet,
        "buyNowPrice": buyNowPrice,
        "description": description,
        "bgImage": bgImage,
        "slideImage": slideImage,
        "bidFinishAt": bidFinishAt,
        "delivery": delivery,
        "bidDuration": bidDuration,
        "label": label,
        "province": province?.toMap(),
        "category": category?.toJson(),
      };
  }


  @override
  String toString() {
    return 'ProductModel{id: $id, name: $name, status: $status,  bidPrice: $bidPrice, originalPrice: $originalPrice, reservePrice: $reservePrice, reserveMet: $reserveMet, slideImage: $slideImage, description: "$description", bgImage: $bgImage, "bidFinishAt": $bidFinishAt, "expectFinishAt": $expectFinishAt, "label": $label}';
  }


  ProductModel copyWith({
    final int? id,
    final String? name,
    final String? status,
    final String? subTitle,
    final double? originalPrice,
    final double? reservePrice,
    final bool? reserveMet,
    final double? buyNowPrice,
    final String? bgImage,
    final List<String>? slideImage,
    final String? description,
    final String? bidFinishAt,
    final String? expectFinishAt,
    final CategoryModel? category,
    final List<BidPriceModel>? bidPrice,
    final List? delivery,
    final String? bidDuration,
    final ProvinceModel? province,
}) {
    return ProductModel(
      id: id?? this.id,
      name: name?? this.name,
      status: status?? this.status,
      subTitle: subTitle?? this.subTitle,
      description: description?? this.description,
      originalPrice: originalPrice?? this.originalPrice,
      buyNowPrice: buyNowPrice?? this.buyNowPrice,
      reservePrice: buyNowPrice?? this.reservePrice,
      reserveMet: reserveMet?? this.reserveMet,
      bidFinishAt: bidFinishAt?? this.bidFinishAt,
      expectFinishAt: expectFinishAt?? this.expectFinishAt,
      slideImage: slideImage?? this.slideImage,
      bidPrice: bidPrice?? this.bidPrice,
      category: category?? this.category,
      delivery: delivery?? this.delivery,
      bidDuration: bidDuration?? this.bidDuration,
      province: province?? this.province,
    );
  }

}