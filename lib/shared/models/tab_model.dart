import 'package:flutter/cupertino.dart';

class TabModel {
  final String id;
  final String name;
  final Widget page;
  final IconData icon;
  final bool? selected;
  TabModel({required this.id, required this.name, required this.page, required this.icon, this.selected = false});
}