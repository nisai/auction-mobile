import 'package:mobile/utils/constant.dart';

class CategoryModel {

  final int? id;
  final String? name;
  final String? image;
  final bool? firstLevel;
  bool selected = false;

  bool get hasImage => image != null;

  final CategoryModel? parent;

  String get imageUrl => "${Constant.BASE_API}$image";

  CategoryModel({this.id, this.name, this.image, this.parent, this.firstLevel});

  static CategoryModel fromJson(Map<String, dynamic> json) {

    var parent;
    if(json['parent'] != null) {
      parent = CategoryModel.fromJson(json['parent']);
    }

    return CategoryModel(
        id: json['id'],
        name: json['name'],
        image: json['logoImage'],
        parent: parent,
        firstLevel: json['firstLevel'],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      "id": id,
      "name": name,
      "image": image,
      "imageUrl": imageUrl,
      "selected": selected,
      "parent": parent,
      "firstLevel": firstLevel,
    };
  }

  @override
  bool operator ==(Object other) => identical(this, other) || other is CategoryModel && id == other.id;

  @override
  int get hashCode => id.hashCode;

  @override
  String toString() {
    return 'CategoryModel{id: $id, name: $name, image: $image, imageUrl: $imageUrl, selected: $selected, parent: $parent, firstLevel: $firstLevel}';
  }
}
