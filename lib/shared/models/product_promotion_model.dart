import 'package:mobile/shared/models/promotion_model.dart';

class ProductPromotionModel {
  final int? id;
  final PromotionModel? promotion;

  ProductPromotionModel({
    this.id,
    this.promotion,
  });

  static ProductPromotionModel fromJson(Map<String, dynamic>? json) {
    return ProductPromotionModel(
      id: json?['id'],
      promotion: PromotionModel.fromJson(json?['promotion'] as Map<String, dynamic>),
    );
  }

  Map toMap() {
    return {"id": id, "promotion": promotion};
  }

  @override
  String toString() {
    return 'ProductPromotionModel{id: $id, promotion: $promotion}';
  }
}