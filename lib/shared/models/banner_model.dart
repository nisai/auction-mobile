import 'package:mobile/utils/urls.dart';

class BannerModel {

  final String? imageUrl;
  final String? linkUrl;

  BannerModel({this.imageUrl, this.linkUrl});

  static BannerModel fromJson(Map<String, dynamic> json) {
    return BannerModel(
      imageUrl: '${URLs.baseUrl}${json['imageUrl']}',
      linkUrl: '${URLs.baseUrl}${json['linkUrl']}',
    );
  }

  Map<String, dynamic> toMap() {
    return {
      "imageUrl": imageUrl,
      "linkUrl": linkUrl,
    };
  }

}