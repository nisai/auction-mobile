
class UserAccountModel {
  final int? id;
  final double? amount;
  final bool? hasPaymentCode;

  UserAccountModel({this.id, this.amount, this.hasPaymentCode});


  static UserAccountModel fromJson(Map<String, dynamic>? json) {
    return UserAccountModel(
        id: json?['id'],
        amount: json?['amount'],
        hasPaymentCode: json?['hasPaymentCode'],
    );
  }

  Map<String, dynamic> toMap() {
    return {
      "id": id,
      "hasPaymentCode": hasPaymentCode,
      "amount": amount,
    };
  }

  @override
  bool operator ==(Object other) => identical(this, other) || other is UserAccountModel && id == other.id;

  @override
  int get hashCode => id.hashCode;

  @override
  String toString() {
    return 'UserAccountModel{id: $id, amount: $amount, hasPaymentCode: $hasPaymentCode}';
  }
}
