class SongModel {
  int? id;
  int? albumId;
  String? title;
  String? subTitle;
  String? album;
  String? singer;
  String? cover;
  String? image;
  String? url;
  int? size;
  String? duration;
  int? view;
  int? download;

  SongModel({this.id,
      this.albumId,
      this.title,
      this.subTitle,
      this.album,
      this.singer,
      this.cover,
      this.image,
      this.url,
      this.size,
      this.duration,
      this.view,
      this.download});


  static SongModel fromJson(Map<String, dynamic> json) {
    return SongModel(
        id: json['id'],
        albumId: json['album_id'],
        title: json['title'],
        subTitle: json['subTitle'],
        album: json['album'],
        singer: json['singer'],
        cover: json['cover'],
        image: json['image'],
        url: json['url'],
        size: json['size'],
        duration: json['duration'],
        view: json['view'],
        download: json['download']
    );
  }

  Map<String, dynamic> toMap() {
    return {
      "id": id,
      "album_id": albumId,
      "title": title,
      "subTitle": subTitle,
      "album": album,
      "singer": singer,
      "cover": cover,
      "image": image,
      "url": url,
      "size": size,
      "duration": duration,
      "view": view,
      "download": download
    };
  }

  @override
  bool operator ==(Object other) => identical(this, other) || other is SongModel && id == other.id;

  @override
  int get hashCode => id.hashCode;

  @override
  String toString() {
    return 'SongModel{'
        'id: $id, '
        'title: $title, '
        'album_id: $albumId, '
        'subTitle: $subTitle, '
        'album: $album,'
        'singer: $singer,'
        'cover: $cover,'
        'image: $image,'
        'url: $url,'
        'size: $size,'
        'duration: $duration,'
        'view: $view,'
        'download: $download'
        '}';
  }
}