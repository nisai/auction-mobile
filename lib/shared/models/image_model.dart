import 'package:mobile/utils/constant.dart';

class ImageModel {
  final String image;
  final bool uploaded;

  String get imageUrl {
    if(uploaded) {
      return  '${Constant.BASE_API}$image';
    }
    return image;
  }

  ImageModel({
    required this.image,
    required this.uploaded
  });

  static ImageModel fromJson(Map<String, dynamic>? json) {
    return ImageModel(
      image: json?['image'],
      uploaded: json?['uploaded'],
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'image': image,
      'uploaded': uploaded,
    };
  }

  @override
  bool operator ==(Object other) => identical(this, other) || other is ImageModel && image == other.image;

  @override
  int get hashCode => image.hashCode;

  @override
  String toString() {
    return 'ImageModel{image: $image, uploaded: $uploaded) }';
  }

}
