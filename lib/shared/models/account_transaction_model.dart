
class AccountTransactionModel {

  final int? id;
  final double? amount;
  final String? transactionType;
  final String? updatedAt;
  final String? createdAt;
  final String? transactionDetail;
  final String? partyAccountDetail;

  AccountTransactionModel({this.id, this.amount, this.transactionType, this.updatedAt, this.createdAt, this.transactionDetail, this.partyAccountDetail});


  static AccountTransactionModel fromJson(Map<String, dynamic>? json) {
    return AccountTransactionModel(
        id: json?['id'],
        amount: json?['amount'],
        transactionType: json?['transactionType'],
        updatedAt: json?['updatedAt'],
        createdAt: json?['createdAt'],
        transactionDetail: json?['transactionDetail'],
        partyAccountDetail: json?['partyAccountDetail'],
    );
  }

  Map<String, dynamic> toMap() {
    return {
      "id": id,
      "amount": amount,
      "transactionType": transactionType,
      "updatedAt": updatedAt,
      "createdAt": createdAt,
      "transactionDetail": transactionDetail,
      "partyAccountDetail": partyAccountDetail,
    };
  }

  @override
  bool operator ==(Object other) => identical(this, other) || other is AccountTransactionModel && id == other.id;

  @override
  int get hashCode => id.hashCode;

  @override
  String toString() {
    return 'AccountTransactionModel{id: $id, amount: $amount, transactionType: $transactionType}';
  }
}
