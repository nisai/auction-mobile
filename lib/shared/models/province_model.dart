class ProvinceModel {

  final int? id;
  final String? name;

  ProvinceModel({this.id, this.name});

  static ProvinceModel fromJson(Map<String, dynamic>? json) {
    return ProvinceModel(
        id: json?['id'],
        name: json?['name'],
    );
  }

  Map<String, dynamic> toMap() {
    return {
      "id": id,
      "name": name,
    };
  }

  @override
  bool operator ==(Object other) => identical(this, other) || other is ProvinceModel && id == other.id;

  @override
  int get hashCode => id.hashCode;

  @override
  String toString() {
    return 'CategoryModel{id: $id, name: $name}';
  }

}
