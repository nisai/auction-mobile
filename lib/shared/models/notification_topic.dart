class NotificationTopic {
  static const String bidWin = 'bidWin';
  static const String bidFinishReserveMet = 'bidFinishReserveMet';
  static const String questionAsked = 'questionAsked';
  static const String questionAnswered = 'questionAnswered';
  static const String bidFinishReserveNotMet = 'bidFinishReserveNotMet';
  static const String favoriteFinish24h = 'favoriteFinish24h';
  static const String bidFinish24h = 'bidFinish24h';
  static const String insufficientBalance = 'insufficientBalance';
  static const String productOrder = 'productOrder';
  static const String productBid = 'productBid';
  static const String favBid = 'favBid';
  static const String favRelist = 'favRelist';
  static const String feeCharge = 'feeCharge';
  static const String topUp = 'topUp';
  static const String higherBid = 'higherBid';
}