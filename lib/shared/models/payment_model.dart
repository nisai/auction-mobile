class PaymentModel {

  static const WING = 'wing';
  static const PIPAY = 'pipay';

  final String? id;
  final String? name;
  final String? image;
  bool selected;
  PaymentModel({
    this.id,
    this.name,
    this.image,
    this.selected = false});

  @override
  String toString() {
    return '{id: $id, name: $name, image: $image, selected: $selected}';
  }
}
