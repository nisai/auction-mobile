class SingerModel {

  int? id;
  String? title;
  String? image;

  SingerModel({this.id, this.title, this.image});

  static SingerModel fromJson(Map<String, dynamic>? json) {
    return SingerModel(
        id: json?['id'],
        title: json?['title'],
        image: json?['image'],
    );
  }

  Map<String, dynamic> toMap() {
    return {
      "id": id,
      "title": title,
      "image": image,
    };
  }

  @override
  bool operator ==(Object other) => identical(this, other) || other is SingerModel && id == other.id;

  @override
  int get hashCode => id.hashCode;

  @override
  String toString() {
    return 'SingerModel{id: $id, title: $title, image: $image}';
  }
}
