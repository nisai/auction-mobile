import 'package:mobile/shared/models/user_model.dart';

class AppUser {
  AppUser({
    this.jwt,
    this.user,
  });

  final String? jwt;
  final UserModel? user;

  AppUser copyWith({
    String? jwt,
    UserModel? user,
  }) =>
      AppUser(
        jwt: jwt ?? this.jwt,
        user: user ?? this.user,
      );

  factory AppUser.fromMap(Map<String, dynamic> json) => AppUser(
    jwt: json["jwt"] as String,
    user: UserModel.fromJson(json["user"]),
  );

  Map<String, dynamic> toMap() => {
    "jwt": jwt,
    "user": user?.toMap(),
  };

  @override
  String toString() => 'App(jwt: $jwt, user: $user)';

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;

    return o is AppUser && o.jwt == jwt && o.user == user;
  }

  @override
  int get hashCode => jwt.hashCode ^ user.hashCode;

}
