
class CodeValue {

  final String? value;
  final String? code;

  const CodeValue({
    this.value,
    this.code,
  });

  static CodeValue fromJson(Map<String, dynamic>? json) {
    return CodeValue(
      value: json?["value"],
      code: json?["code"],
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'key': value,
      'code': code,
    };
  }

  @override
  bool operator ==(Object other) => identical(this, other) || other is CodeValue && value == other.value;

  @override
  int get hashCode => value.hashCode;

  @override
  String toString() {
    return 'CodeValue{value: $value, code: $code }';
  }

}
