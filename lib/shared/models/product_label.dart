
enum ProductLabel {
  HIGH_LIGHTED,
  URGENT,
  FINISH_SOON,
  R1000,
}

String? productLabelToJson(final ProductLabel? e) {
  switch (e) {
    case ProductLabel.HIGH_LIGHTED:
      return 'HIGH_LIGHTED';
    case ProductLabel.URGENT:
      return "URGENT";
    case ProductLabel.FINISH_SOON:
      return "FINISH_SOON";
    case ProductLabel.R1000:
      return "R1000";
    default:
  }
  return null;
}

String? productLabelToTitle(final ProductLabel? e) {
  switch (e) {
    case ProductLabel.HIGH_LIGHTED:
      return 'HIGH LIGHTED';
    case ProductLabel.URGENT:
      return "URGENT";
    case ProductLabel.FINISH_SOON:
      return "FINISH SOON";
    case ProductLabel.R1000:
      return "1000 riel";
    default:
  }
  return null;
}