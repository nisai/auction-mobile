import 'package:mobile/utils/constant.dart';

class UserModel {

  final int? id;
  final String? uid;
  final String? firstName;
  final String? lastName;
  final String? userName;
  final String? mobile;
  final String? email;
  final String? role;
  final String? status;
  final String? providerId;
  final String? profileImage;
  final String? createdAt;

  bool get hasProfile => profileImage != null;

  String get profileUrl =>"${Constant.BASE_API}$profileImage";

  UserModel({
    this.firstName,
    this.lastName,
    this.role,
    this.status,
    this.providerId,
    this.profileImage,
    this.id,
    this.uid,
    this.userName,
    this.mobile,
    this.email,
    this.createdAt,
  });

  static UserModel fromJson(Map<String, dynamic>? json) {
    return UserModel(
        id: json?['id'],
        uid: json?['uid'],
        firstName: json?['firstName'],
        lastName: json?['lastName'],
        userName: json?['userName'],
        mobile: json?['mobile'],
        email: json?['email'],
        role: json?['role'],
        status: json?['status'],
        profileImage: json?['profileImage'],
        providerId: json?['providerId'],
        createdAt: json?['createdAt'],
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'uid': uid,
      'firstName': firstName,
      'lastName': lastName,
      'userName': userName,
      'email': email,
      'mobile': mobile,
      'role': role,
      'profileImage': profileImage,
      'createdAt': createdAt,
    };
  }

  @override
  bool operator ==(Object other) => identical(this, other) || other is UserModel && id == other.id;

  @override
  int get hashCode => id.hashCode;

  @override
  String toString() {
    return 'UserModel{id: $id, uid: $uid, userName: $userName, role: $role, status: $status, profileImage: $profileImage, email: $email, mobile: $mobile, createdAt: $createdAt }';
  }

}
