class PromotionModel {

  final int? id;
  final String? name;
  final String? detail;
  final String? image;
  final double? fee;
  final String? type;
  final int? duration;

  PromotionModel({this.id, this.name, this.detail, this.image, this.fee, this.type, this.duration});

  @override
  bool operator ==(Object other) => identical(this, other) || other is PromotionModel && id == other.id;

  @override
  int get hashCode => id.hashCode;

  static PromotionModel fromJson(Map<String, dynamic>? map) {
    return PromotionModel(
      id: map?["id"],
      name: map?["name"],
      image: map?["image"],
      detail: map?['detail'],
      fee: map?["fee"],
      duration: map?["duration"],
      type: map?["type"],
    );
  }

  Map toMap() {
    return {"id": id, "name": name, "detail": detail, "fee": fee, "duration": duration, "type": type};
  }

  @override
  String toString() {
    return 'PromotionModel{id: $id, name: $name, detail: $detail, fee: $fee, duration: "$duration", type: $type}';
  }
}