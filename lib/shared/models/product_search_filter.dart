

import 'package:mobile/shared/models/category_model.dart';
import 'package:mobile/shared/models/product_label.dart';

class ProductSearchFilter {

  final CategoryModel? category;
  final ProductLabel? label;
  final bool? forceSearch;

  bool get isCategory => category != null;
  bool get isLabel => label != null;
  bool get isSearch => forceSearch != null && forceSearch == true;

  String? get getValue {
    return productLabelToJson(label);
  }

  String? get getName {
    if(isCategory) {
      return category?.name;
    }
    return productLabelToTitle(label);
  }


  ProductSearchFilter({this.category, this.label, this.forceSearch = false});

}
