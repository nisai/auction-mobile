import 'package:mobile/shared/models/product_model.dart';
import 'package:mobile/shared/models/user_model.dart';

class UserRateModel {
  final int? id;
  final int? rate;
  final UserModel? user;
  final UserModel? createdBy;
  final String? comment;
  final String? createdAt;

  bool get hasUser => user != null;

  UserRateModel({this.id, this.rate, this.user, this.createdBy, this.createdAt, this.comment});

  static UserRateModel fromJson(Map<String, dynamic>? json) {

    return UserRateModel(
      id: json?['id'],
      rate: int.tryParse('${json?['rate']}'),
      user: UserModel.fromJson(json?['user']),
      createdBy: UserModel.fromJson(json?['createdBy']),
      createdAt: json?['createdAt'],
      comment: json?['comment'],
    );
  }

  Map<String, dynamic> toMap() {
    return {
      "id": id,
      "rate": rate,
      "user": user?.toMap(),
      "createdBy": createdBy?.toMap(),
      "comment": comment,
      "createdAt": createdAt,
    };
  }

  @override
  bool operator ==(Object other) => identical(this, other) || other is UserRateModel && id == other.id;

  @override
  int get hashCode => id.hashCode;

  @override
  String toString() {
    return 'UserRateModel{id: $id, rate: $rate, user: $user, createdBy: $createdBy createdAt: $createdAt, comment: $comment}';
  }
}


class BidPriceModel {

  final int? id;
  final String? amount;
  final UserModel? createdBy;
  final String? createdAt;

  bool get hasCreateBy => createdBy != null;

  BidPriceModel({this.id, this.amount, this.createdAt, this.createdBy});

  static BidPriceModel fromJson(Map<String, dynamic>? json) {

    return BidPriceModel(
      id: json?['id'],
      amount: '${json?['amount']}',
      createdBy: UserModel.fromJson(json?['createdBy']),
      createdAt: json?['createdAt'],
    );
  }

  Map<String, dynamic> toMap() {
    return {
      "id": id,
      "amount": amount,
      "createdAt": createdAt,
      "createdBy": createdBy,
    };
  }

  @override
  bool operator ==(Object other) => identical(this, other) || other is BidPriceModel && id == other.id;

  @override
  int get hashCode => id.hashCode;

  @override
  String toString() {
    return 'BidPriceModel{id: $id, amount: $amount, createdBy: $createdBy, createdAt: $createdAt}';
  }
}


class AutoBidPriceModel {

  final int? id;
  final String? amount;
  final UserModel? createdBy;
  final ProductModel? product;
  final String? createdAt;

  bool get hasCreateBy => createdBy != null;

  AutoBidPriceModel({this.id, this.amount, this.createdAt, this.createdBy, this.product});

  static AutoBidPriceModel fromJson(Map<String, dynamic>? json) {

    return AutoBidPriceModel(
      id: json?['id'],
      amount: '${json?['amount']}',
      createdBy: UserModel.fromJson(json?['createdBy']),
      product: ProductModel.fromJson(json?['product']),
      createdAt: json?['createdAt'],
    );
  }

  Map<String, dynamic> toMap() {
    return {
      "id": id,
      "amount": amount,
      "createdAt": createdAt,
      "createdBy": createdBy,
    };
  }

  @override
  bool operator ==(Object other) => identical(this, other) || other is AutoBidPriceModel && id == other.id;

  @override
  int get hashCode => id.hashCode;

  @override
  String toString() {
    return 'AutoBidPriceModel{id: $id, amount: $amount, createdBy: $createdBy, createdAt: $createdAt, product: $product}';
  }
}



class WatchProductModel{
  final int? id;
  final ProductModel? product;
  WatchProductModel({this.id, this.product});

  static WatchProductModel fromJson(Map<String, dynamic>? map) {
    return WatchProductModel(
        id: map?['id'],
        product: ProductModel.fromJson(map?['product']),
    );
  }
}

class BidProductModel {
  final int? id;
  final double? amount;
  final ProductModel? product;

  BidProductModel({this.id, this.amount, this.product});


  static BidProductModel fromJson(Map<String, dynamic>? map) {
    return BidProductModel(
      id: map?['id'],
      amount: map?['amount'],
      product: ProductModel.fromJson(map?['product']),
    );
  }
}

class WinLostProductModel {
  final int? id;
  final BidPriceModel? bidPrice;
  final ProductModel? product;

  WinLostProductModel({this.id, this.bidPrice, this.product});

  static WinLostProductModel fromJson(Map? map) {
    return WinLostProductModel(
      id: map?['id'],
      bidPrice: BidPriceModel.fromJson(map?['bidPrice']),
      product: ProductModel.fromJson(map?['product']),
    );
  }
}


class ProductOrderModel {
  final int? id;
  final ProductModel? product;

  ProductOrderModel({this.id, this.product});

  static ProductOrderModel fromJson(final Map? map) {
    return ProductOrderModel(
      id: map?['id'],
      product: ProductModel.fromJson(map?['product']),
    );
  }
}