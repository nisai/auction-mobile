import 'package:mobile/shared/models/user_model.dart';

class CommentModel {

  final int? id;
  final String? content;
  final UserModel? createdBy;
  final String? createdAt;
  final bool? byOwner;
  
  bool get hasCreateBy => createdBy != null;

  CommentModel({this.id, this.content, this.createdAt, this.createdBy, this.byOwner});

  static CommentModel fromJson(Map<String, dynamic>? json) {
    return CommentModel(
        id: json?['id'],
        content: '${json?['content']}',
        createdBy: UserModel.fromJson(json?['createdBy']),
        createdAt: json?['createdAt'],
        byOwner: json?['byOwner'],
    );
  }

  Map<String, dynamic> toMap() {
    return {
      "id": id,
      "content": content,
      "createdAt": createdAt,
      "createdBy": createdBy,
      "byOwner": byOwner,
    };
  }

  @override
  bool operator ==(Object other) => identical(this, other) || other is CommentModel && id == other.id;

  @override
  int get hashCode => id.hashCode;

  @override
  String toString() {
    return 'CommentModel{id: $id, content: $content, createdBy: $createdBy, createdAt: $createdAt, byOwner: $byOwner}';
  }
}
