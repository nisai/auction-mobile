import 'package:mobile/utils/constant.dart';

class NotificationModel {
  final int? id;
  final String? title;
  final String? detail;
  final String? link;
  final String? type;
  final String? status;
  final String? createdAt;
  final String? image;

  String get imageUrl {
    if (!hasImage) return '';
    return "${Constant.BASE_API}$image";
  }

  bool get hasImage {
    return image != null && image!.isNotEmpty;
  }

  String? get  createDate {
    try {
      DateTime? date = DateTime.parse(createdAt?? '');
      //DateFormat.yMMMd().format(DateTime.now())
      //String created = DateFormat('dd-MM-yyyy – kk:mm').format(date);
      return date.toString();
    } catch(e) {

    }
    return null;
  }

  NotificationModel({
    this.status,
    this.detail,
    this.id,
    this.title,
    this.link,
    this.type,
    this.createdAt,
    this.image,
  });

  static NotificationModel fromJson(Map<String, dynamic>? json) {
    return NotificationModel(
        id: json?['id'],
        title: json?['title'],
        detail: json?['detail'],
        link: json?['link'],
        type: json?['type'],
        status: json?['status'],
        createdAt: json?['createdAt'],
        image: json?['image'],
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'title': title,
      'detail': detail,
      'link': link,
      'type': type,
      'status': status,
      'image': image,
    };
  }

  @override
  bool operator ==(Object other) => identical(this, other) || other is NotificationModel && id == other.id;

  @override
  int get hashCode => id.hashCode;

  @override
  String toString() {
    return 'NotificationModel{'
        'id: $id, '
        'title: $title,'
        '}';
  }
}
