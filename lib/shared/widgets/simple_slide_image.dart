import 'package:flutter/material.dart';
import 'package:flutter_swiper_null_safety/flutter_swiper_null_safety.dart';
import 'package:mobile/shared/widgets/network_image.dart';

class SimpleSlidImage extends StatelessWidget {

  final List<String>? slides;
  final SwiperOnTap? onTap;
  final double? radius;
  const SimpleSlidImage({Key? key, this.slides, required this.onTap, this.radius}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Swiper(
      itemBuilder: (BuildContext context, final int index){
        final url = slides?[index];
        print("url $url");
        return CacheNetworkImageWidget(url: url, radius: radius?? 0);
      },
      itemCount: slides?.length?? 0,
      pagination: SwiperPagination(),
      control: SwiperControl(),
      onTap: onTap,
    );
  }
}