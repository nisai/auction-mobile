import 'package:flutter/material.dart';
import 'placeholder_notification.dart';

class PlaceholderNotifications extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: 1,
      itemBuilder: (context, index) => PlaceholderNotification(),
    );
  }
}
