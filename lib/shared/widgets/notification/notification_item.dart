import 'package:flutter/material.dart';
import 'package:mobile/shared/models/notification_model.dart';
import 'package:mobile/shared/widgets/avatar_fade_Image.dart';
import 'package:mobile/shared/widgets/styles.dart';
import 'package:mobile/utils/helpers.dart';

class NotificationItem extends StatelessWidget {

  final NotificationModel item;
  NotificationItem({Key? key, required this.item}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: EdgeInsets.symmetric(vertical: 5.0),
        child: Container(color: item.status == 'SEEN' ? Colors.white : Colors.grey[300],
      child: ListTile(
        leading:  Container(
          width: 50,
          height: 50,
          child: item.hasImage? AvatarFadeImage(url: item.imageUrl): CircleAvatar(
            child: Center(
              child: Text(
                '',
              ),
            ),
          ),
        ),
        title: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            TextSpacer(
              Text('${item.title}',
                  style: Theme.of(context).textTheme.subtitle1),
            ),
            TextSpacer(
              Text(
                '${item.detail}',
                overflow: TextOverflow.ellipsis,
                style: Theme.of(context).textTheme.subtitle2,
              ),
            ),
            TextSpacer(
              Text(
                '${Helpers.getTimeHuman(createdAt: item.createdAt)}',
                overflow: TextOverflow.ellipsis,
                style: Theme.of(context).textTheme.caption,
              ),
            ),
          ],
        ),
      ),
    )
    );
  }
}
