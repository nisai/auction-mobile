import 'package:flutter/material.dart';
import 'package:flutter_swiper_null_safety/flutter_swiper_null_safety.dart';

import 'package:mobile/shared/models/banner_model.dart';
import 'package:mobile/shared/widgets/network_image.dart';
import 'package:url_launcher/url_launcher.dart';

class SimpleBannerWidget extends StatelessWidget {

  final List<BannerModel> banners;
  final double radius;
  const SimpleBannerWidget({Key? key, required this.banners, this.radius = 0.0}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Swiper(
          autoplay: true,
          duration: 5,
          itemBuilder: (BuildContext context, final int index){
            final BannerModel? banner = banners[index];
            return Stack(
              children: <Widget>[
                CacheNetworkImageWidget(url: banner?.imageUrl?? '', radius: radius),
                Positioned.fill(
                  child: Padding(
                    padding: EdgeInsets.only(bottom: 30.0),
                    child: Align(
                        alignment: Alignment.bottomCenter,
                        child: SizedBox(
                          width: 100.0,
                          height: 35.0,
                          child: Container(
                            decoration: new BoxDecoration(
                                border: new Border.all(width: 1.0 , color: Colors.transparent), //color is transparent so that it does not blend with the actual color specified
                                borderRadius: const BorderRadius.all(const Radius.circular(30.0)),
                                color: new Color.fromRGBO(76, 101, 175, 0.4) // Specifies the background color and the opacity
                            ),
                            child: TextButton(
                              child: Text('Detail', style: TextStyle(color: Colors.white),),
                              onPressed: () async {
                                await _openLink(banner?.linkUrl?? '');
                              },
                            ),
                          ),
                        )
                    ),
                  ),
                ),
              ],
            );
          },
          itemCount: banners.length,
          pagination: SwiperPagination(),
          control: SwiperControl(),
          onTap: (index) async {

          }
      ),
    );
  }

  Future<void> _openLink(final String url)
  async {
    print('url $url');
    await canLaunch(url) ? await launch(url) : throw 'Could not launch $url';
  }
}