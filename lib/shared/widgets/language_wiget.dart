import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mobile/modules/language/language_controller.dart';

class LanguageWidget extends StatelessWidget {

  const LanguageWidget();

  @override
  Widget build(BuildContext context) {
    Get.put(LanguageController());
    return GetBuilder<LanguageController>(builder: (controller) {
      final String url = controller.isEnglish? 'assets/flags/en.jpg': 'assets/flags/kh.jpg';
      return GestureDetector(
        onTap: controller.toggleLanguage,
        child: _buildImage(url),
      );
    });
  }

  Widget _buildImage(final String url) {
    return Container(
      width: 50,
      height: 50,
      child: Padding(padding: EdgeInsets.only(right: 10), child: Image.asset(url)),
    );
  }
}