import 'package:flutter/material.dart';
import 'package:mobile/shared/models/user_item_model.dart';
import 'package:mobile/shared/widgets/network_image.dart';
import 'package:mobile/shared/widgets/wishlist_button_widget.dart';
class WishlistProductItem extends StatelessWidget {

  final WatchProductModel item;
  const WishlistProductItem({Key? key, required this.item}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.all(
          Radius.circular(15),
        ),
      ),
      padding: EdgeInsets.all(5),
      width: 185.0,
      height: 200.0,
      child: Stack(
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Container(
                height: 145,
                child: Center(
                  child: CacheNetworkImageWidget(url: '${item.product?.imageUrl}', radius: 0.0,),
                ),
              ),
              SizedBox(height: 3.0),
              Text(
                '${item.product?.category?.name}',
                style: TextStyle(fontSize: 10.0),
              ),
              SizedBox(height: 3.0),
              SizedBox(
                height: 30.0,
                child: Text(
                  '${item.product?.name}',
                  style: TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.bold,
                    height: 1,
                  ),
                  maxLines: 2,
                  overflow: TextOverflow.ellipsis,
                ),
              ),
              Spacer(),
              Text(
                'USD ${item.product?.originalPrice?? 0.0}',
                style: TextStyle(
                  fontSize: 14,
                  fontWeight: FontWeight.bold,
                  height: 1,
                ),
              ),
              Spacer(),
            ],
          ),
            Align(
              alignment: Alignment.topRight,
              child: Container(
                width: 40,
                height: 40,
                child: WishlistButtonWidget(productId: item.product?.id),
              ),
            ),
        ],
      )
    );
  }
}
