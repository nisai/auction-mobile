import 'package:flutter/material.dart';
import 'package:mobile/shared/widgets/wishlist_button_widget.dart';
import 'package:mobile/utils/utility.dart';

class WishListToggleWidget extends StatelessWidget {

  final int? productId;
  final double iconSize;

  WishListToggleWidget({Key? key, this.productId = 0, required this.iconSize}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<bool?>(
      future: Utility.isLoggedIn(), //// async work
      builder: (BuildContext context, AsyncSnapshot<bool?> snapshot) {
        final loggedIn = snapshot.data?? false;
        if(loggedIn) {
          return  WishlistButtonWidget(productId: productId, iconSize: iconSize);
        }
        return Container();
      },
    );
  }

}