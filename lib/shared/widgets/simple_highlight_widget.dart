import 'package:flutter/material.dart';
class SimpleHighlightButtonWidget extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {

      },
      child: Container(
        width: 70,
        height: 25,
        margin: EdgeInsets.symmetric(horizontal: 0, vertical: 2),
        alignment: Alignment.center,
        decoration: BoxDecoration(
          color: const Color(0xFFed1c39),
        ),
        child: Text(
          'Highlight',
          style: TextStyle(fontSize: 11, color: Colors.white ),
          textAlign: TextAlign.left,
        ),
      ),
    );
  }

}