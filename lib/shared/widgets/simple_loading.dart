import 'package:flutter/cupertino.dart';

class SimpleLoading extends StatelessWidget{
  const SimpleLoading();
  @override
  Widget build(BuildContext context) {
    return const Center(
      child: SizedBox(
        child: CupertinoActivityIndicator(),
        height: 24,
        width: 24,
      ),
    );
  }
}