import 'package:flutter/material.dart';
import 'package:flutter_countdown_timer/index.dart';
import 'package:mobile/shared/colors.dart';

class BidCountdownWidget extends StatefulWidget {

  final String createdAt;

  const BidCountdownWidget({Key? key, required this.createdAt}) : super(key: key);
  @override
  _CountdownPageState createState() => _CountdownPageState();
}

class _CountdownPageState extends State<BidCountdownWidget> {

  late CountdownTimerController controller;

    @override
  void initState() {
    super.initState();
    controller = CountdownTimerController(endTime: DateTime.parse(widget.createdAt).millisecondsSinceEpoch, onEnd: onEnd);
  }

  void onEnd() {
    print('onEnd');
  }

  @override
  Widget build(BuildContext context) {
    return CountdownTimer(
      controller: controller,
      widgetBuilder: (final BuildContext context, final CurrentRemainingTime? time) {
        var label = '';
        if((time?.days?? 0)> 1) {
          if((time?.hours?? 0) > 1) {
            label = '${time?.days}days ${time?.hours}h';
          } else {
            label = '${time?.days}days';
          }
        } else if((time?.hours??0) > 1) {
          if((time?.min?? 0) > 1) {
            label = '${time?.hours}h ${time?.min}min';
          } else {
            label = '${time?.hours}h';
          }
        } else {
          if((time?.min?? 0)> 1) {
            label = '${time?.min}min ${time?.sec}sec';
          } else {
            label = '${time?.sec}sec';
          }
        }
        return  Container(
          decoration: BoxDecoration(
            color: kPrimaryColorShadow,
            borderRadius: BorderRadius.all(
              Radius.circular(15),
            ),
          ),
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 8, vertical: 4),
            child:  Container(
              alignment: Alignment.center,
              child: Text(label, style: TextStyle(fontSize: 12),
              ),
            ),
          ),
        );
      },
      endWidget: Text("Bidding end"),
    );
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

}