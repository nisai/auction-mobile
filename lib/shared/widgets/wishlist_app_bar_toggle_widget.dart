import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mobile/routes/app_pages.dart';
import 'package:mobile/services/user_service.dart';

class WishListAppBarToggleWidget extends StatelessWidget {

  final _auth = Get.find<AuthService>();

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<bool?>(
      future: _auth.checkLoggedIn(pingServer: true), //// async work
      builder: (BuildContext context, AsyncSnapshot<bool?> snapshot) {
        final loggedIn = snapshot.data?? false;
        if(loggedIn) {
          return  Padding(
            padding: const EdgeInsets.all(8.0),
            child: IconButton(
              icon: Icon(
                Icons.favorite,
                color: Colors.white,
              ),
              onPressed: () {
                Get.rootDelegate.toNamed(Routes.WISHLIST);
              },
            ),
          );
        }
        return Container();
      },
    );
  }

}