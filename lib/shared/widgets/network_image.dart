import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';

class CacheNetworkImageWidget extends StatelessWidget {
  final String? url;
  final double? width;
  final double? height;
  final double radius;
  final bool isCycle;
  const CacheNetworkImageWidget({Key? key, this.url, this.width = double.infinity, this.height = double.infinity, this.radius = 15, this.isCycle = false})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return CachedNetworkImage(
      imageUrl: "$url",
      width: width,
      height: height,
      imageBuilder: (context, imageProvider) => Container(
        decoration: BoxDecoration(
          borderRadius: isCycle? BorderRadius.circular(radius) : BorderRadius.only(topRight:  Radius.circular(radius)),
          image: DecorationImage(
              image: imageProvider,
              fit: BoxFit.cover
          ),
        ),
      ),
      placeholder: (context, url) => CupertinoActivityIndicator(),
      errorWidget: (context, url, error) => _buildPlaceholderImage(),
    );
  }

  Widget _buildPlaceholderImage() {
    return Container(
      decoration: new BoxDecoration(
        image: new DecorationImage(
          image: new AssetImage('assets/images/image_placeholder.png'),
          fit: BoxFit.fitHeight,
        ),
      ),
    );
  }

}
