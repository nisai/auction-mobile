import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mobile/services/user_service.dart';
import 'package:mobile/shared/models/user_item_model.dart';

class WishlistButtonWidget extends StatefulWidget {

  final int? productId;
  final double iconSize;

  const WishlistButtonWidget({Key? key, this.productId = 0, this.iconSize = 24}) : super(key: key);

  @override
  State<StatefulWidget> createState() => WishlistButtonWidgetState();

}

class WishlistButtonWidgetState extends State<WishlistButtonWidget> {

  final _authService = Get.find<AuthService>();
  bool _isFavorite = false;
  late int? _wishlistId;

  @override
  void initState() {
    Future.microtask((){
       _authService.getWatchListByProductId(productId: widget.productId).then((WatchProductModel? model) {
         final exists = model != null;
         if(mounted) {
           setState(() {
             _isFavorite = exists;
             _wishlistId = model?.id;
           });
         }
       });
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: _isFavorite? _favoriteButton(): _unFavoriteButton() ,
    );
  }

  Future<void> _addWishlist() async {
    try {
      if(mounted) {
        setState(() {
          _isFavorite = !_isFavorite;
        });
        _authService.createWatchList(productId: widget.productId).then((value) {
          print('create done: ${widget.productId}');
        });
      }
    } catch(e) {
      print('_addWishlist: error : $e');
    }
  }

  Future<void> _removeWishlist() async {
    try {
      if(mounted) {
        setState(() {
          _isFavorite = !_isFavorite;
        });
        if(_wishlistId != null) {
          _authService.deleteWatchList(wishlistId: _wishlistId).then((value) {
            print('deleteWatchList done: $_wishlistId');
          });
        }
      }
    } catch(e) {
      print('_removeWishlist: error : $e');
    }
  }

  Widget _favoriteButton() {
    return IconButton(
        iconSize: widget.iconSize,
        onPressed: _removeWishlist,
        icon: Icon(Icons.favorite, color: Colors.red)
    );
  }

  Widget _unFavoriteButton() {
    return IconButton(
      iconSize: widget.iconSize,
      onPressed: _addWishlist,
      icon: Icon(Icons.favorite_border, color: Colors.white),
    );
  }

}