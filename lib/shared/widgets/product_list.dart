import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mobile/routes/app_pages.dart';
import 'package:mobile/shared/models/product_model.dart';
import 'package:mobile/shared/widgets/product_item_widget.dart';

class ProductList extends StatelessWidget {

  final List<ProductModel> list;

  const ProductList({Key? key, required this.list}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      physics: BouncingScrollPhysics(),
      scrollDirection: Axis.horizontal,
      padding: EdgeInsets.only(left: 5, right: 5),
      itemCount: list.length,
      itemBuilder: (BuildContext context, int index) {
        final ProductModel item = list[index];
        return GestureDetector(
            onTap: () async {
              Get.rootDelegate.toNamed(Routes.productDetail(productId: '${item.id}'));
            },
            child: Padding(
              padding: EdgeInsets.only(right: 5),
              child: ProductItem(product: item),
            )
        );
      },
    );
  }

}