import 'package:flutter/material.dart';
import 'package:mobile/shared/colors.dart';

class SimpleHeader extends StatelessWidget {

  final String text;
  final String? actionText;
  final GestureTapCallback? onTap;
  SimpleHeader({
     required this.text,
     this.onTap,
     this.actionText});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            "$text",
            style: TextStyle(
              fontSize: 14,
              fontWeight: FontWeight.bold,
              color: Colors.grey[400],
            ),
          ),
          if(actionText != null)
          GestureDetector(
            onTap: onTap,
            child: Row(
              children: [
                Text(
                  "$actionText",
                  style: TextStyle(
                    fontSize: 14,
                    fontWeight: FontWeight.bold,
                    color: kPrimaryColor,
                  ),
                ),
                SizedBox(width: 8),
                Icon(
                  Icons.arrow_forward_ios,
                  size: 14,
                  color: kPrimaryColor,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
