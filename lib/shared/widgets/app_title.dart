import 'package:flutter/material.dart';

class AppTitle extends StatelessWidget {
  final String? title;

  const AppTitle({Key? key, this.title = 'Khmer Song'}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Text(title?? '',
        style: TextStyle(
          fontSize: 18.0,
          color: Colors.white
        ),
      ),
    );
  }
}