import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mobile/routes/app_pages.dart';


class SimpleBuyNowButtonWidget extends StatelessWidget{
  final String productId;

  const SimpleBuyNowButtonWidget({Key? key, required this.productId}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Get.rootDelegate.toNamed(Routes.productBuying(productId: productId));
      },
      child: Container(
        width: 70,
        height: 25,
        margin: EdgeInsets.symmetric(horizontal: 0, vertical: 2),
        alignment: Alignment.center,
        decoration: BoxDecoration(
          color: const Color(0xFF548ecb),
        ),
        child: Text(
          'BUY NOW',
          style: TextStyle(fontSize: 11, color: Colors.white),
        ),
      ),
    );
  }

}