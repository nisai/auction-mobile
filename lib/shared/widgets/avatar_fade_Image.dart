import 'package:flutter/material.dart';

class AvatarFadeImage extends StatelessWidget {
  final String url;

  AvatarFadeImage({required this.url});

  @override
  Widget build(BuildContext context) {
    return ClipOval(
      child: FadeInImage.assetNetwork(
        placeholder: 'assets/images/image_placeholder.png',
        image: url,
        fit: BoxFit.cover,
      ),
    );
  }
}