import 'package:flutter/material.dart';
import 'package:mobile/shared/models/product_model.dart';
import 'package:mobile/shared/widgets/network_image.dart';
import 'package:mobile/shared/widgets/simple_buynow_widget.dart';
import 'package:mobile/shared/widgets/simple_highlight_widget.dart';
import 'package:mobile/shared/widgets/simple_ugent_widget.dart';
import 'package:mobile/shared/widgets/wishlist_toggle_widget.dart';
import 'package:mobile/utils/helpers.dart';

class ProductItem extends StatelessWidget {
  final double height;
  final double imageHeight;
  final ProductModel product;
  const ProductItem({Key? key, required this.product, this.height = 310, this.imageHeight = 190}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.all(
          Radius.circular(15),
        ),
      ),
      width: 185.0,
      height: height,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Container(
            child: Stack(
              children: [

                Container(
                  height: imageHeight,
                  child: CacheNetworkImageWidget(
                      url: '${product.imageUrl}',
                      height: 185,
                      radius: 0.0
                  ),
                ),

                Positioned(
                  top: 10,
                  left: 0,
                  child: _buildLabelButtons(),
                ),

                Positioned(
                  right: 0,
                  top: 0,
                  child: WishListToggleWidget(productId: product.id, iconSize: 30),
                ),

              ],
            ),
          ),
          Padding(
              padding: EdgeInsets.symmetric(horizontal: 5.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    child: Text(
                      '${product.name}',
                      style: TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.bold,
                        height: 1.5,
                      ),
                      maxLines: 2,
                      overflow: TextOverflow.ellipsis,
                      textAlign: TextAlign.left,
                    ),
                  ),
                  SizedBox(height: 5.0),
                  Row(
                    children: [
                      Text(
                        'USD ${product.getHighestBid?? 0.0}',
                        style: TextStyle(
                            fontSize: 14,
                            fontWeight: FontWeight.bold,
                            height: 1,
                            color: Colors.red
                        ),
                        textAlign: TextAlign.right,
                      ),
                      Spacer(),
                      _buildReservePriceLabel(),
                    ],
                  ),
                  if(product.canBuyNow)
                    Row(
                      children: [
                        Text(
                          'USD ${product.buyNowPrice?? 0.0}',
                          style: TextStyle(
                              fontSize: 14,
                              fontWeight: FontWeight.bold,
                              height: 1,
                              color: Colors.red
                          ),
                          textAlign: TextAlign.right,
                        ),
                        Spacer(),
                        GestureDetector(
                          onTap: () {

                          },
                          child: Container(
                            width: 70,
                            height: 25,
                            margin: EdgeInsets.symmetric(horizontal: 0, vertical: 2),
                            alignment: Alignment.center,
                            decoration: BoxDecoration(
                              color: const Color(0xFFed1c39),
                            ),
                            child: Text(
                              'BUY NOW',
                              style: TextStyle(fontSize: 11, color: Colors.white),
                            ),
                          ),
                        ),
                      ],
                    ),

                  Padding(
                      padding: EdgeInsets.symmetric(vertical: 10),
                      child:  Row(
                        children: [
                          Container(
                            child: Row(
                              children: [
                                Icon(Icons.timer, size: 11),
                                Text(
                                  ' ${Helpers.getRemainFromExpectDate(expectFinishedAt: product.expectFinishAt)}',
                                  style: TextStyle(
                                    fontSize: 11,
                                    fontWeight: FontWeight.bold,
                                    height: 1,
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Spacer(),
                          Container(
                            child: Row(
                              children: [
                                Icon(Icons.handyman, size: 11,),
                                Text(
                                  '(${product.bidPrice?.length?? 0})',
                                  style: TextStyle(
                                    fontSize: 11,
                                    fontWeight: FontWeight.bold,
                                    height: 1,
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Spacer(),
                          Text(
                            '${product.province?.name?? 'unknow'}',
                            style: TextStyle(
                              fontSize: 11,
                              fontWeight: FontWeight.bold,
                              height: 1,
                            ),
                          ),
                        ],
                      )
                  )
                ],
              ),
          )

        ],
      ),
    );
  }

  Widget _buildReservePriceLabel() {

    if(!product.hasReversePrice)
     return  Container(
        child: Text("No Reserve", style: TextStyle(fontSize: 11, fontWeight: FontWeight.bold)),
      );

    if(!product.isReservePriceMet)
    return Container(
      child: Text("Reserve Not Met", style: TextStyle(fontSize: 11, fontWeight: FontWeight.bold)),
    );

    //if(product.isReservePriceMet)
    return Container(
      child: Text("Reserve Met", style: TextStyle(fontSize: 11, fontWeight: FontWeight.bold)),
    );
  }

  Widget _buildLabelButtons() {
    print('product: ${product.toString()}');
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        /// Text Button #1
        if(product.isHighlight)
        SimpleHighlightButtonWidget(),
        if(product.isUrgent)
        SimpleUrgentButtonWidget(),
        if(product.canBuyNow)
        SimpleBuyNowButtonWidget(productId: '${product.id}'),
      ],
    );
  }
}
