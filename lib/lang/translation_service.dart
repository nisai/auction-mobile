import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:mobile/utils/constant.dart';
import 'en_US.dart';
import 'km_KH.dart';

class TranslationService extends Translations {

  static final box = GetStorage();

  static String get getCurrentLanguage => box.read(Constant.languageKey) ?? Constant.englishLanguageCode;

  static Locale get locale => getCurrentLanguage == Constant.khmerLanguageCode?  Locale('km', 'KH') : Locale('en', 'US');

  static final supportedLocales = <Locale> [Locale ('en', 'US'), Locale('km', 'KH')];

  @override 
  Map<String, Map<String, String>> get keys => {
    'en_US': en_US,
    'km_KH': km_KH,
  };

}